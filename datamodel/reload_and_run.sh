#!/bin/sh

JAVA_OPTS="-Djavax.net.ssl.sessionCacheSize=10000 -server"

IS_A_CONTAINER=false
if [ -f /etc/bookings/IN_A_CONTAINER ]; then
  IS_A_CONTAINER=true
  JAVA_OPTS="$JAVA_OPTS -XX:+UseContainerSupport"
fi

ENV=dev
if [ ${IS_A_CONTAINER} = true ]; then
  if [ -f /etc/sysconfig/bookings.puppet ]; then
    ENV=$(cat /etc/sysconfig/bookings.puppet | grep KUBERNETES_CLUSTER_ENV | awk '{split($0,a,"=");print a[2]}')
  fi
else
  if [ -f /etc/bookings/bookings.puppet ]; then
    ENV=$(cat /etc/sysconfig/bookings.puppet | grep ENVIRON | awk '{split($0,a,"=");print a[2]}')
  fi
fi

execute_app () {
  exec java ${JAVA_OPTS} -cp target/app.jar:target/lib/* \
      com.booking.dataqualityapi.MainApplication \
    --spring.config.location=file:config/config.yaml,classpath:application.properties
}

run_in_dev () {
  JAVA_OPTS="$JAVA_OPTS -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5006"
  if [ ${IS_A_CONTAINER} = true ]; then
    touch RESTART

    while true;
    do
      if [ -f ./RESTART ]; then
          pkill -f "java.*com.booking.dataqualityapi.MainApplication"

          # begin of insertion block
          # add this cycle to make sure previous instance of java service is finished
          while nc -z localhost 5006 > /dev/null 2>&1; do
              echo "Waiting for java shutdown (port 5006 is still open)..."
              sleep 1
          done
          # end of insertion block

          execute_app &

          rm RESTART
      else
          sleep 1;
      fi
    done
  else
    pkill -f "java.*com.booking.dataqualityapi.MainApplication"
    sleep 2

    execute_app &
  fi
}

if [ ${IS_A_CONTAINER} = true ]; then
  echo "Running on BPlatform with ${ENV} environment"
else
  echo "Running on Baremetal with ${ENV} environment"
fi

if [ ${ENV} = dev ]; then
  run_in_dev
else
  execute_app
fi
