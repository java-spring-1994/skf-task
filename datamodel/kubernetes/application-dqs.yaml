apiVersion: shipper.booking.com/v1
kind: Application
metadata:
  name: dq-datamodel-api
  labels:
    service-directory.service: dq-datamodel-api
spec:
  revisionHistoryLimit: 1
  template:
    clusterRequirements:
      regions:
      - name: eu-nl
        # 'replicas' determines how many clusters your application should be
        # scheduled on in this region.
        replicas: 1
      # you can specify any particular cluster capabilities your application
      # needs, like DLB (the Booking load balancer) or iSCSi network-attached storage
      capabilities:
      - dlb
    chart:
      name: java
      version: ^2
      repoUrl: https://artifactory.booking.com/charts
    values:
      name: dq-datamodel-api
      replicaCount: 1
      dependencies:
        mysql:
          dataquality: ro, rw
      env:
      - name: SERVER_ROLE
        value: b-data-quality-dq-datamodel-api
      - name: SPRING_PROFILES_ACTIVE
        value: "dqs"
      image:
        repository: docker.artifactory.booking.com/projects/data-quality/dq-datamodel-api
        # because DQS is like production (but for people developing against
        # your service), it should have similar levels of care as prod. as
        # such, we suggest building your docker images from a CI pipeline
        # rather than by hand. the CI pipeline might use 'sed' to swap
        # CI_COMMIT_SHA for the image produced by the pipeline.
        tag: CI_COMMIT_SHA
      service:
        hostname: dq-datamodel-api.dqs.booking.com
        hostpath: /
        scope: internal
      stagingService:
        hostname: dq-datamodel-api-staging.dqs.booking.com
        enabled: true
        hostpath: /
        scope: internal
    strategy:
      # DQS is a production-like environment, so we need a real rollout strategy just like prod
      steps:
        # this 'staging' step represents a release with 1 pod of capacity, but no production traffic
      - name: staging
        capacity:
          # these values are percentages of the final capacity (# of pods) for
          # that Release in other words, if my deployment has 10 pods, this is
          # 100% (or 1%) of 10 pods. Always rounded up to the nearest whole
          # pod.
          incumbent: 100
          contender: 1
        traffic:
          incumbent: 100
          contender: 0
      - name: vanguard
        capacity:
          incumbent: 90
          contender: 10
        traffic:
          # these traffic values are weights, not percentages: they could just as well be '9' and '1' or '900' and '100'.
          incumbent: 90
          contender: 10
      - name: full on
        capacity:
          incumbent: 0
          contender: 100
        traffic:
          incumbent: 0
          contender: 100
