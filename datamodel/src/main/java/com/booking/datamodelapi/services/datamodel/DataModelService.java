package com.booking.datamodelapi.services.datamodel;

import com.booking.datamodelapi.dto.DataEntity;
import com.booking.datamodelapi.dto.DataEntityType;
import com.booking.datamodelapi.dto.DataModelSchemaType;
import com.booking.datamodelapi.exceptions.datamodel.DataModelIncorrectSchemaVersionException;
import com.booking.datamodelapi.exceptions.datamodel.DataModelOperationException;
import com.booking.datamodelapi.exceptions.datamodel.DataModelSchemaConversionException;
import com.booking.datamodelapi.model.DataSource;
import com.booking.datamodelapi.model.datamodel.schema.DataModelSchema;
import com.booking.datamodelapi.repository.DataSourceRepository;
import com.booking.datamodelapi.repository.datamodel.schema.DataModelSchemaRepository;
import com.booking.datamodelapi.utils.datamodel.schema.DataModelSchemaUtils;
import java.util.List;

import io.confluent.kafka.schemaregistry.avro.AvroCompatibilityLevel;
import org.apache.avro.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class DataModelService {

    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory.getLogger(DataModelService.class);

    private final DataSourceRepository dataSourceRepository;

    private final DataModelSchemaRepository dataModelSchemaRepository;

    @Autowired
    public DataModelService(
        final DataSourceRepository dataSourceRepository,
        final DataModelSchemaRepository dataModelSchemaRepository
    ) {
        this.dataSourceRepository = dataSourceRepository;
        this.dataModelSchemaRepository = dataModelSchemaRepository;
    }



    public DataModelSchema getSchema(final DataModelSchemaType schemaType, final String schemaName, final String schemaVersion)
        throws DataModelOperationException, DataModelIncorrectSchemaVersionException {
        DataModelSchemaUtils.validateSchemaVersionFormat(schemaVersion);
        Optional<DataModelSchema> result = dataModelSchemaRepository.findOneByTypeAndNameAndVersion(schemaType, schemaName, schemaVersion);
        if (result.isPresent()) {
            return result.get();
        } else {
            throw new DataModelOperationException(
                String.format(
                    "Could not find data model schema with provided type, name and version: %s, %s, %s",
                    schemaType, schemaName, schemaVersion)
            );
        }
    }

    public List<String> listVersions(final DataModelSchemaType schemaType, final String schemaName) {
        return dataModelSchemaRepository.listVersions(schemaType, schemaName);
    }

    public DataModelSchema getSchemaLatestVersion(final DataModelSchemaType schemaType, final String schemaName)
        throws DataModelOperationException {
        Optional<DataModelSchema> result = dataModelSchemaRepository.findOneByTypeAndNameLatestVersion(schemaType, schemaName);
        if (result.isPresent()) {
            return result.get();
        } else {
            throw new DataModelOperationException(
                String.format(
                    "Could not find data model schema (latest version) with provided type and name: %s, %s",
                    schemaType, schemaName)
            );
        }
    }

    public String convertJsonSchemaToAvroByNameAndVersion(final String schemaName, final String schemaVersion)
        throws DataModelOperationException, DataModelSchemaConversionException, DataModelIncorrectSchemaVersionException {
        DataModelSchema jsonSchema = this.getSchema(DataModelSchemaType.JSON, schemaName, schemaVersion);
        return DataModelSchemaUtils.SchemaConverter.convertJsonToAvro(jsonSchema.getContent()).toString();
    }

    public String convertJsonSchemaToAvroByNameLatestVersion(final String schemaName)
        throws DataModelOperationException, DataModelSchemaConversionException {
        DataModelSchema jsonSchema = this.getSchemaLatestVersion(DataModelSchemaType.JSON, schemaName);
        return DataModelSchemaUtils.SchemaConverter.convertJsonToAvro(jsonSchema.getContent()).toString();
    }

    /**
     * Checks Json schema's Avro compatibility.
     * @param schemaName Json schema's data model repository name.
     * @param newJsonSchemaContent New Schema content.
     * @param avroCompatibilityLevel Type of compatibility to check.
     * @return true or false based on the result of the compatibility check.
     * @throws DataModelOperationException Thrown if latest data model schema could not be found for the specified path.
     * @throws DataModelSchemaConversionException Thrown if json schema couldn't be converted to Avro.
     */
    public boolean checkJsonSchemaAvroCompatibility(final String schemaName,
                                                    final String newJsonSchemaContent,
                                                    final AvroCompatibilityLevel avroCompatibilityLevel)
                                                        throws DataModelOperationException,
                                                        DataModelSchemaConversionException {
        DataModelSchema existingJsonSchema = this.getSchemaLatestVersion(DataModelSchemaType.JSON, schemaName);
        Schema oldSchema = DataModelSchemaUtils.SchemaConverter.convertJsonToAvro(existingJsonSchema.getContent());
        Schema newSchema = DataModelSchemaUtils.SchemaConverter.convertJsonToAvro(newJsonSchemaContent);
        return avroCompatibilityLevel.compatibilityChecker.isCompatible(newSchema, oldSchema);
    }

    /**
     * Checks Json schema's Avro compatibility.
     * @param schemaName Json schema's data model repository name.
     * @param newJsonSchemaContent New Schema content.
     * @param avroCompatibilityLevelString Type of compatibility(string) to check.
     * @return true or false based on the result of the compatibility check.
     * @throws DataModelOperationException Thrown if latest data model schema could not be found for the specified path.
     * @throws DataModelSchemaConversionException Thrown if json schema couldn't be converted to Avro.
     */
    public boolean checkJsonSchemaAvroCompatibility(final String schemaName,
                                                    final String newJsonSchemaContent,
                                                    final String avroCompatibilityLevelString)
                                                        throws DataModelOperationException,
                                                        DataModelSchemaConversionException {
        AvroCompatibilityLevel avroCompatibilityLevel = AvroCompatibilityLevel.forName(avroCompatibilityLevelString);
        if (avroCompatibilityLevel == null) {
            String err = String.format(
                    "Could not parse \"%s\" into a valid AvroCompatibilityLevel.",
                    avroCompatibilityLevelString);
            LOGGER.debug(err);
            throw new DataModelOperationException(err);
        } else {
            LOGGER.debug("Compatibility level is " + avroCompatibilityLevel.name);
        }
        return checkJsonSchemaAvroCompatibility(schemaName, newJsonSchemaContent, avroCompatibilityLevel);
    }

    /**
     * Checks Json schema's Avro compatibility - type BACKWARD.
     * @param schemaName Json schema's data model repository name.
     * @param newJsonSchemaContent New Schema content.
     * @return true or false based on the result of the compatibility check.
     * @throws DataModelOperationException Thrown if latest data model schema could not be found for the specified path.
     * @throws DataModelSchemaConversionException Thrown if json schema couldn't be converted to Avro.
     */
    public boolean checkJsonSchemaAvroCompatibilityBackward(final String schemaName,
                                                            final String newJsonSchemaContent)
                                                                throws DataModelOperationException,
                                                                DataModelSchemaConversionException {
        AvroCompatibilityLevel avroCompatibilityLevel = AvroCompatibilityLevel.BACKWARD;
        return checkJsonSchemaAvroCompatibility(schemaName, newJsonSchemaContent, avroCompatibilityLevel);
    }


    public static String convertJsonSchemaToAvro(final String jsonSchemaContent) throws DataModelSchemaConversionException {
        return DataModelSchemaUtils.SchemaConverter.convertJsonToAvro(jsonSchemaContent).toString();
    }

    public DataModelSchema saveSchema(
        final DataModelSchemaType schemaType, final String schemaName, final String schemaVersion, final String content)
        throws DataModelOperationException, DataModelIncorrectSchemaVersionException {
        // todo validate content here?
        DataModelSchemaUtils.validateSchemaVersionFormat(schemaVersion);
        Optional<DataModelSchema> result = dataModelSchemaRepository.findOneByTypeAndNameAndVersion(schemaType, schemaName, schemaVersion);
        if (result.isPresent()) {
            throw new DataModelOperationException(
                String.format(
                    "Data model schema %s (type %s) with version %s is already presented. To update you must increment version",
                    schemaName, schemaType, schemaVersion)
            );
        } else {
            return dataModelSchemaRepository.save(
                new DataModelSchema(schemaType, schemaName, schemaVersion, content)
            );
        }
    }

    public DataModelSchema saveSchemaAutoVersion(
        final DataModelSchemaType schemaType, final String schemaName, final String content)
        throws DataModelIncorrectSchemaVersionException {
        // todo validate content here?
        Optional<DataModelSchema> result = dataModelSchemaRepository.findOneByTypeAndNameLatestVersion(schemaType, schemaName);
        if (result.isPresent()) {
            DataModelSchema schemaObject = result.get();
            return dataModelSchemaRepository.save(
                new DataModelSchema(schemaType, schemaName, DataModelSchemaUtils.incVersion(schemaObject.getVersion()), content)
            );
        } else {
            return dataModelSchemaRepository.save(
                new DataModelSchema(schemaType, schemaName, content)
            );
        }
    }

    public DataModelSchema getAssociatedSchemaByEntityId(final int entityId) throws DataModelOperationException {
        Optional<DataSource> result = dataSourceRepository.findById(entityId);
        if (result.isPresent()) {
            return result.get().getDataModelSchema();
        } else {
            throw new DataModelOperationException("Invalid data entity id (does not exist): " + entityId);
        }
    }

    public DataModelSchema getAssociatedSchemaForKafkaTopic(final String federation, final String topic)
        throws DataModelOperationException {
        DataSource dataEntity = dataSourceRepository.findOneByTypeAndAccountAndNamespaceAndName(
            DataEntityType.KAFKA.convertToDbValue(), DataEntity.DEFAULT_ENTITY_ACCOUNT, federation, topic
        );
        if (dataEntity == null) {
            throw new DataModelOperationException(String.format("Kafka topic does not exist in data catalog: %s.%s", federation, topic));
        } else {
            return dataEntity.getDataModelSchema();
        }
    }
}
