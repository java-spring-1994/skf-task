package com.booking.datamodelapi.dto.schema;

public class DataModelSchemaCompatibilitySettings {

    /**
     * Schema type conversion must be valid.
     * (Now used only for json-schema -> avro-schema conversion).
     * Default is true.
     */
    private boolean convertible = true;

    public boolean isConvertible() {
        return convertible;
    }

    public DataModelSchemaCompatibilitySettings setConvertible(boolean convertible) {
        this.convertible = convertible;
        return this;
    }
}
