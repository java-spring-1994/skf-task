package com.booking.datamodelapi.dto.schema;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This is the return type for Avro compatibility checks.
 */
public class DataModelAvroCompatible {
    @JsonProperty("compatibility_type")
    private String compatibilityType;

    @JsonProperty("is_compatible")
    private boolean isCompatible;

    public DataModelAvroCompatible(
            String compatibilityType,
            boolean isCompatible) {
        this.compatibilityType = compatibilityType;
        this.isCompatible = isCompatible;
    }

    public DataModelAvroCompatible() {}

    public String getCompatibilityType() {
        return compatibilityType;
    }

    public void setCompatibilityType(String compatibilityType) {
        this.compatibilityType = compatibilityType;
    }

    public boolean getIsCompatible() {
        return isCompatible;
    }

    public void setIsCompatible(boolean compatible) {
        isCompatible = compatible;
    }
}
