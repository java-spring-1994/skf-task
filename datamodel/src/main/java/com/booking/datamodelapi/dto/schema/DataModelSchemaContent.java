package com.booking.datamodelapi.dto.schema;

public class DataModelSchemaContent {
    private String content;

    public String getContent() {
        return content;
    }

    public DataModelSchemaContent() {}

    public DataModelSchemaContent(String content) {
        this.content = content;
    }
}
