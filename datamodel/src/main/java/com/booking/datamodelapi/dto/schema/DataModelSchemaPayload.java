package com.booking.datamodelapi.dto.schema;

/**
 * This is used to store data model schema.
 */
public class DataModelSchemaPayload {
    private String content;

    // default
    private DataModelSchemaCompatibilitySettings compatibilitySettings = new DataModelSchemaCompatibilitySettings();

    public DataModelSchemaPayload() {}

    public DataModelSchemaPayload(String content, DataModelSchemaCompatibilitySettings compatibilitySettings) {
        this.content = content;
        this.compatibilitySettings = compatibilitySettings;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public DataModelSchemaCompatibilitySettings getCompatibilitySettings() {
        return compatibilitySettings;
    }

    public void setCompatibilitySettings(DataModelSchemaCompatibilitySettings compatibilitySettings) {
        this.compatibilitySettings = compatibilitySettings;
    }
}
