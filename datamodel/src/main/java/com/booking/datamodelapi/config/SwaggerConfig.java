package com.booking.datamodelapi.config;

import com.booking.systemsettings.SystemSettings;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket postsApi() {

        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("com.booking.datamodelapi.controllers"))
                .paths(PathSelectors.any())
                .build()
                .ignoredParameterTypes(Authentication.class)
                .apiInfo(apiInfo()).protocols(getProtocols());
    }

    private Set<String> getProtocols() {
        final Set<String> result = new LinkedHashSet<>();
        if (SystemSettings.DEV_SERVER()) {
            result.add("http");
        } else {
            result.add("https");
        }
        return result;
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Data Model Rest API",
                "Api for interaction with data quality rest service",
                "1.0.2",
                "need to be defined",
                new Contact("Data Management Team", "http://data.booking.com",
                        "dataquality@booking.com"),
                "License of API", "API license URL", Collections.emptyList());
    }
}

