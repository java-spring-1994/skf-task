package com.booking.datamodelapi.utils.datamodel.schema;

import com.booking.datamodelapi.exceptions.datamodel.DataModelIncorrectSchemaVersionException;
import com.booking.datamodelapi.exceptions.datamodel.DataModelSchemaConversionException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.DevNullProcessingReport;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.core.tree.CanonicalSchemaTree;
import com.github.fge.jsonschema.core.tree.SchemaTree;
import com.github.fge.jsonschema.core.tree.key.SchemaKey;
import com.github.fge.jsonschema.core.util.ValueHolder;
import com.github.fge.jsonschema2avro.AvroWriterProcessor;
import org.apache.avro.Schema;


public class DataModelSchemaUtils {

    public static final String INITIAL_SCHEMA_VERSION = "0.0.1";

    public static class SchemaConverter {

        private static AvroWriterProcessor avroConversionProcessor = new AvroWriterProcessor();
        private static ProcessingReport report = new DevNullProcessingReport();

        public static Schema convertJsonToAvro(final String jsonSchemaContent) throws DataModelSchemaConversionException {
            Schema result;
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                final SchemaTree tree = new CanonicalSchemaTree(SchemaKey.anonymousKey(), objectMapper.readTree(jsonSchemaContent));
                final ValueHolder<SchemaTree> input = ValueHolder.hold("schema", tree);

                result = avroConversionProcessor.process(report, input).getValue();
            } catch (ProcessingException | JsonProcessingException e) {
                throw new DataModelSchemaConversionException(e.getMessage());
            }
            return result;
        }
    }

    public static void validateSchemaVersionFormat(final String version) throws DataModelIncorrectSchemaVersionException {
        String[] parts = version.split("\\.");
        if (parts.length != 3) {
            throw new DataModelIncorrectSchemaVersionException("Version must contain 3 components separated with dots");
        }
        for (String part : parts) {
            try {
                int parsedPart = Integer.parseInt(part);
                if (parsedPart < 0) {
                    throw new DataModelIncorrectSchemaVersionException("Version can not contain negative numbers");
                }
            } catch (NumberFormatException exc) {
                throw new DataModelIncorrectSchemaVersionException("Version must contain only numbers");
            }
        }
    }

    private static int getVersionPart(final String originalVersion, final int part) {
        return Integer.parseInt(originalVersion.split("\\.")[part]);
    }

    public static String incVersion(final String originalVersion) throws DataModelIncorrectSchemaVersionException {
        validateSchemaVersionFormat(originalVersion);
        return String.format("%d.%d.%d",
            getVersionPart(originalVersion, 0),
            getVersionPart(originalVersion, 1),
            getVersionPart(originalVersion, 2) + 1
        );
    }
}
