package com.booking.datamodelapi.repository;

import com.booking.datamodelapi.model.DataSource;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface DataSourceRepository extends CrudRepository<DataSource, Integer> {

    @Query("select entity from DataSource entity where entity.namespace = :namespace and entity.name =:name and entity.type = 'hive-table'")
    DataSource findHiveDataEntityByNamespaceAndName(@Param("namespace") final String namespace, @Param("name") final String name);

    DataSource findOneByTypeAndAccountAndNamespaceAndName(
            final String type, final String account,
            final String namespace, final String name
    );

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    List<DataSource> findAllHiveDataSources(
            Optional<String> namespace,
            Optional<String> name,
            Optional<Integer> staffId,
            Map<String, String> labels,
            Integer limit,
            Integer offset);

    @NotNull Optional<DataSource> findById(@NotNull Integer id);
}
