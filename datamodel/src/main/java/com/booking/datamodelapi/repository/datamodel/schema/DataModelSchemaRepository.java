package com.booking.datamodelapi.repository.datamodel.schema;

import com.booking.datamodelapi.dto.DataModelSchemaType;
import com.booking.datamodelapi.model.datamodel.schema.DataModelSchema;

import java.util.List;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DataModelSchemaRepository extends CrudRepository<DataModelSchema, Integer> {

    Optional<DataModelSchema> findOneByTypeAndNameAndVersion(DataModelSchemaType type, String name, String version);

    @Query(
        value = "SELECT s.*\n"
            + "FROM dq_data_model_schema s\n"
            + "INNER JOIN\n"
            + "    (SELECT schema_name, schema_type, MAX(schema_version) AS max_schema_version\n"
            + "    FROM dq_data_model_schema\n"
            + "    WHERE schema_name = :name and schema_type = :#{#type.toString()}\n"
            + "    GROUP BY schema_name, schema_type) group_s \n"
            + "ON s.schema_name = group_s.schema_name AND s.schema_type = group_s.schema_type AND s.schema_version = group_s.max_schema_version",
        nativeQuery = true)
    Optional<DataModelSchema> findOneByTypeAndNameLatestVersion(@Param("type") DataModelSchemaType type, @Param("name") String name);

    @Query(
        value = "SELECT distinct schema_version FROM dq_data_model_schema "
            + "WHERE schema_name = :name and schema_type = :#{#type.toString()} "
            + "ORDER BY schema_version ASC",
        nativeQuery = true)
    List<String> listVersions(@Param("type") DataModelSchemaType type, @Param("name") String name);

    @NotNull Optional<DataModelSchema> findById(@NotNull Integer id);
}
