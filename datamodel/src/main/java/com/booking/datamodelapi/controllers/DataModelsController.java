package com.booking.datamodelapi.controllers;

import com.booking.datamodelapi.dto.DataModelSchemaType;
import com.booking.datamodelapi.dto.schema.DataModelAvroCompatible;
import com.booking.datamodelapi.dto.schema.DataModelSchemaContent;
import com.booking.datamodelapi.dto.schema.DataModelSchemaPayload;
import com.booking.datamodelapi.exceptions.datamodel.DataModelIncorrectSchemaVersionException;
import com.booking.datamodelapi.exceptions.datamodel.DataModelOperationException;
import com.booking.datamodelapi.exceptions.datamodel.DataModelSchemaConversionException;
import com.booking.datamodelapi.model.datamodel.schema.DataModelSchema;
import com.booking.datamodelapi.services.datamodel.DataModelService;
import io.confluent.kafka.schemaregistry.avro.AvroCompatibilityLevel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Api("/data-models")
@RestController
@RequestMapping("/data-models")
public class DataModelsController {

    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory.getLogger(DataModelsController.class);

    @Autowired
    private DataModelService dataModelService;

    // ------ Schema management -------

    @ApiOperation("Get data model schema (latest version)")
    @GetMapping(value = "/schema/{type}/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public DataModelSchema getSchemaByTypeAndNameLatest(@PathVariable("type") final DataModelSchemaType type, @PathVariable("name") final String name)
        throws DataModelOperationException {
        return dataModelService.getSchemaLatestVersion(type, name);
    }

    @ApiOperation("Get data model schema")
    @GetMapping(value = "/schema/{type}/{name}/{version:.+}", produces = MediaType.APPLICATION_JSON_VALUE)
    public DataModelSchema getSchemaByTypeAndNameAndVersion(
        @PathVariable("type") final DataModelSchemaType type,
        @PathVariable("name") final String name,
        @ApiParam(example = "1.0.0")
        @PathVariable("version") final String version)
        throws DataModelOperationException, DataModelIncorrectSchemaVersionException {
        return dataModelService.getSchema(type, name, version);
    }

    @ApiOperation("List data model schema version")
    @GetMapping(value = "/schema/versions/{type}/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> listDataModelSchemaVersions(
        @PathVariable("type") final DataModelSchemaType type,
        @PathVariable("name") final String name) {
        return dataModelService.listVersions(type, name);
    }

    @ApiOperation("Convert json data model schema to avro by name (latest version)")
    @GetMapping(value = "/schema/json-to-avro/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public DataModelSchemaContent convertJsonSchemaToAvroByName(@PathVariable("name") final String name)
        throws DataModelOperationException, DataModelSchemaConversionException {
        return new DataModelSchemaContent(dataModelService.convertJsonSchemaToAvroByNameLatestVersion(name));
    }

    @ApiOperation("Convert json data model schema to avro by name and version")
    @GetMapping(value = "/schema/json-to-avro/{name}/{version:.+}", produces = MediaType.APPLICATION_JSON_VALUE)
    public DataModelSchemaContent convertJsonSchemaToAvroByNameAndVersion(
        @PathVariable("name") final String name, @PathVariable("version") final String version)
        throws DataModelOperationException, DataModelSchemaConversionException, DataModelIncorrectSchemaVersionException {
        return new DataModelSchemaContent(dataModelService.convertJsonSchemaToAvroByNameAndVersion(name, version));
    }

    @ApiOperation("Convert json schema to avro schema")
    @PostMapping(value = "/schema/json-to-avro", produces = MediaType.APPLICATION_JSON_VALUE)
    public DataModelSchemaContent convertJsonSchemaToAvro(@RequestBody DataModelSchemaContent payload)
        throws DataModelSchemaConversionException {
        return new DataModelSchemaContent(DataModelService.convertJsonSchemaToAvro(payload.getContent()));
    }

    @ApiOperation("Check latest json schema's Avro backwards compatibility")
    @PostMapping(value = "/schema/json-avro-compatibility/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public DataModelAvroCompatible checkAvroBackwardsCompatibilityForJsonSchema(@PathVariable("name") final String name,
                                                                       @RequestBody DataModelSchemaContent payload)
            throws DataModelSchemaConversionException, DataModelOperationException {
        boolean compatibility = dataModelService.checkJsonSchemaAvroCompatibilityBackward(name, payload.getContent());
        return new DataModelAvroCompatible(AvroCompatibilityLevel.BACKWARD.name(), compatibility);
    }

    @ApiOperation("Check latest json schema's Avro compatibility")
    @PostMapping(value = "/schema/json-avro-compatibility/{name}/{avroCompatibilityLevel}", produces = MediaType.APPLICATION_JSON_VALUE)
    public DataModelAvroCompatible checkAvroCompatibilityForJsonSchema(
            @PathVariable("name") final String name,
            @PathVariable("avroCompatibilityLevel") final AvroCompatibilityLevel avroCompatibilityLevel,
            @RequestBody DataModelSchemaContent payload)
                throws DataModelSchemaConversionException, DataModelOperationException {
        boolean compatibility = dataModelService.checkJsonSchemaAvroCompatibility(name, payload.getContent(), avroCompatibilityLevel);
        return new DataModelAvroCompatible(avroCompatibilityLevel.name(), compatibility);
    }


    @ApiOperation(value = "Save data model schema")
    @PostMapping(value = "/schema/{type}/{name}/{version:.+}", produces = MediaType.APPLICATION_JSON_VALUE)
    // todo add authorisation
    public DataModelSchema saveDataModelSchema(
        @PathVariable("type") final DataModelSchemaType type, @PathVariable("name") final String name, @PathVariable("version") final String version,
        @RequestBody DataModelSchemaPayload payload)
            throws DataModelSchemaConversionException, DataModelIncorrectSchemaVersionException, DataModelOperationException {
        if (type.equals(DataModelSchemaType.JSON) && payload.getCompatibilitySettings().isConvertible()) {
            // dry-run json->avro conversion, to check ability. This will throw exception if not possible.
            DataModelService.convertJsonSchemaToAvro(payload.getContent());
        }
        return dataModelService.saveSchema(type, name, version, payload.getContent());
    }

    @ApiOperation(value = "Save data model schema (version is automatically incremented)")
    @PostMapping(value = "/schema/{type}/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    // todo add authorisation
    public DataModelSchema saveDataModelSchema(
        @PathVariable("type") final DataModelSchemaType type, @PathVariable("name") final String name,
        @RequestBody DataModelSchemaPayload payload
    ) throws DataModelSchemaConversionException, DataModelIncorrectSchemaVersionException, DataModelOperationException {
        if (type.equals(DataModelSchemaType.JSON) && payload.getCompatibilitySettings().isConvertible()) {
            // dry-run json->avro conversion, to check ability. This will throw exception if not possible.
            DataModelService.convertJsonSchemaToAvro(payload.getContent());
        }
        return dataModelService.saveSchemaAutoVersion(type, name, payload.getContent());
    }

    @ApiOperation("Get associated data model schema by entity id")
    @GetMapping(value = "/schema-by-entity/{entity_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public DataModelSchema getAssociatedSchemaByEntityId(@PathVariable("entity_id") final int entityId) throws DataModelOperationException {
        return dataModelService.getAssociatedSchemaByEntityId(entityId);
    }

    @ApiOperation("Get associated data model schema for kafka topic")
    @GetMapping(value = "/schema-for-kafka/{federation}/{topic}", produces = MediaType.APPLICATION_JSON_VALUE)
    public DataModelSchema getAssociatedSchemaForKafkaTopic(@PathVariable("federation") final String federation, @PathVariable("topic")  final String topic)
        throws DataModelOperationException {
        return dataModelService.getAssociatedSchemaForKafkaTopic(federation, topic);
    }


}
