package com.booking.datamodelapi.converters.spring;

import com.booking.datamodelapi.dto.DataModelSchemaType;
import org.springframework.core.convert.converter.Converter;

public class DataModelSchemaTypeConverter
    implements Converter<String, DataModelSchemaType> {

    @Override
    public DataModelSchemaType convert(String from) {
        return DataModelSchemaType.fromString(from);
    }
}