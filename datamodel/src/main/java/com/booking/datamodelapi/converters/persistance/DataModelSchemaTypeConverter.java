package com.booking.datamodelapi.converters.persistance;

import com.booking.datamodelapi.dto.DataModelSchemaType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class DataModelSchemaTypeConverter implements AttributeConverter<DataModelSchemaType, String> {
    @Override
    public String convertToDatabaseColumn(DataModelSchemaType state) {
        return state.toString();
    }

    @Override
    public DataModelSchemaType convertToEntityAttribute(String state) {
        return DataModelSchemaType.fromString(state);
    }
}