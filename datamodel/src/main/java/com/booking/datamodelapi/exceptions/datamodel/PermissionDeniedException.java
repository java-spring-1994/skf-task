package com.booking.datamodelapi.exceptions.datamodel;

public class PermissionDeniedException extends Exception {
    public PermissionDeniedException(String message) {
        super(message);
    }
}
