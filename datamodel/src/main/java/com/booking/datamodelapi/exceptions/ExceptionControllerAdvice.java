package com.booking.datamodelapi.exceptions;

import com.booking.dataavailability.dto.ExceptionMessage;
import com.booking.datamodelapi.exceptions.datamodel.DataModelIncorrectSchemaVersionException;
import com.booking.datamodelapi.exceptions.datamodel.DataModelOperationException;
import com.booking.datamodelapi.exceptions.datamodel.DataModelSchemaConversionException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.ws.rs.NotFoundException;

@ControllerAdvice
public class ExceptionControllerAdvice {
    @ExceptionHandler(DataModelOperationException.class)
    public final ResponseEntity<ExceptionMessage> handleDataModelOperationException(DataModelOperationException ex) {
        ExceptionMessage error = new ExceptionMessage("Unable to execute data model operation", ex.getMessage());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DataModelSchemaConversionException.class)
    public final ResponseEntity<ExceptionMessage> handleDataModelSchemaConversionException(DataModelSchemaConversionException ex) {
        ExceptionMessage error = new ExceptionMessage("Unable to apply data model schema conversion", ex.getMessage());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DataModelIncorrectSchemaVersionException.class)
    public final ResponseEntity<ExceptionMessage> handleDataModelIncorrectSchemaVersionException(DataModelIncorrectSchemaVersionException ex) {
        ExceptionMessage error = new ExceptionMessage("Incorrect data model schema version", ex.getMessage());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<ExceptionMessage> handleAuthClientException(NotFoundException ex) {
        return new ResponseEntity<>(new ExceptionMessage("Requested resource hasn't been not found", ex.getMessage()),
                HttpStatus.NOT_FOUND);
    }
}
