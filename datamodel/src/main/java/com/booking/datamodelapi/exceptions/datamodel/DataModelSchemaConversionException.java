package com.booking.datamodelapi.exceptions.datamodel;

public class DataModelSchemaConversionException extends Exception {
    public DataModelSchemaConversionException(String message) {
        super(message);
    }
}