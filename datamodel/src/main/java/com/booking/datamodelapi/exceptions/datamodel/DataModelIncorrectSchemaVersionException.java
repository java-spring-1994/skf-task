package com.booking.datamodelapi.exceptions.datamodel;

public class DataModelIncorrectSchemaVersionException extends Exception {
    public DataModelIncorrectSchemaVersionException(String message) {
        super(message);
    }
}
