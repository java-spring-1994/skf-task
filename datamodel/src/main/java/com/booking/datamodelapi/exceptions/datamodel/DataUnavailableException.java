package com.booking.datamodelapi.exceptions.datamodel;

public class DataUnavailableException extends Exception {
    public DataUnavailableException(String message) {
        super(message);
    }
}
