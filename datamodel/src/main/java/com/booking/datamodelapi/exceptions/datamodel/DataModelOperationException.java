package com.booking.datamodelapi.exceptions.datamodel;

public class DataModelOperationException extends Exception {
    public DataModelOperationException(final String msg) {
        super(msg);
    }
}