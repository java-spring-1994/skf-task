package com.booking.datamodelapi.model.datamodel.schema;

import com.booking.datamodelapi.dto.DataModelSchemaType;
import com.booking.datamodelapi.converters.persistance.DataModelSchemaTypeConverter;
import com.booking.datamodelapi.model.DataSource;
import com.booking.datamodelapi.utils.datamodel.schema.DataModelSchemaUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "dq_data_model_schema")
public class DataModelSchema {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "schema_name", length = 500)
    private String name;

    @Column(name = "content", columnDefinition = "TEXT")
    private String content;

    @Column(name = "schema_type", columnDefinition = "enum('avro', 'json')")
    @Convert(converter = DataModelSchemaTypeConverter.class)
    private DataModelSchemaType type;

    @Column(name = "schema_version", length = 10)
    private String version;

    @JsonIgnore
    @Column(name = "mysql_row_updated_at")
    private Date updatedAt;

    @OneToMany(mappedBy = "schema", fetch = FetchType.LAZY)
    private List<DataModelSchemaMapping> dataEntityMappings = new ArrayList<>();

    public DataModelSchema() {}

    public DataModelSchema(final DataModelSchemaType type, final String name, final String content) {
        this(type, name, DataModelSchemaUtils.INITIAL_SCHEMA_VERSION, content);
    }

    public DataModelSchema(final DataModelSchemaType type, final String name, final String version, final String content) {
        this.type = type;
        this.name = name;
        this.content = content;
        this.version = version;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getContent() {
        return content;
    }

    public DataModelSchemaType getType() {
        return type;
    }

    public String getVersion() {
        return version;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    @JsonIgnoreProperties("dataModelSchema") // to avoid recursive serialization of reversed links
    public List<DataSource> getAssignedDataEntities() {
        return dataEntityMappings.stream().map(DataModelSchemaMapping::getDataEntity).collect(Collectors.toList());
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setType(DataModelSchemaType type) {
        this.type = type;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
