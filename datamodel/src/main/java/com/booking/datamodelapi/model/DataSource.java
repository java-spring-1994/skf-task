package com.booking.datamodelapi.model;

import com.booking.datamodelapi.model.EntityInstance.Ams4Instance;
import com.booking.datamodelapi.model.EntityInstance.HiveInstanceType;
import com.booking.datamodelapi.model.EntityInstance.Lhr4Instance;
import com.booking.datamodelapi.model.datamodel.TimeSeriesConfig;
import com.booking.datamodelapi.model.datamodel.schema.DataModelSchema;
import com.booking.datamodelapi.model.datamodel.schema.DataModelSchemaMapping;
import com.booking.datamodelapi.model.EntityInstance.GoogleCloudInstance;
import com.booking.datamodelapi.model.EntityInstance.MysqlImportInstance;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Where;

//CHECKSTYLE:OFF
//CHECKSTYLE:ON

@Entity
@Table(name = "dq_data_entity")
public class DataSource implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "entity_name", length = 500)
    private String name;
    @Column(name = "entity_namespace", length = 500)
    private String namespace;
    @Column(name = "entity_account", length = 500)
    private String account;
    @Column(name = "entity_type", columnDefinition = "enum('hive-table','mysql-table', 'kafka-topic', 'aws-glue')")
    private String type;
    @Column(name = "mysql_row_created_at")
    private Date mysqlRowCreatedAt;

    @OneToMany(mappedBy = "dataSource", fetch = FetchType.LAZY)
    @Where(clause = "active = 1")
    private Set<DataSourceOwner> owners;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @ElementCollection
    @CollectionTable(name = "dq_data_entity_label", joinColumns = {@JoinColumn(name = "entity_id")})
    @Column(name = "label_value")
    @MapKeyColumn(name = "label_key")
    private Map<String, String> labels;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "dq_entity_watcher", joinColumns = @JoinColumn(name = "entity_id"))
    @Column(name = "user_id")
    public Set<Integer> watchers;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @OneToOne(targetEntity = Lhr4Instance.class, optional = true,  fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "entity_name", referencedColumnName = "table_name", updatable = false, insertable = false),
            @JoinColumn(name = "entity_namespace", referencedColumnName = "schema_name", updatable = false, insertable = false)
    })
    @Where(clause = "mysql_row_status = 1")
    private Lhr4Instance lhr4Instance;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @OneToOne(targetEntity = Ams4Instance.class, optional = true, fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "entity_name", referencedColumnName = "table_name", updatable = false, insertable = false),
            @JoinColumn(name = "entity_namespace", referencedColumnName = "schema_name", updatable = false, insertable = false)
    })
    @Where(clause = "mysql_row_status = 1")
    private Ams4Instance ams4Instance;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @OneToOne(targetEntity = GoogleCloudInstance.class, optional = true, fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "entity_name", referencedColumnName = "table_name", updatable = false, insertable = false),
            @JoinColumn(name = "entity_namespace", referencedColumnName = "schema_name", updatable = false, insertable = false)
    })
    @Where(clause = "mysql_row_status = 1")
    private GoogleCloudInstance googleCloudInstance;

    @OneToMany(targetEntity = MysqlImportInstance.class, fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "hive_table_name", referencedColumnName = "entity_name", updatable = false, insertable = false),
            @JoinColumn(name = "hive_schema_name", referencedColumnName = "entity_namespace", updatable = false, insertable = false)
    })

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<MysqlImportInstance> mysqlImportInstance;

    @JsonIgnore
    @OneToOne(mappedBy = "dataSource", fetch = FetchType.LAZY)
    private TimeSeriesConfig tsConfig;

    @OneToOne(mappedBy = "dataEntity", fetch = FetchType.LAZY)
    private DataModelSchemaMapping dataModelSchemaMapping;

    public DataSource() {
    }

    public Set<Integer> getWatchers() {
        return watchers;
    }

    public void setWatchers(Set<Integer> watchers) {
        this.watchers = watchers;
    }

    public Lhr4Instance getLhr4Instance() {
        return lhr4Instance;
    }

    public void setLhr4Instance(Lhr4Instance lhr4Instance) {
        this.lhr4Instance = lhr4Instance;
    }

    public Ams4Instance getAms4Instance() {
        return ams4Instance;
    }

    public void setAms4Instance(Ams4Instance ams4Instance) {
        this.ams4Instance = ams4Instance;
    }

    public GoogleCloudInstance getGoogleCloudInstance() {
        return googleCloudInstance;
    }

    public void setGoogleCloudInstance(GoogleCloudInstance googleCloudInstance) {
        this.googleCloudInstance = googleCloudInstance;
    }

    public Set<DataSourceOwner> getOwners() {
        return owners;
    }

    public void setOwners(Set<DataSourceOwner> owners) {
        this.owners = owners;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }


    public List<MysqlImportInstance> getMysqlImportInstance() {
        return mysqlImportInstance;
    }

    public void setMysqlImportInstance(List<MysqlImportInstance> mysqlImportInstance) {
        this.mysqlImportInstance = mysqlImportInstance;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getMysqlRowCreatedAt() {
        return mysqlRowCreatedAt;
    }

    public void setMysqlRowCreatedAt(Date mysqlRowCreatedAt) {
        this.mysqlRowCreatedAt = mysqlRowCreatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public TimeSeriesConfig getTsConfig() {
        return tsConfig;
    }

    @JsonIgnoreProperties("assignedDataEntities") // to avoid recursive serialization of reversed links
    public DataModelSchema getDataModelSchema() {
        return Optional.ofNullable(dataModelSchemaMapping).map(DataModelSchemaMapping::getSchema).orElse(null);
    }

    public boolean isPartitioned() {
        return isPartitioned(getAms4Instance()) || isPartitioned(getLhr4Instance());
    }

    private boolean isPartitioned(HiveInstanceType hiveInstance) {
        return Optional.ofNullable(hiveInstance).map(HiveInstanceType::isPartitioned).orElse(false);
    }
}
