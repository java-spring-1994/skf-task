package com.booking.datamodelapi.model.datamodel.schema;

import com.booking.datamodelapi.model.DataSource;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "dq_data_model_schema_mapping")
public class DataModelSchemaMapping {

    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "schema_id")
    private DataModelSchema schema;

    @OneToOne
    @JoinColumn(name = "entity_id")
    private DataSource dataEntity;

    @Column(name = "mysql_row_updated_at")
    private Date updatedAt;

    public DataModelSchema getSchema() {
        return schema;
    }

    public DataSource getDataEntity() {
        return dataEntity;
    }
}
