package com.booking.datamodelapi.model.EntityInstance;

import javax.persistence.Inheritance;
import javax.persistence.Transient;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.Column;
import java.util.List;

@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@MappedSuperclass
public abstract class HiveInstanceType<T extends ColumnMetaType> extends EntityInstanceType {

    @Transient
    public String type = "hive";

    @Column(name = "schema_name")
    public String schema;

    @Column(name = "table_name")
    public String table;

    @Column(name = "table_comment")
    public String description;

    @Column(name = "hdfs_location")
    private String hdfsLocation;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "num_files")
    private Integer numFiles;

    @Column(name = "raw_data_size")
    private Long tableRawDataSize;

    @Column(name = "num_buckets")
    private Integer numBuckets;

    @Column(name = "total_size")
    private Long totalSize;

    @Column(name = "num_rows")
    private Long numRowsInTable;

    @Column(name = "table_type")
    private String tableType;

    public abstract List<T> getColumnMeta();

    public abstract void setColumnMeta(List<T> columnMeta);

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHdfsLocation() {
        return hdfsLocation;
    }

    public void setHdfsLocation(String hdfsLocation) {
        this.hdfsLocation = hdfsLocation;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getNumFiles() {
        return numFiles;
    }

    public void setNumFiles(Integer numFiles) {
        this.numFiles = numFiles;
    }

    public Long getTableRawDataSize() {
        return tableRawDataSize;
    }

    public void setTableRawDataSize(Long tableRawDataSize) {
        this.tableRawDataSize = tableRawDataSize;
    }

    public Integer getNumBuckets() {
        return numBuckets;
    }

    public void setNumBuckets(Integer numBuckets) {
        this.numBuckets = numBuckets;
    }

    public Long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }

    public Long getNumRowsInTable() {
        return numRowsInTable;
    }

    public void setNumRowsInTable(Long numRowsInTable) {
        this.numRowsInTable = numRowsInTable;
    }

    public String getTableType() {
        return tableType;
    }

    public void setTableType(String tableType) {
        this.tableType = tableType;
    }

    public boolean isPartitioned() {
        List<T> columnMeta = getColumnMeta();
        for (T column: columnMeta) {
            if (column.getPartitionedBy() != 0) {
                return true;
            }
        }
        return false;
    }
}
