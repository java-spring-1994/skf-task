package com.booking.datamodelapi.controllers;

import com.booking.datamodelapi.converters.spring.DataModelSchemaTypeConverter;
import com.booking.datamodelapi.dto.DataModelSchemaType;
import com.booking.datamodelapi.dto.schema.DataModelSchemaCompatibilitySettings;
import com.booking.datamodelapi.dto.schema.DataModelSchemaContent;
import com.booking.datamodelapi.dto.schema.DataModelSchemaPayload;
import com.booking.datamodelapi.exceptions.ExceptionControllerAdvice;
import com.booking.datamodelapi.model.datamodel.schema.DataModelSchema;
import com.booking.datamodelapi.services.datamodel.DataModelService;
import com.booking.spring.filter.BookingServletFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(DataModelsController.class)
public class DataModelsControllerTest {

    private MockMvc mockMvc;

    @MockBean
    private DataModelService dataModelService;

    @MockBean
    BookingServletFilter bookingServletFilter;

    @Autowired
    private DataModelsController dataModelsController;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setupBefore() {
        FormattingConversionService formattingConversionService=new FormattingConversionService();
        formattingConversionService.addConverter(new DataModelSchemaTypeConverter());
        mockMvc = MockMvcBuilders.standaloneSetup(dataModelsController)
            .setControllerAdvice(new ExceptionControllerAdvice())
            .setConversionService(formattingConversionService)
            .build();
    }

    @Test
    public void saveDataModelSchemaTestConversionCondition() throws Exception {
        Mockito
            .when(
                dataModelService.saveSchema(
                    ArgumentMatchers.any(DataModelSchemaType.class),
                    ArgumentMatchers.any(String.class),
                    ArgumentMatchers.any(String.class),
                    ArgumentMatchers.any(String.class))).thenReturn(new DataModelSchema());


        // valid json schema, can be converted to avro
        String jsonSchemaContent = "{\"$schema\": \"http://json-schema.org/schema#7\", \"title\": \"ip\", \"type\": \"object\", \"properties\": {\"hostname\": {\"type\": \"string\"}, \"ip_address\": {\"type\": \"string\"}}, \"required\": [\"hostname\", \"ip_address\"]}";
        String jsonString = objectMapper.writer().writeValueAsString(new DataModelSchemaPayload(
            jsonSchemaContent, new DataModelSchemaCompatibilitySettings())
        );
        mockMvc.perform(
            post("/data-models/schema/json/test/1.0.0")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonString)
                .header("Origin","*"))
            .andExpect(status().isOk());
    }

    @Test
    public void saveDataModelSchemaTestConversionConditionFails() throws Exception {
        Mockito
            .when(
                dataModelService.saveSchema(
                    ArgumentMatchers.any(DataModelSchemaType.class),
                    ArgumentMatchers.any(String.class),
                    ArgumentMatchers.any(String.class),
                    ArgumentMatchers.any(String.class))).thenReturn(new DataModelSchema());

        // invalid json schema, can not be converted to avro
        // todo need better example, when json is correct, but can not be converted to avro because of conversion limitations
        String jsonSchemaContent = "{}}";
        String jsonString = objectMapper.writer().writeValueAsString(new DataModelSchemaPayload(
            jsonSchemaContent, new DataModelSchemaCompatibilitySettings())
        );

        mockMvc.perform(
            post("/data-models/schema/json/test/1.0.0")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonString)
                .header("Origin","*"))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void saveDataModelSchemaTestConversionDisabled() throws Exception {
        Mockito
            .when(
                dataModelService.saveSchema(
                    ArgumentMatchers.any(DataModelSchemaType.class),
                    ArgumentMatchers.any(String.class),
                    ArgumentMatchers.any(String.class),
                    ArgumentMatchers.any(String.class))).thenReturn(new DataModelSchema());

        // invalid json schema, can not be converted to avro
        String jsonSchemaContent = "{}}";
        // todo need better example, when json is correct, but can not be converted to avro because of conversion limitations
        // disable 'convertible' assertion
        String jsonString = objectMapper.writer().writeValueAsString(new DataModelSchemaPayload(
            jsonSchemaContent, new DataModelSchemaCompatibilitySettings().setConvertible(false))
        );

        mockMvc.perform(
            post("/data-models/schema/json/test/1.0.0")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonString)
                .header("Origin","*"))
            .andExpect(status().isOk());
    }

    @Test
    public void saveDataModelSchemaTestConversionOnlyForJson() throws Exception {
        Mockito
            .when(
                dataModelService.saveSchema(
                    ArgumentMatchers.any(DataModelSchemaType.class),
                    ArgumentMatchers.any(String.class),
                    ArgumentMatchers.any(String.class),
                    ArgumentMatchers.any(String.class))).thenReturn(new DataModelSchema());

        // simple valid avro schema
        String avroSchemaContent = "{\"namespace\": \"example.avro\", \"type\": \"record\", \"name\": \"User\", \"fields\": [{\"name\": \"name\", \"type\": \"string\"}]}";
        String jsonString = objectMapper.writer().writeValueAsString(new DataModelSchemaPayload(
            avroSchemaContent, new DataModelSchemaCompatibilitySettings())
        );

        mockMvc.perform(
            post("/data-models/schema/avro/test/1.0.0")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonString)
                .header("Origin","*"))
            .andExpect(status().isOk());
    }

    @Test
    public void checkAvroCompatibilityTest() throws Exception {
        String jsonSchemaLatest = "{\"$schema\": \"http://json-schema.org/schema#7\", \"title\": \"ip\", \"type\": \"object\", \"properties\": {\"hostname\": {\"type\": \"string\"}, \"ip_address\": {\"type\": \"string\"}}, \"required\": [\"hostname\", \"ip_address\"]}";
        String jsonSchemaNew = "{\"$schema\": \"http://json-schema.org/schema#7\", \"title\": \"ip\", \"type\": \"object\", \"properties\": {\"hostname\": {\"type\": \"string\"}, \"ip_address\": {\"type\": \"string\"}}, \"required\": [\"ip_address\"]}";
        DataModelSchema dataModelSchemaIntermediate = Mockito.mock(DataModelSchema.class);
        Mockito.when(dataModelSchemaIntermediate.getContent()).thenReturn(jsonSchemaLatest);
        Mockito.doReturn(dataModelSchemaIntermediate).when(dataModelService)
                .getSchemaLatestVersion(
                        ArgumentMatchers.any(DataModelSchemaType.class),
                        ArgumentMatchers.any(String.class));

        // simple valid json schema wrapped in DataModelSchemaContent
        String jsonString = objectMapper.writer().writeValueAsString(new DataModelSchemaContent(
                jsonSchemaNew)
        );
        
        mockMvc.perform(
                post("/data-models/schema/json-avro-compatibility/random_name")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .content(jsonString)
                        .header("Origin","*"))
                .andExpect(status().isOk());
    }
}