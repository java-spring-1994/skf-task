package com.booking.datamodelapi.utils.datamodel.schema;

import com.booking.datamodelapi.exceptions.datamodel.DataModelIncorrectSchemaVersionException;
import com.booking.datamodelapi.exceptions.datamodel.DataModelSchemaConversionException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DataModelSchemaUtilsTest {

    @Test
    public void convertJsonToAvroNormalOk() throws DataModelSchemaConversionException, JsonProcessingException {

        final String jsonSchemaContent = "{\n"
        +"            \"type\": \"object\",\n"
        +"            \"additionalProperties\": false,\n"
        +"            \"properties\": {\n"
        +"                \"id\": { \"type\": \"integer\" },\n"
        +"                \"name\": { \"type\": \"string\" },\n"
        +"                \"type\": { \"enum\": [ \"food\", \"drink\" ] },\n"
        +"                \"tags\": {\n"
        +"                    \"type\": \"array\",\n"
        +"                    \"items\": { \"type\": \"string\" }\n"
        +"                }\n"
        +"            },\n"
        +"            \"required\": [\"id\", \"type\"]\n"
        +"        }";

        final String expectedAvroSchemaContent = "{\n"
            + "\"type\": \"record\",\n"
            + "            \"name\": \"record_schema\",\n"
            + "            \"namespace\": \"com.booking\",\n"
            + "            \"fields\": [\n"
            + "                { \"name\": \"id\", \"type\": \"long\" },\n"
            + "                { \"name\": \"name\", \"type\": [\"null\", \"string\"] },\n"
            + "                {\n"
            + "                    \"name\": \"tags\",\n"
            + "                    \"type\": [\"null\", {\n"
            + "                        \"type\": \"array\", \"items\": \"string\"\n"
            + "                    }]\n"
            + "                },\n"
            + "                {\n"
            + "                    \"name\": \"type\",\n"
            + "                    \"type\": {\n"
            + "                        \"type\": \"enum\",\n"
            + "                        \"name\": \"type\",\n"
            + "                        \"namespace\": \"com.booking.type\",\n"
            + "                        \"symbols\": [ \"food\", \"drink\" ]\n"
            + "                    }\n"
            + "                }\n"
            + "            ]\n"
            + "        }";

        final String actualAvroSchemaContent = DataModelSchemaUtils.SchemaConverter.convertJsonToAvro(jsonSchemaContent).toString();
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode expectedJson = objectMapper.readTree(expectedAvroSchemaContent);
        JsonNode actualJson = objectMapper.readTree(actualAvroSchemaContent);

        Assert.assertEquals(expectedJson, actualJson);
    }

    @Test
    @Ignore("This was mitigated in the library as a temp workaround") // todo cleanup if needed, or enable later
    public void convertJsonToAvroAdditionalPropertiesFalseRequired() {
        final String jsonSchemaContent = "{\n"
        +"            \"type\": \"object\",\n"
        +"            \"properties\": {\n"
        +"                \"id\": { \"type\": \"integer\" },\n"
        +"                \"name\": { \"type\": \"string\" },\n"
        +"                \"type\": { \"enum\": [ \"food\", \"drink\" ] },\n"
        +"                \"tags\": {\n"
        +"                    \"type\": \"array\",\n"
        +"                    \"items\": { \"type\": \"string\" }\n"
        +"                }\n"
        +"            }\n"
        +"        }";

        Assert.assertThrows(
            DataModelSchemaConversionException.class,
            () -> DataModelSchemaUtils.SchemaConverter.convertJsonToAvro(jsonSchemaContent));
    }

    @Test
    public void convertJsonToAvroRealSchemasNoExceptions() throws DataModelSchemaConversionException, IOException {

        String jsonSchemaContent = IOUtils.toString(
            this.getClass().getResource("page_load.json"),
            StandardCharsets.UTF_8);
        DataModelSchemaUtils.SchemaConverter.convertJsonToAvro(jsonSchemaContent);
    }

    @Test
    public void validateSchemaVersionFormatTest() {
        assertDoesNotThrow(
            () -> DataModelSchemaUtils.validateSchemaVersionFormat("1.0.0")
        );
        assertDoesNotThrow(
            () -> DataModelSchemaUtils.validateSchemaVersionFormat("0.0.1")
        );
        assertDoesNotThrow(
            () -> DataModelSchemaUtils.validateSchemaVersionFormat("12.345.6789")
        );

        assertThrows(
            DataModelIncorrectSchemaVersionException.class,
            () -> DataModelSchemaUtils.validateSchemaVersionFormat("")
        );
        assertThrows(
            DataModelIncorrectSchemaVersionException.class,
            () -> DataModelSchemaUtils.validateSchemaVersionFormat("1.2")
        );
        assertThrows(
            DataModelIncorrectSchemaVersionException.class,
            () -> DataModelSchemaUtils.validateSchemaVersionFormat("1.2.3.4")
        );
        assertThrows(
            DataModelIncorrectSchemaVersionException.class,
            () -> DataModelSchemaUtils.validateSchemaVersionFormat("1.0.a")
        );
        assertThrows(
            DataModelIncorrectSchemaVersionException.class,
            () -> DataModelSchemaUtils.validateSchemaVersionFormat("1.0.-1")
        );
    }
}