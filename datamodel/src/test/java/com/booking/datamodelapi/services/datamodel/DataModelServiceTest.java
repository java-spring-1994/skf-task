package com.booking.datamodelapi.services.datamodel;

import com.booking.datamodelapi.dto.DataModelSchemaType;
import com.booking.datamodelapi.exceptions.datamodel.DataModelOperationException;
import com.booking.datamodelapi.exceptions.datamodel.DataModelSchemaConversionException;
import com.booking.datamodelapi.model.datamodel.schema.DataModelSchema;
import com.booking.datamodelapi.repository.DataSourceRepository;
import com.booking.datamodelapi.repository.datamodel.schema.DataModelSchemaRepository;
import io.confluent.kafka.schemaregistry.avro.AvroCompatibilityLevel;
import org.junit.Assert;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataModelServiceTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataModelServiceTest.class);

    static String schemaLatest = "{\n"
        +"            \"type\": \"object\",\n"
        +"            \"additionalProperties\": false,\n"
        +"            \"properties\": {\n"
        +"                \"id\": { \"type\": \"integer\" },\n"
        +"                \"name\": { \"type\": \"string\" },\n"
        +"                \"type\": { \"enum\": [ \"food\", \"drink\" ] },\n"
        +"                \"tags\": {\n"
        +"                    \"type\": \"array\",\n"
        +"                    \"items\": { \"type\": \"string\" }\n"
        +"                }\n"
        +"            },\n"
        +"            \"required\": [\"id\", \"type\"]\n"
        +"        }";
    static DataModelService dataModelService;

    @BeforeAll
    public static void setup() throws DataModelOperationException {
        dataModelService = prepareDataModelServiceInstance();
        LOGGER.debug(String.format("%s SETUP complete", DataModelServiceTest.class.getSimpleName()));
    }

    public static DataModelService prepareDataModelServiceInstance() throws DataModelOperationException {
        DataModelService dataModelServiceIntermediate = new DataModelService(
                Mockito.mock(DataSourceRepository.class),
                Mockito.mock(DataModelSchemaRepository.class)
        );
        DataModelSchema dataModelSchemaIntermediate = Mockito.mock(DataModelSchema.class);
        Mockito.when(dataModelSchemaIntermediate.getContent()).thenReturn(schemaLatest);
        DataModelService dataModelService = Mockito.spy(dataModelServiceIntermediate);
        Mockito.doReturn(dataModelSchemaIntermediate)
                .when(dataModelService)
                .getSchemaLatestVersion(Mockito.any(DataModelSchemaType.class), Mockito.anyString());
        return dataModelService;
    }

    /**
     * Adds a required field. Leads to a breaking change.
     */
    @Test
    public void testIncompatibleSchema() throws DataModelOperationException, DataModelSchemaConversionException {

        String newSchema = "{\n"
            +"            \"type\": \"object\",\n"
            +"            \"additionalProperties\": false,\n"
            +"            \"properties\": {\n"
            +"                \"id\": { \"type\": \"integer\" },\n"
            +"                \"id2\": {\"type\": \"integer\"},\n"
            +"                \"name\": { \"type\": \"string\" },\n"
            +"                \"type\": { \"enum\": [ \"food\", \"drink\" ] },\n"
            +"                \"tags\": {\n"
            +"                    \"type\": \"array\",\n"
            +"                    \"items\": { \"type\": \"string\" }\n"
            +"                }\n"
            +"            },\n"
            +"            \"required\": [\"id\", \"type\", \"id2\"]\n"
            +"        }";
        Assert.assertFalse(dataModelService.checkJsonSchemaAvroCompatibility(
                "schemaLatest",
                newSchema,
                AvroCompatibilityLevel.BACKWARD
        ));
    }

    /**
     * Removes one required field. Doesn't lead to a breaking change.
     */
    @Test
    public void testCompatibleSchema() throws DataModelOperationException, DataModelSchemaConversionException {
        String newSchema = "{\n"
            +"            \"type\": \"object\",\n"
            +"            \"additionalProperties\": false,\n"
            +"            \"properties\": {\n"
            +"                \"id\": { \"type\": \"integer\" },\n"
            +"                \"name\": { \"type\": \"string\" },\n"
            +"                \"type\": { \"enum\": [ \"food\", \"drink\" ] },\n"
            +"                \"tags\": {\n"
            +"                    \"type\": \"array\",\n"
            +"                    \"items\": { \"type\": \"string\" }\n"
            +"                }\n"
            +"            },\n"
            +"            \"required\": [\"id\"]\n"
            +"        }";
        Assert.assertTrue(dataModelService.checkJsonSchemaAvroCompatibility(
            "schemaLatest",
            newSchema,
            AvroCompatibilityLevel.BACKWARD
        ));
    }

    @Test
    public void testAvroCompatibilityLevelStrings() throws DataModelOperationException, DataModelSchemaConversionException {
        Assert.assertThrows(
            DataModelOperationException.class,
            () -> dataModelService.checkJsonSchemaAvroCompatibility(
                "schemaLatest",
                schemaLatest,
                "Back"
            )
        );
        Assert.assertTrue(
            dataModelService.checkJsonSchemaAvroCompatibility(
                "schemaLatest",
                schemaLatest,
                "Backward"
            )
        );
    }
}
