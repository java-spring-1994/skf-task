public class MultiplierHelper {

    public static void multiply(int[][] m1, int[][] m2, int[][] resultMatrix, int i, int j, int n ) {
        for (int k = 0; k < n; k++) {
            resultMatrix[i][j] += (m1[i][k] * m2[k][j]) % 2;
            resultMatrix[i][j] %= 2;
        }
    }

    public static boolean sameMatrices(int[][] m1, int[][] m2, int n) {
        for (int i = 0 ; i < n ; i++) {
            for (int j= 0; j < n; j++) {
                if (m1[i][j] != m2[i][j]) {
                    System.out.println("i, j" + i +" " + j + " " + m1[i][j] + " " + m2[i][j]);
                    return false;
                }
            }
        }
        return true;
    }
}
