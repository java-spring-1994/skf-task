public class MatrixMultiplicationWithoutThread {

	public int[][] run(int[][] m1, int[][] m2) {
		int n = m1.length;
		int[][] resultMatrix = new int[n][n];

		// Basic Operation
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				for (int k = 0; k < n; k++) {
					resultMatrix[i][j] += (m1[i][k] * m2[k][j]) % 2;
					resultMatrix[i][j] %= 2;
				}
			}
		}

		return resultMatrix;
	}

}
