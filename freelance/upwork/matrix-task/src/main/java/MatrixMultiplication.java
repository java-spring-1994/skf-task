import java.time.Duration;
import java.time.Instant;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

public class MatrixMultiplication {

	// Task: Multiply 2 matrices of same size
	// Inference 1: Both matrices are square matrices
	// Inference 2: Both matrices have same dimensionality

	public static void main(String[] args) throws InterruptedException, ExecutionException {

		int matrix1[][];
		int matrix2[][];
		int resultMatrix1[][];
		int resultMatrix2[][];
		Random rand = new Random();

		// Input: n -> size of matrices
		// matrix1 -> 2D array
		// matrix2 -> 2D array

		// Output: resultMatrix -> 2D array

		// Generating input
		System.out.println("input n (Size of matrices) between 1 to 10000");
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		if (n > 10000 || n < 0) {
			System.out.println("please provide correct val of n");
			return;
		}

		matrix1 = new int[n][n];
		matrix2 = new int[n][n];
		resultMatrix1 = new int[n][n];
		resultMatrix2 = new int[n][n];

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				matrix1[i][j] = rand.nextInt(100);
			}
		}

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				matrix2[i][j] = rand.nextInt(100);
			}
		}

		// starting the timer
		Instant start1 = Instant.now();

		// Operation without thread
		MatrixMultiplicationWithoutThread mult1 = new MatrixMultiplicationWithoutThread();
		resultMatrix1 = mult1.run(matrix1, matrix2);

		// ending the timer
		Instant end1 = Instant.now();

		// starting the timer
		Instant start2 = Instant.now();

		// Operation with thread
		MatrixMultiplicationWithThread multiplicationWithThread = new MatrixMultiplicationWithThread();
		resultMatrix2 = multiplicationWithThread.calculate(matrix1, matrix2);

		// ending the timer
		Instant end2 = Instant.now();

		// time calculation
		Duration timeTaken1 = Duration.between(start1, end1);
		Duration timeTaken2 = Duration.between(start2, end2);

		System.out.println("Time taken without Thread: " + timeTaken1.toMillis() + "ms");
		System.out.println("Time taken with Thread: " + timeTaken2.toMillis() + "ms");

		// check correctness

		if (MultiplierHelper.sameMatrices(resultMatrix1, resultMatrix2, n) == true ) {
			System.out.println("thread calculated correctly");
		} else {
			System.out.println("wrong result in threading");
		}
	}

}
