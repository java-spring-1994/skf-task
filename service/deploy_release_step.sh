#!/bin/sh

SHIPPER_APPLICATION=dataquality-api
SHIPPER_RELEASE_STEP=$1

find_contender() {
    gen="$1"

    # enjoy
    contender=$(kubectl get rel -l shipper-app=$SHIPPER_APPLICATION -o go-template --template='{{range $rel := .items}}{{range $k,$v := $rel.metadata.annotations}}{{if and (eq $k "shipper.booking.com/release.generation") (eq $v "'$gen'")}}{{$rel.metadata.name}}{{end}}{{end}}{{end}}')

    echo -n "$contender"
}

wait_for() {
    rel=$1
    while true; do
        targetStep=$(kubectl get rel $rel -o=jsonpath="{.spec.targetStep}")
        achievedStep=$(kubectl get rel $rel -o=jsonpath="{.status.achievedStep.step}")

        if [ "$targetStep" = "$achievedStep" ]; then
            break
        fi

        echo "Waiting 5s for release '$rel' to reach its target step [want: $targetStep; have: $achievedStep]"
        sleep 5
    done
}


highestObserved=$(kubectl get app $SHIPPER_APPLICATION -o jsonpath="{.metadata.annotations['shipper\.booking\.com/app\.highestObservedGeneration']}")

if [ -z "$highestObserved" ]; then
    echo "Failed to detect the highest observed generation for app '$SHIPPER_APPLICATION'" >&2
    exit 1
fi

contenderGen=$((highestObserved+1))

echo "Trying contender generation '$contenderGen'"
release=$(find_contender $contenderGen)

if [ -z "$release" ]; then
    #rollingOut=$(kubectl get app $SHIPPER_APPLICATION -o jsonpath="{.status.state.rollingOut}")
    #if [ "$rollingOut" = "false" ]; then
    #    echo "Failed to find the contender for app '$SHIPPER_APPLICATION' because it doesn't look like it's being rolled out" >&2
    #    exit 1
    #fi

    echo "Trying contender generation '$highestObserved'"
    release=$(find_contender $highestObserved)
fi

if [ -z "$release" ]; then
    echo "Failed to find the contender for app '$SHIPPER_APPLICATION'" >&2
    exit 1
fi

echo "Working with '$release'"

# For every subsequent step we need to make sure that the previous step has
# been achieved.
wait_for $release

kubectl patch rel $release --type=merge --patch='{"spec":{"targetStep":'$SHIPPER_RELEASE_STEP'}}'

sleep 3
wait_for $release
exit 0
