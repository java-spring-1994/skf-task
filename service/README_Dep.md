#Intro

1. [Project home page on B.Platform](https://office.booking.com/bservices/project/data-quality/service/dataquality-api)
1. [Booking DQS Deployment](https://docs.booking.com/kubernetes/rolling-out/deploying-dqs.html)
1. [Booking Prod Deployment](https://docs.booking.com/kubernetes/rolling-out/deploying-prod.html)

####Service DNS names

1. [DQ Staging DQS](https://dataquality-staging.dqs.booking.com/swagger-ui.html)
1. [DQ DQS](https://dataquality.dqs.booking.com/swagger-ui.html)
1. [DQ Prod Staging](https://dataquality-staging.prod.booking.com/swagger-ui.html)
1. [DQ Prod](https://dataquality.prod.booking.com/swagger-ui.html)

#Usefull commands

```bash
   bkcloud cluster list # display cluster list
   #use cluster to check specific pod health
   bkcloud use cluster bplatform-eu-nl-dqs-a #use specific claster, if you need check app logs for example
   bkcloud context #display current context
   #use context if you need to deploy into dqs, prod, dev
   bkcloud use context data-quality/dataquality-api/kubernetes-dqs, is you want to deploy into some context(dev,dqs, prod)
   kubectl get svc -o yaml | grep fqdn
```

#Deployment to DQS

1. Rebuild project

   ```
   mvn clean package
   ```
    

1. Switch to dqs context
    ```
    bkcloud use context data-quality/dataquality-api/kubernetes-dqs
    ```
    
1. Create a git tag:
    ```bash
    DATETIME=$(date +%s) # Don't inline this, it will be used later!
    git tag -a dataquality-$DATETIME -m "Rollout $DATETIME"
    ```
1. Update the .deploy file:
    ```bash
    cat << EOF > .deploy
    commit: $(git rev-parse HEAD)
    tag: dataquality-$DATETIME
    deployed-from: $(hostname)
    deployed-by: $USER
    deploy-date: $DATETIME
    EOF
    ```

1. Build dq docker image:
    ```bash
    docker build --pull .  -t docker-registry.booking.com/projects/data-quality/dataquality-api:$(git rev-parse HEAD)
    ``` 

1. Push the image to the Booking Docker registry
    ```bash
    docker push docker-registry.booking.com/projects/data-quality/dataquality-api:$(git rev-parse HEAD)
    ```
    
1. Create or update the Shipper application object (using the same tag):
    ```bash
    cat kubernetes/application-dqs.yaml | sed s/CI_COMMIT_SHA/$(git rev-parse HEAD)/g | kubectl apply -f -
    ```
    
1. See New Application Release
    ```
         kubectl get all
    ```
1. See description of your release
   ```
    kubectl describe release.shipper.booking.com/dataquality-api-4f49b642-0 
    
    Name:         dataquality-api-4f49b642-0
    Namespace:    data-quality
    Labels:       service-directory.installation=kubernetes-dqs
                  service-directory.persona=b-data-quality-dataquality-api
                  service-directory.project=data-quality
                  service-directory.rollout=ebb9d4a7-a61c-11e8-be17-246e968fc2a8
                  service-directory.service=dataquality-api
                  shipper-app=dataquality-api
                  shipper-release=dataquality-api-4f49b642-0
                  shipper-release-hash=4f49b642
                  traffic=prod
    Annotations:  shipper.booking.com/release.clusters: bplatform-eu-nl-dqs-a
                  shipper.booking.com/release.generation: 57
                  shipper.booking.com/release.template.iteration: 0
    API Version:  shipper.booking.com/v1
    Kind:         Release
    Metadata:
      Cluster Name:        
      Creation Timestamp:  2018-10-31T15:30:21Z
      Environment:
        Chart:
          Name:      java
          Repo URL:  https://artifactory.booking.com/charts
          Version:   0.0.33
        Cluster Requirements:
          Capabilities:
            dlb
          Regions:
            Name:      eu-nl
            Replicas:  1
        Sidecars:      <nil>
    ....
   ```
1. Test Application Release, or at least check that your api is listed
   ```
   https://dataquality-staging.dqs.booking.com/swagger-ui.html
   ```
   [Check Integration tests](https://data.booking.com/watt/tests/dq-api-test.html?env=dqs_staging)
   
1. If everything looks good, give live traffic 
   ```
   kubectl patch release  dataquality-api-4f49b642-0 -p '{"spec":{"targetStep":1}}' --type="merge"
   ```
1. Finally - release application
    ```
    kubectl patch release dataquality-api-4f49b642-0 -p '{"spec":{"targetStep":2}}' --type="merge"
    ```