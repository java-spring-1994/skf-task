dataqualityapi
===

Data Qualit Services - set of services related to data.booking.com

Getting Started
---

1. [DQS deployment Link](https://dataquality.dqs.booking.com/swagger-ui.html)
2. [PROD deployment Link](https://dataquality.prod.booking.com/swagger-ui.html)

