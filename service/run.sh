#!/bin/sh

# Execute prelaunch commands if the script is present
[ -f /usr/bin/prelaunch.sh ] && prelaunch.sh

# ====== Run the application =====
# Bumping max memory limit because default value is 25% of available RAM
# 80% doesn't work with the container limit of 2Gb
JAVA_OPTS="$JAVA_OPTS -XX:MaxRAMPercentage=50.0"
# To enable heap dump, should appear in working directory of the VM
JAVA_OPTS="$JAVA_OPTS -XX:+HeapDumpOnOutOfMemoryError"
# To log VM settings
JAVA_OPTS="$JAVA_OPTS -XshowSettings:vm"
# Run
exec java ${JAVA_OPTS} -cp target/app.jar:target/lib/* \
      com.booking.dataqualityapi.MainApplication \
    --spring.config.location=file:config/config.yaml,classpath:application.properties