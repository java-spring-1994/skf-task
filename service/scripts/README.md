#Data Quality Anomaly detection
 
 
Most of anomaly detection systems focuses on just anomaly detection.

In DQ we choose do not got with complex anomaly detection system,
but use good quality metrics and statistical anomaly detection algorithm
with small number of false positive findings. 

DQ anomaly detection is based on Classical Gausian Anomaly Detection.

#Anomaly detection pipline

**Metric Collection** -> **Save Metrics REST CAll(MetricsController.collectMetrics)** -> **Start Livy Job for 
Calculate Anomalies** -> **Save Anomalies using REST API**

##Deployment

Anomaly detection script need to be deployed on LHR4

```
 hadoop fs -rm /tmp/dq-anomaly-detection/anomaly_detection.py
 hadoop fs -put anomaly_detection.py /tmp/dq-anomaly-detection/anomaly_detection.py
 hdfs dfs -chmod -R 777  /tmp/dq-anomaly-detection/
```
##Run From HADOOP KVM

```
/opt/spark/current23/bin/spark-submit --packages mysql:mysql-connector-java:8.0.16 anomaly_detection.py --dc AMS4 --schema default --table et_uvi_first_seen_abnormal
```

##Links

1. [Elastic Search Anomaly Detection](https://www.elastic.co/products/stack/machine-learning)
We got good results using elastic search anomaly detection for metrics data.
ML Kibana price isn't too expensive per year, so it can be considered as an option for 
metrics processing.


| Anomaly  Algorithm     | Detected Anomalies | 
| ------------- |:-------------:| 
| Kibana,  major anomalies score > 50      | 1925 |
| DQ Detected Anomalies, score >20     | 2488     |
| Classical Gausina distribution| amount of FP foundings where to big, close to 19k |
| Intersection beetween them | 1555(81%) from kibana anomalies      |
|Number of timeserias metrics| 22695 |

2. [Anomaly Detection](https://www.ritchieng.com/machine-learning-anomaly-detection/)
   Density estimation, anomaly detection system, and multivariate gaussian distribution.
    
3. [Example of anomaly detection](https://towardsdatascience.com/wondering-how-to-build-an-anomaly-detection-model-87d28e50309)
   Basic Anomaly Detection Algorithm.
    
4. [Prophet (Facebook)](https://facebook.github.io/prophet/)
 Prophet is a procedure for forecasting time series data based on an additive model where non-linear
 trends are fit with yearly, weekly, and daily seasonality, plus holiday effects. 
 It works best with time series that have strong seasonal effects and several seasons of historical data. 
 Prophet is robust to missing data and shifts in the trend, and typically handles outliers well.
 
5. [ThirdEye](https://github.com/apache/incubator-pinot/tree/master/thirdeye)
 ThirdEye supports collaboration but focuses on the data-integration aspect of anomaly detection
 and root-cause analysis.


#External Contacts

Karthik Nagesh - author of anomaly detection algorithm.