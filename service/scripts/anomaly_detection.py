print 'Running Data Quality - Anomaly Detection Script'

import pyspark.sql.types as Types
from pyspark.sql.functions import min, max, lit,udf,log2,log,pow,col,exp,isnan,stddev,sqrt,row_number,mean,when,isnull,to_date,concat
from pyspark.sql.window import Window
from pyspark.sql import SparkSession
import numpy as np
import pandas as pd
import json
import argparse
import requests
import sys
import re

print 'Parsing arguments'

# Parse arguments and assign to variables
parser = argparse.ArgumentParser()

parser.add_argument('--dryrun', '--d', action = 'store_true', help = 'if specified, anomalies will not be written to MySQL')
parser.add_argument('--test', '--t', action = 'store_true', help = 'if specified, will point to dqs. Default points to production')
parser.add_argument('--all-tables', '--a', action = 'store_true', help = 'if specified, anomalies will be calculated for all tables')
parser.add_argument('--dc', type = str, help = 'datacenter')
parser.add_argument('--schema', type = str, help = 'schema name')
parser.add_argument('--table', type = str, help = 'table name')
parser.add_argument('--window', '-w', type = int, default = 3, help = 'lookback window in months for calculating anomalies')
parser.add_argument('--anomaly-score-boundary', '-b', type = int, default = 5, help = 'rows with anomaly score greater than this number will be marked anomolous')


args = parser.parse_args()
schema = args.schema
table = args.table 
dc = args.dc
window = args.window
all_tables = args.all_tables
anomaly_score_boundary = args.anomaly_score_boundary
test = args.test
dryrun = args.dryrun

print('{0}'.format(args))

dq_api_base_url = 'https://dataquality.prod.booking.com/anomalies'
if test:
  print 'Got --test argument. Poinitng to prod staging API'
  dq_api_base_url = 'https://dataquality-staging.prod.booking.com/anomalies'

dq_api_save_url = ''
metrics_query_filter = ''
dq_api_delete_url = ''
if all_tables:
  print 'Got --all-tables argument. Calculating anomalies for all tables'
  metrics_query_filter = "date >= (CURRENT_DATE - INTERVAL {0} MONTH)".format(window)
  dq_api_delete_url = '{0}/all'.format(dq_api_base_url)
  dq_api_save_url = '{0}/save/all'.format(dq_api_base_url)
else:
  metrics_query_filter = "dc = '{0}' AND schema_name = '{1}' AND table_name = '{2}' AND date >= (CURRENT_DATE - INTERVAL {3} MONTH)".format(dc, schema, table, window)
  dq_api_delete_url = '{0}/{1}/{2}/{3}'.format(dq_api_base_url, dc, schema, table)
  dq_api_save_url = '{0}/save'.format(dq_api_base_url)

# Load db.json to parse connection details for dataquality mysql db
with open('/var/run/secrets/booking.com/db.json') as f:
  data = json.load(f)

# get pool roster name for slave db pool
dq_ro_roster = data["database"]["dataquality"]["ro"]["roster"]

# get the first host from the roster file for slave db pool
with open('/etc/bookings/pool_roster/{0}'.format(dq_ro_roster)) as f:
  dq_ro_host = f.readline().split('\t')[1]

#dq_ro_host = "inttooldb-8002.fab4.prod.booking.com"

# Extract username and password for read only connection
dq_ro_username = data["database"]["dataquality"]["ro"]["username"]
dq_ro_password = data["database"]["dataquality"]["ro"]["password"]

# Build URL and properties for read only connection
dq_ro_url = 'jdbc:mysql://{0}/dataquality?&serverTimezone=Europe/London'.format(dq_ro_host)
dq_ro_properties = { "user" : dq_ro_username, "password" : dq_ro_password, "driver" : "com.mysql.cj.jdbc.Driver" }
 
# Extract master db host
dq_rw_host = data["database"]["dataquality"]["rw"]["host"]

# Extract username and password for read write connection
dq_rw_username = data["database"]["dataquality"]["rw"]["username"]
dq_rw_password = data["database"]["dataquality"]["rw"]["password"] 

# Build URL and properties for read write connection
dq_rw_url = 'jdbc:mysql://{0}/dataquality?&serverTimezone=Europe/London&useServerPrepStmts=false&rewriteBatchedStatements=true'.format(dq_rw_host)
dq_rw_properties = { "user" : dq_rw_username, "password" : dq_rw_password, "driver" : "com.mysql.cj.jdbc.Driver" }

# Build spark session
hc = (SparkSession
         .builder
         .appName("data-quality-anomaly-detection-{0}-{1}-{2}".format(dc, schema, table))
         .config("spark.task.maxFailures",u"20")
         .config("spark.network.timeout",u"800")
         .config("spark.sql.broadcastTimeout",u"600")
         # right now we don't have any interaction with hive, so commenting
         #.enableHiveSupport()
         .getOrCreate()
    )

# Read metrics information from mysql table into dataframe
wind=Window.partitionBy("tbl_name_col_metric")
all_metrics_df = hc.read.jdbc(url = dq_ro_url, table = "dq_metrics", properties = dq_ro_properties)\
            .filter(metrics_query_filter)\
            .drop('id', 'mysql_row_updated_at')\
            .withColumn("tbl_name_col_metric", 
                concat(col("dc"), lit('/'), col('table_name'), lit('.'), col('column_name'), lit(':'), col("metric"))\
            )\
            .withColumn("original_value", col("value"))\
            .withColumn("value", (col("value")-min(col("value")).over(wind))/(max(col("value")).over(wind)-min(col("value")).over(wind)))

# A smoother to make sure stddev is not zero, as it could affect the formula
laplacianSmoother = 0.000000001

# Sliding window of 2 weeks
lookBackWindow = -14

def firstPart(stddev):
    firstPart = 1.0 / np.sqrt(2 * np.pi * (col(stddev) + laplacianSmoother) ** 2)
    return firstPart


gaussian_first = udf(firstPart, Types.FloatType())


def secondPart(mean, stddev, value):
    first = firstPart(stddev)
#     second=np.exp(-0.5*((value-mean)*1.0/(stddev+laplacianSmoother))**2)
#     return first*second
    return first


gaussian_second = udf(secondPart, Types.FloatType())

#all_metrics_df.show(20, False)

wind = Window.partitionBy("tbl_name_col_metric").orderBy(all_metrics_df["date"].asc()).rowsBetween(lookBackWindow, -1)
wind_rowNumber = Window.partitionBy("tbl_name_col_metric").orderBy(all_metrics_df["date"].asc())

anomalies_df = all_metrics_df\
                .withColumn("recordNumber",row_number().over(wind_rowNumber))\
                .withColumn("running_avg",mean(col("value")).over(wind))\
                .withColumn("runningstdDev",stddev(col("value")).over(wind))\
                .withColumn("firstPart", \
                      when(\
                        ((isnan(col("runningstdDev"))) | isnull(col("runningstdDev"))) | (col("recordNumber") < (-1 * lookBackWindow)), -1\
                      )\
                      .otherwise(\
                        lit(1)/sqrt(lit(2)*lit(np.pi)*pow((col("runningstdDev")+lit(laplacianSmoother)),2))\
                      )\
                )\
                .withColumn("expPart",\
                      when(\
                        (isnan(col("runningstdDev"))) | (isnull(col("runningstdDev"))) | (isnan(col("running_avg"))) | (isnull(col("running_avg"))) | (col("recordNumber") < (-1 * lookBackWindow)), -1\
                      )\
                      .otherwise(\
                        pow(((col("value") - col("running_avg")) * 1.0) ,2) / (lit(2)*pow((col("runningstdDev")+lit(laplacianSmoother)),2))\
                      )\
                )\
                .withColumn("ProbValue",\
                      when(\
                        (col("firstPart") ==-1) | (col("expPart") == -1) | (col("value") == col("running_avg")), 1\
                      )\
                      .otherwise(\
                        col("firstPart")*exp(lit(-1)*col("expPart"))\
                      )\
                )\
    .withColumn("anomaly_score", -log(col("ProbValue") + laplacianSmoother))\
    .filter("anomaly_score > {0}".format(anomaly_score_boundary))\
    .drop('expPart', 'firstPart', 'runningstdDev', 'tbl_name_col_metric', 'ProbValue', 'recordNumber')\
    .drop('value')\
    .withColumnRenamed('original_value','value')


if not dryrun:
  pandasDF = anomalies_df\
                      .withColumnRenamed('dc', 'cluster')\
                      .withColumnRenamed('schema_name', 'schemaName')\
                      .withColumnRenamed('table_name', 'tableName')\
                      .withColumnRenamed('column_name', 'column')\
                      .withColumnRenamed('anomaly_score', 'anomalyScore')\
                      .withColumnRenamed('running_avg', 'runningAvg')\
                      .toPandas()

  numAnomalies = len(pandasDF.index)
  json_payload = pandasDF.to_json(orient='records')
  print 'Postng {0} anomalies to DQ API {1}'.format(numAnomalies, dq_api_save_url)                    
  response = requests.post(url = dq_api_save_url, data = json_payload, headers={'Content-type': 'application/json'})
  if response.status_code == 200:
    print 'Successfully updated {0} anomalies in MySQL. Got respose'.format(numAnomalies, response.text)
  else:
    print 'Trying non-transactional save. Could not save anomalies using DQ API because {0}.'.format(response.text)
    if not test:
      response = requests.delete(url = dq_api_delete_url)
      if response.status_code == 200:
        print 'Successfully deleted existing anomalies. Writing new anomalies back to MySQL'
        try:
          anomalies_df.coalesce(1).write.jdbc(url = dq_rw_url, mode = 'Append', table = "dq_metrics_anomalies", properties = dq_rw_properties)
          print 'Successfully wrote {0} anomolous rows to MySQL'.format(anomalies_df.count())
        except Exception as e:
          print 'Could not store anomalies to MySQL because {0}'.format(str(e))
          sys.exit(1)
      else:
        print 'Could not delete existing anomalies from mysql becase {0}'.format(response.text)
        sys.exit(1)
    else:
      print 'Cannot save non-transactionally in test mode'
else:
  print 'This was a dry run. Found {0} anomolous rows, but not writing to MySQL'.format(anomalies_df.count())

