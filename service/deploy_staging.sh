#!/bin/sh

PROJECT=data-quality
SERVICE=dataquality-api

RELEASE_ENV=$1 # prod or dqs

docker login docker.artifactory.booking.com

mvn clean package \
&& make dot-deploy \
&& docker build --pull . -t docker.artifactory.booking.com/projects/${PROJECT}/${SERVICE}:$(git rev-parse HEAD) \
&& docker push docker.artifactory.booking.com/projects/${PROJECT}/${SERVICE}:$(git rev-parse HEAD) \
&& cat kubernetes/application-${RELEASE_ENV}.yaml | sed s/CI_COMMIT_SHA/$(git rev-parse HEAD)/g | kubectl apply -f -