## Introduction

This README file provides a short introduction to the authentication functionality in this Java service. It is intended
to complement the implementation and comments in the `com.booking.dataqualityapi.auth.*` classes.

## Background Reading

We use Spring Security in order to provide the authentication plumbing for the application. Here are some useful links
about the security features in Spring:

* https://spring.io/guides/gs/securing-web/
* https://auth0.com/blog/implementing-jwt-authentication-on-spring-boot/
  * This has a reasonable working example (althought it's for JWT tokens the flow and classes remain about the same)
* https://octoperf.com/blog/2018/03/08/securing-rest-api-spring-security/
  * Another example for reference

You probably don't need to read/digest them all, but have them handy in case you need something to refer to.

In terms of Booking-specific information: 

* We use the employee SSO flow described 
  [here](https://gitlab.booking.com/security/iam/wikis/Employee-authentication-via-SAML-through-Accounts-portal) to 
  authenticate certain calls where we expect a human user.
* For general S2S (service-to-service), potentially being done on behalf of a human user, see 
  [here](https://gitlab.booking.com/security/iam/wikis/integration-with-iam). 

## Assumptions
### Employee SSO and S2S
We assume that a variety of use cases will exist in the future:

* Service operations that we expect solely to be called by a human (e.g. an AJAX call from a website that someone is
  browsing). 
* Service operations that are called from another service, potentially taking an action on-behalf-of another user
* Service operations that should support both human and robot/service traffic

As such, we want to build an authentication mechanism that is flexible enough to support all these cases. 

### Employee SSO Flow
We use the employee SSO flow previously linked to with **one** exception and that is on auth failure we fail (HTTP 401)
instead of redirecting to the federation endpoint since for our REST API the redirection for auth purposes may not
make sense (e.g. a client may not know how to follow it, the semantics of the request may change if the client
decides to change POSTs to GETs). Instead, we assume that the client has already successfully obtained a token
from elsewhere.

**Note:** Redirection does make sense in the API for resource-based methods. For example if we somehow changed the
identifier for notification 1 to 100 then when someone issues a GET for ID 1 we could send a redirect for ID 100. I
just don't want to use it in an auth context.

### Not all operations require authentication
Currently the service views read requests as unauthenticated (e.g. for reading data) but authenticated for
data modification (as this modifies the records for a given user). At the time of writing there is only
one operation that requires authentication - updating the settings for a user.

This may change in the future.

## Implementation Overview
We can divide our implementation into two pieces:

* Generic authentication code
* Spring plumbing

(**Note:** There is a reasonably tight coupling between the two - see the _Decisions Made_ section for more
information).

### Generic authentication code
* `com.booking.dataqualityapi.auth.checker.*` contains the functionality to call on AuthX to authenticate using either Employee SSO
  or S2S auth
  * There is one interface that checkers are expected to implement - `AuthXBasedChecker`
* `com.booking.dataqualityapi.auth.token.*` contains classes to represent the result of a successful authentication using either
  Employee SSO or S2S auth and allows callers to pluck out information about the authenticated "thing" (e.g. the service name, a
  user login). 
  * Objects of these classes are returned from the relevant `AuthXBasedChecker`
  * There is one interface that all token classes are expected to implement: `AuthXBasedAuthenticationInfo`
* `AuthUtils` contains utility functions for dealing with `Authentication` objects, such as making sure they're the right type and 
  pulling the required attributes out of them.

### Spring Plumbing
* `DataQualityApiSecurityConfigurerAdapter` - this is our hook into Spring Security. Currently it lets all requests through except
  for those what are explicitly configured to be authenticated. This class has some static maps (divided into the "type" of auth
  we want to enforce - employee SSO, S2S or both) that can be updated when we want to add new routes to be authenticated. Each map
  accepts an Ant pattern (see [here](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/util/AntPathMatcher.html)
  for more information) and what HTTP methods should be protected
* `AuthXCheckerWrappingFilter` lets us hook our `AuthXBasedChecker` implementations into Spring

### Call Flow

Conceptually, for a controller method that needs authentication and/or authorisation:

1. `DataQualityApiSecurityConfigurerAdapter` configures the application at start-up with whatever security config it
   knows about.
1. A request is received and if it needs authentication (based on its path and HTTP method) it will hit the relevant filter 
1. If the filter authenticates successfully it will pass the request on, otherwise it will throw an `AuthenticationException` that will
   be surfaced back to the caller as a HTTP 401
1. Controller methods have access to the `Authentication` object (actually one of our `AuthXBasedAuthenticationInfo` implementations) by either
   calling `SecurityContextHolder.getContext().getAuthentication()` or by accepting an `Authentication` object in the method signature. Accepting
   an object in the signature is recommended for easier testability
1. The controller method can then do whatever authorization checks it has to. If the style of check will be repeated in multiple methods, it is
   recommended to include it as a static method in the `AuthUtils` class.

## Decisions Made

### Coupling with Spring
Probably the most important/visible decision made is that our authentication classes extend/implement Spring interfaces
(where it makes sense to do so - see `com.booking.dataqualityapi.auth.token.*` for an example of this)
and throw Spring exceptions (such as `AuthenticationException` and its subclasses). This means that our implementation
is tightly coupled to Spring. Since the application is Spring Boot-based this is probably OK.
