package com.booking.dataqualityapi.controllers;

import com.booking.authxclient.exceptions.AuthXClientException;
import com.booking.dataqualityapi.auth.AuthUtils;
import com.booking.dataqualityapi.config.SwaggerConfig;
import com.booking.dataqualityapi.dto.AuthInfo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.MediaType;

@RestController("Controller to test authentication")
public class AuthController {
    @ApiOperation(value = "Authentication method, returns user name and staff id's"
            + " for single signed booking autorized users",
            nickname = "auth",
            authorizations = {@Authorization(value = SwaggerConfig.BOOKING_AUTH)})
    @GetMapping(value = "/auth", produces = MediaType.APPLICATION_JSON)
    public AuthInfo auth(@ApiParam(hidden = true) Authentication authentication) throws AuthXClientException {
        final String staffLoginName = AuthUtils.getLoginName(authentication).orElseThrow(() ->
                new AuthXClientException("Login name couldn't be extracted from cookies"));
        final Long staffId = AuthUtils.getStaffId(staffLoginName);
        return new AuthInfo(staffId, staffLoginName);
    }
}
