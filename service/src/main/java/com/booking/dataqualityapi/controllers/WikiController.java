package com.booking.dataqualityapi.controllers;

import com.booking.authxclient.exceptions.AuthXClientException;
import com.booking.dataqualityapi.auth.AuthUtils;
import com.booking.dataqualityapi.config.SwaggerConfig;
import com.booking.dataqualityapi.dto.WikiTextPayload;
import com.booking.dataqualityapi.dto.dataentity.DataEntity;
import com.booking.dataqualityapi.model.WikiEntity;
import com.booking.dataqualityapi.model.WikiWorkflow;
import com.booking.dataqualityapi.services.WikiService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * Wiki operations controller.
 */
@RestController
public class WikiController {

    private final WikiService wikiService;

    @Autowired
    public WikiController(WikiService wikiService) {
        this.wikiService = wikiService;
    }

    @ApiOperation(value = "Get latest wiki content for schema.entity", nickname = "getWikiEntityContent",
            response = WikiEntity.class)
    @GetMapping(value = "/wiki/{type}/{namespace}/{entity}", produces = MediaType.APPLICATION_JSON)
    public WikiEntity getWikiEntityContent(
            @ApiParam(value = "data-entity type", allowableValues = "hive-table, mysql-table, kafka-topic",
                    example = "hive-table", defaultValue = "hive-table", required = true)
            @PathVariable(required = true) String type,
            @ApiParam(value = "data entity namespace, in hive case it is database",
                    example = "default", required = true)
            @PathVariable(required = true) String namespace,
            @ApiParam(value = "Entity name, in hive case it is table",
                    example = "reservation_flatter", required = true)
            @PathVariable(required = true) String entity) {
        return wikiService.getWikiEntityContent(type, namespace, entity);
    }

    @ApiOperation(value = "Get all wiki contents for schema.entity", nickname = "getAllWikiEntityContents",
            response = java.util.List.class)
    @GetMapping(value = "/wikiAll/{type}/{namespace}/{entity}", produces = MediaType.APPLICATION_JSON)
    public List<WikiEntity> getAllWikiEntityContent(
            @ApiParam(value = "data-entity type", allowableValues = "hive-table, mysql-table, kafka-topic",
                    example = "hive-table", defaultValue = "hive-table", required = true)
            @PathVariable(required = true) String type,
            @ApiParam(value = "data entity namespace, in hive case it is database",
                    example = "default", required = true)
            @PathVariable(required = true) String namespace,
            @ApiParam(value = "Entity name, in hive case it is table",
                    example = "reservation_flatter", required = true)
            @PathVariable(required = true) String entity) {
        return wikiService.getAllWikiEntityContent(type, namespace, entity);
    }


    @ApiOperation(value = "POST the latest wiki content for schema entity", nickname = "setWikiEntityContent",
            response = WikiEntity.class, authorizations = {@Authorization(value = SwaggerConfig.BOOKING_AUTH)})
    @PostMapping(value = "/wiki/{type}/{namespace}/{entity}", produces = MediaType.APPLICATION_JSON)
    public WikiEntity postWikiEntityContent(@ApiParam(value = "data-entity type", allowableValues = "hive-table, mysql-table, kafka-topic",
                                            example = "hive", defaultValue = "hive", required = true)
                                            @PathVariable(required = true) String type,
                                            @ApiParam(value = "data entity namespace, in hive case it is database",
                                                    example = "default", required = true)
                                            @PathVariable(required = true) String namespace,
                                            @ApiParam(value = "Entity name, in hive case it is table",
                                                    example = "reservation_flatter", required = true)
                                            @PathVariable(required = true) String entity,
                                            @RequestBody WikiTextPayload text,
                                            @ApiParam(hidden = true) Authentication authentication) throws IOException, AuthXClientException {

        final String staffLoginName = AuthUtils.getLoginName(authentication)
                .orElseThrow(() -> new AuthenticationCredentialsNotFoundException("No staff login name found in update wiki request "));
        final Long staffId = AuthUtils.getStaffId(staffLoginName);
        return wikiService.createOrUpdateWikiEntity(type, namespace, entity, staffLoginName, Math.toIntExact(staffId), text);
    }

    @ApiOperation(value = "Get latest wiki content for workflow entity", nickname = "getWikiWorkflowContent",
            response = WikiWorkflow.class)
    @GetMapping(value = "/wiki/workflow/{workflowName}", produces = MediaType.APPLICATION_JSON)
    public WikiWorkflow getWikiWorkflowContent(
            @ApiParam(value = "Workflow name",
                    example = "hdbi-table-property-splits", required = true)
            @PathVariable(required = true) String workflowName) {
        return wikiService.getWikiWorkflowContent(workflowName);
    }

    @ApiOperation(value = "Get all wiki content for workflow entity", nickname = "getAllWikiWorkflowContent",
            response = java.util.List.class)
    @GetMapping(value = "/wikiAll/workflow/{workflowName}", produces = MediaType.APPLICATION_JSON)
    public List<WikiWorkflow> getAllWikiWorkflowContent(
            @ApiParam(value = "Workflow name",
                    example = "hdbi-table-property-splits", required = true)
            @PathVariable(required = true) String workflowName) {
        return wikiService.getAllWikiWorkflowContent(workflowName);
    }


    @ApiOperation(value = "POST the latest wiki content for workflow", nickname = "setWikiWorkflowContent",
            response = WikiEntity.class, authorizations = {@Authorization(value = SwaggerConfig.BOOKING_AUTH)})
    @PostMapping(value = "/wiki/workflow/{workflowName}", produces = MediaType.APPLICATION_JSON)
    public WikiWorkflow postWikiWorkflowContent(
            @ApiParam(value = "Workflow name",
            example = "hdbi-table-property-splits", required = true)
            @PathVariable(required = true) String workflowName,
            @RequestBody WikiTextPayload text,
            @ApiParam(hidden = true) Authentication authentication) throws IOException, AuthXClientException {

        final String staffLoginName = AuthUtils.getLoginName(authentication)
                .orElseThrow(() -> new AuthenticationCredentialsNotFoundException("No staff login name found in update wiki request "));
        final Long staffId = AuthUtils.getStaffId(staffLoginName);
        return wikiService.createOrUpdateWikiWorkFlow(workflowName, staffLoginName, Math.toIntExact(staffId), text);
    }

    @ApiOperation(value = "Get a list of tables which has wikis", nickname = "getTablesWhichHasWikis",
            response = WikiEntity.class, authorizations = {@Authorization(value = SwaggerConfig.BOOKING_AUTH)})
    @GetMapping(value = "/wiki/entities", produces = MediaType.APPLICATION_JSON)
    public Set<DataEntity> getWikiEntities() {
        return wikiService.getAllDataEntitiesWithWiki();
    }

}
