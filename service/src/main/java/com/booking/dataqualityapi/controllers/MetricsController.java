package com.booking.dataqualityapi.controllers;

import com.booking.authxclient.exceptions.AuthXClientException;
import com.booking.dataquality.dto.monitoring.MonitoringConfig;
import com.booking.dataqualityapi.auth.AuthUtils;
import com.booking.dataqualityapi.config.SwaggerConfig;
import com.booking.dataqualityapi.dto.DataCenter;
import com.booking.dataqualityapi.dto.monitoring.ColumnMetricInfo;
import com.booking.dataqualityapi.dto.monitoring.HasMetricsInfo;
import com.booking.dataqualityapi.dto.monitoring.MetricPayload;
import com.booking.dataqualityapi.dto.monitoring.TableColumnMetricConfig;
import com.booking.dataqualityapi.model.monitoring.DefaultMetricsConfig;
import com.booking.dataqualityapi.model.monitoring.MonitoringRequest;
import com.booking.dataqualityapi.model.monitoring.TableColumnMetricsConfig;
import com.booking.dataqualityapi.services.monitoring.MetricsService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;


@RestController
public class MetricsController {

    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory.getLogger(MetricsController.class);

    @Autowired
    private MetricsService metricsService;

    @ApiOperation(value = "Get Metrics from mysql database V3",
            nickname = "getMetrics")
    @GetMapping(value = "/metrics_v3", produces = MediaType.APPLICATION_JSON_VALUE)
    public MetricPayload[] metricsGetV3(
            @RequestParam("dc") final DataCenter dc,
            @ApiParam(value = "data entity namespace, in hive case it is database",
                    example = "feature_store", required = true)
            @RequestParam("namespace") final String namespace,
            @ApiParam(value = "data entity name ",
                    example = "web_search_log", required = true)
            @RequestParam("name") final String name,
            @ApiParam(value = "data entity name ",
                    example = "total_hotels", required = true)
            @RequestParam("column") final String column,
            @ApiParam(value = "data entity metric ",
                    example = "avg", required = true)
            @RequestParam("metric") final String metric,
            @ApiParam(value = "date from return metrics, format YYYY-MM-DD",
                    example = "2019-04-05", required = true)
            @DateTimeFormat(pattern = "yyyy-MM-dd")
            @RequestParam("from") final Date from,
            @ApiParam(value = "date until return metrics, format YYYY-MM-DD",
                    example = "2019-06-05", required = true)
            @DateTimeFormat(pattern = "yyyy-MM-dd")
            @RequestParam("to") final Date to) throws DateTimeParseException {
        return metricsService.getMetrics(dc, namespace, name, column, metric, from, to);
    }

    @ApiOperation(value = "Checks that there is at least one metric between dates",
            nickname = "hasMetrics")
    @GetMapping(value = "/hasMetrics", produces = MediaType.APPLICATION_JSON_VALUE)
    public HasMetricsInfo hasMetrics(
            @ApiParam(value = "data entity namespace ",
                    example = "feature_store", required = true)
            @RequestParam("namespace") final String namespace,
            @ApiParam(value = "data entity name ",
                    example = "web_search_log", required = true)
            @RequestParam("name") final String name,
            @ApiParam(value = "date from return metrics, format YYYY-MM-DD",
                    example = "2019-06-05", required = true)
            @DateTimeFormat(pattern = "yyyy-MM-dd")
            @RequestParam("from") final Date from,
            @ApiParam(value = "date until return metrics, format YYYY-MM-DD",
                    example = "2019-09-05", required = true)
            @DateTimeFormat(pattern = "yyyy-MM-dd")
            @RequestParam("to") final Date to) throws DateTimeParseException {
        return new HasMetricsInfo(metricsService.getAnyMetricId(namespace,name,from,to).isPresent());
    }

    @ApiOperation("Request Metrics Collection (Monitoring)")
    @PostMapping(value = "/metrics/{schema}/{table}/request_monitoring", produces = MediaType.APPLICATION_JSON_VALUE)
    public MonitoringRequest addMonitoringRequest(
            @PathVariable("schema") String schema,
            @PathVariable("table") String table
    ) {
        return metricsService.addMonitoringRequest(schema, table);
    }

    @ApiOperation("Check Monitoring Request")
    @GetMapping(value = "/metrics/{schema}/{table}/request_monitoring", produces = MediaType.APPLICATION_JSON_VALUE)
    public MonitoringRequest getMonitoringRequest(
            @ApiParam(example = "default",required = true)
            @PathVariable("schema") String schema,
            @ApiParam(example = "reservation_flatter",required = true)
            @PathVariable("table") String table
    ) {
        return metricsService.getMonitoringRequest(schema, table);
    }

    @ApiOperation(value = "Reject Monitoring Request",
            response = MonitoringRequest.class)
    @PostMapping(value = "/metrics/{schema}/{table}/reject_request_monitoring",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public MonitoringRequest rejectMonitoringRequest(
            @ApiParam(example = "default", required = true)
            @PathVariable("schema") String schema,
            @ApiParam(example = "reservation_flatter", required = true)
            @PathVariable("table") String table,
            @ApiParam(hidden = true) Authentication authentication
    ) throws AuthXClientException {
        final String staffLoginName = AuthUtils.getLoginName(authentication)
                .orElseThrow(() -> new AuthenticationCredentialsNotFoundException("No staff login name found in update wiki request "));
        final Long staffId = AuthUtils.getStaffId(staffLoginName);
        if (!AuthUtils.isDMTeam(staffId)) {
            throw new AuthXClientException("Operation isn't allowed for user " + staffLoginName);
        }
        return metricsService.rejectMonitoringRequest(schema, table, staffLoginName);
    }


    @ApiOperation(response = MonitoringRequest.class, value = "Approve Monitoring Request. Please check table structure before approved. "
            + "Partition table should have time config configurated.")
    @PostMapping(value = "/metrics/{schema}/{table}/approve_request_monitoring", produces = MediaType.APPLICATION_JSON_VALUE)
    public MonitoringRequest approveMonitoringRequest(
            @ApiParam(example = "default", required = true)
            @PathVariable("schema") String schema,
            @ApiParam(example = "reservation_flatter", required = true)
            @PathVariable("table") String table,
            @ApiParam(hidden = true) Authentication authentication
    ) throws AuthXClientException {
        final String staffLoginName = AuthUtils.getLoginName(authentication)
                .orElseThrow(() -> new AuthenticationCredentialsNotFoundException("No staff login name found in update wiki request "));
        final Long staffId = AuthUtils.getStaffId(staffLoginName);
        if (!AuthUtils.isDMTeam(staffId)) {
            throw new AuthXClientException("Operation isn't allowed for user " + staffLoginName);
        }
        return metricsService.approveMonitoringRequest(schema, table, staffLoginName);
    }

    @ApiOperation("save metrics")
    @PostMapping(value = "/metrics", produces = MediaType.APPLICATION_JSON_VALUE)
    public int collectMetrics(@RequestBody final MetricPayload[] metrics) {
        return metricsService.saveMetrics(metrics);
    }

    @ApiOperation("Get Monitoring Config (All Approved Requests)")
    @GetMapping(value = "/metrics/monitoring_config", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MonitoringConfig> getMonitoringConfig() {
        return metricsService.getMonitoringConfig();
    }

    @ApiOperation("Get Default Metrics Config")
    @GetMapping(value = "/metrics/default_metrics_config", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DefaultMetricsConfig> getDefaultMetricsConfig() {
        return metricsService.getDefaultMetricsConfig();
    }


    @ApiOperation("Get Table Metrics Config")
    @GetMapping(value = "/metrics/{schema}/{table}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TableColumnMetricsConfig> getTableColumnMetricsConfig(@ApiParam(example = "default", required = true)
                                                              @PathVariable("schema") String schema,
                                                                  @ApiParam(example = "reservation_flatter", required = true)
                                                              @PathVariable("table") String table) {
        return metricsService.getTableColumnMetricsConfig(schema, table);
    }


    @ApiOperation(value = "Add metric for categorical column. "
            + " Categorical column is a string column which contains limited number of values. For example language column, count column."
            + " Categorical metric is calculated by next formula rows(col=val)*100/rows ",
            authorizations = {@Authorization(value = SwaggerConfig.BOOKING_AUTH)})
    @PostMapping(value = "/metrics/{schema}/{table}/{column}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TableColumnMetricsConfig> addCategoricalColumnMetricConfig(@ApiParam(example = "default", required = true)
                                                                  @PathVariable("schema") String schema,
                                                                  @ApiParam(example = "reservation_flatter", required = true)
                                                                  @PathVariable("table") String table,
                                                                  @ApiParam(example = "test", required = true)
                                                                  @PathVariable("column") String column,
                                                                  @ApiParam(hidden = true) Authentication authentication,
                                                                  @RequestBody final TableColumnMetricConfig cfg) throws AuthXClientException {
        final String staffLoginName = AuthUtils.getLoginName(authentication).orElseThrow(() ->
                new AuthXClientException("Login name couldn't be extracted from cookies"));
        final List<TableColumnMetricsConfig> result = new LinkedList<>();
        result.add(metricsService.addCategoricalCountMetric(schema, table, column, staffLoginName, cfg));
        result.add(metricsService.addCategoricalPercentageMetric(schema, table, column, staffLoginName, cfg));
        return result;
    }

    @ApiOperation("get column <-> metric pairs")
    @GetMapping(value = "/column-metrics/{namespace}/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ColumnMetricInfo[] getColumnMetrics(@ApiParam(value = "data entity namespace, in hive case it is database",
            example = "default", required = true)
                                               @PathVariable String namespace,
                                               @ApiParam(value = "Entity name, in hive case it is table",
                                                       example = "synthetic_dw_reservation", required = true)
                                               @PathVariable String name) {
        return metricsService.getColumnMetrics(namespace, name);
    }
}
