package com.booking.dataqualityapi.controllers;

import com.booking.dataquality.model.datamodel.MetastoreCheck;
import com.booking.dataqualityapi.dto.datamodel.MetastoreCheckPayload;
import com.booking.dataqualityapi.dto.datamodel.TimeSeriesConfigPayload;
import com.booking.dataqualityapi.model.datamodel.TimeSeriesConfig;
import com.booking.dataqualityapi.services.datamodel.DataModelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;


@Api("/data-models")
@RestController
@RequestMapping("/data-models")
public class DataModelsController {

    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory.getLogger(DataModelsController.class);

    @Autowired
    private DataModelService dataModelService;

    @ApiOperation("Get Active Metastore Checks")
    @GetMapping(value = "/{data_entity_id}/metastore-checks/v2", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MetastoreCheck> getActiveMetastoreChecks(@PathVariable("data_entity_id") int dataEntityId) {
        return dataModelService.getMetastoreChecks(dataEntityId);
    }

    @ApiOperation("Disable Metastore Checks")
    @DeleteMapping(value = "/{data_entity_id}/metastore-checks")
    public Boolean disableMetastoreChecks(@PathVariable("data_entity_id") int dataEntityId) {
        return dataModelService.disableMetastoreChecks(dataEntityId);
    }

    @ApiOperation("Add Or Update Metastore Check")
    @PostMapping(value = "/{data_entity_id}/metastore-checks/v2", produces = MediaType.APPLICATION_JSON_VALUE)
    public MetastoreCheck addOrUpdateMetastoreCheckV2(
        @PathVariable("data_entity_id") int dataEntityId,
        @RequestBody MetastoreCheckPayload metastoreCheckPayload) {
        metastoreCheckPayload.setEntityId(dataEntityId);
        return dataModelService.saveMetastoreCheck(metastoreCheckPayload);
    }

    @ApiOperation("Get time series config")
    @GetMapping(value = "/{data_entity_id}/time-series-config", produces = MediaType.APPLICATION_JSON_VALUE)
    public TimeSeriesConfig getTimeSeriesConfig(@PathVariable("data_entity_id") int dataEntityId) {
        return dataModelService.getTimeSeriesConfig(dataEntityId);
    }

    @ApiOperation(value = "Save time series config. Date format like in python, e.g. %Y-%m-%d")
    @PostMapping(value = "/{data_entity_id}/time-series-config", produces = MediaType.APPLICATION_JSON_VALUE)
    public TimeSeriesConfig saveTimeSeriesConfig(
        @PathVariable("data_entity_id") int dataEntityId,
        @RequestBody TimeSeriesConfigPayload configPayload
    ) {
        return dataModelService.saveTimeSeriesConfig(dataEntityId, configPayload);
    }

    @ApiOperation(value = "Delete time series config by entity id")
    @DeleteMapping(value = "/{data_entity_id}/time-series-config", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteTimeSeriesConfig(
            @PathVariable("data_entity_id") int dataEntityId
    ) throws NotFoundException {
        dataModelService.deleteTimeSeriesConfig(dataEntityId);
    }

    @ApiOperation(value = "Delete time series config by entity namespace and name")
    @DeleteMapping(value = "/{namespace}/{name}/time-series-config", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteTimeSeriesConfig(
            @ApiParam(value = "data entity namespace, in hive case it is database",
                    required = true)
            @RequestParam("namespace") final String namespace,
            @ApiParam(value = "data entity name ",
                    required = true)
            @RequestParam("name") final String name
    ) throws NotFoundException {
        dataModelService.deleteTimeSeriesConfig(namespace, name);
    }
}
