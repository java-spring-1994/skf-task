package com.booking.dataqualityapi.controllers.ownership;

import com.booking.authxclient.exceptions.AuthXClientException;
import com.booking.dataqualityapi.dto.dataentity.SchemaEntity;
import com.booking.dataqualityapi.dto.ownership.dq.OwnershipMeta;
import com.booking.dataqualityapi.dto.ownership.dq.SchemaEntityOwnershipInfo;
import com.booking.dataqualityapi.services.ownership.OwnershipService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.IOException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * An endpoint to provide aggregated ownership for schemas: personal and team.
 */
@RestController
@Api("/schema-ownership-v2")
@RequestMapping("/schema-ownership-v2")
public class SchemaOwnershipControllerV2 {
    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory.getLogger(SchemaOwnershipControllerV2.class);

    @Autowired
    private OwnershipService ownershipService;

    @ApiOperation("Get ownership info for schema (namespace)")
    @GetMapping(value = "/{entityType}/{entityNamespace}", produces = MediaType.APPLICATION_JSON_VALUE)
    public SchemaEntityOwnershipInfo getSchemaOwnership(
        @PathVariable String entityType,
        @PathVariable String entityNamespace
    ) throws AuthXClientException, InterruptedException, IOException {
        return ownershipService.getSchemaOwnershipInfo(entityType, entityNamespace);
    }

    @ApiOperation(value = "Assign ownership to schema")
    @PostMapping(value = "/{entityType}/{entityNamespace}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String assignSchemaOwnership(
        @PathVariable String entityType,
        @PathVariable String entityNamespace,
        @RequestBody OwnershipMeta ownershipMeta
    ) throws InterruptedException, IOException, AuthXClientException {
        return ownershipService.assignSchemaOwnership(entityType, entityNamespace, ownershipMeta);
    }

    @ApiOperation("Get owned schemas for orgunit")
    @GetMapping(value = "/orgunit/{orgUnitId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SchemaEntity> getOwnedTeamSchemas(
        @PathVariable long orgUnitId
    ) throws AuthXClientException, InterruptedException, IOException {
        return ownershipService.getOwnedTeamSchemaEntitiesV2(orgUnitId);
    }

    @ApiOperation("Get personally owned schemas for staff")
    @GetMapping(value = "/staff/{staffId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SchemaEntity> getOwnedPersonalSchemas(@PathVariable int staffId) {
        return ownershipService.getOwnedPersonalSchemaEntities(staffId);
    }
}
