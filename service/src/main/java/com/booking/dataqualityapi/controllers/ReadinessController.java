package com.booking.dataqualityapi.controllers;

import com.booking.dataqualityapi.dto.EntityType;
import com.booking.dataqualityapi.dto.OozieCoordinatorInfo;
import com.booking.dataqualityapi.dto.OozieWorkflowInstanceInfo;
import com.booking.dataqualityapi.dto.readiness.DataReadinessInfoDTO;
import com.booking.dataqualityapi.dto.readiness.ReadinessInfo;
import com.booking.dataqualityapi.dto.workflow.WorkflowInfo;
import com.booking.dataqualityapi.dto.workflow.WorkflowType;
import com.booking.dataqualityapi.model.DataSource;
import com.booking.dataqualityapi.model.readiness.DataReadinessInfo;
import com.booking.dataqualityapi.model.readiness.DataReadinessMetadata;
import com.booking.dataqualityapi.services.DataSourcesService;
import com.booking.dataqualityapi.services.ReadinessService;
import com.booking.dataqualityapi.services.WorkflowService;
import com.booking.dataqualityapi.utils.DatacenterUtils;
import com.booking.dataqualityapi.utils.EntityTypeUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api("/readiness")
@RestController
@RequestMapping("/readiness")
public class ReadinessController {

    private static final Logger logger = LoggerFactory.getLogger(ReadinessController.class);

    @Autowired
    private ReadinessService readinessService;

    @Autowired
    private WorkflowService workflowService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private DataSourcesService dataSourcesService;

    public ReadinessController() {
    }

    @ApiOperation("Get readiness infor for nominal date for date set")
    @GetMapping(value = "/v2/{entityType}/{namespace}/{name}/{datacenter}")
    public ResponseEntity<DataReadinessInfoDTO> getReadinessInfo(
            @ApiParam(value = "Entity Type", required = true, example = "HIVE", allowableValues = "HIVE,MYSQL")
            @PathVariable EntityType entityType,
            @ApiParam(value = "Schema name", required = true, example = "default")
            @PathVariable String namespace,
            @ApiParam(value = "Table name", required = true, example = "reservation_flatter")
            @PathVariable String name,
            @ApiParam(value = "Data center name", required = true, example = "AMS4", allowableValues = "AMS4, LHR4, GC")
            @PathVariable DatacenterUtils.Datacenter datacenter,
            @ApiParam(value = "Nominal date, date format is yyyy-MM-dd", example = "2019-11-11")
            @RequestParam(value = "date", required = false) String nominalDate,
            @ApiParam(value = "Nominal hour, date format is HH (24 hour format)", example = "00")
            @RequestParam(value = "hour", required = false) String nominalHour,
            @ApiParam(value = "Nominal minute, format is mm", example = "00")
            @RequestParam(value = "minute", required = false) String nominalMinute
    ) {
        DataReadinessInfo result;
        if (StringUtils.isNotEmpty(nominalDate)) {
            String currentMaterializedDate = "";
            currentMaterializedDate += nominalDate;
            if (StringUtils.isNotEmpty(nominalHour)) {
                currentMaterializedDate += " " + nominalHour;
            } else {
                //Just to have date time format
                currentMaterializedDate += " 00";
            }
            if (StringUtils.isNotEmpty(nominalMinute)) {
                currentMaterializedDate += ":" + nominalMinute;
            } else {
                currentMaterializedDate += ":00";
            }
            result = readinessService.getReadinessInfocurrentMaterializedDate(entityType.toString(), namespace, name,
                    datacenter.toString().toLowerCase(), currentMaterializedDate);
        } else {
            result = readinessService.getReadinessInfo(entityType.toString(), namespace, name, datacenter.toString().toLowerCase());
        }
        if (result != null) {
            Map<String, String> metadata = readinessService.getReadinessMetadata(result.getId());
            DataReadinessInfoDTO dto = new DataReadinessInfoDTO(result.getName(), result.getNamespace(), EntityType.valueOf(result.getEntityType()),
                    result.getDataCenter(), result.getWorkflowIdentifier(), WorkflowType.valueOf(result.getWorkflowType()),
                    result.getCurrentMaterializedTime(), result.getNextMaterializedTime(),
                    result.getStatus(), metadata);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }

        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @ApiOperation("Get Hive Table Readiness Info for nominal date")
    @GetMapping(value = "/hive/{namespace}/{name}/{datacenter}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ReadinessInfo> getTableReadinessInfo(
            @ApiParam(value = "Schema name", required = true, defaultValue = "default")
            @PathVariable String namespace,
            @ApiParam(value = "Table name", required = true, defaultValue = "reservation_flatter")
            @PathVariable String name,
            @ApiParam(value = "Data center name", required = true, defaultValue = "AMS4")
            @PathVariable DatacenterUtils.Datacenter datacenter,
            @ApiParam(value = "Nominal date, date format is yyyy-MM-dd", defaultValue = "2018-11-11")
            @RequestParam(value = "date", required = false) Optional<String> nominalDate,
            @ApiParam(value = "Nominal hour, date format is HH (24 hour format)", defaultValue = "00")
            @RequestParam(value = "hour", required = false) String nominalHour,
            @ApiParam(value = "Nominal minute, format is mm", defaultValue = "00")
            @RequestParam(value = "minute", required = false) String nominalMinute
    ) {
        try {
            String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            ReadinessInfo result = ReadinessInfo.ReadinessInfoBuilder.aReadinessInfo()
                    .status(ReadinessInfo.Status.UNKNOWN)
                    .entityNamespace(namespace)
                    .entityName(name)
                    .cluster(datacenter.name())
                    .build();

            // First check in data collected by listener
            String workflowName = workflowService.getWorkflowForEntity(namespace, name);

            //if not found, then look within internal workflow registry
            if (StringUtils.isEmpty(workflowName)) {
                WorkflowInfo wfInfo = workflowService.getAssociatedWorkflow(namespace, name);
                if (wfInfo != null && "oozie".equals(wfInfo.getType())) {
                    workflowName = wfInfo.getName().substring(wfInfo.getName().lastIndexOf("/") + 1);
                }
            }
            if (StringUtils.isNotEmpty(workflowName)) {

                OozieCoordinatorInfo oozieCoordinatorInfo = readinessService.getCoordinatorInfo(workflowName, datacenter);

                if (oozieCoordinatorInfo != null) {

                    result.setNextMaterializedTime(oozieCoordinatorInfo.getNextMaterializedTime());

                    String nominalDateValue = nominalDate.orElse(currentDate);
                    OozieWorkflowInstanceInfo workflowInstanceInfo =
                            getOozieWorkflowInstanceInfo(oozieCoordinatorInfo, datacenter, nominalDateValue,
                                    nominalHour, nominalMinute);

                    if (workflowInstanceInfo != null) {
                        result.setWorkflowName(workflowName);
                        result.setWorkflowInstanceInfo(workflowInstanceInfo);
                        if ("SUCCEEDED".equals(workflowInstanceInfo.getWorkflowStatus())) {
                            result.setStatus(ReadinessInfo.Status.READY);

                            try {
                                // query metastore and get all partitions that were changed between wf starttime and endtime
                                result.setActualPartitionOutput(
                                        readinessService.getActualWfOutputs(datacenter, result.getEntityNamespace(), result.getEntityName(),
                                                readinessService.getTimestampForOozieDateTime(result.getWorkflowInstanceInfo().getWorkflowStartTime()),
                                                readinessService.getTimestampForOozieDateTime(result.getWorkflowInstanceInfo().getWorkflowEndTime()))
                                );
                            } catch (Exception e) {
                                logger.error("Could not get actual output details: '{}'", e.getMessage());
                            }
                        } else {
                            result.setStatus(ReadinessInfo.Status.NOT_READY);
                        }
                    } else {
                        logger.warn(
                            "Unable to get oozie workflow instance for workflow '{}', datacenter '{}', nominal date {}, nominal hour {}, nominal minute {}",
                            name, datacenter, nominalDateValue, nominalHour, nominalMinute);
                    }
                } else {
                    logger.warn("Unable to get oozie coordinator instance for workflow '{}', datacenter '{}'", workflowName, datacenter);
                }
            } else {
                logger.info("No workflow assigned to the table: {}.{}", namespace, name);
            }

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception exc) {
            logger.error("Error while retrieving readiness info for table {}.{} for {} because {}",
                    namespace, name, datacenter, exc.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation("Get Workflow Readiness Info for nominal date")
    @GetMapping(value = "/workflow/{name}/{datacenter}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ReadinessInfo> getWorkflowReadinessInfo(
            @ApiParam(value = "Workflow name", required = true, defaultValue = "reservation-flatter")
            @PathVariable String name,
            @ApiParam(value = "Data center name", required = true, defaultValue = "AMS4")
            @PathVariable DatacenterUtils.Datacenter datacenter,
            @ApiParam(value = "Nominal date, date format is yyyy-MM-dd", defaultValue = "2018-11-11")
            @RequestParam(value = "date", required = false) Optional<String> nominalDate,
            @ApiParam(value = "Nominal hour, date format is HH (24 hour format)", defaultValue = "00")
            @RequestParam(value = "hour", required = false) String nominalHour,
            @ApiParam(value = "Nominal minute, format is mm", defaultValue = "00")
            @RequestParam(value = "minute", required = false) String nominalMinute
    ) {
        try {
            String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

            ReadinessInfo result = ReadinessInfo.ReadinessInfoBuilder.aReadinessInfo()
                    .status(ReadinessInfo.Status.UNKNOWN)
                    .workflowName(name)
                    .cluster(datacenter.name())
                    .build();

            OozieCoordinatorInfo oozieCoordinatorInfo = readinessService.getCoordinatorInfo(name, datacenter);

            if (oozieCoordinatorInfo != null) {

                result.setNextMaterializedTime(oozieCoordinatorInfo.getNextMaterializedTime());

                String nominalDateValue = nominalDate.orElse(currentDate);
                OozieWorkflowInstanceInfo workflowInstanceInfo =
                        getOozieWorkflowInstanceInfo(oozieCoordinatorInfo, datacenter, nominalDateValue,
                                nominalHour, nominalMinute);

                if (workflowInstanceInfo != null) {
                    result.setWorkflowInstanceInfo(workflowInstanceInfo);
                    if ("SUCCEEDED".equals(workflowInstanceInfo.getWorkflowStatus())) {
                        result.setStatus(ReadinessInfo.Status.READY);
                    } else {
                        result.setStatus(ReadinessInfo.Status.NOT_READY);
                    }

                    String schemaTable = workflowService.getEntityForWorkflow(name);

                    if (schemaTable != null) {
                        result.setEntityNamespace(schemaTable.split("\\.")[0]);
                        result.setEntityName(schemaTable.split("\\.")[1]);

                        if (result.getStatus().equals(ReadinessInfo.Status.READY)) {
                            try {
                                // query metastore and get all partitions that were changed between wf starttime and endtime
                                result.setActualPartitionOutput(
                                        readinessService.getActualWfOutputs(datacenter, result.getEntityNamespace(), result.getEntityName(),
                                                readinessService.getTimestampForOozieDateTime(result.getWorkflowInstanceInfo().getWorkflowStartTime()),
                                                readinessService.getTimestampForOozieDateTime(result.getWorkflowInstanceInfo().getWorkflowEndTime()))
                                );
                            } catch (Exception e) {
                                logger.error("Could not get actual output details: '{}'", e.getMessage());
                            }
                        }

                        // TODO
                        // this will be populated once vladimir develops feature for owner to define partitions
                        //result.setExpectedPartitionOutput(null);
                    }
                } else {
                    logger.warn(
                        "Unable to get oozie workflow instance for workflow '{}', datacenter '{}', nominal date {}, nominal hour {}, nominal minute {}",
                        name, datacenter, nominalDateValue, nominalHour, nominalMinute);
                }
            } else {
                logger.warn("Unable to get oozie coordinator instance for workflow '{}', datacenter '{}'", name, datacenter);
            }

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (SQLException | IOException exc) {
            logger.error("Error while retrieving readiness info for workflow {} because {}", name, exc.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private OozieWorkflowInstanceInfo getOozieWorkflowInstanceInfo(OozieCoordinatorInfo oozieCoordinatorInfo, DatacenterUtils.Datacenter datacenter,
                                                                   String nominalDate, String nominalHour, String nominalMinute) throws IOException {

        return readinessService.getWorkflowStatus(oozieCoordinatorInfo, datacenter, nominalDate, nominalHour, nominalMinute);
    }

    @ApiOperation("Add Readiness information for a table information")
    @PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity<String> saveReadinessInfo(
            @RequestBody DataReadinessInfoDTO body
    ) {
        try {

            DataReadinessInfo dataReadinessInfo = new DataReadinessInfo(
                    body.getName(), body.getNamespace(), body.getEntityType().toString(), body.getDataCenter(),
                    body.getWorkflowIdentifier(), body.getWorkflowType().toString(), body.getCurrentMaterializedTime(),
                    body.getNextMaterializedTime(), body.getStatus()
            );
            DataSource d = dataSourcesService.findDataEntityByNamespaceNameAndType(EntityTypeUtils.getEntityTypeInDqEntity(dataReadinessInfo.getEntityType()),
                    dataReadinessInfo.getNamespace(), dataReadinessInfo.getName());
            if (d == null) {
                return new ResponseEntity<>("Can't find Table with provided namespace and name and entity type", HttpStatus.FAILED_DEPENDENCY);
            }
            int dataReadinessId = readinessService.saveDataReadinessInfo(dataReadinessInfo);
            if (body.getMetadata() != null) {
                for (Map.Entry<String, String> entry : body.getMetadata().entrySet()) {
                    DataReadinessMetadata dataReadinessMetadata = new DataReadinessMetadata(dataReadinessId, entry.getKey(), entry.getValue());
                    readinessService.saveDataReadinessMetadata(dataReadinessMetadata);
                }
            }
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception exc) {
            return new ResponseEntity<>(exc.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
