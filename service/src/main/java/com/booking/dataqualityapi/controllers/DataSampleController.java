package com.booking.dataqualityapi.controllers;

import com.booking.dataqualityapi.dto.partition.PartitionInfo;
import com.booking.dataqualityapi.exceptions.DataUnavailableException;
import com.booking.dataqualityapi.exceptions.PermissionDeniedException;
import com.booking.dataqualityapi.model.DataSource;
import com.booking.dataqualityapi.model.EntityInstance.HiveInstanceType;
import com.booking.dataqualityapi.model.HiveTableDependency;
import com.booking.dataqualityapi.repository.HiveTableDependencyRepositoryImpl;
import com.booking.dataqualityapi.repository.HiveTablePartitionRepositoryImpl;
import com.booking.dataqualityapi.services.CacheService;
import com.booking.dataqualityapi.services.DataSourcesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.net.ConnectException;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api("/datasample")
@RestController
@RequestMapping("/datasample")
public class DataSampleController {

    private static final Logger logger = LoggerFactory.getLogger(DataSampleController.class);
    private static final Pattern dcPattern = Pattern.compile("(lhr4|ams4|gc)");
    private static final Pattern tableSchemaPattern = Pattern.compile("[0-9a-zA-Z-_]+");
    //private static final Pattern partitionPattern = Pattern.compile("([0-9a-zA-Z-_.]+=[0-9a-zA-Z-_.]+)(\\/[0-9a-zA-Z-_.]+=[0-9a-zA-Z-_.]+)*");

    @Autowired
    CacheService cacheService;

    @Autowired
    DataSourcesService dataSourcesService;

    @Autowired
    HiveTablePartitionRepositoryImpl partitionRepository;

    @Autowired
    HiveTableDependencyRepositoryImpl dependencyRepositoryImpl;

    @ApiOperation("Get Data Sample")
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> dataSampleGet(@RequestParam("dc") String dc,
                                                @RequestParam("schema") String schema,
                                                @RequestParam("table") String table,
                                                @RequestParam(name = "partition", required =  false) String partition) {

        ResponseEntity<String> responseEntity;
        if (dcPattern.matcher(dc).matches()
                && tableSchemaPattern.matcher(schema).matches()
                && tableSchemaPattern.matcher(table).matches()
        ) {
            if ("gc".equals(dc)) {
                return new ResponseEntity<>(responseMessage("Sorry! Support for GC is still under construction!"), HttpStatus.OK);
            }
            try {
                logger.info("Fetching sample data for {} from {}", table, dc);
                if (StringUtils.isEmpty(partition)) {
                    DataSource dataSource = dataSourcesService.findByHiveDataEntityNamespaceAndName(schema, table);
                    HiveInstanceType hiveInstance = dataSourcesService.getHiveInstanceFor(dataSource, dc);
                    if (hiveInstance.isPartitioned()) {
                        String actualSchema = schema;
                        String actualTable = table;

                        if ("VIRTUAL_VIEW".equalsIgnoreCase(hiveInstance.getTableType())) {
                            List<HiveTableDependency> sourceTables = dependencyRepositoryImpl.inputTables(dc, schema, table);

                            if (sourceTables != null && sourceTables.size() == 1) {
                                actualSchema = sourceTables.get(0).getInputEntityNamespace();
                                actualTable = sourceTables.get(0).getInputEntityName();
                            }
                        }
                        List<PartitionInfo> partitionInfo = partitionRepository.getPartitions(dc, actualSchema, actualTable, Optional.of(""), Optional.of(1));
                        if (partitionInfo != null && !partitionInfo.isEmpty()) {
                            partition = partitionInfo.get(0).getPartitionName();
                        }
                    }
                }
                responseEntity = new ResponseEntity<>(cacheService.getSampleData(dc, schema + "." + table, partition), HttpStatus.OK);
            } catch (Exception e) {
                logger.debug("Could not get sample data for {} {} from {} because {}", table, partition, dc, e.getMessage());
                String responseMessage = "";
                if (e.getCause() instanceof DataUnavailableException) {
                    responseMessage = "This table has no data!";
                } else if (e.getCause() instanceof ConnectException) {
                    responseMessage = "Unable to connect to Hive! There might be high load on the servers right now. Please try again in a while.";
                } else if (e.getCause() instanceof PermissionDeniedException) {
                    responseMessage = "We do not have sufficient privileges to access this table's data.";
                }
                responseEntity = new ResponseEntity<>(responseMessage, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            logger.error("Invalid dc / schema / table in request");
            responseEntity = new ResponseEntity<>("", HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @ApiOperation("Clears Entire Cache")
    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity<String> clearCache() {

        ResponseEntity<String> responseEntity;

        try {
            cacheService.clearCache();
            responseEntity = new ResponseEntity<>("", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(String.format("Could not clear cache because :: %s", e.getMessage()));
            responseEntity = new ResponseEntity<>("", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return responseEntity;
    }

    private String responseMessage(String message) {
        return "{ \"columns\" : [ \"Message\" ], \"data\" : [ { \"Message\" : \" " + message + " \" } ] }";
    }
}