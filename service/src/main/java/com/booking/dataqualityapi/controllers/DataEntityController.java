package com.booking.dataqualityapi.controllers;

import com.booking.dataqualityapi.dto.OozieWorkflowDeploymentInfo;
import com.booking.dataqualityapi.dto.dataentity.registration.DataEntityRegistrationAttributes;
import com.booking.dataqualityapi.dto.dataentity.registration.DataEntityRegistrationResolution;
import com.booking.dataqualityapi.dto.workflow.WorkflowInfo;
import com.booking.dataqualityapi.services.DataSourcesService;
import com.booking.dataqualityapi.services.WorkflowService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by vfedotov on 10/4/18.
 */

@Api("/data-entities")
@RestController
@RequestMapping("/data-entities")
public class DataEntityController {

    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowsController.class);

    @Autowired
    private WorkflowService workflowService;
    @Autowired
    private DataSourcesService dataSourcesService;

    @ApiOperation("Get Associated Workflow")
    @GetMapping(value = "/{data_entity_id}/workflow", produces = MediaType.APPLICATION_JSON_VALUE)
    public WorkflowInfo getWorkflow(@PathVariable("data_entity_id") String dataEntityId)
        throws SQLException {
        return workflowService.getAssociatedWorkflow(dataEntityId);
    }

    @ApiOperation("Get Auto Detected Workflow Info")
    @GetMapping(value = "/hive/{namespace}/{name}/auto-detected-workflow", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OozieWorkflowDeploymentInfo> getAutoDetectedWorkflowInfo(
            @PathVariable("namespace") String namespace,
            @PathVariable("name") String name
    ) throws MalformedURLException, SQLException {
        return workflowService.getAutoDetectedWorkflowInfo(namespace, name);
    }

    @ApiOperation(value = "Register data entity", response = DataEntityRegistrationResolution.class)
    @PostMapping(value = "/register/{type}/{account}/{namespace}/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public DataEntityRegistrationResolution registerDataEntity(
        @ApiParam(value = "data entity type", allowableValues = "hive-table, mysql-table, kafka-topic, aws-glue",
            example = "aws-glue", defaultValue = "aws-glue", required = true)
        @PathVariable String type,
        @ApiParam(value = "data entity account", defaultValue = "", required = true)
        @PathVariable String account,
        @ApiParam(value = "data entity namespace, in hive case it is database",
            example = "default", required = true)
        @PathVariable String namespace,
        @ApiParam(value = "data entity name, in hive case it is table",
            example = "reservation_flatter", required = true)
        @PathVariable String name,
        @RequestBody DataEntityRegistrationAttributes attributes) {
        return dataSourcesService.registerDataEntity(type, account, namespace, name, attributes);
    }
}

