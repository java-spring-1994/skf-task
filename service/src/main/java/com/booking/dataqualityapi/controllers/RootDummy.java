package com.booking.dataqualityapi.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@Api("/")
@ApiIgnore // Api(hidden = true) does not hide the whole controller
@RestController
@RequestMapping("/")
public class RootDummy {

    @ApiOperation("To mitigate swagger issue which always calls disabled 'csrf'")
    @GetMapping(value = "/csrf")
    public void csrf() {
        // empty
    }
}
