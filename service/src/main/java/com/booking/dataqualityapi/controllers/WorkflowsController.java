package com.booking.dataqualityapi.controllers;

import com.booking.dataqualityapi.dto.OozieWorkflowDeploymentInfo;
import com.booking.dataqualityapi.dto.OozieWorkflowInstanceInfo;
import com.booking.dataqualityapi.dto.OozieWorkflowSloInfo;
import com.booking.dataqualityapi.dto.workflow.WorkflowCoordInfo;
import com.booking.dataqualityapi.model.job.WorkflowJobStats;
import com.booking.dataqualityapi.model.job.WorkflowJobSummary;
import com.booking.dataqualityapi.model.workflow.WorkflowSession;
import com.booking.dataqualityapi.services.OozieApiService;
import com.booking.dataqualityapi.services.WorkflowService;
import com.booking.dataqualityapi.utils.DatacenterUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.apache.oozie.client.OozieClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * Created by vfedotov on 9/28/18.
 */

@Api("/workflows")
@RestController
@RequestMapping("/workflows")
public class WorkflowsController {

    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowsController.class);

    @Autowired
    private WorkflowService workflowService;

    @Autowired
    private OozieApiService oozieApiService;

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @ApiOperation("Get Workflow Deployment Info")
    @GetMapping(value = "/{workflow_id}/deployment-info", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OozieWorkflowDeploymentInfo> getWorkflowDeploymentInfo(
        @PathVariable("workflow_id") String workflowId
    ) throws MalformedURLException, SQLException {
        return workflowService.getOozieWorkflowDeploymentInfo(workflowId);
    }


    @ApiOperation(value = "Get workflow deployment info by workflow name", nickname = "getDeploymentInfo")
    @GetMapping(value = "deployment/{wf_name}", produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public List<OozieWorkflowDeploymentInfo> getDeploymentInfo(
        @ApiParam(value = "Workflow name", example = "reservation-flatter", required = true)
        @PathVariable(name = "wf_name") final String wfName
    ) throws SQLException, MalformedURLException {
        return workflowService.getWorkflowDeploymentInfo(wfName);
    }

    @ApiOperation(value = "Get workflow coordinator info by name", nickname = "getCoordInfo")
    @GetMapping(value = "coord/{wf_name}", produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public List<WorkflowCoordInfo> getCoordInfo(@ApiParam(value = "Workflow name",
            example = "reservation-flatter", required = true)
            @PathVariable(name = "wf_name") final String wfName) throws OozieClientException {

        List<WorkflowCoordInfo> result = new ArrayList<>();

        for (DatacenterUtils.Datacenter dc: DatacenterUtils.DEFAULT_OOZIE_DC_LIST) {
            WorkflowCoordInfo coordInfo = oozieApiService.getCoordInfo(wfName, dc);
            if (coordInfo != null) {
                result.add(coordInfo);
            }
        }
        return result;
    }

    @ApiOperation(value = "Get workflow status info", nickname = "getCoordinatorStatuses")
    @GetMapping(value = "statuses/{dc}/{wf_name}", produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public List<OozieWorkflowInstanceInfo> getLatestStatusesInfo(@ApiParam(value = "data center name", allowableValues = "AMS4, LHR4", required = true)
                                                                 @PathVariable(name = "dc") String dc,
                                                                 @ApiParam(value = "Workflow name",
                                                                         example = "reservation-flatter", required = true)
                                                                 @PathVariable(name = "wf_name") final String wfName,
                                                                 @ApiParam(value = "Workflow statuses size",
                                                                         defaultValue = "1")
                                                                 @RequestParam(required = false, name = "size")
                                                                         Optional<Integer> size) throws IOException {
        return oozieApiService.getLastCoordinatorActionsByName(
            DatacenterUtils.Datacenter.valueOf(dc), wfName, size.orElse(1)
        );
    }

    @ApiOperation(value = "Get workflow start and finish times SLO", nickname = "getSmartStatus")
    @GetMapping(value = "status/{dc}/{wf_name}", produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public OozieWorkflowSloInfo getSmartStatusInfo(@ApiParam(value = "data center name", allowableValues = "AMS4, LHR4", required = true)
                                                   @PathVariable(name = "dc") String dc,
                                                   @ApiParam(value = "Workflow name",
                                                           example = "reservation-flatter", required = true)
                                                   @PathVariable(name = "wf_name") final String wfName,
                                                   @ApiParam(value = "Workflow jobs previous size",
                                                           defaultValue = "100")
                                                   @RequestParam(required = false, name = "size")
                                                           Optional<Integer> size,
                                                   @ApiParam(value = "Workflow percentile SLO, range [0..100] ",
                                                           defaultValue = "90")
                                                   @RequestParam(required = false, name = "percentile")
                                                           Optional<Integer> percentileOptional
    ) {
        final int percentile = percentileOptional.orElse(90);
        if (percentile < 0 || percentile > 100) {
            throw new IllegalArgumentException("Percentile allowable values from 0 till 100");
        }
        return workflowService.getSmartStatusInfo(dc.toLowerCase(), wfName, percentile, size.orElse(100));
    }


    @ApiOperation(value = "Get workflow runs info by workflow name", nickname = "getWorkflowSessions")
    @GetMapping(value = "sessions/{wf_name}")
    public List<WorkflowSession> getWorkflowSessions(@ApiParam(value = "Workflow data center",
            example = "AMS4", allowableValues = "AMS4, LHR4")
                                                     @RequestParam(required = false, name = "dc")
                                                             Optional<String> dc,
                                                     @ApiParam(value = "Workflow name",
                                                             example = "reservation-flatter", required = true)
                                                     @PathVariable(name = "wf_name")
                                                             String wfName,
                                                     @ApiParam(value = "from date, default week ago",
                                                             example = "2019-11-01")
                                                     @RequestParam(name = "from", required = false)
                                                             Optional<String> from,
                                                     @ApiParam(value = "to, default value today", example = "2019-11-12")
                                                     @RequestParam(name = "to", required = false)
                                                             Optional<String> to) {
        final LocalDate fromDate = from.map(value -> LocalDate.parse(value, formatter)).orElse(LocalDate.now().minusDays(7));
        final LocalDate toDate = to.map(value -> LocalDate.parse(value, formatter)).orElse(LocalDate.now());
        return workflowService.getWorkflowSessions(dc, wfName, Date.valueOf(fromDate), Date.valueOf(toDate));
    }

    @ApiOperation(value = "Get workflow job statistics")
    @GetMapping(value = "job-resource-statistics/{wf_name}")
    public List<WorkflowJobStats> getWorkflowJobStats(@ApiParam(value = "Workflow data center",
            example = "AMS4", allowableValues = "AMS4, LHR4")
                                                      @RequestParam(required = false, name = "dc")
                                                              Optional<String> dc,
                                                      @ApiParam(value = "Workflow name",
                                                              example = "hdbi-table-property-splits", required = true)
                                                      @PathVariable(name = "wf_name")
                                                              String wfName,
                                                      @ApiParam(value = "from date, default week ago",
                                                              example = "2019-11-01")
                                                      @RequestParam(name = "from", required = false)
                                                              Optional<String> from,
                                                      @ApiParam(value = "to, default value today", example = "2019-11-12")
                                                      @RequestParam(name = "to", required = false)
                                                              Optional<String> to) {
        final LocalDate fromDate = from.map(value -> LocalDate.parse(value, formatter)).orElse(LocalDate.now().minusDays(7));
        final LocalDate toDate = to.map(value -> LocalDate.parse(value, formatter)).orElse(LocalDate.now());
        return workflowService.getJobStats(Date.valueOf(fromDate), Date.valueOf(toDate), wfName, dc);
    }


    @ApiOperation(value = "Get workflow job summary from date, to date", nickname = "wfResourcesSummary")
    @GetMapping(value = "job-resource-summary/{wf_name}")
    public WorkflowJobSummary getWorkflowJobStats(
                                                  @ApiParam(value = "Workflow name",
                                                              example = "hdbi-table-property-splits", required = true)
                                                      @PathVariable(name = "wf_name")
                                                              String wfName,
                                                  @ApiParam(value = "from date, default week ago",
                                                              example = "2020-04-01")
                                                      @RequestParam(name = "from", required = false)
                                                              Optional<String> from,
                                                  @ApiParam(value = "to, default value today", example = "2020-04-15")
                                                      @RequestParam(name = "to", required = false)
                                                              Optional<String> to) {
        final LocalDate fromDate = from.map(value -> LocalDate.parse(value, formatter)).orElse(LocalDate.now().minusDays(30));
        final LocalDate toDate = to.map(value -> LocalDate.parse(value, formatter)).orElse(LocalDate.now());
        return workflowService.getJobSummaryStats(Date.valueOf(fromDate), Date.valueOf(toDate), wfName);
    }

    @ApiOperation(value = "Get list of all workflows", nickname = "getAllWorkflows")
    @GetMapping(value = "workflow_list")
    public List<String> getAllWorkflows() {
        return workflowService.getAllWorkflows();
    }

    @ApiOperation(value = "search workflows", nickname = "searchWorkflows")
    @GetMapping(value = "workflow_list/{wf_name}")
    public List<String> searchWorkflows(
            @ApiParam(value = "Workflow name",
                      required = true)
            @PathVariable(name = "wf_name")
                    String wfName,
            @ApiParam(value = "exclude")
            @RequestParam(name = "exclude", required = false)
                    List<String> excludedWorkflows
    ) {
        return workflowService.searchWorkflows(wfName, excludedWorkflows);
    }
}
