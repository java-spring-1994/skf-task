package com.booking.dataqualityapi.controllers;

import com.booking.dataqualityapi.dto.DataAvailabilityConfigurationInfo;
import com.booking.dataqualityapi.model.DataAvailabilityConfiguration;
import com.booking.dataqualityapi.repository.MonitoringRequestRepository;
import com.booking.dataqualityapi.services.DataAvailabilityConfigurationService;
import com.booking.dataquality.dto.monitoring.MonitoringConfig;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController(value = "Register table to publish events into availability service.")
public class DataAvailabilityConfigurationController {
    @Autowired
    DataAvailabilityConfigurationService dataAvailabilityConfigurationService;

    @Autowired
    private MonitoringRequestRepository requestRepository;

    @ApiOperation(value = "Enable availability event publication for table",
            nickname = "enableSyncToDAS",
            response = DataAvailabilityConfiguration.class)
    @PostMapping(value = "/data-availability/{namespace}/{name}",
            produces = MediaType.APPLICATION_JSON)
    public DataAvailabilityConfiguration enableDataAvailabilityUpdates(@ApiParam(value = "Table namespace",
            example = "default", required = true)
                                                                       @PathVariable(required = true) String namespace,
                                                                       @ApiParam(value = "Table name",
                                                                               example = "reservation_flatter", required = true)
                                                                       @PathVariable(required = true) String name) {
        return dataAvailabilityConfigurationService.enableDataAvailabilityUpdates(namespace,
                name);
    }

    @ApiOperation(value = "Enable availability event publication for table",
            nickname = "disableSyncToDAS",
            response = DataAvailabilityConfiguration.class)
    @DeleteMapping(value = "/data-availability/{namespace}/{name}",
            produces = MediaType.APPLICATION_JSON)
    public DataAvailabilityConfiguration deleteDataAvailabilitySync(@ApiParam(value = "Table namespace",
            example = "default", required = true)
                                                                    @PathVariable(required = true) String namespace,
                                                                    @ApiParam(value = "Table name",
                                                                            example = "reservation_flatter", required = true)
                                                                    @PathVariable(required = true) String name) {
        return dataAvailabilityConfigurationService.deleteSyncToDas(namespace, name);
    }


    @ApiOperation(value = "Enable availability event publication for table",
            nickname = "syncToDAS",
            response = DataAvailabilityConfiguration.class)
    @GetMapping(value = "/data-availability/{namespace}/{name}",
            produces = MediaType.APPLICATION_JSON)
    public DataAvailabilityConfigurationInfo getDataAvailabilityConfiguration(@ApiParam(value = "Table namespace",
            example = "default", required = true)
                                                                              @PathVariable(required = true) String namespace,
                                                                              @ApiParam(value = "Table name",
                                                                                      example = "reservation_flatter", required = true)
                                                                              @PathVariable(required = true) String name) {
        return dataAvailabilityConfigurationService.getDataAvailabilityTableConfiguration(namespace, name);
    }

    @ApiOperation(value = "Get table list which updates will be published to data availability service")
    @GetMapping(value = "/tables-list-for-sync",
            produces = MediaType.APPLICATION_JSON)
    public List<MonitoringConfig> getTablesSyncForSyncToDas() {
        return dataAvailabilityConfigurationService.getTablesSyncForSyncToDas();
    }

}
