package com.booking.dataqualityapi.controllers;

import com.booking.dataqualityapi.auth.AuthUtils;
import com.booking.dataqualityapi.config.SwaggerConfig;
import com.booking.dataqualityapi.dto.catalogue.CatalogueDerivedMetricInfo;
import com.booking.dataqualityapi.dto.catalogue.CatalogueExternalMetricInfo;
import com.booking.dataqualityapi.dto.catalogue.CatalogueMetricInfo;
import com.booking.dataqualityapi.dto.catalogue.CatalogueMetricListInfo;
import com.booking.dataqualityapi.dto.catalogue.CatalogueMetricSearchInfo;
import com.booking.dataqualityapi.model.WikiEntity;
import com.booking.dataqualityapi.services.catalogue.CatalogueMetricService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController("Metrics catalogue controller.")
public class CatalogueMetricsController {

    @Autowired
    private CatalogueMetricService metricService;

    @ApiOperation(value = "Create derived metric. "
            + "Update metric if metric with such name exists", nickname = "createCatalogueDerivedMetric",
            authorizations = {@Authorization(value = SwaggerConfig.BOOKING_AUTH)})
    @PostMapping(value = "/catalogue-metric/derived",
            produces = MediaType.APPLICATION_JSON)
    public List<CatalogueDerivedMetricInfo> creatOrUpdateDerivedMetric(@ApiParam(hidden = true) Authentication authentication,
                                                                              @RequestBody CatalogueDerivedMetricInfo[] derivedMetrics) {
        final String staffLoginName = AuthUtils.getLoginName(authentication)
            .orElseThrow(() -> new AuthenticationCredentialsNotFoundException("No staff login name found in update metrics request "));
        return metricService.save(staffLoginName, derivedMetrics);
    }

    @ApiOperation(value = "Create external metric", nickname = "createCatalogueExternalMetric",
            authorizations = {@Authorization(value = SwaggerConfig.BOOKING_AUTH)})
    @PostMapping(value = "/catalogue-metric/external",
            produces = MediaType.APPLICATION_JSON)
    public List<CatalogueExternalMetricInfo> creatOrUpdateExternalMetric(@ApiParam(hidden = true) Authentication authentication,
                                                                 @RequestBody CatalogueExternalMetricInfo[] externalMetrics) {
        final String staffLoginName = AuthUtils.getLoginName(authentication)
            .orElseThrow(() -> new AuthenticationCredentialsNotFoundException("No staff login name found in update metrics request "));
        return metricService.save(staffLoginName, externalMetrics);
    }

    @ApiOperation(value = "Get All Metrics", nickname = "getAllMetrics",
            authorizations = {@Authorization(value = SwaggerConfig.BOOKING_AUTH)})
    @GetMapping(value = {"/catalogue-metrics"},
            produces = MediaType.APPLICATION_JSON)
    public CatalogueMetricListInfo getMetrics() {
        return metricService.getAllMetrics();
    }

    @ApiOperation(value = "Get list metrics by names", nickname = "getMetricsByNameList",
            authorizations = {@Authorization(value = SwaggerConfig.BOOKING_AUTH)})
    @PostMapping(value = {"/catalogue-metrics/search"},
            produces = MediaType.APPLICATION_JSON)
    public CatalogueMetricListInfo getMetrics(@RequestBody CatalogueMetricSearchInfo searchInfo) {
        return metricService.getMetrics(searchInfo);
    }

    @ApiOperation(value = "Delete metric by metric name or throws  not found exception. Returns deleted metrics.", nickname = "deleteCatalogueMetric",
            response = WikiEntity.class, authorizations = {@Authorization(value = SwaggerConfig.BOOKING_AUTH)})
    @DeleteMapping(value = "/catalogue-metric/{name}",
            produces = MediaType.APPLICATION_JSON)
    public CatalogueMetricInfo deleteMetric(@ApiParam(hidden = true) Authentication authentication,
                                            @ApiParam(value = "metric name", required = true)
                                            @PathVariable(required = true) final String name) {
        final String staffLoginName = AuthUtils.getLoginName(authentication)
                .orElseThrow(() -> new AuthenticationCredentialsNotFoundException("No staff login name found in delete metric request "));
        return metricService.deleteMetric(name);
    }
}
