package com.booking.dataqualityapi.controllers;

import com.booking.dataqualityapi.dto.HiveTableInfo;
import com.booking.dataqualityapi.dto.dataentity.DataEntityInfo;
import com.booking.dataqualityapi.dto.dataentity.DataEntityListInfo;
import com.booking.dataqualityapi.dto.schemaOverView.SchemaOverViewEntity;
import com.booking.dataqualityapi.model.DataSource;
import com.booking.dataqualityapi.model.EntityInstance.MysqlImportInstance;
import com.booking.dataqualityapi.repository.SchemaRepository;
import com.booking.dataqualityapi.services.DataSourcesService;
import com.google.common.base.Splitter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections.CollectionUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Optional;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.LinkedList;

import java.util.stream.Collectors;

@Api(value = "data entities")
@RestController
public class DataSourceController {

    private DataSourcesService dsService;
    private SchemaRepository schemaRepository;

    @Autowired
    public DataSourceController(DataSourcesService dsService, SchemaRepository schemaRepository) {
        this.dsService = dsService;
        this.schemaRepository = schemaRepository;
    }

    @Autowired
    private ModelMapper modelMapper;



    @ApiOperation("Retrieves hive data entities list")
    @GetMapping(value = {"/entities"}, produces = MediaType.APPLICATION_JSON)
    // Used by mlcloud
    public DataEntityListInfo listEntities(@ApiParam(value = "Entity namespace, for hive it is entity schema")
                                           @RequestParam(value = "namespace", required = false) Optional<String> namespace,
                                           @ApiParam(value = "Entity name, for hive it is table name")
                                           @RequestParam(value = "name", required = false) Optional<String> name,
                                           @ApiParam(value = "Data Entities List offset, default value is 0")
                                           @RequestParam(value = "offset", required = false, defaultValue = "0") Integer offset,
                                           @ApiParam(value = "Data Entities List length, default value is 100, max value is 1000")
                                           @RequestParam(value = "limit", required = false, defaultValue = "100") Integer limit,
                                           @ApiParam(value = "Staff Id, get list of tables owned by particular user")
                                           @RequestParam(value = "staffId", required = false) Optional<Integer> staffId,
                                           @ApiParam(value = "Label, get list of tables with all matching label=value pairs, comma separated")
                                           @RequestParam(value = "label", required = false) Optional<String> label
    ) {
        int length = Math.min(limit, 1000);

        Map<String, String> labelsMap;

        if (label.isPresent()) {
            labelsMap = Splitter.on(",")
                    .trimResults()
                    .omitEmptyStrings().withKeyValueSeparator("=").split(label.orElse("").replace(" ", ""));
        } else {
            labelsMap = new HashMap<>();
        }

        List<DataSource> dataSources = dsService.listDataSources(namespace, name, staffId, labelsMap, length + 1, offset);
        final List<DataEntityInfo> dataEntities = dataSources.stream().limit(length).map(this::toDataEntityInfo).collect(Collectors.toList());
        return new DataEntityListInfo(dataEntities, offset, dataEntities.size(), dataSources.size() > length);
    }

    @ApiOperation("Retrieves hive data entities list with a schema")
    @GetMapping(value = {"/schemaoverview/{schemaType}/{schemaName}"}, produces = MediaType.APPLICATION_JSON)
    // Used by mlcloud
    public List<SchemaOverViewEntity> listEntitiesForSchemaWithInstances(
            @PathVariable("schemaType") String schemaType,
            @PathVariable("schemaName") String schemaName
    ) {
        if ("hive".equals(schemaType)) {
            return schemaRepository.getTablesOfSchema(schemaName);
        }
        if ("mysql".equals(schemaType)) {
            return schemaRepository.getTablesForMysqlSchema(schemaName);
        }

        return new ArrayList<>();

    }

    private DataEntityInfo toDataEntityInfo(DataSource dataSource) {
        DataEntityInfo dei = modelMapper.map(dataSource, DataEntityInfo.class);
        dei.setInstances(getInstances(dataSource));
        dei.setMysqlImport(getMysqlImportInfo(dataSource));
        return dei;
    }

    private DataEntityInfo.MysqlImportInfo getMysqlImportInfo(DataSource dataSource) {
        if (CollectionUtils.isEmpty(dataSource.getMysqlImportInstance())) {
            return null;
        }
        MysqlImportInstance mysqlImportInstance = dataSource.getMysqlImportInstance().get(0);
        return new DataEntityInfo.MysqlImportInfo(mysqlImportInstance.getMysqlSchemaName(),
                mysqlImportInstance.getMysqlTableName());
    }

    private DataEntityInfo.DataInstance[] getInstances(DataSource dataSource) {
        List<DataEntityInfo.DataInstance> instances = new LinkedList<>();

        if (dataSource.getAms4Instance() != null) {
            instances.add(new DataEntityInfo.DataInstance("AMS4", "hive",
                    dataSource.getAms4Instance().getTableType(),
                    dsService.getEntityColumnInfo(dataSource, "AMS4").getPartitionColumns()));

        }
        if (dataSource.getGoogleCloudInstance() != null) {
            instances.add(new DataEntityInfo.DataInstance("GC", "hive",
                    dataSource.getGoogleCloudInstance().getTableType(),
                    dsService.getEntityColumnInfo(dataSource, "GC").getPartitionColumns()));
        }
        if (dataSource.getLhr4Instance() != null) {
            instances.add(new DataEntityInfo.DataInstance("LHR4", "hive",
                    dataSource.getLhr4Instance().getTableType(),
                    dsService.getEntityColumnInfo(dataSource, "LHR4").getPartitionColumns()));
        }
        return instances.toArray(new DataEntityInfo.DataInstance[0]);
    }

    @ApiOperation("Retrieve data entity cluster specific information")
    @GetMapping(value = {"/entity/hive/{namespace}/{name}/{datacenter}"},
            produces = MediaType.APPLICATION_JSON)
    public HiveTableInfo getEntity(
            @ApiParam(value = "Entity namespace", example = "default", required = true)
            @PathVariable(value = "namespace") String namespace,
            @ApiParam(value = "Entity name", example = "vp_search_log", required = true)
            @PathVariable(value = "name") String name,
            @ApiParam(value = "Instance datacenter", allowableValues = "AMS4, GC, LHR4", required = true)
            @PathVariable(value = "datacenter") String datacenter) {
        return dsService.getEntityColumnInfo(namespace, name, datacenter);
    }

    @ApiOperation("Set data entity information")
    @PostMapping(value = {"/entity/{id}/label/{label}"}, produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<String> setLabel(
            @ApiParam(value = "Entity id", example = "1", required = true)
            @PathVariable(value = "id") Integer id,
            @ApiParam(value = "Label key", example = "myLabel", required = true)
            @PathVariable(value = "label") String label,
            @ApiParam(value = "Label value", required = true)
            @RequestParam(value = "value", required = false, defaultValue = "") String value) {

        dsService.saveLabel(id, label, value);

        return new ResponseEntity<>(String.format("label and value have been set %s=%s", label, value), HttpStatus.OK);
    }

    @ApiOperation("Delete label")
    @DeleteMapping(value = {"/entity/{id}/label/{label}"}, produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<String> deleteLabel(
            @ApiParam(value = "Entity id", example = "1", required = true)
            @PathVariable(value = "id") Integer id,
            @ApiParam(value = "Label key", example = "myLabel", required = true)
            @PathVariable(value = "label") String label
    ) {
        Optional<String> deletedLabel = dsService.deleteLabel(id, label);
        if (!deletedLabel.isPresent()) {
            return new ResponseEntity<>(String.format(" label [%s] with id %d hasn't been founded ",label, id), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(String.format("%s label has been deleted",label), HttpStatus.OK);
    }

}