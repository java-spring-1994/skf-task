package com.booking.dataqualityapi.controllers.ownership;

import com.booking.authxclient.exceptions.AuthXClientException;
import com.booking.dataqualityapi.dto.ownership.dq.DataEntityOwnershipInfo;
import com.booking.dataqualityapi.dto.ownership.dq.DataEntityOwnershipInfoDump;
import com.booking.dataqualityapi.dto.ownership.dq.OwnershipAssignmentMeta;
import com.booking.dataqualityapi.services.ownership.OwnershipService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api("/ownership")
@RestController
@RequestMapping("/ownership")
public class OwnershipController {

    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory.getLogger(OwnershipController.class);

    @Autowired
    private OwnershipService ownershipService;

    @ApiOperation("Get ownership info for datasource")
    @GetMapping(value = "/{entityType}/{entityNamespace}/{entityName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DataEntityOwnershipInfo> getOwnership(
        @PathVariable String entityType,
        @PathVariable String entityNamespace,
        @PathVariable String entityName
    ) throws AuthXClientException, InterruptedException, IOException {
        return ownershipService.getOwnershipInfo(entityType, entityNamespace, entityName);
    }

    @ApiOperation("Get ownership info for datasources in batch")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, List<DataEntityOwnershipInfo>> getOwnershipBatch(
        @ApiParam(value = "Comma separated ownable names, format of ownable name is <type>.<namespace>.<name>",
            example = "hive.default.reservation_flatter", required = true)
        @RequestParam(value = "ownableNames") String ownableNames
    ) throws AuthXClientException, InterruptedException, IOException {
        return ownershipService.getOwnershipInfoBatch(ownableNames.split(","));
    }

    @ApiOperation("Get ownership info for datasources in namespace")
    @GetMapping(value = "/{entityType}/{entityNamespace}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DataEntityOwnershipInfo> getOwnershipForEntitiesInNamespace(
        @PathVariable String entityType,
        @PathVariable String entityNamespace
    ) throws AuthXClientException, InterruptedException, IOException {
        return ownershipService.getOwnershipInfoForEntitiesInNamespace(entityType, entityNamespace);
    }

    // todo use enum for entity type
    // todo otherwise wrong usage of the API leads to incorrect ownable names in ownership service
    // todo consider entity account
    @ApiOperation(value = "Assign ownership to datasource")
    @PostMapping(value = "/{entityType}/{entityNamespace}/{entityName}/{orgUnitId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String assignOwnership(
        @PathVariable String entityType,
        @PathVariable String entityNamespace,
        @PathVariable String entityName,
        @PathVariable String orgUnitId,
        @RequestBody OwnershipAssignmentMeta body
    ) throws InterruptedException, IOException, AuthXClientException {
        return ownershipService.assignOwnership(
            entityType, null, entityNamespace, entityName, Long.parseLong(orgUnitId),
            body.getReason(), body.getStaffId(), body.getStaffLoginname());
    }

    // todo consider entity account
    @ApiOperation(value = "Drop ownership for datasource")
    @DeleteMapping(value = "/{entityType}/{entityNamespace}/{entityName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String dropOwnership(
        @PathVariable String entityType,
        @PathVariable String entityNamespace,
        @PathVariable String entityName
    ) throws InterruptedException, IOException, AuthXClientException {
        return ownershipService.dropOwnership(
            entityType, null, entityNamespace, entityName);
    }

    @ApiOperation("Get owned entities for specific orgunit")
    @GetMapping(value = "/orgunit/{orgUnitId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DataEntityOwnershipInfo> getOwnedEntities(
        @PathVariable long orgUnitId
    ) throws AuthXClientException, InterruptedException, IOException {
        return ownershipService.getOwnedEntities(orgUnitId);
    }

    @ApiOperation("Get owned entities for orgunits list")
    @GetMapping(value = "/orgunits", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DataEntityOwnershipInfo> getOwnedEntitiesBatch(
        @RequestParam(value = "ids") String ids
    ) throws AuthXClientException, InterruptedException, IOException {
        String[] idsArray = ids.split(",");
        List<Long> idsList = Arrays.stream(idsArray).map(Long::valueOf).collect(Collectors.toList());
        return ownershipService.getOwnedEntitiesBatch(idsList);
    }

    @ApiOperation("Get all ownership records")
    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DataEntityOwnershipInfoDump> getOwnershipDump()
        throws AuthXClientException, InterruptedException, IOException {
        return ownershipService.getAllOwnershipInfo();
    }
}
