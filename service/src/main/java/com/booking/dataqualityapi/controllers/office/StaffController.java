package com.booking.dataqualityapi.controllers.office;

import com.booking.dataqualityapi.model.office.Staff;
import com.booking.dataqualityapi.services.OfficeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api("/staff")
@RestController
@RequestMapping("/staff")
public class StaffController {

    @Autowired
    private OfficeService officeService;

    @ApiOperation("Get staff info")
    @GetMapping(value = "/{staffId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Staff getStaffInfo(
        @PathVariable int staffId
    ) {
        return officeService.getOneStaffById(staffId);
    }

    @ApiOperation("Check off duty")
    @GetMapping(value = "/{staffId}/is_off_duty", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean checkOffDuty(
        @PathVariable int staffId
    ) {
        return officeService.getOneStaffById(staffId).isOffDuty();
    }
}
