package com.booking.dataqualityapi.controllers;

import com.booking.dataquality.model.alerting.AnomalyAlertConfig;
import com.booking.dataquality.model.monitoring.MetricAnomaly;
import com.booking.dataqualityapi.auth.AuthUtils;
import com.booking.dataqualityapi.dto.alerts.AnomalyAlertConfigInfo;
import com.booking.dataqualityapi.dto.alerts.AnomalyAlertConfigPayload;
import com.booking.dataqualityapi.dto.monitoring.AnomalyInfo;
import com.booking.dataqualityapi.services.monitoring.AnomalyAlertConfigurationService;
import com.booking.dataqualityapi.services.monitoring.AnomalyCalculationService;
import com.booking.dataqualityapi.services.monitoring.AnomalyDetectionService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Future;

@RestController
public class AnomalyDetectionController {

    private final AnomalyDetectionService anomalyDetectionService;
    private final AnomalyCalculationService anomalyCalculationService;
    private final AnomalyAlertConfigurationService anomalyAlertConfigurationService;
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Autowired
    public AnomalyDetectionController(final AnomalyDetectionService anomalyDetectionService,
                                      final AnomalyCalculationService calculationService,
                                      final AnomalyAlertConfigurationService anomalyAlertConfigurationService) {
        this.anomalyDetectionService = anomalyDetectionService;
        this.anomalyCalculationService = calculationService;
        this.anomalyAlertConfigurationService = anomalyAlertConfigurationService;
    }

    @ApiOperation(value = "Return detected anomalies mapped to column metric which score greate then {score}",
            nickname = "getAnomalies")
    @GetMapping(value = "/anomalies/{namespace}/{name}", produces = MediaType.APPLICATION_JSON)
    public Map<String, List<AnomalyInfo>> getAnomalies(
            @ApiParam(value = "data entity namespace, in hive case it is database",
                    example = "default", required = true)
            @PathVariable(required = true) final String namespace,
            @ApiParam(value = "data entity name, in hive case it is table",
                    example = "reservation_flatter", required = true)
            @PathVariable(required = true) final String name,
            @ApiParam(value = "Anomaly score, only anomalies with great or equals to score will be returned",
                    example = "20.0", defaultValue = "20.0", required = false)
            @RequestParam(value = "score", required = false) final Optional<Float> score,
            @ApiParam(value = "Date in format yyyy-mm-dd, from which anomalies will be returned. If not specified 3 months back date is used by default",
                    example = "2019-01-18", required = false)
            @RequestParam(value = "from", required = false) Optional<String> from,
            @ApiParam(value = "Date in format yyyy-mm-dd, to which anomalies will be returned. If not specified current date is used.",
                    example = "2019-03-18", required = false)
            @RequestParam(value = "to", required = false) final Optional<String> to,
            @ApiParam(value = "data center",
                    example = "LHR4", allowableValues = "LHR4, AMS4", required = false)
            @RequestParam(required = false) final Optional<String> dc,
            @ApiParam(value = "metric name",
                    example = "max", required = false)
            @RequestParam(required = false) final Optional<String> metric,
            @ApiParam(value = "column name",
                    required = false)
            @RequestParam(required = false) final Optional<String> column
    ) throws IOException {
        final LocalDate fromDate = from.map(value -> LocalDate.parse(value, formatter)).orElse(LocalDate.now().minusMonths(3));
        final LocalDate toDate = to.map(value -> LocalDate.parse(value, formatter)).orElse(LocalDate.now());
        return anomalyDetectionService.getAnomalies(dc, namespace, name, column, metric, fromDate, toDate, score.orElse(0.0f));
    }


    @ApiOperation(value = "Return detected rows which score greate then {score}",
            nickname = "getAnomalyList")
    @GetMapping(value = "/anomalyList/{namespace}/{name}", produces = MediaType.APPLICATION_JSON)
    public List<AnomalyInfo> getAnomalyList(
            @ApiParam(value = "data entity namespace, in hive case it is database",
                    example = "default", required = true)
            @PathVariable(required = true) final String namespace,
            @ApiParam(value = "data entity name, in hive case it is table",
                    example = "reservation_flatter", required = true)
            @PathVariable(required = true) final String name,
            @ApiParam(value = "Anomaly score, only anomalies with great or equals to score will be returned",
                    example = "20.0", defaultValue = "20.0", required = false)
            @RequestParam(value = "score", required = false) final Optional<Float> score,
            @ApiParam(value = "Date in format yyyy-mm-dd, from which anomalies will be returned. If not specified 3 months back date is used by default",
                    example = "2019-01-18", required = false)
            @RequestParam(value = "from", required = false) Optional<String> from,
            @ApiParam(value = "Date in format yyyy-mm-dd, to which anomalies will be returned. If not specified current date is used.",
                    example = "2019-03-18", required = false)
            @RequestParam(value = "to", required = false) final Optional<String> to,
            @ApiParam(value = "data center",
                    example = "LHR4", allowableValues = "LHR4, AMS4", required = false)
            @RequestParam(required = false) final Optional<String> dc,
            @ApiParam(value = "metric name",
                    example = "max", required = false)
            @RequestParam(required = false) final Optional<String> metric,
            @ApiParam(value = "column name",
                    required = false)
            @RequestParam(required = false) final Optional<String> column
    ) throws IOException {
        final LocalDate fromDate = from.map(value -> LocalDate.parse(value, formatter)).orElse(LocalDate.now().minusMonths(3));
        final LocalDate toDate = to.map(value -> LocalDate.parse(value, formatter)).orElse(LocalDate.now());
        return anomalyDetectionService.getAnomalyList(dc, namespace, name, column, metric, fromDate, toDate, score.orElse(20.0f));
    }


    @ApiOperation(value = "Delete anomaly rows associated with the table",
            nickname = "deleteAnomalies")
    @DeleteMapping(value = "/anomalies/{dc}/{namespace}/{name}", produces = MediaType.APPLICATION_JSON)
    public int deleteAnomalies(
            @ApiParam(value = "data center",
                    example = "lhr4", allowableValues = "lhr4, ams4", required = true)
            @PathVariable(required = true) final String dc,
            @ApiParam(value = "data entity namespace, in hive case it is database",
                    example = "default", required = true)
            @PathVariable(required = true) final String namespace,
            @ApiParam(value = "data entity name, in hive case it is table",
                    example = "reservation_flatter", required = true)
            @PathVariable(required = true) final String name) {
        return anomalyDetectionService.deleteAnomalies(dc, namespace, name);
    }



    @ApiOperation(value = "Start anomaly calculation async job",
            nickname = "calcAnomalies")
    @PostMapping(value = "/anomalies/{dc}/{namespace}/{name}", produces = MediaType.APPLICATION_JSON)
    public Pair<String, String> calculateAnomalies(
            @ApiParam(value = "data center",
                    example = "lhr4", allowableValues = "lhr4, ams4", required = true)
            @PathVariable(required = true) final String dc,
            @ApiParam(value = "data entity namespace, in hive case it is database",
                    example = "default", required = true)
            @PathVariable(required = true) final String namespace,
            @ApiParam(value = "data entity name, in hive case it is table",
                    example = "reservation_flatter", required = true)
            @PathVariable(required = true) final String name,
            @ApiParam(value = "Date in format yyyy-mm-dd, from which anomalies calculated. If not specified 3 months back date is used by default",
                    example = "2019-01-18", required = false)
            @RequestParam(value = "from", required = false) Optional<String> from,
            @ApiParam(value = "Date in format yyyy-mm-dd, to which anomalies calculate. If not specified current date is used.",
                    example = "2019-03-18", required = false)
            @RequestParam(value = "to", required = false) final Optional<String> to,
            @ApiParam(value = "Anomaly with which score higher thant boundary should be stored to db after calculation",
                    defaultValue = "4",
                    example = "4", required = false)
            @RequestParam(required = false)
            final Optional<Integer> boundaryScore) {
        final LocalDate fromDate = from.map(value -> LocalDate.parse(value, formatter)).orElse(LocalDate.now().minusMonths(3));
        final LocalDate toDate = to.map(value -> LocalDate.parse(value, formatter)).orElse(LocalDate.now());
        Future<List<MetricAnomaly>> anomalies = anomalyCalculationService.calculateNewAnomalies(dc, namespace, name, fromDate, toDate, boundaryScore.orElse(4));
        return ImmutablePair.of("msg",String.format(" Anomalies execution task has been created for table %s %s.%s",dc, namespace, name));
    }


    @ApiOperation(value = "Returns anomalies alert configuration.",
            nickname = "getAnomalyAlertConfigurations")
    @GetMapping(value = "/anomalies/alerts-configuration", produces = MediaType.APPLICATION_JSON)
    public List<AnomalyAlertConfigInfo> getAnomalyAlertConfigurations(
            @ApiParam(value = "Schema name", example = "nits", required = false)
            @RequestParam(value = "namespace", required = false) Optional<String> namespace,
            @ApiParam(value = "Table name", example = "features_all", required = false)
            @RequestParam(value = "name", required = false) Optional<String> name,
            @ApiParam(value = "Column name", example = "policy", required = false)
            @RequestParam(value = "column", required = false) Optional<String> column,
            @ApiParam(value = "Metric name", example = "avg", required = false)
            @RequestParam(value = "metric", required = false) Optional<String> metric) {
        return anomalyAlertConfigurationService.findAlertConfigurations(namespace,
                name,
                column,
                metric);
    }

    @ApiOperation(value = "Enable or Update alerting for anomalies in a table columns metrics",
            nickname = "setAnomalyAlertConfiguration")
    @PostMapping(value = "/anomalies/alerts-configuration/{namespace}/{name}/{column}/{metric}",
            produces = MediaType.APPLICATION_JSON)
    public AnomalyAlertConfig setAnomalyAlertConfigurations(
            @ApiParam(value = "Schema name", example = "nits", required = false)
            @PathVariable(value = "namespace", required = true) String namespace,
            @ApiParam(value = "Table name", example = "features_all", required = false)
            @PathVariable(value = "name", required = true) String name,
            @ApiParam(value = "Column name", example = "policy", required = false)
            @PathVariable(value = "column", required = true) String column,
            @ApiParam(value = "Metric name", example = "avg", required = false)
            @PathVariable(value = "metric", required = true) String metric,
            @RequestBody AnomalyAlertConfigPayload boundaryScore,
            @ApiParam(hidden = true) Authentication authentication) {
        final String loginName = AuthUtils.getLoginName(authentication).get();
        return anomalyAlertConfigurationService.createConfiguration(
                namespace, name, column,
                metric, boundaryScore.getAnomalyBoundaryScore(), loginName);
    }

    @ApiOperation(value = "Disable alerting for anomalies in a table columns metrics",
            nickname = "disableAnomalyAlertConfiguration")
    @DeleteMapping(value = "/anomalies/alerts-configuration/{namespace}/{name}/{column}/{metric}",
            produces = MediaType.APPLICATION_JSON)
    public AnomalyAlertConfig disableAnomalyAlertConfigurations(
            @ApiParam(value = "Schema name", example = "nits", required = false)
            @PathVariable(value = "namespace", required = true) String namespace,
            @ApiParam(value = "Table name", example = "features_all", required = false)
            @PathVariable(value = "name", required = true) String name,
            @ApiParam(value = "Column name", example = "policy", required = false)
            @PathVariable(value = "column", required = true) String column,
            @ApiParam(value = "Metric name", example = "avg", required = false)
            @PathVariable(value = "metric", required = true) String metric,
            @ApiParam(hidden = true) Authentication authentication) {
        final String loginName = AuthUtils.getLoginName(authentication).get();
        return anomalyAlertConfigurationService.disableAnomalyAlertConfigurations(
                namespace, name, column,
                metric,  loginName);
    }



    @ApiIgnore
    @ApiOperation(value = "Save anomalies for specific table",
            nickname = "saveAnomalies")
    @PostMapping(value = "/anomalies/save", produces = MediaType.APPLICATION_JSON)
    public ResponseEntity saveAnomalies(
            @RequestBody AnomalyInfo[] anomalyInfoList
    ) {
        try {
            anomalyDetectionService.saveAnomalies(Arrays.asList(anomalyInfoList));
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiIgnore
    @ApiOperation(value = "Save All anomalies",
            nickname = "saveAllAnomalies")
    @PostMapping(value = "/anomalies/save/all", produces = MediaType.APPLICATION_JSON)
    public ResponseEntity saveAllAnomalies(
            @RequestBody List<AnomalyInfo> anomalyInfoList
    ) {
        try {
            anomalyDetectionService.saveAllAnomalies(anomalyInfoList);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
