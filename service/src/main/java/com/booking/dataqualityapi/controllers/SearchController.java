package com.booking.dataqualityapi.controllers;

import com.booking.dataqualityapi.config.SwaggerConfig;
import com.booking.dataqualityapi.dto.ListInfo;
import com.booking.dataqualityapi.model.EntityInstance.MysqlInstance;
import com.booking.dataqualityapi.model.HiveColumnMeta;
import com.booking.dataqualityapi.services.SearchService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.MediaType;
import java.util.Optional;

@RestController
public class SearchController {
    @Autowired
    SearchService searchService;

    @ApiOperation(value = "Returns column list of hive tables",
            nickname = "searchHiveColumns",
            authorizations = {@Authorization(value = SwaggerConfig.BOOKING_AUTH)})
    @GetMapping(value = "/hive-columns/{cluster}", produces = MediaType.APPLICATION_JSON)
    public ListInfo<HiveColumnMeta> getColumns(
            @ApiParam(example = "lhr4", required = true, allowableValues = "lhr4, ams4",
                    defaultValue = "lhr4")
            @PathVariable("cluster") String cluster,
            @ApiParam(value = "Exact match is used for schema", example = "default")
            @RequestParam(value = "schema", required = false) Optional<String> schema,
            @ApiParam(value = "Exact match is used for table", example = "reservation_flatter")
            @RequestParam(value = "table", required = false) Optional<String> table,
            @ApiParam(value = "Prefix match is used for column", example = "")
            @RequestParam(value = "column", required = false) Optional<String> column,
            @RequestParam(value = "offset", required = false, defaultValue = "0") Optional<Integer> offset,
            @RequestParam(value = "limit", required = false, defaultValue = "1000") Optional<Integer> limit
    ) {
        return searchService.getColumns(cluster, schema, table, column, offset.orElse(0),
                limit.orElse(1000));
    }

    @ApiOperation(value = "Returns list of mysql tables which match to schema-table",
            nickname = "searchMysqlTables",
            authorizations = {@Authorization(value = SwaggerConfig.BOOKING_AUTH)})
    @GetMapping(value = "/mysql-tables", produces = MediaType.APPLICATION_JSON)
    public ListInfo<MysqlInstance> getMysqlTables(@ApiParam(value = "full match is used for schema", example = "bp")
                                                  @RequestParam(value = "schema", required = false) Optional<String> schema,
                                                  @ApiParam(value = "full match is used for table", example = "n")
                                                  @RequestParam(value = "table", required = false) Optional<String> table,
                                                  @RequestParam(value = "offset", required = false, defaultValue = "0") Optional<Integer> offset,
                                                  @RequestParam(value = "limit", required = false, defaultValue = "100") Optional<Integer> limit) {
        return searchService.getMysqlTables(schema, table,
                offset.orElse(0),
                limit.orElse(100));
    }
}
