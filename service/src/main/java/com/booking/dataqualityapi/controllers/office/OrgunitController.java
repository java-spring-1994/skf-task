package com.booking.dataqualityapi.controllers.office;

import com.booking.dataqualityapi.model.office.Orgunit;
import com.booking.dataqualityapi.services.OfficeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api("/orgunit")
@RestController
@RequestMapping("/orgunit")
public class OrgunitController {

    @Autowired
    private OfficeService officeService;

    @ApiOperation("Get all active orgunits info")
    @GetMapping(value = "/all_active", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Orgunit> getAllOrgunitInfo() {
        return officeService.getAllActiveOrgunits();
    }

    @ApiOperation("Get all active teams info")
    @GetMapping(value = "/all_active_teams", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Orgunit> getAllActiveTeamsInfo() {
        return officeService.getAllActiveTeams();
    }
}
