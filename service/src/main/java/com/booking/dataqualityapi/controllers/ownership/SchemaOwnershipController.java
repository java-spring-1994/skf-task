package com.booking.dataqualityapi.controllers.ownership;

import com.booking.authxclient.exceptions.AuthXClientException;
import com.booking.dataqualityapi.dto.ownership.dq.OwnershipAssignmentMeta;
import com.booking.dataqualityapi.dto.ownership.dq.SchemaEntityOwnershipInfo;
import com.booking.dataqualityapi.dto.ownership.dq.SchemaEntityOwnershipInfoDump;
import com.booking.dataqualityapi.services.ownership.OwnershipService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.IOException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api("/schema-ownership")
@RestController
@RequestMapping("/schema-ownership")
public class SchemaOwnershipController {

    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory.getLogger(SchemaOwnershipController.class);

    @Autowired
    private OwnershipService ownershipService;

    @ApiOperation("Get team ownership info for schema (namespace)")
    @GetMapping(value = "/{entityType}/{entityNamespace}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SchemaEntityOwnershipInfo> getSchemaTeamOwnership(
        @PathVariable String entityType,
        @PathVariable String entityNamespace
    ) throws AuthXClientException, InterruptedException, IOException {
        return ownershipService.getTeamSchemaOwnershipInfo(entityType, entityNamespace);
    }

    @ApiOperation(value = "Assign team ownership to schema")
    @PostMapping(value = "/{entityType}/{entityNamespace}/{orgUnitId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String assignSchemaTeamOwnership(
        @PathVariable String entityType,
        @PathVariable String entityNamespace,
        @PathVariable String orgUnitId,
        @RequestBody OwnershipAssignmentMeta body
    ) throws InterruptedException, IOException, AuthXClientException {
        return ownershipService.assignTeamSchemaOwnership(
            entityType, entityNamespace, Long.parseLong(orgUnitId),
            body.getReason(), body.getStaffId(), body.getStaffLoginname());
    }

    @ApiOperation("Get owned schemas for specific orgunit")
    @GetMapping(value = "/orgunit/{orgUnitId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SchemaEntityOwnershipInfo> getOwnedSchemaEntities(
        @PathVariable long orgUnitId
    ) throws AuthXClientException, InterruptedException, IOException {
        return ownershipService.getOwnedTeamSchemaEntities(orgUnitId);
    }

    @ApiOperation(value = "Drop team ownership for schema")
    @DeleteMapping(value = "/{entityType}/{entityNamespace}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String dropOwnership(
        @PathVariable String entityType,
        @PathVariable String entityNamespace
    ) throws InterruptedException, IOException, AuthXClientException {
        return ownershipService.dropTeamSchemaOwnership(
            entityType, entityNamespace);
    }

    @ApiOperation("Get all team schema ownership records")
    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SchemaEntityOwnershipInfoDump> getSchemaOwnershipDump()
        throws AuthXClientException, InterruptedException, IOException {
        return ownershipService.getAllSchemaOwnershipInfo();
    }
}
