package com.booking.dataqualityapi.controllers;

import com.booking.dataqualityapi.auth.AuthUtils;
import com.booking.dataqualityapi.config.SwaggerConfig;
import com.booking.dataqualityapi.dto.dqguard.DQGuardMetricPayload;
import com.booking.dataqualityapi.model.DQGuardConfig;
import com.booking.dataqualityapi.services.DQGuardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for integration with DQGuard tool.
 */
@Api("/dqguard")
@RestController
@RequestMapping("/dqguard")
public class DQGuardController {

    private final DQGuardService dqGuardService;

    @Autowired
    public DQGuardController(final DQGuardService dqGuardService) {
        this.dqGuardService = dqGuardService;
    }

    @ApiOperation("Get config for workflow")
    @GetMapping(value = "/workflow-config/{workflow}", produces = MediaType.APPLICATION_JSON_VALUE)
    public DQGuardConfig getWorkflowConfig(
        @PathVariable("workflow") String workflow
    ) {
        return dqGuardService.getConfig(workflow);
    }

    @ApiOperation(
        value = "Save config for workflow",
        authorizations = {@Authorization(value = SwaggerConfig.BOOKING_AUTH)})
    @PostMapping(value = "/workflow-config/{workflow}", produces = MediaType.APPLICATION_JSON_VALUE)
    public DQGuardConfig saveWorkflowConfig(
        @PathVariable("workflow") String workflow,
        @RequestBody String config,
        @ApiParam(hidden = true) Authentication authentication) {
        final String staffLoginName = AuthUtils.getLoginName(authentication)
            .orElseThrow(() -> new AuthenticationCredentialsNotFoundException("No staff login name found in update wiki request "));
        return dqGuardService.saveConfig(workflow, config, staffLoginName);
    }

    @ApiOperation("Check if hive table is under DQGuard monitoring within any workflow")
    @GetMapping(value = "/monitoring-status/hive/{schema}/{table}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Boolean checkIfTableIsMonitored(
        @PathVariable("schema") String schema,
        @PathVariable("table") String table
    ) throws IOException {
        return dqGuardService.checkIfTableIsMonitored(schema, table);
    }

    @ApiOperation("Check if hive table is under DQGuard monitoring within specific workflow")
    @GetMapping(value = "/monitoring-status/hive/{schema}/{table}/{workflow}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Boolean checkIfTableIsMonitoredWithinWorkflow(
        @PathVariable("schema") String schema,
        @PathVariable("table") String table,
        @PathVariable("workflow") String workflow
    ) throws IOException {
        return dqGuardService.checkIfTableIsMonitoredWithinWorkflow(schema, table, workflow);
    }

    @ApiOperation("Check if oozie workflow is integrated with DQGuard monitoring")
    @GetMapping(value = "/monitoring-status/{workflow}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Boolean checkIfWorkflowIsIntegrated(
        @PathVariable("workflow") String workflow
    ) throws IOException {
        return dqGuardService.checkIfWorkflowIsIntegrated(workflow);
    }

    @ApiOperation("Save metrics")
    @PostMapping(value = "/metrics", produces = MediaType.APPLICATION_JSON_VALUE)
    public long saveMetrics(@RequestBody final DQGuardMetricPayload[] metrics) {
        return 1;
    }
}
