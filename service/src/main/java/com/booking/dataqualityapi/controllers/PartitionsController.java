package com.booking.dataqualityapi.controllers;

import com.booking.dataqualityapi.dto.partition.PartitionListInfo;
import com.booking.dataqualityapi.dto.partition.PartitionsAnalytics;
import com.booking.dataqualityapi.dto.partition.TablePartitionsInfo;
import com.booking.dataqualityapi.services.PartitionService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class PartitionsController {


    private PartitionService partitionService;
    private static final String[] DATA_CENTERS = {"GC", "AMS4", "LHR4"};

    @Autowired
    public PartitionsController(final PartitionService partitionService) {
        this.partitionService = partitionService;
    }

    @GetMapping(value = "/partitions_overview/{schema}/{table}", produces = MediaType.APPLICATION_JSON)
    public Map<String, PartitionsAnalytics> getPartitionsOverviewPayload(
        @PathVariable String schema,
        @PathVariable String table,
        @RequestParam(value = "from", required = false) Optional<String> dateFrom,
        @RequestParam(value = "to", required = false) Optional<String> dateTo) throws SQLException {

        Map<String, PartitionsAnalytics> result = new HashMap<>();
        for (String value : DATA_CENTERS) {
            result.put(value, calculateMissedPartitions(value, schema, table, dateFrom, dateTo, Optional.empty()));
        }
        return result;
    }

    @ApiOperation(value = "returns entity partitions list by name", nickname = "getPartitions",
            notes = "returns entity partitions list sorted by name desc from offset, length",
            response = PartitionListInfo.class)
    @GetMapping(value = "/partitions/hive/{namespace}/{name}/{datacenter}", produces = MediaType.APPLICATION_JSON)
    public PartitionListInfo getPartitions(@ApiParam(value = "Entity namespace, for hive it is entity schema",  example = "default", required = true)
                                           @PathVariable(value = "namespace", required = true) String namespace,
                                           @ApiParam(value = "Entity name, for hive it is table name", example = "vp_search_log",required = true)
                                           @PathVariable(value = "name", required = true) String name,
                                           @ApiParam(value = "Data Entity datacenter", allowableValues = "AMS4, LHR4, GC",required = true)
                                           @PathVariable(value = "datacenter", required = true) String datacenter,
                                           @ApiParam(value = "part of partition name for search like %search%", required = false)
                                           @RequestParam(value = "search", required = false) Optional<String> search,
                                           @ApiParam(value = "Data Entities List offset, default value is 0", required = false)
                                           @RequestParam(value = "offset", required = false) Optional<Integer> offset,
                                           @ApiParam(value = "Data Entities List length, default value is 1000, max value is 1000", required = false)
                                           @RequestParam(value = "length", required = false) Optional<Integer> length) throws SQLException {
        return partitionService.getPartitionsListInfo(datacenter, namespace, name, search, offset, length);
    }

    @ApiOperation(value = "Finds partitions within datacenter with missed partition analytics", nickname = "calculateMissedPartitions",
            notes = "If partitions format is yyyy_mm_dd, yyyymmdd, yyyy_mm_dd/hh than missed partitions will be calculated. "
                    + "Partitions ordered by date or by name.",
            response = PartitionsAnalytics.class)
    @GetMapping(value = "/hive/{datacenter}/{schema}/{table}/partitions", produces = MediaType.APPLICATION_JSON)
    public PartitionsAnalytics calculateMissedPartitions(@ApiParam(value = "data center name",
                                                         allowableValues = "AMS4, LHR4, GC",
                                                         required = true)
                                                         @PathVariable(required = true) String datacenter,

                                                         @ApiParam(value = "Table schema name", example = "default", required = true)
                                                         @PathVariable(required = true) String schema,

                                                         @ApiParam(value = "Table name", example = "raw_events_ams_time",required = true)
                                                         @PathVariable(required = true) String table,

                                                         @ApiParam(value = "'Date from', to analise missed partitions, date format is yyyy-MM-dd")
                                                         @RequestParam(value = "from", required = false) Optional<String> dateFrom,

                                                         @ApiParam(value = "'Date to', to analise missed partitions, date format is yyyy-MM-dd")
                                                         @RequestParam(value = "to", required = false) Optional<String> dateTo,

                                                         @ApiParam(value = "part of partition name for search like %search%", required = false)
                                                         @RequestParam(value = "search", required = false) Optional<String> search)
            throws SQLException {

        return partitionService.calculateMissedPartitions(datacenter, schema, table, dateFrom, dateTo, search);
    }

    @ApiOperation(value = "Returns the first and the last partitions for table, partitions count",
            nickname = "tablePartitionsInfo",
            response = TablePartitionsInfo.class)
    @GetMapping(value = "/hive/{datacenter}/{schema}/{table}/partitions-info", produces = MediaType.APPLICATION_JSON)
    public TablePartitionsInfo getTablePartitionInfo(@ApiParam(value = "data center name, possible variants AMS4, LHR4, GC",
            required = true, example = "AMS4", allowableValues = "AMS4, LHR4, GC")
                                                     @PathVariable(required = true) String datacenter,
                                                     @ApiParam(value = "Table schema name",example = "default",
                                                             required = true)
                                                     @PathVariable(required = true) String schema,
                                                     @ApiParam(value = "Table name", example = "vp_search_log",
                                                             required = true)
                                                     @PathVariable(required = true) String table)
            throws SQLException {
        return partitionService.getTablePartitionInfo(datacenter, schema, table);
    }


}
