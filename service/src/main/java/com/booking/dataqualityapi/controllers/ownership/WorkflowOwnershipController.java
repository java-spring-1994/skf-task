package com.booking.dataqualityapi.controllers.ownership;

import com.booking.authxclient.exceptions.AuthXClientException;
import com.booking.dataqualityapi.dto.ownership.dq.OwnershipAssignmentMeta;
import com.booking.dataqualityapi.dto.ownership.dq.WorkflowOwnershipInfo;
import com.booking.dataqualityapi.dto.workflow.WorkflowTypeLowerCase;
import com.booking.dataqualityapi.services.ownership.OwnershipService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.IOException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api("/workflow-ownership")
@RestController
@RequestMapping("/workflow-ownership")
public class WorkflowOwnershipController {
    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowOwnershipController.class);

    @Autowired
    private OwnershipService ownershipService;

    @ApiOperation("Get ownership info for workflow")
    @GetMapping(value = "/{workflowType}/{workflowName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public WorkflowOwnershipInfo getWorkflowOwnership(
        @PathVariable WorkflowTypeLowerCase workflowType,
        @PathVariable String workflowName
    ) throws AuthXClientException, InterruptedException, IOException {
        return ownershipService.getWorkflowOwnershipInfo(workflowType, workflowName);
    }

    @ApiOperation(value = "Assign ownership to workflow")
    @PostMapping(value = "/{workflowType}/{workflowName}/{orgUnitId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String assignWorkflowOwnership(
        @PathVariable WorkflowTypeLowerCase workflowType,
        @PathVariable String workflowName,
        @PathVariable Long orgUnitId,
        @RequestBody OwnershipAssignmentMeta body
    ) throws InterruptedException, IOException, AuthXClientException {
        return ownershipService.assignWorkflowOwnership(
            workflowType, workflowName,
            orgUnitId, body.getReason(), body.getStaffId(), body.getStaffLoginname());
    }

    @ApiOperation("Get owned workflows for specific orgunit")
    @GetMapping(value = "/orgunit/{orgUnitId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<WorkflowOwnershipInfo> getOwnedWorkflows(
        @PathVariable long orgUnitId
    ) throws AuthXClientException, InterruptedException, IOException {
        return ownershipService.getOwnedWorkflows(orgUnitId);
    }
}
