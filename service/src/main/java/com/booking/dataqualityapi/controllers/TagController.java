package com.booking.dataqualityapi.controllers;

import com.booking.authxclient.exceptions.AuthXClientException;
import com.booking.dataqualityapi.auth.AuthUtils;
import com.booking.dataqualityapi.config.SwaggerConfig;
import com.booking.dataqualityapi.dto.AssignTagInfo;
import com.booking.dataqualityapi.dto.CreateTagInfo;
import com.booking.dataqualityapi.model.EntityTag;
import com.booking.dataqualityapi.model.Tag;
import com.booking.dataqualityapi.services.TagService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TagController {

    private final TagService tagService;

    @Autowired
    public TagController(final TagService tagService) {
        this.tagService = tagService;
    }

    @ApiOperation(value = "Create a Tag", nickname = "createTag",
            response = Tag.class, authorizations = {@Authorization(value = SwaggerConfig.BOOKING_AUTH)})
    @PostMapping(value = "/tag", produces = MediaType.APPLICATION_JSON)
    public Tag postTagContent(@Valid CreateTagInfo tagInfoForm,
                              @ApiParam(hidden = true) Authentication authentication)
            throws IOException, AuthXClientException {
        return tagService.create(tagInfoForm, AuthUtils.getStaffId(authentication));
    }

    @ApiOperation(value = "Get tags list which contains the specified string", nickname = "getTags",
            response = Tag[].class)
    @GetMapping(value = "/tags", produces = MediaType.APPLICATION_JSON)
    public List<Tag> getTags(
            @ApiParam(value = "Tag name search input field", required = false)
            @RequestParam(value = "tagsInput", required = false) final Optional<String> tagInput,
            @ApiParam(value = "Tag field input", required = false, allowableValues = "system, free_text")
            @RequestParam(value = "tagType", required = false) final Optional<String> tagType) {
        return tagService.getTags(tagInput, tagType);
    }


    @ApiOperation(value = "Get tags list which contains the specified string", nickname = "getEntityTags",
            response = Tag[].class)
    @GetMapping(value = "/tags/{entityType}/{namespace}/{name}", produces = MediaType.APPLICATION_JSON)
    public List<EntityTag> getTags(
            @ApiParam(value = "Tag field input", example = "hive", allowableValues = "hive,mysql", required = true)
            @PathVariable(value = "entityType", required = true) final String entityType,
            @ApiParam(value = "Tag field input", example = "mareshkau", required = true)
            @PathVariable(value = "namespace", required = true) final String namespace,
            @ApiParam(value = "Tag field input",
                    example = "caregorized_text_from_user_image_stream", required = true)
            @PathVariable(value = "name", required = true) final String name) {
        return tagService.getTags(entityType, namespace, name);
    }

    @ApiOperation(value = "Assign a Tag To Data Entity", nickname = "assignTagToEntity",
            response = Tag.class, authorizations = {@Authorization(value = SwaggerConfig.BOOKING_AUTH)})
    @PostMapping(value = "/tag/{entityType}/{namespace}/{name}", produces = MediaType.APPLICATION_JSON)
    public EntityTag assignTagToDataEntity(@ApiParam(value = "Entity type", example = "hive", allowableValues = "hive,mysql", required = true)
                                           @PathVariable(required = true) final String entityType,
                                           @ApiParam(value = "Entity namespace", example = "mareshkau", required = true)
                                           @PathVariable(required = true) final String namespace,
                                           @ApiParam(value = "Entity name",
                                                   example = "caregorized_text_from_user_image_stream", required = true)
                                           @PathVariable(required = true) final String name,
                                           @ApiParam(required = true, value = "Tag Assign Information")
                                           @Valid AssignTagInfo assignForm,
                                           @ApiParam(hidden = true) Authentication authentication) throws AuthXClientException {
        return tagService.addTagToDataEntity(entityType, name, namespace, assignForm,  AuthUtils.getStaffId(authentication),
                AuthUtils.getLoginName(authentication).get());
    }

    @ApiOperation(value = "Delete tag-entity relationship", nickname = "deleteTagFromEntity",
            response = Tag.class, authorizations = {@Authorization(value = SwaggerConfig.BOOKING_AUTH)})
    @DeleteMapping(value = "/tag/{tag}/{entityType}/{namespace}/{name}")
    public Tag deleteTagFromDataEntity(@ApiParam(value = "Entity type", example = "hive", allowableValues = "hive,mysql", required = true)
                                       @PathVariable(required = true) final String entityType,
                                       @ApiParam(value = "Entity namespace", example = "mareshkau", required = true)
                                       @PathVariable(required = true) final String namespace,
                                       @ApiParam(value = "Entity name",
                                               example = "caregorized_text_from_user_image_stream", required = true)
                                       @PathVariable(required = true) final String name,
                                       @ApiParam(value = "Tag Name", required = true)
                                       @PathVariable(required = true) final String tag,
                                       @ApiParam(hidden = true) Authentication authentication) throws AuthXClientException {
        return tagService.deleteTagFromEntity(entityType, namespace, name, tag,
                AuthUtils.getLoginName(authentication).get()).getTag();
    }

}
