package com.booking.dataqualityapi.controllers;

import com.booking.dataqualityapi.model.KafkaHiveMappingInfo;
import com.booking.dataqualityapi.repository.KafkaHiveMappingInfoRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;

@Api("/kafka-hive-mapping")
@RestController
@RequestMapping("/kafka-hive-mapping")
public class KafkaHiveMappingController {

    @Autowired
    KafkaHiveMappingInfoRepository kafkaHiveMappingInfoRepository;

    @ApiOperation("Get Associated Kafka Topic")
    @GetMapping(value = "/get-kafka-topic/{cluster}/{hiveDatabase}/{tableName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public KafkaHiveMappingInfo getKafkaTopic(@PathVariable("cluster") String cluster,
                                              @PathVariable("hiveDatabase") String hiveDatabase, @PathVariable("tableName") String tableName)
            throws SQLException {
        return kafkaHiveMappingInfoRepository.findKafkaHiveMappingInfoByClusterAndHiveDatabaseAndAndTableName(cluster, hiveDatabase, tableName);
        //
    }

    @ApiOperation("Get Associated Hive table")
    @GetMapping(value = "/get-hive-table/{cluster}/{federation}/{topicName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public KafkaHiveMappingInfo getHiveTable(@PathVariable("cluster") String cluster,
                                             @PathVariable("federation") String federation, @PathVariable("topicName") String topicName)
            throws SQLException {
        return kafkaHiveMappingInfoRepository.findKafkaHiveMappingInfoByClusterAndFederationAndTopicName(cluster, federation, topicName);
    }

}
