package com.booking.dataqualityapi.controllers;

import com.booking.dataqualityapi.dto.EntityDependencyInfo;
import com.booking.dataqualityapi.dto.workflow.WorkflowQueryInfo;
import com.booking.dataqualityapi.model.DQWorkflowTableMapping;
import com.booking.dataqualityapi.model.HiveTableDependency;
import com.booking.dataqualityapi.services.EntityDependencyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "API for getting incoming and ongoing dependencies for data entity")
public class EntityDependencyController {

    private static final Logger logger = LoggerFactory.getLogger(EntityDependencyController.class);

    private final EntityDependencyService dependencyService;

    @Autowired
    public EntityDependencyController(final EntityDependencyService dependencyService) {
        this.dependencyService = dependencyService;
    }

    @ApiOperation(value = "Get table incoming dependencies, ie table depends from this ...", nickname = "dependsFrom")
    @GetMapping(value = "/dependencies/hive/{datacenter}/{namespace}/{name}/incoming", produces = MediaType.APPLICATION_JSON)
    public List<HiveTableDependency> getIncomingDependencies(
            @ApiParam(value = "data center name, possible variants",
                    allowableValues = "AMS4, LHR4, GC", required = true)
            @PathVariable(required = true) String datacenter,
            @ApiParam(value = "data entity namespace, in hive case it is database",
                    example = "default", required = true)
            @PathVariable(required = true) String namespace,
            @ApiParam(value = "Entity name, in hive case it is table",
                    example = "vp_search_log", required = true)
            @PathVariable(required = true) String name) {
        return dependencyService.findTableDependsFrom(datacenter.toLowerCase(), namespace, name);
    }

    @ApiOperation(value = "Get tables which are using the table as input source", nickname = "tableUsedIn")
    @GetMapping(value = "/dependencies/hive/{datacenter}/{namespace}/{name}/outgoing", produces = MediaType.APPLICATION_JSON)
    public List<HiveTableDependency> getOutgoingDependencies(
            @ApiParam(value = "data center name", allowableValues = "AMS4, LHR4, GC",required = true)
            @PathVariable(required = true) String datacenter,
            @ApiParam(value = "data entity namespace",
                    example = "default", required = true)
            @PathVariable(required = true) String namespace,
            @ApiParam(value = "Entity name",example = "vp_search_log",required = true)
            @PathVariable(required = true) String name) {
        return dependencyService.findTableUsedIn(datacenter.toLowerCase(), namespace, name);
    }

    @ApiOperation(value = "Get query by id", nickname = "query")
    @GetMapping(value = "/dependencies/query/{id}", produces = MediaType.APPLICATION_JSON)
    public WorkflowQueryInfo getQuery(
            @ApiParam(value = "Query id",required = true)
            @PathVariable(required = true) Integer id) {
        return dependencyService.getWorkflowQuery(id);
    }

    @ApiOperation(value = "Get workflow dependencies", nickname = "entityWorkflowDependencies")
    @GetMapping(value = "/dependencies/v2/hive/{datacenter}/{namespace}/{name}", produces = MediaType.APPLICATION_JSON)
    public EntityDependencyInfo getEntityDependencyInfo(
            @ApiParam(value = "data center name", allowableValues = "AMS4, LHR4, GC",required = true)
            @PathVariable(required = true) String datacenter,
            @ApiParam(value = "data entity namespace",
                    example = "default", required = true)
            @PathVariable(required = true) String namespace,
            @ApiParam(value = "Entity name",example = "vp_search_log",required = true)
            @PathVariable(required = true) String name) {
        return dependencyService.getDependencyInfoForEntity(datacenter.toLowerCase(), namespace, name);
    }

    @ApiOperation(value = "Get workflow info", nickname = "entityWorkflowInfo")
    @GetMapping(value = "/dependencies/v2/hive/{workflowId}", produces = MediaType.APPLICATION_JSON)
    public EntityDependencyInfo getWorkFlowInfoById(
            @ApiParam(value = "workflow id",required = true)
            @PathVariable(required = true) String workflowId
    ) {
        return dependencyService.getDependencyInfoForWorkFlowId(workflowId);
    }

    @ApiOperation(value = "Get workflow dependencies", nickname = "workflowDependencies")
    @GetMapping(value = "/dependencies/v2/workflow/{datacenter}/{workflowname}", produces = MediaType.APPLICATION_JSON)
    public EntityDependencyInfo getWorkflowDependencyInfo(
            @ApiParam(value = "data center name", allowableValues = "AMS4, LHR4, GC",required = true)
            @PathVariable(required = true) String datacenter,
            @ApiParam(value = "Workflow name",example = "reservation-flatter",required = true)
            @PathVariable(required = true) String workflowname,
            @DateTimeFormat(pattern = "yyyy-MM-dd")
            @ApiParam(value = "downstram dependency last used since",
                    example = "2019-06-05", required = false)
            @RequestParam(value = "downstramDependencyLastUsed", required = false) Optional<Date> dependencyLastUsed) {
        if (dependencyLastUsed.isPresent()) {
            return dependencyService.getDependencyInfoForWorkflow(datacenter.toLowerCase(), workflowname, dependencyLastUsed.get());
        }
        return dependencyService.getDependencyInfoForWorkflow(datacenter.toLowerCase(), workflowname);
    }

    @ApiOperation(value = "Get workflow input tables", nickname = "workflowInputTables")
    @GetMapping(value = "/dependencies/v2/workflow/table/{datacenter}/{workflowName}/input", produces = MediaType.APPLICATION_JSON)
    public List<DQWorkflowTableMapping> getWorkflowInputTables(
            @ApiParam(value = "data center name", allowableValues = "AMS4, LHR4, GC",required = true)
            @PathVariable(required = true) String datacenter,
            @ApiParam(value = "Workflow name",example = "reservation-flatter",required = true)
            @PathVariable(required = true) String workflowName,
            @ApiParam(value = "updated since days ago",
                example = "1", required = false)
            @RequestParam(value = "updatedDaysAgo", required = false) Optional<Integer> updatedDaysAgo
    ) {
        logger.info("getWorkflowInputTables({},{},{})", datacenter, workflowName, updatedDaysAgo);
        if (updatedDaysAgo.isPresent()) {
            return dependencyService.getWorkflowInputTables(datacenter.toLowerCase(), workflowName, updatedDaysAgo.get());
        } else {
            return dependencyService.getWorkflowInputTables(datacenter.toLowerCase(), workflowName);
        }
    }

    @ApiOperation(value = "Get workflow output tables", nickname = "workflowOutputTables")
    @GetMapping(value = "/dependencies/v2/workflow/table/{datacenter}/{workflowName}/output", produces = MediaType.APPLICATION_JSON)
    public List<DQWorkflowTableMapping> getWorkflowOutputTables(
            @ApiParam(value = "data center name", allowableValues = "AMS4, LHR4, GC",required = true)
            @PathVariable(required = true) String datacenter,
            @ApiParam(value = "Workflow name",example = "reservation-flatter",required = true)
            @PathVariable(required = true) String workflowName,
            @ApiParam(value = "updated since days ago",
                example = "1", required = false)
            @RequestParam(value = "updatedDaysAgo", required = false) Optional<Integer> updatedDaysAgo
    ) {
        logger.info("getWorkflowOutputTables({},{},{})", datacenter, workflowName, updatedDaysAgo);
        if (updatedDaysAgo.isPresent()) {
            return dependencyService.getWorkflowOutputTables(datacenter.toLowerCase(), workflowName, updatedDaysAgo.get());
        } else {
            return dependencyService.getWorkflowOutputTables(datacenter.toLowerCase(), workflowName);
        }
    }
}
