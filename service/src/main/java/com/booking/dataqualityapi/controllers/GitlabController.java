package com.booking.dataqualityapi.controllers;

import com.booking.dataqualityapi.services.GitlabService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vfedotov on 10/7/18.
 */

@Api("/gitlab")
@RestController
@RequestMapping("/gitlab")
public class GitlabController {
    private static final Logger logger = LoggerFactory.getLogger(GitlabController.class);

    @Autowired
    private GitlabService gitlabService;

    @ApiOperation("Check that oozie workflow exists in repository")
    @GetMapping(value = "/check-oozie-workflow")
    public ResponseEntity checkOozieWorkflow(
            @RequestParam("wf_name") String wfName
    ) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        try {
            final String filePath = String.format("apps/oozie/workflows/%s/workflow.xml", wfName);
            boolean exists = gitlabService.checkFileExistsInRepo(
                    GitlabService.CORE_MAIN_PROJECT_ID,
                    filePath,
                    GitlabService.CORE_MAIN_DEFAULT_BRANCH);
            if (exists) {
                status = HttpStatus.OK;
            }
        } catch (IOException exc) {
            logger.error("Unable to check file in repository: " + exc.getMessage());
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity(status);
    }
}
