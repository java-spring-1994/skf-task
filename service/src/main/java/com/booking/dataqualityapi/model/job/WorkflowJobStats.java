package com.booking.dataqualityapi.model.job;

import java.util.Date;

public class WorkflowJobStats {
    private String id;
    private String actionName;
    private String actionType;
    private Long allocatedCpuMilliseconds;
    private Long allocatedMemoryMbMilliseconds;
    private Long occupiedCpuMilliseconds;
    private Long occupiedMemoryMbMilliseconds;
    private Long readBytes;
    private Long totalDuration;
    private Long writtenBytes;
    private Long shuffleBytes;
    private String user;
    private String workflowId;
    private String cluster;
    private String coordinatorId;
    private String coordinatorName;
    private Date startTime;
    private Date endTime;
    private Date workflowNominalTime;
    private String name;
    private String state;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public Long getAllocatedCpuMilliseconds() {
        return allocatedCpuMilliseconds;
    }

    public void setAllocatedCpuMilliseconds(Long allocatedCpuMilliseconds) {
        this.allocatedCpuMilliseconds = allocatedCpuMilliseconds;
    }

    public Long getAllocatedMemoryMbMilliseconds() {
        return allocatedMemoryMbMilliseconds;
    }

    public void setAllocatedMemoryMbMilliseconds(Long allocatedMemoryMbMilliseconds) {
        this.allocatedMemoryMbMilliseconds = allocatedMemoryMbMilliseconds;
    }

    public Long getOccupiedCpuMilliseconds() {
        return occupiedCpuMilliseconds;
    }

    public void setOccupiedCpuMilliseconds(Long occupiedCpuMilliseconds) {
        this.occupiedCpuMilliseconds = occupiedCpuMilliseconds;
    }

    public Long getOccupiedMemoryMbMilliseconds() {
        return occupiedMemoryMbMilliseconds;
    }

    public void setOccupiedMemoryMbMilliseconds(Long occupiedMemoryMbMilliseconds) {
        this.occupiedMemoryMbMilliseconds = occupiedMemoryMbMilliseconds;
    }

    public Long getReadBytes() {
        return readBytes;
    }

    public void setReadBytes(Long readBytes) {
        this.readBytes = readBytes;
    }

    public Long getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(Long totalDuration) {
        this.totalDuration = totalDuration;
    }

    public Long getWrittenBytes() {
        return writtenBytes;
    }

    public void setWrittenBytes(Long writtenBytes) {
        this.writtenBytes = writtenBytes;
    }

    public Long getShuffleBytes() {
        return shuffleBytes;
    }

    public void setShuffleBytes(Long shuffleBytes) {
        this.shuffleBytes = shuffleBytes;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(String workflowId) {
        this.workflowId = workflowId;
    }

    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public String getCoordinatorId() {
        return coordinatorId;
    }

    public void setCoordinatorId(String coordinatorId) {
        this.coordinatorId = coordinatorId;
    }

    public String getCoordinatorName() {
        return coordinatorName;
    }

    public void setCoordinatorName(String coordinatorName) {
        this.coordinatorName = coordinatorName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getWorkflowNominalTime() {
        return workflowNominalTime;
    }

    public void setWorkflowNominalTime(Date workflowNominalTime) {
        this.workflowNominalTime = workflowNominalTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}

