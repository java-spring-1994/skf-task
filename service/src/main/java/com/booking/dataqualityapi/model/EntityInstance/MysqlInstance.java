package com.booking.dataqualityapi.model.EntityInstance;

public class MysqlInstance {

    private final String schema;

    private final String table;

    private final String [] columns;

    public MysqlInstance(String schema, String table, String[] columns) {
        this.schema = schema;
        this.table = table;
        this.columns = columns;
    }

    public String getSchema() {
        return schema;
    }

    public String getTable() {
        return table;
    }

    public String[] getColumns() {
        return columns;
    }
}
