package com.booking.dataqualityapi.model.readiness;

import com.booking.dataqualityapi.dto.readiness.DataCenter;
import com.booking.dataqualityapi.dto.readiness.ReadinessStatus;

import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.GenerationType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;

import java.util.Date;

@Entity
@Table(name = "dq_data_readiness")
public class DataReadinessInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "entity_name", length = 500)
    private String name;
    @Column(name = "entity_namespace", length = 500)
    private String namespace;
    @Column(name = "entity_type", length = 500)
    private String entityType;
    @Column(name = "datacenter", columnDefinition = "enum('LHR4','AMS4','GC')")
    @Enumerated(EnumType.STRING)
    private DataCenter dataCenter;
    @Column(name = "workflow_identifier", length = 500)
    private String workflowIdentifier;
    @Column(name = "workflow_type", length = 500)
    private String workflowType;
    @Column(name = "current_materialized_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date currentMaterializedTime;
    @Column(name = "next_materialized_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date nextMaterializedTime;
    @Column(name = "data_status", columnDefinition = "enum('READY','NOT_READY', 'UNKNOWN')")
    @Enumerated(EnumType.STRING)
    private ReadinessStatus status;

    public DataReadinessInfo(){
    }

    public DataReadinessInfo(String name, String namespace, String entityType, DataCenter cluster,
                             String workflowIdentifier, String workflowType, Date currentMaterializedTime,
                             Date nextMaterializedTime, ReadinessStatus status) {
        this.name = name;
        this.namespace = namespace;
        this.entityType = entityType;
        this.dataCenter = cluster;
        this.workflowIdentifier = workflowIdentifier;
        this.workflowType = workflowType;
        this.currentMaterializedTime = currentMaterializedTime;
        this.nextMaterializedTime = nextMaterializedTime;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNamespace() {
        return namespace;
    }

    public String getEntityType() {
        return entityType;
    }

    public DataCenter getDataCenter() {
        return dataCenter;
    }

    public String getWorkflowIdentifier() {
        return workflowIdentifier;
    }

    public String getWorkflowType() {
        return workflowType;
    }

    public Date getCurrentMaterializedTime() {
        return currentMaterializedTime;
    }

    public Date getNextMaterializedTime() {
        return nextMaterializedTime;
    }

    public ReadinessStatus getStatus() {
        return status;
    }
}
