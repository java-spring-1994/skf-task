package com.booking.dataqualityapi.model.readiness;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GenerationType;
import javax.persistence.Column;

@Entity
@Table(name = "dq_data_readiness_metadata")
public class DataReadinessMetadata {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "data_readiness_id")
    private int dataReadinessId;
    @Column(name = "meta_key", length = 500)
    private String key;
    @Column(name = "meta_value", length = 500)
    private String value;

    public DataReadinessMetadata() {
    }

    public DataReadinessMetadata(int dataReadinessId, String key, String value) {
        this.dataReadinessId = dataReadinessId;
        this.key = key;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public int getDataReadinessId() {
        return dataReadinessId;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
