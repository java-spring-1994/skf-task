package com.booking.dataqualityapi.model;

public class HiveTable {
    private final String namespace;
    private final String tableName;
    private final String hdfsLocation;
    private final String tableComment;
    private final String createdBy;
    private final Long numBuckets;
    private final Long numFiles;
    private final Long tableTotalSize;
    private final Long numRowsInTable;
    private final Long tableRawDataSize;

    public HiveTable(final String namespace, final String tableName,
                     final String hdfsLocation, final String tableComment,
                     final String createdBy,
                     final Long numBuckets, final Long numFiles,
                     final Long tableTotalSize, final Long numRowsInTable,
                     final Long tableRawDataSize) {
        this.namespace = namespace;
        this.tableName = tableName;
        this.hdfsLocation = hdfsLocation;
        this.tableComment = tableComment;
        this.numBuckets = numBuckets;
        this.numFiles = numFiles;
        this.tableTotalSize = tableTotalSize;
        this.numRowsInTable = numRowsInTable;
        this.tableRawDataSize = tableRawDataSize;
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getNamespace() {
        return namespace;
    }

    public String getTableName() {
        return tableName;
    }

    public String getHdfsLocation() {
        return hdfsLocation;
    }

    public String getTableComment() {
        return tableComment;
    }

    public Long getNumBuckets() {
        return numBuckets;
    }

    public Long getNumFiles() {
        return numFiles;
    }

    public Long getTableTotalSize() {
        return tableTotalSize;
    }

    public Long getNumRowsInTable() {
        return numRowsInTable;
    }

    public Long getTableRawDataSize() {
        return tableRawDataSize;
    }
}
