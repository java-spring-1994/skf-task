package com.booking.dataqualityapi.model.office;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class Staff {
    private Integer id;

    @JsonProperty("loginname")
    private String loginName;

    private String name;

    private String email;

    @JsonProperty("off_duty")
    private boolean offDuty;

    @JsonProperty("workday_orgunit_id")
    private Integer workdayOrgunitId;

    public Staff(Integer id, String loginName, String name, String email, boolean offDuty, Integer workdayOrgunitId) {
        this.id = id;
        this.loginName = loginName;
        this.name = name;
        this.email = email;
        this.offDuty = offDuty;
        this.workdayOrgunitId = workdayOrgunitId;
    }

    public Integer getId() {
        return id;
    }

    public String getLoginName() {
        return loginName;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public boolean isOffDuty() {
        return offDuty;
    }

    public Integer getWorkdayOrgunitId() {
        return workdayOrgunitId;
    }

    public static class ResultSetExtractor implements RowMapper<Staff> {

        @Override
        public Staff mapRow(ResultSet resultSet, int i) throws SQLException {
            // to handle 'null' orgId when staff is off duty
            Integer workdayOrgunitOrgId =
                resultSet.getObject("workday_orgunit_id") == null
                    ? null : resultSet.getInt("workday_orgunit_id");
            return new Staff(
                resultSet.getObject("id", Integer.class),
                resultSet.getObject("loginname", String.class),
                resultSet.getObject("name", String.class),
                resultSet.getObject("email", String.class),
                resultSet.getObject("off_duty", Boolean.class),
                workdayOrgunitOrgId
            );
        }
    }
}
