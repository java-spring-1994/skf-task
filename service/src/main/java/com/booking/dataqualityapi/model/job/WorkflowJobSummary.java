package com.booking.dataqualityapi.model.job;

public class WorkflowJobSummary {
    private Long allocatedCpuMilliseconds;
    private Long allocatedMemoryMbMilliseconds;
    private Long occupiedCpuMilliseconds;
    private Long occupiedMemoryMbMilliseconds;
    private Long readBytes;
    private Long totalDurationMilliseconds;
    private Long writtenBytes;
    private Long shuffleBytes;
    private String coordinatorName;


    public Long getAllocatedCpuMilliseconds() {
        return allocatedCpuMilliseconds;
    }

    public void setAllocatedCpuMilliseconds(Long allocatedCpuMilliseconds) {
        this.allocatedCpuMilliseconds = allocatedCpuMilliseconds;
    }

    public Long getAllocatedMemoryMbMilliseconds() {
        return allocatedMemoryMbMilliseconds;
    }

    public void setAllocatedMemoryMbMilliseconds(Long allocatedMemoryMbMilliseconds) {
        this.allocatedMemoryMbMilliseconds = allocatedMemoryMbMilliseconds;
    }

    public Long getOccupiedCpuMilliseconds() {
        return occupiedCpuMilliseconds;
    }

    public void setOccupiedCpuMilliseconds(Long occupiedCpuMilliseconds) {
        this.occupiedCpuMilliseconds = occupiedCpuMilliseconds;
    }

    public Long getOccupiedMemoryMbMilliseconds() {
        return occupiedMemoryMbMilliseconds;
    }

    public void setOccupiedMemoryMbMilliseconds(Long occupiedMemoryMbMilliseconds) {
        this.occupiedMemoryMbMilliseconds = occupiedMemoryMbMilliseconds;
    }

    public Long getReadBytes() {
        return readBytes;
    }

    public void setReadBytes(Long readBytes) {
        this.readBytes = readBytes;
    }

    public Long getTotalDurationMilliseconds() {
        return totalDurationMilliseconds;
    }

    public void setTotalDurationMilliseconds(Long totalDurationMilliseconds) {
        this.totalDurationMilliseconds = totalDurationMilliseconds;
    }

    public Long getWrittenBytes() {
        return writtenBytes;
    }

    public void setWrittenBytes(Long writtenBytes) {
        this.writtenBytes = writtenBytes;
    }

    public Long getShuffleBytes() {
        return shuffleBytes;
    }

    public void setShuffleBytes(Long shuffleBytes) {
        this.shuffleBytes = shuffleBytes;
    }

    public String getCoordinatorName() {
        return coordinatorName;
    }

    public void setCoordinatorName(String coordinatorName) {
        this.coordinatorName = coordinatorName;
    }
}
