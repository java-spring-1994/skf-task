package com.booking.dataqualityapi.model.EntityInstance;

import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.OrderBy;
import java.util.List;

@Entity
@Table(name = "dq_hive_ams4_table_instance")
@Where(clause = "mysql_row_status = 1")
public class Ams4Instance extends HiveInstanceType<Ams4Instance.Ams4ColumnMeta> {

    @Transient
    public String datacenter = "ams4";

    @Where(clause = "mysql_row_status = 1")
    @OneToMany(targetEntity = Ams4ColumnMeta.class, fetch = FetchType.LAZY)
    @OrderBy("columnOrderIndex")
    @JoinColumns({
            @JoinColumn(name = "table_name", referencedColumnName = "table_name", updatable = false, insertable = false),
            @JoinColumn(name = "schema_name", referencedColumnName = "schema_name", updatable = false, insertable = false)
    })
    private List<Ams4ColumnMeta> columnMeta;

    public List<Ams4ColumnMeta> getColumnMeta() {
        return columnMeta;
    }

    public void setColumnMeta(List<Ams4ColumnMeta> columnMeta) {
        this.columnMeta = columnMeta;
    }

    @Entity
    @Table(name = "dq_hive_ams4_column_meta")
    static class Ams4ColumnMeta extends ColumnMetaType {}

}
