package com.booking.dataqualityapi.model;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.MappedSuperclass;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(
        name = "HiveTableDependencyMapping",
        classes = {
                @ConstructorResult(targetClass = HiveTableDependency.class, columns = {
                        @ColumnResult(name = "entity_name", type = String.class),
                        @ColumnResult(name = "entity_namespace", type = String.class),
                        @ColumnResult(name = "input_entity_name", type = String.class),
                        @ColumnResult(name = "input_entity_namespace", type = String.class),
                        @ColumnResult(name = "dc", type = String.class),
                        @ColumnResult(name = "instance_status", type = String.class),
                        @ColumnResult(name = "query_id", type = Integer.class)})}
)

@MappedSuperclass
public class HiveTableDependency {

    private final String entityName;
    private final String entityNamespace;
    private final String dc;
    private final String inputEntityName;
    private final String inputEntityNamespace;
    private final String instanceStatus;
    private final Integer queryId;


    public HiveTableDependency(final String entityName,
                               final String entityNamespace,
                               final String inputEntityName,
                               final String inputEntityNamespace,
                               final String dc,
                               final String instanceStatus,
                               final Integer queryId) {
        this.entityName = entityName;
        this.entityNamespace = entityNamespace;
        this.inputEntityName = inputEntityName;
        this.inputEntityNamespace = inputEntityNamespace;
        this.dc = dc;
        this.instanceStatus = instanceStatus;
        this.queryId = queryId;

    }

    public String getEntityName() {
        return entityName;
    }

    public String getEntityNamespace() {
        return entityNamespace;
    }

    public String getDc() {
        return dc;
    }

    public String getInputEntityName() {
        return inputEntityName;
    }

    public String getInputEntityNamespace() {
        return inputEntityNamespace;
    }

    public String getInstanceStatus() {
        return instanceStatus;
    }

    public Integer getQueryId() {
        return queryId;
    }

}
