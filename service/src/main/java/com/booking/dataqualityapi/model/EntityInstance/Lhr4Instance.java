package com.booking.dataqualityapi.model.EntityInstance;

import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.OrderBy;
import java.util.List;

@Entity
@Table(name = "dq_hive_lhr4_table_instance")
@Where(clause = "mysql_row_status = 1")
public class Lhr4Instance extends HiveInstanceType<Lhr4Instance.Lhr4ColumnMeta> {

    @Transient
    public String datacenter = "lhr4";

    @OneToMany(targetEntity = Lhr4ColumnMeta.class, fetch = FetchType.LAZY)
    @OrderBy("columnOrderIndex")
    @JoinColumns({
            @JoinColumn(name = "table_name", referencedColumnName = "table_name", updatable = false, insertable = false),
            @JoinColumn(name = "schema_name", referencedColumnName = "schema_name", updatable = false, insertable = false)
    })
    @Where(clause = "mysql_row_status = 1")
    private List<Lhr4ColumnMeta> columnMeta;

    public List<Lhr4ColumnMeta> getColumnMeta() {
        return columnMeta;
    }

    public void setColumnMeta(List<Lhr4ColumnMeta> columnMeta) {
        this.columnMeta = columnMeta;
    }

    @Entity
    @Table(name = "dq_hive_lhr4_column_meta")
    static class Lhr4ColumnMeta extends ColumnMetaType {}

}
