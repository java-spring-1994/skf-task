package com.booking.dataqualityapi.model.catalogue;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "dq_catalogue_metrics")
@Inheritance(strategy = InheritanceType.JOINED)
public class CatalogueMetric {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "`name`", length = 255, nullable = false, unique = true)
    private String name;

    @Column(name = "`display_name`", length = 255)
    private String displayName;

    @Column(name = "`description`", columnDefinition = "TEXT")
    private String description;

    @Column(name = "`type`", columnDefinition = "enum('external','derived')", nullable = false)
    private String type;

    @Column(name = "`owner`", length = 255, nullable = false)
    private String owner;

    @Column(name = "`hidden`", nullable = false, columnDefinition = "TINYINT")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean hidden;

    @Column(name = "`readiness`", nullable = false)
    private int readiness;


    @Column(name = "`mysql_row_created_at`")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;

    @Column(name = "`mysql_row_updated_at`")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public int getReadiness() {
        return readiness;
    }

    public void setReadiness(int readiness) {
        this.readiness = readiness;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public int getId() {
        return id;
    }
}
