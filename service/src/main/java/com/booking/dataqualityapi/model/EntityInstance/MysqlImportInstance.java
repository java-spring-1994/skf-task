package com.booking.dataqualityapi.model.EntityInstance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "dq_hive_mysql_mapping_info")
public class MysqlImportInstance implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "mysql_db")
    private String mysqlDB;
    @Column(name = "mysql_schema_name")
    private String mysqlSchemaName;
    @Column(name = "mysql_table_name")
    private String mysqlTableName;
    @Column(name = "hadoop_cluster_name")
    private String hadoopClusterName;
    @Column(name = "hive_schema_name")
    private String hiveSchemaName;
    @Column(name = "hive_table_name")
    private String hiveTableName;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMysqlDB() {
        return mysqlDB;
    }

    public void setMysqlDB(String mysqlDB) {
        this.mysqlDB = mysqlDB;
    }

    public String getMysqlSchemaName() {
        return mysqlSchemaName;
    }

    public void setMysqlSchemaName(String mysqlSchemaName) {
        this.mysqlSchemaName = mysqlSchemaName;
    }

    public String getMysqlTableName() {
        return mysqlTableName;
    }

    public void setMysqlTableName(String mysqlTableName) {
        this.mysqlTableName = mysqlTableName;
    }

    public String getHadoopClusterName() {
        return hadoopClusterName;
    }

    public void setHadoopClusterName(String hadoopClusterName) {
        this.hadoopClusterName = hadoopClusterName;
    }

    public String getHiveSchemaName() {
        return hiveSchemaName;
    }

    public void setHiveSchemaName(String hiveSchemaName) {
        this.hiveSchemaName = hiveSchemaName;
    }

    public String getHiveTableName() {
        return hiveTableName;
    }

    public void setHiveTableName(String hiveTableName) {
        this.hiveTableName = hiveTableName;
    }
}
