package com.booking.dataqualityapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "dq_tables_for_autopublishing_to_das")
public class DataAvailabilityConfiguration {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "`schema_name`", length = 200, nullable = false)
    private String namespace;
    @Column(name = "`table_name`", length = 200, nullable = false)
    private String name;
    @Column(name = "`interval`", columnDefinition = "enum('day','hour')", nullable = false)
    private String interval;
    @Column(name = "`mysql_row_updated_at`")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedTimestamp;

    public DataAvailabilityConfiguration() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public Date getUpdatedTimestamp() {
        return updatedTimestamp;
    }

    public void setUpdatedTimestamp(Date updatedTimestamp) {
        this.updatedTimestamp = updatedTimestamp;
    }
}
