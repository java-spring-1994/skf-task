package com.booking.dataqualityapi.model.catalogue;

import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.MapKeyColumn;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Entity
@Table(name = "dq_catalogue_external_metrics")
@PrimaryKeyJoinColumn(name = "`metric_id`")
public class CatalogueExternalMetric extends CatalogueMetric {

    @Column(name = "`source`", length = 255)
    private String source;

    @Column(name = "`endpoint`", length = 255)
    private String endpoint;

    @Column(name = "`name`", length = 255)
    private String tableName;

    @Column(name = "`namespace`", length = 255)
    private String namespace;

    @Column(name = "`column`", length = 255)
    private String column;

    @Column(name = "`aggregation`", length = 255)
    private String aggregation;

    @Column(name = "runtime_key", length = 255)
    private String runtimeKey;

    @Column(name = "`mysql_row_updated_at`")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateTime;

    @ManyToMany(mappedBy = "externalMetrics",cascade = {
            CascadeType.MERGE
    })
    private Set<CatalogueDerivedMetric> derivedMetrics = new HashSet<>();

    @ElementCollection
    @CollectionTable(name = "dq_catalogue_external_metric_filters",
            joinColumns = {@JoinColumn(name = "metric_id", referencedColumnName = "metric_id")})
    @MapKeyColumn(name = "filter_key")
    @Column(name = "filter_value")
    private Map<String,String> filterMap = new HashMap<>();

    @Column(name = "revenue_metric", nullable = false,  columnDefinition = "TINYINT")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean revenueMetric;

    public Set<CatalogueDerivedMetric> getDerivedMetrics() {
        return derivedMetrics;
    }

    public void setDerivedMetrics(Set<CatalogueDerivedMetric> derivedMetrics) {
        this.derivedMetrics = derivedMetrics;
    }

    public Map<String, String> getFilterMap() {
        return filterMap;
    }

    public void setFilterMap(Map<String, String> filterMap) {
        this.filterMap = filterMap;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getAggregation() {
        return aggregation;
    }

    public void setAggregation(String aggregation) {
        this.aggregation = aggregation;
    }

    public String getRuntimeKey() {
        return runtimeKey;
    }

    public void setRuntimeKey(String runtimeKey) {
        this.runtimeKey = runtimeKey;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void addDerivedMetric(CatalogueDerivedMetric metric) {
        derivedMetrics.add(metric);
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public boolean isRevenueMetric() {
        return revenueMetric;
    }

    public void setRevenueMetric(boolean revenueMetric) {
        this.revenueMetric = revenueMetric;
    }
}



