package com.booking.dataqualityapi.model.monitoring;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "dq_entity_column_metrics")
public class ColumnTypeMetricsConfig {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Integer id;

    @Column(name = "metric", nullable = false, length = 200)
    private String metric;

    @Column(name = "metric_name", nullable = false, length = 200)
    private String metricName;

    @Column(name = "schema_name", nullable = false, length = 200)
    private String schemaName;

    @Column(name = "table_name", nullable = false, length = 200)
    private String tableName;

    @Column(name = "column_name", nullable = false, length = 200)
    private String columnName;

    @Column(name = "mysql_row_updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;

    public String getMetric() {
        return metric;
    }

    public String getMetricName() {
        return metricName;
    }
}
