package com.booking.dataqualityapi.model;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.MappedSuperclass;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(
        name = "DQWorkflowTableMapping",
        classes = {
                @ConstructorResult(targetClass = DQWorkflowTableMapping.class, columns = {
                        @ColumnResult(name = "entity_namespace", type = String.class),
                        @ColumnResult(name = "entity_name", type = String.class),
                        @ColumnResult(name = "workflow_name", type = String.class),
                        @ColumnResult(name = "dc", type = String.class),
                        @ColumnResult(name = "table_status", type = String.class)
                })}
)

@MappedSuperclass
public class DQWorkflowTableMapping {

    private final String entityNamespace;
    private final String entityName;
    private final String workflowName;
    private final String dc;
    private final String status;

    public DQWorkflowTableMapping(String entityNamespace, String entityName, String workflowName, String dc,
                                  String tableStatus) {
        this.entityNamespace = entityNamespace;
        this.entityName = entityName;
        this.workflowName = workflowName;
        this.dc = dc;
        this.status = tableStatus;
    }

    public String getEntityNamespace() {
        return entityNamespace;
    }

    public String getEntityName() {
        return entityName;
    }

    public String getWorkflowName() {
        return workflowName;
    }

    public String getDc() {
        return dc;
    }

    public String getStatus() {
        return status;
    }
}
