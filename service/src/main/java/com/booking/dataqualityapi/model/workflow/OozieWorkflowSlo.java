package com.booking.dataqualityapi.model.workflow;

import java.util.Date;

/**
 *  oozie workflow slo dtos.
 */
public class OozieWorkflowSlo {
    private Date startTime;
    private Date endTime;
    private Date nominalTime;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getNominalTime() {
        return nominalTime;
    }

    public void setNominalTime(Date nominalTime) {
        this.nominalTime = nominalTime;
    }
}
