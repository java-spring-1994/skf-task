package com.booking.dataqualityapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.TemporalType;
import javax.persistence.Temporal;

@Entity
@Table(name = "dq_wiki_workflow_posts")
public class WikiWorkflow {
    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "content", columnDefinition = "TEXT")
    private String content;
    @Column(name = "staff_login_name")
    private String staffLoginName;
    @Column(name = "mysql_row_updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;

    @Column(name = "workflow_name")
    private String workflowName;

    public WikiWorkflow() {

    }

    public WikiWorkflow(String text, String staffLoginName, Date date, String workflow) {
        this.content = text;
        this.staffLoginName = staffLoginName;
        this.createTime = date;
        this.workflowName = workflow;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStaffLoginName() {
        return staffLoginName;
    }

    public void setStaffLoginName(String staffLoginName) {
        this.staffLoginName = staffLoginName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    public String getWorkflowName() {
        return workflowName;
    }

    public void setWorkflowName(String workflowName) {
        this.workflowName = workflowName;
    }
}
