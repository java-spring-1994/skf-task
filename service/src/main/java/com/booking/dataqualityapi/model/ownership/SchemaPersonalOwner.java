package com.booking.dataqualityapi.model.ownership;

import com.booking.dataqualityapi.dto.dataentity.DataEntity;
import com.booking.dataqualityapi.dto.dataentity.DataEntityType;
import com.booking.dataqualityapi.dto.dataentity.SchemaEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "dq_schema_personal_owners")
public class SchemaPersonalOwner {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "entity_namespace")
    private String entityNamespace;
    @Column(name = "entity_type")
    private String entityType;

    @Column(name = "staff_id")
    private Integer staffId;
    @Column(name = "staff_loginname")
    private String staffLoginName;
    @Column(name = "staff_name")
    private String staffName;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public boolean active = true;

    @Column(name = "reason", columnDefinition = "TEXT")
    private String reason;

    @Column(name = "mysql_row_updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    private Date createTime;

    public SchemaPersonalOwner() {}

    public SchemaPersonalOwner(
        String entityType, String entityNamespace, Integer staffId, String staffLoginName, String staffName, String reason
    ) {
        this.entityNamespace = entityNamespace;
        this.entityType = entityType;
        this.staffId = staffId;
        this.staffLoginName = staffLoginName;
        this.staffName = staffName;
        this.reason = reason;
    }

    public String getEntityNamespace() {
        return entityNamespace;
    }

    public void setEntityNamespace(String namespace) {
        this.entityNamespace = namespace;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    public String getStaffLoginName() {
        return staffLoginName;
    }

    public void setStaffLoginName(String staffLoginName) {
        this.staffLoginName = staffLoginName;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public SchemaEntity getSchemaEntity() {
        return new SchemaEntity(
            entityNamespace, DataEntity.DEFAULT_ENTITY_ACCOUNT, DataEntityType.valueOf(entityType.toUpperCase())
        );
    }
}
