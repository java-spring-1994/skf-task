package com.booking.dataqualityapi.model.monitoring;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "dq_metrics_to_calculate")
public class DefaultMetricsConfig {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Integer id;

    @Column(name = "column_type", nullable = false, length = 200)
    private String columnType;

    @Column(name = "metric", nullable = false, length = 200)
    private String metric;

    @Column(name = "metric_name", nullable = false, length = 200)
    private String metricName;

    @Column(name = "mysql_row_updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;

    public String getColumnType() {
        return columnType;
    }

    public String getMetric() {
        return metric;
    }

    public String getMetricName() {
        return metricName;
    }
}
