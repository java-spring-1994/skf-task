package com.booking.dataqualityapi.model.catalogue;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "dq_catalogue_derived_metrics")
@PrimaryKeyJoinColumn(name = "`metric_id`")
public class CatalogueDerivedMetric extends CatalogueMetric {

    @Column(name = "`formula`", length = 255)
    private String formula;

    @Column(name = "`mysql_row_updated_at`")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateTime;

    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(name = "dq_catalogue_derived_metric_external_metric_map",
            joinColumns = @JoinColumn(name = "derived_metric_id"),
            inverseJoinColumns = @JoinColumn(name = "external_metric_id")
    )
    private Set<CatalogueExternalMetric> externalMetrics = new HashSet<>();

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void addExternalMetric(CatalogueExternalMetric externalMetric) {
        externalMetrics.add(externalMetric);
    }

    public Set<CatalogueExternalMetric> getExternalMetrics() {
        return externalMetrics;
    }

    public void setExternalMetrics(Set<CatalogueExternalMetric> externalMetrics) {
        this.externalMetrics = externalMetrics;
    }
}
