package com.booking.dataqualityapi.model.datamodel;

import com.booking.dataqualityapi.model.DataSource;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.AttributeConverter;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Converter;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

@Entity
@Table(name = "dq_time_series_config")
public class TimeSeriesConfig {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Integer id;

    @Column(name = "date_column", length = 200)
    private String dateColumn;

    @Column(name = "date_format", length = 200)
    private String dateFormat;

    @Column(name = "date_partitioned", columnDefinition = "TINYINT", length = 200)
    private Boolean datePartitioned;

    @Column(name = "hour_column", length = 200)
    private String hourColumn;

    @Column(name = "hour_format", length = 200)
    private String hourFormat;

    @Column(name = "enum_partition_column", length = 200)
    private String enumColumn;

    @Convert(converter = StringEnumConverter.class)
    @Column(name = "enum_partition_values", length = 2000)
    private String[] enumValues = new String[0];

    @Column(name = "hour_partitioned", columnDefinition = "TINYINT", length = 200)
    private Boolean hourPartitioned;

    @Column(name = "mysql_row_updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedTime;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "entity_id")
    private DataSource dataSource;

    @JsonGetter(value = "entityId")
    public int getEntityId() {
        return dataSource.getId();
    }

    public TimeSeriesConfig() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getDateColumn() {
        return dateColumn;
    }

    public void setDateColumn(String dateColumn) {
        this.dateColumn = dateColumn;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public String getHourColumn() {
        return hourColumn;
    }

    public void setHourColumn(String hourColumn) {
        this.hourColumn = hourColumn;
    }

    public Boolean getDatePartitioned() {
        return datePartitioned;
    }

    public void setDatePartitioned(Boolean datePartitioned) {
        this.datePartitioned = datePartitioned;
    }

    public Boolean getHourPartitioned() {
        return hourPartitioned;
    }

    public void setHourPartitioned(Boolean hourPartitioned) {
        this.hourPartitioned = hourPartitioned;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public String getHourFormat() {
        return hourFormat;
    }

    public void setHourFormat(String hourFormat) {
        this.hourFormat = hourFormat;
    }

    public String getEnumColumn() {
        return enumColumn;
    }

    public void setEnumColumn(String enumColumn) {
        this.enumColumn = enumColumn;
    }

    public String[] getEnumValues() {
        return enumValues;
    }

    public void setEnumValues(String[] enumValues) {
        this.enumValues = enumValues;
    }

    //CHECKSTYLE:OFF
    @Override
    public String toString() {
        return "TimeSeriesConfig{" +
                "id=" + id +
                ", dateColumn='" + dateColumn + '\'' +
                ", dateFormat='" + dateFormat + '\'' +
                ", datePartitioned=" + datePartitioned +
                ", hourColumn='" + hourColumn + '\'' +
                ", hourFormat='" + hourFormat + '\'' +
                ", enumColumn='" + enumColumn + '\'' +
                ", enumValues=" + Arrays.toString(enumValues) +
                ", hourPartitioned=" + hourPartitioned +
                ", updatedTime=" + updatedTime +
                ", dataSource=" + dataSource +
                '}';
    }
    //CHECKSTYLE:ON

    @Converter
    public static class StringEnumConverter implements AttributeConverter<String[], String> {
        @Override
        public String convertToDatabaseColumn(String[] values) {
            return Arrays.stream(values).collect(Collectors.joining(","));
        }

        @Override
        public String[] convertToEntityAttribute(String dbData) {
            return Optional.ofNullable(dbData).map(val -> val.split(",")).orElse(new String[0]);
        }
    }
}
