package com.booking.dataqualityapi.model.workflow;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "dq_workflow_first_committer")
public class WorkflowFirstCommitterCache {
    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "workflow_git_path", length = 1000)
    private String workflowGitPath;
    @Column(name = "workflow_name", length = 100)
    private String name;

    @Column(name = "user_name", length = 100)
    private String userName;
    @Column(name = "user_email", length = 100)
    private String userEmail;

    @Column(name = "commit_epoch", length = 100)
    private Integer epoch;
    @Column(name = "commit_hash", length = 40)
    private String commitHash;

    @Column(name = "mysql_row_updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedTime;

    public WorkflowFirstCommitterCache() {}

    public WorkflowFirstCommitterCache(
        String workflowGitPath, String name,
        String userName, String userEmail,
        String commitHash, Integer epoch) {
        this.workflowGitPath = workflowGitPath;
        this.name = name;
        this.userName = userName;
        this.userEmail = userEmail;
        this.commitHash = commitHash;
        this.epoch = epoch;
    }

    public String getWorkflowGitPath() {
        return workflowGitPath;
    }

    public void setWorkflowGitPath(String workflowGitPath) {
        this.workflowGitPath = workflowGitPath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getCommitHash() {
        return commitHash;
    }

    public void setCommitHash(String commitHash) {
        this.commitHash = commitHash;
    }

    public Integer getEpoch() {
        return epoch;
    }

    public void setEpoch(Integer epoch) {
        this.epoch = epoch;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date createTime) {
        this.updatedTime = updatedTime;
    }
}
