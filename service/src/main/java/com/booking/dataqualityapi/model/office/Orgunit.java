package com.booking.dataqualityapi.model.office;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class Orgunit {

    @JsonProperty("workday_orgunit_id")
    private int workdayOrgunitId;

    @JsonProperty("workday_orgunit_name")
    private String workdayOrgunitName;

    @JsonProperty("workday_orgunit_type")
    private String workdayOrgunitType;

    @JsonProperty("parent_workday_orgunit_id")
    private Integer parentWorkdayOrgunitId;

    @JsonProperty("is_inactive")
    private boolean isInactive;

    private Orgunit(
        int workdayOrgunitId, String workdayOrgunitName, String workdayOrgunitType,
        Integer parentWorkdayOrgunitId, boolean isInactive) {
        this.workdayOrgunitId = workdayOrgunitId;
        this.workdayOrgunitName = workdayOrgunitName;
        this.workdayOrgunitType = workdayOrgunitType;
        this.parentWorkdayOrgunitId = parentWorkdayOrgunitId;
        this.isInactive = isInactive;
    }

    public int getWorkdayOrgunitId() {
        return workdayOrgunitId;
    }

    public void setWorkdayOrgunitId(int workdayOrgunitId) {
        this.workdayOrgunitId = workdayOrgunitId;
    }

    public String getWorkdayOrgunitName() {
        return workdayOrgunitName;
    }

    public void setWorkdayOrgunitName(String workdayOrgunitName) {
        this.workdayOrgunitName = workdayOrgunitName;
    }

    public String getWorkdayOrgunitType() {
        return workdayOrgunitType;
    }

    public void setWorkdayOrgunitType(String workdayOrgunitType) {
        this.workdayOrgunitType = workdayOrgunitType;
    }

    public Integer getParentWorkdayOrgunitId() {
        return parentWorkdayOrgunitId;
    }

    public void setParentWorkdayOrgunitId(Integer parentWorkdayOrgunitId) {
        this.parentWorkdayOrgunitId = parentWorkdayOrgunitId;
    }

    public boolean isInactive() {
        return isInactive;
    }

    public void setInactive(boolean inactive) {
        isInactive = inactive;
    }

    public static class ResultSetExtractor implements RowMapper<Orgunit> {

        @Override
        public Orgunit mapRow(ResultSet resultSet, int i) throws SQLException {
            // to handle 'null' values
            String workdayOrgunitType =
                resultSet.getObject("workday_orgunit_type") == null
                    ? null : resultSet.getString("workday_orgunit_type");
            Integer parentWorkdayOrgunitId =
                resultSet.getObject("parent_workday_orgunit_id") == null
                    ? null : resultSet.getInt("parent_workday_orgunit_id");
            return new Orgunit(
                resultSet.getObject("workday_orgunit_id", Integer.class),
                resultSet.getObject("workday_orgunit_name", String.class),
                workdayOrgunitType,
                parentWorkdayOrgunitId,
                resultSet.getObject("is_inactive", Integer.class) == 1
            );
        }
    }
}
