package com.booking.dataqualityapi.model.EntityInstance;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class ColumnMetaType {

    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "schema_name")
    private String namespace;

    @Column(name = "table_name")
    private String tableName;

    @Column(name = "column_name")
    private String columnName;

    @Column(name = "column_type")
    private String type;

    @Column(name = "column_comment")
    private String columnComment;

    @Column(name = "partition_col")
    private Integer partitionedBy;

    @Column(name = "column_order_idx")
    private Integer columnOrderIndex;

    @Column(name = "partition_order_idx")
    private Integer partitionOrderIndex;

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColumnComment() {
        return columnComment;
    }

    public void setColumnComment(String columnComment) {
        this.columnComment = columnComment;
    }

    public Integer getPartitionedBy() {
        return partitionedBy;
    }

    public void setPartitionedBy(Integer partitionedBy) {
        this.partitionedBy = partitionedBy;
    }

    public Integer getColumnOrderIndex() {
        return columnOrderIndex;
    }

    public void setColumnOrderIndex(Integer columnOrderIndex) {
        this.columnOrderIndex = columnOrderIndex;
    }

    public Integer getPartitionOrderIndex() {
        return partitionOrderIndex;
    }

    public void setPartitionOrderIndex(Integer partitionOrderIndex) {
        this.partitionOrderIndex = partitionOrderIndex;
    }
}

