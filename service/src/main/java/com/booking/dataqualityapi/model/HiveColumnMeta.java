package com.booking.dataqualityapi.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class HiveColumnMeta {
    private Integer id;
    private String schemaName;
    private String tableName;
    private String columnName;
    private String columnType;
    private String columnComment;
    private Integer columnOrderIdx;
    private Integer partitionCol;
    private Integer partitionOrderIdx;
    private Integer bucketCol;
    private Integer sortCol;
    private String sortOrder;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public String getColumnComment() {
        return columnComment;
    }

    public void setColumnComment(String columnComment) {
        this.columnComment = columnComment;
    }

    public Integer getColumnOrderIdx() {
        return columnOrderIdx;
    }

    public void setColumnOrderIdx(Integer columnOrderIdx) {
        this.columnOrderIdx = columnOrderIdx;
    }

    public Integer getPartitionCol() {
        return partitionCol;
    }

    public void setPartitionCol(Integer partitionCol) {
        this.partitionCol = partitionCol;
    }

    public Integer getPartitionOrderIdx() {
        return partitionOrderIdx;
    }

    public void setPartitionOrderIdx(Integer partitionOrderIdx) {
        this.partitionOrderIdx = partitionOrderIdx;
    }

    public Integer getBucketCol() {
        return bucketCol;
    }

    public void setBucketCol(Integer bucketCol) {
        this.bucketCol = bucketCol;
    }

    public Integer getSortCol() {
        return sortCol;
    }

    public void setSortCol(Integer sortCol) {
        this.sortCol = sortCol;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    /**
     * Row (dq_hive_lhr4_column_meta, dq_hive_ams4_column_meta, dq_hive_gc_column_meta,) to Class Converter.
     */
    public static class HiveColumnMetaExtractor implements RowMapper<HiveColumnMeta> {

        @Override
        public HiveColumnMeta mapRow(ResultSet resultSet, int i) throws SQLException {
            final HiveColumnMeta columnMeta = new HiveColumnMeta();
            columnMeta.setId(resultSet.getObject("id", Integer.class));
            columnMeta.setSchemaName(resultSet.getString("schema_name"));
            columnMeta.setTableName(resultSet.getString("table_name"));
            columnMeta.setColumnName(resultSet.getString("column_name"));
            columnMeta.setColumnType(resultSet.getString("column_type"));
            columnMeta.setColumnComment(resultSet.getString("column_comment"));
            columnMeta.setColumnOrderIdx(resultSet.getObject("column_order_idx", Integer.class));
            columnMeta.setPartitionCol(resultSet.getObject("partition_col", Integer.class));
            columnMeta.setPartitionOrderIdx(resultSet.getObject("partition_order_idx", Integer.class));
            columnMeta.setBucketCol(resultSet.getObject("bucket_col", Integer.class));
            columnMeta.setSortCol(resultSet.getObject("sort_col", Integer.class));
            columnMeta.setSortOrder(resultSet.getString("sort_order"));
            return columnMeta;
        }
    }
}


