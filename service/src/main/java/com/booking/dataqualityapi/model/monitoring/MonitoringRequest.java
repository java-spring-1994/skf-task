package com.booking.dataqualityapi.model.monitoring;

import com.booking.dataqualityapi.model.DataSource;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.Where;

@Entity
@Table(
    name = "dq_tables_for_monitoring",
    uniqueConstraints = {@UniqueConstraint(columnNames = {"schema_name", "table_name"})}
)
public class MonitoringRequest {
    public MonitoringRequest(
        String schemaName, String tableName, String status
    ) {
        this.schemaName = schemaName;
        this.tableName = tableName;
        this.status = status;
    }

    public MonitoringRequest() {}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Integer id;

    @Column(name = "schema_name", nullable = false, length = 200)
    private String schemaName;

    @Column(name = "table_name", nullable = false, length = 200)
    private String tableName;

    @Column(name = "mysql_row_updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;

    @Column(name = "status", columnDefinition = "enum('pending','approved', 'rejected') default 'pending'")
    private String status;

    @Column(name = "admin_note", columnDefinition = "TEXT")
    private String adminNote;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "table_name", referencedColumnName = "entity_name", updatable = false, insertable = false),
        @JoinColumn(name = "schema_name", referencedColumnName = "entity_namespace", updatable = false, insertable = false)
    })
    @Where(clause = "entity_type = 'hive-table'")
    private DataSource dataSource;

    public void setId(Integer id) {
        this.id = id;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public String getStatus() {
        return status;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public String getAdminNote() {
        return adminNote;
    }

    public void setAdminNote(String adminNote) {
        this.adminNote = adminNote;
    }
}
