package com.booking.dataqualityapi.model;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.MappedSuperclass;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(
        name = "ColumnInfoMapping",
        classes = {
                @ConstructorResult(
                        targetClass = HiveTableColumn.class,
                        columns = {
                                @ColumnResult(name = "schema_name", type = String.class),
                                @ColumnResult(name = "table_name", type = String.class),
                                @ColumnResult(name = "hdfs_location", type = String.class),
                                @ColumnResult(name = "table_comment", type = String.class),
                                @ColumnResult(name = "created_by", type = String.class),
                                @ColumnResult(name = "num_buckets", type = Long.class),
                                @ColumnResult(name = "num_files", type = Long.class),
                                @ColumnResult(name = "total_size", type = Long.class),
                                @ColumnResult(name = "num_rows", type = Long.class),
                                @ColumnResult(name = "raw_data_size", type = Long.class),
                                @ColumnResult(name = "column_name", type = String.class),
                                @ColumnResult(name = "column_type", type = String.class),
                                @ColumnResult(name = "partition_col", type = Boolean.class)})
        }
)
@MappedSuperclass
public class HiveTableColumn {
    private final HiveTable hiveTable;
    private final String columnName;
    private final String columnType;
    private final Boolean isPartitionColumn;

    public HiveTableColumn(final String namespace,
                           final String tableName,
                           final String hdfsLocation,
                           final String tableComment,
                           final String createdBy,
                           final Long numBuckets,
                           final Long numFiles,
                           final Long tableTotalSize,
                           final Long numRowsInTable,
                           final Long tableRawDataSize,
                           final String columnName,
                           final String columnType,
                           final Boolean isPartitionColumn) {
        hiveTable = new HiveTable(namespace, tableName, hdfsLocation, tableComment,
                createdBy, numBuckets, numFiles, tableTotalSize, numRowsInTable,
                tableRawDataSize);
        this.columnName = columnName;
        this.columnType = columnType;
        this.isPartitionColumn = isPartitionColumn;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getColumnType() {
        return columnType;
    }

    public Boolean getPartitionColumn() {
        return isPartitionColumn;
    }

    public HiveTable getHiveTable() {
        return hiveTable;
    }
}
