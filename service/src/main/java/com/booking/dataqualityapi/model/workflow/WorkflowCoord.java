package com.booking.dataqualityapi.model.workflow;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "dq_workflow_coord_info")
public class WorkflowCoord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "dc", columnDefinition = "enum('ams4','lhr4','gc')")
    private String dc;

    @Column(name = "coord_id", length = 100)
    private String coordId;

    @Column(name = "coord_name", length = 500)
    private String coordName;

    @Column(name = "coord_status", columnDefinition = "enum('PREP','RUNNING','SUCCEEDED','KILLED','FAILED','SUSPENDED')")
    private String coordStatus;

    @Column(name = "start_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;

    @Column(name = "end_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;

    @Column(name = "mysql_row_created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime;

    @Column(name = "mysql_row_updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDc() {
        return dc;
    }

    public void setDc(String dc) {
        this.dc = dc;
    }

    public String getCoordId() {
        return coordId;
    }

    public void setCoordId(String coordId) {
        this.coordId = coordId;
    }

    public String getCoordName() {
        return coordName;
    }

    public void setCoordName(String coordName) {
        this.coordName = coordName;
    }

    public String getCoordStatus() {
        return coordStatus;
    }

    public void setCoordStatus(String coordStatus) {
        this.coordStatus = coordStatus;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }
}
