package com.booking.dataqualityapi.repository.workflow;

import com.booking.dataqualityapi.model.job.WorkflowJobStats;
import com.booking.dataqualityapi.model.job.WorkflowJobSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Repository to work with hadoop_resources_stats.job table
 */
@Repository
public class WorkflowJobStatsRepository {

    private NamedParameterJdbcTemplate readTemplate;

    private static final String READ_QUERY_TEMPLATE =
            "SELECT id, action_name, action_type , "
                    + " allocated_cpu_milliseconds, "
                    + " allocated_memory_mb_milliseconds, "
                    + " cluster, "
                    + " coordinator_id, "
                    + " coordinator_name, "
                    + " end_time, "
                    + " name, "
                    + " occupied_cpu_milliseconds, "
                    + " occupied_memory_mb_milliseconds, "
                    + " read_bytes, "
                    + " start_time, "
                    + " state, "
                    + " total_duration, "
                    + " user, "
                    + " workflow_id, "
                    + " workflow_nominal_time, "
                    + " written_bytes, "
                    + " shuffle_bytes "
                    + " FROM job where "
                    + " job.coordinator_name=:coordinator_name "
                    + " and job.start_time >:from_time  and job.start_time < :to_time "
                    + " and ( job.cluster = 'AMS4' or job.cluster = 'LHR4' ) "
                    + "  ";

    private static final String AGGREGATE_QUERY_TEMPLATE =
            "SELECT  job.coordinator_name as coordinator_name, "
                    + "    sum(allocated_cpu_milliseconds) as allocated_cpu_milliseconds_sum, "
                    + "    sum(allocated_memory_mb_milliseconds) as allocated_memory_mb_milliseconds_sum, "
                    + "    sum(occupied_cpu_milliseconds) as occupied_cpu_milliseconds_sum, "
                    + "    sum(occupied_memory_mb_milliseconds) as occupied_memory_mb_milliseconds_sum, "
                    + "    sum(read_bytes) as read_bytes_sum, "
                    + "    sum(total_duration) as total_duration_sum, "
                    + "    sum(written_bytes) as written_bytes_sum, "
                    + "    sum(shuffle_bytes) as shuffle_bytes_sum, "
                    + "    sum(UNIX_TIMESTAMP(end_time)-UNIX_TIMESTAMP(start_time)) as duration_sec_sum "
                    + "    FROM job where "
                    + "    job.coordinator_name=:coordinator_name  "
                    + "    and job.start_time >=:from_time and job.start_time <=:to_time "
                    + "    and job.end_time >=:from_time and job.start_time <=:to_time "
                    + "    and job.coordinator_name is not null"
                    + "    and ( job.cluster = 'AMS4' or job.cluster = 'LHR4' ) "
                    + "    group by job.coordinator_name";



    @Autowired
    public WorkflowJobStatsRepository(
            @Qualifier("getMysqlHadoopResourcesStatsConnectionRo") DataSource ro) {
        readTemplate = new NamedParameterJdbcTemplate(ro);
    }


    public WorkflowJobSummary getWorkflowSummary(final Date from,
                                                 final Date to,
                                                 final String coordinatorName) {
        final StringBuilder query = new StringBuilder();
        query.append(AGGREGATE_QUERY_TEMPLATE);
        final MapSqlParameterSource select_params = new MapSqlParameterSource();
        select_params.addValue("from_time", from);
        select_params.addValue("to_time", to);
        select_params.addValue("coordinator_name", coordinatorName);
        return readTemplate.query(query.toString(), select_params, new ResultSetExtractor<WorkflowJobSummary>() {
            @Override
            public WorkflowJobSummary extractData(ResultSet resultSet) throws SQLException {
                WorkflowJobSummary jobSummary = new WorkflowJobSummary();
                if (!resultSet.next()) {
                    return jobSummary;
                }
                jobSummary.setCoordinatorName(resultSet.getString("coordinator_name"));
                jobSummary.setAllocatedCpuMilliseconds(resultSet.getLong("allocated_cpu_milliseconds_sum"));
                jobSummary.setAllocatedMemoryMbMilliseconds(resultSet.getLong("allocated_memory_mb_milliseconds_sum"));
                jobSummary.setOccupiedCpuMilliseconds(resultSet.getLong("occupied_cpu_milliseconds_sum"));
                jobSummary.setOccupiedMemoryMbMilliseconds(resultSet.getLong("occupied_memory_mb_milliseconds_sum"));
                jobSummary.setReadBytes(resultSet.getLong("read_bytes_sum"));
                jobSummary.setTotalDurationMilliseconds(resultSet.getLong("total_duration_sum"));
                jobSummary.setWrittenBytes(resultSet.getLong("written_bytes_sum"));
                jobSummary.setShuffleBytes(resultSet.getLong("shuffle_bytes_sum"));
                jobSummary.setTotalDurationMilliseconds(resultSet.getLong("duration_sec_sum") * 1000);
                return jobSummary;
            }
        });
    }

    public List<WorkflowJobStats> getJobs(final Date from,
                                          final Date to,
                                          final String coordinatorName,
                                          final Optional<String> cluster) {
        final StringBuilder query = new StringBuilder();
        query.append(READ_QUERY_TEMPLATE);
        final MapSqlParameterSource select_params = new MapSqlParameterSource();
        select_params.addValue("from_time", from);
        select_params.addValue("to_time", to);
        select_params.addValue("coordinator_name", coordinatorName);
        if (cluster.isPresent()) {
            query.append(" and job.cluster=:cluster ");
            select_params.addValue("cluster", cluster.get());
        }
        query.append(" order by start_time desc ");
        return readTemplate.query(query.toString(), select_params, new RowMapper<WorkflowJobStats>() {
            @Override
            public WorkflowJobStats mapRow(ResultSet resultSet, int i) throws SQLException {
                WorkflowJobStats jobStats = new WorkflowJobStats();
                jobStats.setId(resultSet.getString("id"));
                jobStats.setActionName(resultSet.getString("action_name"));
                jobStats.setActionType(resultSet.getString("action_type"));
                jobStats.setAllocatedCpuMilliseconds(resultSet.getLong("allocated_cpu_milliseconds"));
                jobStats.setAllocatedMemoryMbMilliseconds(resultSet.getLong("allocated_memory_mb_milliseconds"));
                jobStats.setCluster(resultSet.getString("cluster"));
                jobStats.setCoordinatorId(resultSet.getString("coordinator_id"));
                jobStats.setCoordinatorName(resultSet.getString("coordinator_name"));
                jobStats.setStartTime(resultSet.getTimestamp("start_time"));
                jobStats.setEndTime(resultSet.getTimestamp("end_time"));
                jobStats.setName(resultSet.getString("name"));
                jobStats.setOccupiedCpuMilliseconds(resultSet.getLong("occupied_cpu_milliseconds"));
                jobStats.setOccupiedMemoryMbMilliseconds(resultSet.getLong("occupied_memory_mb_milliseconds"));
                jobStats.setReadBytes(resultSet.getLong("read_bytes"));
                jobStats.setState(resultSet.getString("state"));
                jobStats.setTotalDuration(resultSet.getLong("total_duration"));
                jobStats.setUser(resultSet.getString("user"));
                jobStats.setWorkflowId(resultSet.getString("workflow_id"));
                jobStats.setWorkflowNominalTime(resultSet.getTimestamp("workflow_nominal_time"));
                jobStats.setWrittenBytes(resultSet.getLong("written_bytes"));
                jobStats.setShuffleBytes(resultSet.getLong("shuffle_bytes"));
                return jobStats;
            }
        });
    }

}
