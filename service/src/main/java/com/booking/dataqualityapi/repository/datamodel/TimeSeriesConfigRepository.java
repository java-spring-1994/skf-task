package com.booking.dataqualityapi.repository.datamodel;

import com.booking.dataqualityapi.model.datamodel.TimeSeriesConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TimeSeriesConfigRepository extends JpaRepository<TimeSeriesConfig, Integer> {
    @Query("from TimeSeriesConfig c where c.dataSource.id=:entityId")
    TimeSeriesConfig find1ByEntityId(@Param("entityId") final int entityId);

    @Query("from TimeSeriesConfig c where c.dataSource.namespace = :namespace and c.dataSource.name =:name and c.dataSource.type = 'hive-table'")
    TimeSeriesConfig find1ByEntityNamespaceAndName(@Param("namespace") final String namespace, @Param("name") final String name);
}
