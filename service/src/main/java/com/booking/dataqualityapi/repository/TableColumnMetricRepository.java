package com.booking.dataqualityapi.repository;

import com.booking.dataqualityapi.model.monitoring.TableColumnMetricsConfig;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TableColumnMetricRepository
        extends CrudRepository<TableColumnMetricsConfig, Integer> {

    @Query("select tableMetricsConfig from TableColumnMetricsConfig tableMetricsConfig "
            + " where tableMetricsConfig.schemaName = :schemaName "
            + " and tableMetricsConfig.tableName = :tableName ")
    List<TableColumnMetricsConfig> findByTableName(@Param("schemaName") final String schemaName,
                                                 @Param("tableName") final String tableName);
}
