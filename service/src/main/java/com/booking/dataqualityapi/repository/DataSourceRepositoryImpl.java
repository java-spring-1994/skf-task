package com.booking.dataqualityapi.repository;

import com.booking.dataqualityapi.model.DataSource;
import com.booking.dataqualityapi.model.DataSourceOwner;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.MapJoin;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DataSourceRepositoryImpl {

    // todo using EntityManagerFactory causes issues in findAllHiveDataSources func, need to fix
    private EntityManager em;

    @Autowired
    public DataSourceRepositoryImpl(final EntityManager em) {
        this.em = em;
    }

    // implementation of the corresponding method in DataSourceRepository interface
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public List<DataSource> findAllHiveDataSources(
            Optional<String> namespace,
            Optional<String> name,
            Optional<Integer> staffId,
            Map<String, String> labels,
            Integer limit,
            Integer offset) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<DataSource> cq = cb.createQuery(DataSource.class);

            Root<DataSource> ds = cq.from(DataSource.class);
            List<Predicate> predicates = new ArrayList<>();

            namespace.ifPresent(s -> predicates.add(cb.equal(ds.get("namespace"), s)));

            name.ifPresent(s -> predicates.add(cb.equal(ds.get("name"), s)));

            predicates.add(cb.equal(ds.get("type"),"hive-table"));

            if (staffId.isPresent()) {
                Join<DataSourceOwner, DataSource> dso = ds.join("owners");
                predicates.add(cb.equal(dso.get("staffId"), staffId.get()));
            }

            for (Map.Entry<String, String> labelEntry : labels.entrySet()) {
                MapJoin<DataSource, String, String> lc = ds.joinMap("labels");
                predicates.add(
                        cb.and(
                                cb.equal(lc.key(), labelEntry.getKey()),
                                cb.equal(lc.value(), labelEntry.getValue())
                        )
                );
            }

            cq.where(predicates.toArray(new Predicate[0]));
            return em
                    .createQuery(cq)
                    .setMaxResults(limit)
                    .setFirstResult(offset)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
