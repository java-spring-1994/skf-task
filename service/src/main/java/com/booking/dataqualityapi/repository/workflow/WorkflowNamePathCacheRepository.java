package com.booking.dataqualityapi.repository.workflow;

import com.booking.dataqualityapi.model.workflow.WorkflowNamePathCache;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Repository for oozie workflow name -> path correspondence cache.
 */

@Repository
public interface WorkflowNamePathCacheRepository extends CrudRepository<WorkflowNamePathCache, Integer> {

    @Query("from WorkflowNamePathCache c where c.name=:name and c.dc = :dc")
    WorkflowNamePathCache find1ByNameAndDc(
        @Param("name") final String name, @Param("dc") final String dc);
}