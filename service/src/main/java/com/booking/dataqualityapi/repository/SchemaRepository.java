package com.booking.dataqualityapi.repository;

import com.booking.dataqualityapi.dto.schemaOverView.SchemaOverViewEntity;
import com.booking.dataqualityapi.utils.DatacenterUtils;
import com.booking.dataqualityapi.utils.jpa.AutoClosableEntityManager;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SchemaRepository {

    private static final String GET_SCHEMA_INFO_FOR_CLUSTER =
            " SELECT t.*, p.last_partition_created, p.last_partition_updated "
                    + " FROM ( "
                    +       " SELECT * "
                    +  " FROM dq_hive_%s_table_instance "
                    +  "WHERE schema_name = :schema_name and mysql_row_status = 'ACTIVE' "
                    +  ") t"
                    + " LEFT JOIN dq_hive_%s_partition_info p "
                    + "USING (schema_name, table_name)";

    private static final String SELECT_TABLES_FOR_MYSQL_SCHEMA = "select table_name"
            + " from dq_mysql_tables_info "
            + " where schema_name = :schema_name ";

    private EntityManagerFactory emf;

    @Autowired
    public SchemaRepository(final EntityManagerFactory emf) {
        this.emf = emf;
    }


    public SchemaOverViewEntity getSchemaOverViewFromDatabse(Object[] entity, String datacenter) {
        final SchemaOverViewEntity schemaOverViewEntity = new SchemaOverViewEntity();
        schemaOverViewEntity.setSchemaName((String) entity[1]);
        schemaOverViewEntity.setTableName((String) entity[2]);
        schemaOverViewEntity.setCreatedBy((String) entity[5]);
        if (entity[12] != null) {
            schemaOverViewEntity.setTotalSize(((BigInteger)entity[12]).longValue());
        }
        schemaOverViewEntity.setDatacenter(datacenter);
        if (entity[6] != null && entity[6].getClass().equals(String.class)) {
            schemaOverViewEntity.setLastModifiedAgo(Long.parseLong((String) entity[6]));
        }
        if (entity[20] != null) {
            schemaOverViewEntity.setLastPartitionCreated((String) entity[20]);
        }
        if (entity[21] != null) {
            schemaOverViewEntity.setLastPartitionUpdated((String) entity[21]);
        }
        return  schemaOverViewEntity;
    }


    public List<SchemaOverViewEntity> getTablesOfSchema(String namespace) {
        try (AutoClosableEntityManager em = AutoClosableEntityManager.wrap(emf.createEntityManager())) {
            final Query select = em.getInstance().createNativeQuery(String.format(GET_SCHEMA_INFO_FOR_CLUSTER, "ams4", "ams4"));
            select.setParameter("schema_name", namespace);
            final List<Object[]> response = select.getResultList();
            final List<SchemaOverViewEntity> result = new ArrayList<>(response.size());
            for (Object[] entity : response) {
                result.add(getSchemaOverViewFromDatabse(entity, DatacenterUtils.Datacenter.AMS4.toString()));
            }

            final Query select2 = em.getInstance().createNativeQuery(String.format(GET_SCHEMA_INFO_FOR_CLUSTER, "lhr4", "lhr4"));
            select2.setParameter("schema_name", namespace);
            final List<Object[]> response2 = select2.getResultList();
            for (Object[] entity : response2) {
                result.add(getSchemaOverViewFromDatabse(entity, DatacenterUtils.Datacenter.LHR4.toString()));
            }
            return result;
        }
    }

    public List<SchemaOverViewEntity> getTablesForMysqlSchema(String schemaName) {
        try (AutoClosableEntityManager em = AutoClosableEntityManager.wrap(emf.createEntityManager())) {
            final Query select = em.getInstance().createNativeQuery(SELECT_TABLES_FOR_MYSQL_SCHEMA);
            select.setParameter("schema_name", schemaName);
            final List<String> response = select.getResultList();
            final List<SchemaOverViewEntity> result = new ArrayList<>();
            for (String table: response) {
                final SchemaOverViewEntity schemaOverViewEntity = new SchemaOverViewEntity();
                schemaOverViewEntity.setTableName(table);
                result.add(schemaOverViewEntity);
            }
            return result;
        }
    }
}
