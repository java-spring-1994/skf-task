package com.booking.dataqualityapi.repository;

import com.booking.dataqualityapi.model.monitoring.DefaultMetricsConfig;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DefaultMetricsConfigRepository extends JpaRepository<DefaultMetricsConfig, Integer> {

    List<DefaultMetricsConfig> findAll();
}