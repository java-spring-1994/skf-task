package com.booking.dataqualityapi.repository;

import javax.persistence.EntityManagerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DataSourceOwnerRepositoryImpl {

    private EntityManagerFactory emf;

    @Autowired
    public DataSourceOwnerRepositoryImpl(final EntityManagerFactory emf) {
        this.emf = emf;
    }

}
