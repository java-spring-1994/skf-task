package com.booking.dataqualityapi.repository.monitoring;

import com.booking.dataquality.repository.monitoring.AbstractMetricRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import javax.sql.DataSource;

@Repository
public class MetricRepository extends AbstractMetricRepository {

    @Autowired
    public MetricRepository(@Qualifier("getMysqlDqConnectionRw") DataSource rw,
                            @Qualifier("getMysqlDqConnectionRo") DataSource ro) {
        super(rw, ro);
    }
}
