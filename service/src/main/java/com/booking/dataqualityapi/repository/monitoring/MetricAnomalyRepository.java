package com.booking.dataqualityapi.repository.monitoring;

import com.booking.dataquality.repository.monitoring.AbstractMetricAnomalyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

@Repository
public class MetricAnomalyRepository extends AbstractMetricAnomalyRepository {
    @Autowired
    public MetricAnomalyRepository(
            @Qualifier("getMysqlDqConnectionRw") DataSource rw,
            @Qualifier("getMysqlDqConnectionRo") DataSource ro) {
        super(rw,ro);
    }
}
