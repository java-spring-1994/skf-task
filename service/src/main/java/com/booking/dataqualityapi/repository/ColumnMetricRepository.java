package com.booking.dataqualityapi.repository;

import com.booking.dataqualityapi.dto.monitoring.ColumnMetricInfo;
import com.booking.dataqualityapi.utils.jpa.AutoClosableEntityManager;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ColumnMetricRepository {

    private static final String GET_COLUMN_METRICS = "SELECT column_name, metric FROM dq_metrics where "
            + " schema_name =:schema_name and table_name=:table_name group by column_name, metric ";

    private EntityManagerFactory emf;

    @Autowired
    public ColumnMetricRepository(final EntityManagerFactory emf) {
        this.emf = emf;
    }


    public ColumnMetricInfo[] getColumnMetrics(String namespace, String name) {
        try (AutoClosableEntityManager em = AutoClosableEntityManager.wrap(emf.createEntityManager())) {
            final Query select = em.getInstance().createNativeQuery(GET_COLUMN_METRICS);
            select.setParameter("schema_name", namespace);
            select.setParameter("table_name", name);
            final List<Object[]> response = select.getResultList();
            final List<ColumnMetricInfo> result = new ArrayList<>(response.size());
            for (Object[] entity : response) {
                final ColumnMetricInfo column = new ColumnMetricInfo();
                column.setColumn((String) entity[0]);
                column.setMetric((String) entity[1]);
                result.add(column);
            }
            return result.toArray(new ColumnMetricInfo[0]);
        }
    }
}
