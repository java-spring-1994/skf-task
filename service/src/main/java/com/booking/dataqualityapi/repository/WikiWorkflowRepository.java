package com.booking.dataqualityapi.repository;

import com.booking.dataqualityapi.model.WikiWorkflow;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Repository for wiki entity database operations.
 */

@Repository
public interface WikiWorkflowRepository extends CrudRepository<WikiWorkflow, Integer> {

    @Query("select wiki from WikiWorkflow wiki where wiki.workflowName = :name "
            + "order by wiki.createTime desc ")
    List<WikiWorkflow> findWikiByWorkFlowName(@Param("name") final String name);
}
