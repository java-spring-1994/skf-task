package com.booking.dataqualityapi.repository;

import com.booking.dataqualityapi.model.monitoring.MonitoringRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MonitoringRequestRepository extends JpaRepository<MonitoringRequest, Integer> {

    @Query("from MonitoringRequest r where r.schemaName=:schemaName and r.tableName=:tableName")
    MonitoringRequest find1BySchemaAndName(@Param("schemaName") final String schema, @Param("tableName") final String table);

    @Query("from MonitoringRequest r where r.status='approved'")
    MonitoringRequest[] findAllByStatusApproved();
}
