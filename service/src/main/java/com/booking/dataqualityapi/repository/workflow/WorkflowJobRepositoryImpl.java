package com.booking.dataqualityapi.repository.workflow;

import com.booking.dataqualityapi.model.workflow.WorkflowSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public class WorkflowJobRepositoryImpl {

    private static final String GET_WF_SESSIONS_QUERY = " select "
            + "   wInfo.workflow_id as workflow_id, "
            + "   wInfo.workflow_status as workflow_status, "
            + "   mapping.query_log_id as query_log_id, "
            + "   wInfo.start_time as start_time, "
            + "   wInfo.end_time as end_time, "
            + "   wInfo.dc as dc, "
            + "   wInfo.nominal_time as nominal_time, "
            + "   mapping.query_type as query_type, "
            + "   wInfo.coord_id as coord_id, "
            + "   mapping.mysql_row_created_at  as query_date"
            + " from dq_workflow_info wInfo"
            + " left join dq_workflow_query_id_mapping  mapping on (wInfo.workflow_id=mapping.workflow_id)"
            + " where wInfo.workflow_name=:wf_name  "
            + " and wInfo.start_time >=:from_date and wInfo.start_time<=:to_date ";
    private static final String DC_CONDITION = " and wInfo.dc=:dc ";

    private static final String ORDER_QUERY = " order by wInfo.start_time desc, mapping.query_log_id asc; ";

    private NamedParameterJdbcTemplate readTemplate;

    @Autowired
    public WorkflowJobRepositoryImpl(
            @Qualifier("getMysqlDqConnectionRo") DataSource ro) {
        readTemplate = new NamedParameterJdbcTemplate(ro);
    }


    public List<WorkflowSession> getWorkflowSessions(final Optional<String> dc, final String wfName,
                                                     final Date from, final Date to) {
        final StringBuilder query = new StringBuilder();
        query.append(GET_WF_SESSIONS_QUERY);
        final MapSqlParameterSource select_params = new MapSqlParameterSource();
        if (dc.isPresent()) {
            query.append(DC_CONDITION);
            select_params.addValue("dc", dc.get().toLowerCase());
        }
        select_params.addValue("wf_name", wfName);
        select_params.addValue("from_date", from);
        select_params.addValue("to_date", to);
        query.append(ORDER_QUERY);
        return readTemplate.query(query.toString(), select_params, new RowMapper<WorkflowSession>() {
            @Override
            public WorkflowSession mapRow(ResultSet resultSet, int i) throws SQLException {
                WorkflowSession session = new WorkflowSession();
                session.setDc(resultSet.getString("dc"));
                session.setSessionId(resultSet.getString("workflow_id"));
                session.setCoordId(resultSet.getString("coord_id"));
                session.setStatus(resultSet.getString("workflow_status"));
                session.setQueryId(Optional.ofNullable(resultSet.getString("query_log_id")).map(Integer::valueOf).orElse(null));
                session.setQueryType(resultSet.getString("query_type"));
                session.setStartTime(resultSet.getTimestamp("start_time"));
                session.setEndTime(resultSet.getTimestamp("end_time"));
                session.setQueryDate(resultSet.getTimestamp("query_date"));
                session.setNominalTime(resultSet.getTimestamp("nominal_time"));
                return session;
            }
        });
    }

}
