package com.booking.dataqualityapi.repository.office;

import com.booking.dataqualityapi.model.office.Staff;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class StaffRepository {

    private static final String TABLE = "Staff";

    private static final String GET_STAFF_BY_ID_TEMPLATE = "select * from %s where id=?";

    private static final String GET_STAFF_BY_LOGIN_NAME_TEMPLATE = "select * from %s where loginName=?";

    private JdbcTemplate readTemplate;

    @Autowired
    public StaffRepository(
        @Qualifier("getMysqlOfficeConnectionRo") DataSource ro) {
        readTemplate = new JdbcTemplate(ro);
    }

    /**
     * Retrieve one staff record by id.
     */
    public Staff getById(final int id) {
        final String query = String.format(GET_STAFF_BY_ID_TEMPLATE, TABLE);
        return readTemplate.queryForObject(
            query, new Staff.ResultSetExtractor(),
            id
        );
    }

    /**
     * Retrieve one staff record by loginName.
     */
    public Staff getByLoginName(final String loginName) {
        final String query = String.format(GET_STAFF_BY_LOGIN_NAME_TEMPLATE, TABLE);
        return readTemplate.queryForObject(
                query, new Staff.ResultSetExtractor(),
                loginName
        );
    }
}
