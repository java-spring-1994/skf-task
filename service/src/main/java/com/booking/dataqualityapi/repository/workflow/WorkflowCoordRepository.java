package com.booking.dataqualityapi.repository.workflow;

import com.booking.dataqualityapi.model.workflow.WorkflowCoord;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorkflowCoordRepository extends CrudRepository<WorkflowCoord, Integer> {
    @Query("from WorkflowCoord workflowCoord where workflowCoord.coordName=:coordName and workflowCoord.coordStatus = 'RUNNING' ")
    List<WorkflowCoord> findRunningByCoordName(
            @Param("coordName") final String coordName);

    @Query("SELECT DISTINCT(coordName) FROM WorkflowCoord ")
    List<String> findAllWorkFlowName();

    @Query(value = "SELECT DISTINCT(coord_name) FROM dq_workflow_coord_info where coord_name LIKE %:wfName% limit 10",
            nativeQuery = true)
    List<String> findWorkFlowByName(@Param("wfName") String wfName);

    @Query(value = "SELECT DISTINCT(coord_name) FROM dq_workflow_coord_info where coord_name LIKE %:wfName% and "
            + "coord_name not IN (:exclude) limit 10", nativeQuery = true)
    List<String> findWorkFlowByNameExcluding(
            @Param("wfName") String wfName, @Param("exclude") List<String> exclude);
}
