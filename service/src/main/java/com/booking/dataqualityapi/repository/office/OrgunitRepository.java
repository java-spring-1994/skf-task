package com.booking.dataqualityapi.repository.office;

import com.booking.dataqualityapi.model.office.Orgunit;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class OrgunitRepository {

    private static final String ORGUNIT_TABLE = "Workday_OrgUnit";

    private static final String GET_ALL_ACTIVE_ORGUNITS = "select * from %s where is_inactive=0";

    private static final String GET_ALL_ACTIVE_TEAMS =
        "select * from %s where is_inactive=0 and workday_orgunit_type='Team' and parent_workday_orgunit_id is not null";

    private JdbcTemplate readTemplate;

    @Autowired
    public OrgunitRepository(
        @Qualifier("getMysqlOfficeConnectionRo") DataSource ro) {
        readTemplate = new JdbcTemplate(ro);
    }

    /**
     * Retrieve all active orgunits.
     */
    public List<Orgunit> getAllActive() {
        final String query = String.format(GET_ALL_ACTIVE_ORGUNITS, ORGUNIT_TABLE);
        return readTemplate.query(
            query, new Orgunit.ResultSetExtractor()
        );
    }

    /**
     * Retrieve all active teams.
     */
    public List<Orgunit> getAllActiveTeams() {
        final String query = String.format(GET_ALL_ACTIVE_TEAMS, ORGUNIT_TABLE);
        return readTemplate.query(
            query, new Orgunit.ResultSetExtractor()
        );
    }
}
