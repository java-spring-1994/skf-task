package com.booking.dataqualityapi.repository.readiness;

import com.booking.dataqualityapi.model.readiness.DataReadinessInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DataReadinessRepository extends JpaRepository<DataReadinessInfo, Integer> {
    @Query(value = "SELECT * FROM dq_data_readiness d WHERE d.entity_name = :name AND d.entity_namespace = :namespace "
            + "AND d.entity_type = :entity_type AND d.datacenter = :dataCenter AND d.current_materialized_time = :nominal_time"
            + " ORDER BY d.current_materialized_time DESC LIMIT 1", nativeQuery = true)
    DataReadinessInfo findReadinessByEntityTypeNameNamespaceAndCurrentMaterializedTime(@Param("entity_type") String entityType,
                                                                                       @Param("namespace") String name,
                                                                                       @Param("name") String namespace,
                                                                                       @Param("dataCenter") String dataCenter,
                                                                                       @Param("nominal_time") String nominalTime);

    @Query(value = "SELECT * FROM dq_data_readiness d WHERE d.entity_name = :name AND d.entity_namespace = :namespace "
            + "AND d.entity_type = :entity_type AND d.datacenter = :dataCenter"
            + " ORDER BY d.current_materialized_time DESC LIMIT 1", nativeQuery = true)
    DataReadinessInfo findReadinessByEntityTypeNameAndNamespace(@Param("entity_type") String entityType,
                                                                @Param("namespace") String name,
                                                                @Param("name") String namespace,
                                                                @Param("dataCenter") String dataCenter);
}
