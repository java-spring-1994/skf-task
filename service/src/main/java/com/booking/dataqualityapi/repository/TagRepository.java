package com.booking.dataqualityapi.repository;

import com.booking.dataqualityapi.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TagRepository extends JpaRepository<Tag, Integer> {

    List<Tag> findByTagIgnoreCaseContainingOrderByTagAsc(String tag);

    Tag getByTag(String tag);

    List<Tag>  findAllByOrderByTagAsc();
}
