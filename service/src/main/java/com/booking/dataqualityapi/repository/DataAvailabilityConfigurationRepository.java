package com.booking.dataqualityapi.repository;

import com.booking.dataqualityapi.model.DataAvailabilityConfiguration;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DataAvailabilityConfigurationRepository
        extends CrudRepository<DataAvailabilityConfiguration, Integer>  {

    @Query("from DataAvailabilityConfiguration cfg where cfg.namespace=:namespace and cfg.name=:name")
    DataAvailabilityConfiguration find1ByNamespaceAndName(@Param("namespace") final String namespace,
                                                  @Param("name") final String name);
}
