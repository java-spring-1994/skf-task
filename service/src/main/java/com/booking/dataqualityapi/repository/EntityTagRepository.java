package com.booking.dataqualityapi.repository;

import com.booking.dataqualityapi.model.EntityTag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EntityTagRepository extends JpaRepository<EntityTag, Integer> {

    @Query("select entityTag from EntityTag entityTag where entityTag.dataSource.namespace = :namespace "
            + "and entityTag.dataSource.name =:name and entityTag.dataSource.type =:type")
    List<EntityTag> findByNamespaceAndNameAndType(@Param("type") final String type, @Param("namespace")
            final String namespace, @Param("name") final String name);

    @Query("select entityTag from EntityTag entityTag "
            + " where entityTag.dataSource.namespace = :namespace and entityTag.dataSource.name =:name "
            + " and entityTag.tag.tag =:tag and entityTag.dataSource.type=:type")
    EntityTag find1ByTypeAndEntityAndTag(@Param("type") final String type,
                                         @Param("namespace") final String namespace,
                                         @Param("name") final String name,
                                         @Param("tag") final String tag);

}
