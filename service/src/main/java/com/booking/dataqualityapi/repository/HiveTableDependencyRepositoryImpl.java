package com.booking.dataqualityapi.repository;

import com.booking.dataqualityapi.model.HiveTableDependency;
import com.booking.dataqualityapi.utils.DatacenterUtils;
import com.booking.dataqualityapi.utils.jpa.AutoClosableEntityManager;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class HiveTableDependencyRepositoryImpl {

    private static String DC_PARAM = "dc_param";
    private static String ENTITY_NAME_PARAM = "entity_name_param";
    private static String ENTITY_NAMESPACE_PARAM = "entity_namespace_param";

    private static String INPUT_ENTITY_NAME_PARAM = "input_entity_name_param";
    private static String INPUT_ENTITY_NAMESPACE_PARAM = "input_entity_namespace_param";

    private static final String DEPENDENCIES_SELECT_SQL_FORMAT = "select "
            + " dep.entity_name as entity_name , "
            + " dep.entity_namespace as entity_namespace ,"
            + " dep.input_entity_name as input_entity_name,"
            + " dep.input_entity_namespace as input_entity_namespace,"
            + " dep.dc as dc,"
            + " dep.mysql_row_status as mysql_row_status, "
            + " dep.mysql_row_updated_at as mysql_row_updated_at, "
            + " inst.mysql_row_status as 'instance_status', "
            + " dep.query_log_id as query_id "
            + " from dq_hive_table_dependency dep ";

    private static final String INPUT_DEPENDENCIES_SELECT_SQL_FORMAT = DEPENDENCIES_SELECT_SQL_FORMAT
            + " left outer join %s inst  " // <- insert instance table here
            + " on inst.table_name=dep.input_entity_name and inst.schema_name=dep.input_entity_namespace "
            + " where "
            + " dep.mysql_row_status='ACTIVE' and  "
            + " dep.dc=:" + DC_PARAM + " and "
            + " dep.entity_name=:" + ENTITY_NAME_PARAM + " and "
            + " dep.entity_namespace=:" + ENTITY_NAMESPACE_PARAM + " ;";

    private static final String OUTPUT_DEPENDENCIES_SELECT_SQL_FORMAT = DEPENDENCIES_SELECT_SQL_FORMAT
            + " left outer join %s inst  " // <- insert instance table here
            + " on inst.table_name=dep.entity_name and inst.schema_name=dep.entity_namespace "
            + " where "
            + " dep.mysql_row_status='ACTIVE' and  "
            + " dep.dc=:" + DC_PARAM + " and "
            + " dep.input_entity_name=:" + INPUT_ENTITY_NAME_PARAM + " and "
            + " dep.input_entity_namespace=:" + INPUT_ENTITY_NAMESPACE_PARAM + " ;";


    private EntityManagerFactory emf;

    @Autowired
    public HiveTableDependencyRepositoryImpl(final EntityManagerFactory emf) {
        this.emf = emf;
    }

    public List<HiveTableDependency> inputTables(final String dc,
                                                 final String namespace,
                                                 final String name) {
        try (AutoClosableEntityManager em = AutoClosableEntityManager.wrap(emf.createEntityManager())) {
            final Query q = em.getInstance().createNativeQuery(String.format(INPUT_DEPENDENCIES_SELECT_SQL_FORMAT,
                    DatacenterUtils.getHiveInstanceTable(dc)), "HiveTableDependencyMapping");
            q.setParameter(DC_PARAM, dc);
            q.setParameter(ENTITY_NAME_PARAM, name);
            q.setParameter(ENTITY_NAMESPACE_PARAM, namespace);
            return Collections.checkedList(q.getResultList(), HiveTableDependency.class);
        }
    }


    public List<HiveTableDependency> outputTables(final String dc,
                                                  final String namespace,
                                                  final String name) {
        try (AutoClosableEntityManager em = AutoClosableEntityManager.wrap(emf.createEntityManager())) {
            final Query q = em.getInstance().createNativeQuery(String.format(OUTPUT_DEPENDENCIES_SELECT_SQL_FORMAT,
                DatacenterUtils.getHiveInstanceTable(dc)), "HiveTableDependencyMapping");
            q.setParameter(DC_PARAM, dc);
            q.setParameter(INPUT_ENTITY_NAME_PARAM, name);
            q.setParameter(INPUT_ENTITY_NAMESPACE_PARAM, namespace);
            return Collections.checkedList(q.getResultList(), HiveTableDependency.class);
        }
    }

}
