package com.booking.dataqualityapi.repository.readiness;

import com.booking.dataqualityapi.model.readiness.DataReadinessMetadata;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DataReadinessMetadataRepository extends JpaRepository<DataReadinessMetadata, Integer> {
    List<DataReadinessMetadata> findDataReadinessMetadataByDataReadinessId(int dataReadinessId);
}
