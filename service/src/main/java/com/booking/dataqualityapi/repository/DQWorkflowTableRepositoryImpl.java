package com.booking.dataqualityapi.repository;

import com.booking.dataqualityapi.model.DQWorkflowTableMapping;
import com.booking.dataqualityapi.utils.DatacenterUtils;
import com.booking.dataqualityapi.utils.jpa.AutoClosableEntityManager;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
//CHECKSTYLE:OFF
public class DQWorkflowTableRepositoryImpl {

    private static final Logger LOGGER = LoggerFactory.getLogger(DQWorkflowTableRepositoryImpl.class);

    private static String DC_PARAM = "dc_param";
    private static String WORKFLOW_PARAM = "workflow_param";
    private static String UPDATED_DAYS_AGO = "updated_days_ago";

    private static final String INPUT_DEPENDENCIES_SELECT_SQL_FORMAT = "select "
            + " input.entity_namespace as entity_namespace , "
            + " input.entity_name as entity_name ,"
            + " input.workflow_name as workflow_name,"
            + " input.dc as dc, "
            + " instance.mysql_row_status as table_status "
            + " from dq_workflow_input_table_mapping input "
            + " left join %s instance "
            + " on (instance.schema_name=input.entity_namespace and instance.table_name=input.entity_name) "
            + " where "
            + " input.mysql_row_status='ACTIVE' and  "
            + " input.dc=:" + DC_PARAM + " and "
            + " input.workflow_name=:" + WORKFLOW_PARAM + " ";


    private static final String INPUT_DEPENDENCIES_LAST_DAYS_SELECT_SQL_FORMAT = INPUT_DEPENDENCIES_SELECT_SQL_FORMAT
            + " and input.mysql_row_updated_at > date_sub(NOW(), INTERVAL :" + UPDATED_DAYS_AGO + " DAY);";

    private static final String OUTPUT_DEPENDENCIES_SELECT_SQL_FORMAT = "select "
            + " output.entity_namespace as entity_namespace , "
            + " output.entity_name as entity_name ,"
            + " output.workflow_name as workflow_name,"
            + " output.dc as dc,"
            + " instance.mysql_row_status as table_status "
            + " from dq_workflow_output_table_mapping output "
            + " left join %s instance "
            + " on (instance.schema_name=output.entity_namespace and instance.table_name=output.entity_name) "
            + " where "
            + " output.mysql_row_status='ACTIVE' and  "
            + " output.dc=:" + DC_PARAM + " and "
            + " output.workflow_name=:" + WORKFLOW_PARAM + " ";

    private static final String OUTPUT_DEPENDENCIES_LAST_DAYS_SELECT_SQL_FORMAT = OUTPUT_DEPENDENCIES_SELECT_SQL_FORMAT
            + "and output.mysql_row_updated_at > date_sub(NOW(), INTERVAL :" + UPDATED_DAYS_AGO + " DAY);";

    private static final String INSERT_SQL = "INSERT "
            + "IGNORE INTO `dq_workflow_output_table_mapping`"
            + " (entity_name, entity_namespace, dc, workflow_name, manually_assigned)"
            + " VALUES (:name,:namespace,:dc,:workflow_name,:manually_assigned)";

    private EntityManagerFactory emf;

    @Autowired
    // todo create and use a qualified entity manager factory for ro connection
    public DQWorkflowTableRepositoryImpl(final EntityManagerFactory emf) {
        this.emf = emf;
    }

    public List<DQWorkflowTableMapping> inputTables(final String dc,
                                                    final String workflowName) {
        LOGGER.info("inputTables({},{})", dc, workflowName);
        try (AutoClosableEntityManager em = AutoClosableEntityManager.wrap(emf.createEntityManager())) {
            final Query q = em.getInstance().createNativeQuery(String.format(INPUT_DEPENDENCIES_SELECT_SQL_FORMAT,
                DatacenterUtils.getHiveInstanceTable(dc)), "DQWorkflowTableMapping");
            q.setParameter(DC_PARAM, dc);
            q.setParameter(WORKFLOW_PARAM, workflowName);
            return Collections.checkedList(q.getResultList(), DQWorkflowTableMapping.class);
        }
    }

    public List<DQWorkflowTableMapping> inputTablesLastDays(
            final String dc, final String workflowName, final int lastDays
    ) {
        LOGGER.info("inputTables({},{},{})", dc, workflowName, lastDays);
        try (AutoClosableEntityManager em = AutoClosableEntityManager.wrap(emf.createEntityManager())) {
            final Query q = em.getInstance().createNativeQuery(
                String.format(INPUT_DEPENDENCIES_LAST_DAYS_SELECT_SQL_FORMAT,DatacenterUtils.getHiveInstanceTable(dc))
                ,"DQWorkflowTableMapping"
            );
            q.setParameter(DC_PARAM, dc);
            q.setParameter(WORKFLOW_PARAM, workflowName);
            q.setParameter(UPDATED_DAYS_AGO, lastDays);
            return Collections.checkedList(q.getResultList(), DQWorkflowTableMapping.class);
        }
    }

    public List<DQWorkflowTableMapping> outputTables(final String dc,
                                                     final String workflowName) {
        LOGGER.info("outputTables({},{})", dc, workflowName);
        try (AutoClosableEntityManager em = AutoClosableEntityManager.wrap(emf.createEntityManager())) {
            final Query q = em.getInstance().createNativeQuery(String.format(OUTPUT_DEPENDENCIES_SELECT_SQL_FORMAT,
                    DatacenterUtils.getHiveInstanceTable(dc)),
                    "DQWorkflowTableMapping");
            q.setParameter(DC_PARAM, dc);
            q.setParameter(WORKFLOW_PARAM, workflowName);
            return Collections.checkedList(q.getResultList(), DQWorkflowTableMapping.class);
        }
    }

    public List<DQWorkflowTableMapping> outputTablesLastDays(
            final String dc, final String workflowName, final int lastDays
    ) {
        LOGGER.info("outputTables({},{},{})", dc, workflowName, lastDays);
        try (AutoClosableEntityManager em = AutoClosableEntityManager.wrap(emf.createEntityManager())) {
            final Query q = em.getInstance().createNativeQuery(
                    String.format(OUTPUT_DEPENDENCIES_LAST_DAYS_SELECT_SQL_FORMAT,
                            DatacenterUtils.getHiveInstanceTable(dc))
                    ,"DQWorkflowTableMapping"
            );
            q.setParameter(DC_PARAM, dc);
            q.setParameter(WORKFLOW_PARAM, workflowName);
            q.setParameter(UPDATED_DAYS_AGO, lastDays);
            return Collections.checkedList(q.getResultList(), DQWorkflowTableMapping.class);
        }
    }
}
//CHECKSTYLE:ON
