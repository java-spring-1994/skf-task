package com.booking.dataqualityapi.repository.alerting;

import com.booking.dataquality.repository.alerting.AnomalyAlertConfigurationRepositoryInterface;
import org.springframework.stereotype.Repository;

@Repository
public interface AnomalyAlertConfigurationRepository extends AnomalyAlertConfigurationRepositoryInterface{

}
