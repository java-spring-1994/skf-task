package com.booking.dataqualityapi.repository;

import com.booking.dataqualityapi.model.KafkaHiveMappingInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface KafkaHiveMappingInfoRepository extends JpaRepository<KafkaHiveMappingInfo, Integer> {

    KafkaHiveMappingInfo findKafkaHiveMappingInfoByClusterAndHiveDatabaseAndAndTableName(
           @Param("cluster") final String cluster, @Param("hiveDatabase") final String hiveDatabase, @Param("tableName") final String tableName);

    KafkaHiveMappingInfo findKafkaHiveMappingInfoByClusterAndFederationAndTopicName(
            @Param("cluster") final String cluster, @Param("federation") final String federation, @Param("topicName") final String topicName);
}
