package com.booking.dataqualityapi.repository;

import com.booking.dataqualityapi.model.DataSourceOwner;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DataSourceOwnerRepository extends CrudRepository<DataSourceOwner, Integer> {

}
