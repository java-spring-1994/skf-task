package com.booking.dataqualityapi.repository;

import com.booking.dataqualityapi.model.DQGuardConfig;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Repository for DQGuard config.
 */
@Repository
public interface DQGuardConfigRepository extends CrudRepository<DQGuardConfig, Integer> {

    @Query("from DQGuardConfig c where c.workflow=:workflow")
    DQGuardConfig find1ByWorkflow(@Param("workflow") final String workflow);
}
