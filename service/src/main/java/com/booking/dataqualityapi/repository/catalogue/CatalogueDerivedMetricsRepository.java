package com.booking.dataqualityapi.repository.catalogue;

import com.booking.dataqualityapi.model.catalogue.CatalogueDerivedMetric;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CatalogueDerivedMetricsRepository extends JpaRepository<CatalogueDerivedMetric, Integer> {
    Optional<CatalogueDerivedMetric> findOneByName(final String name);
}
