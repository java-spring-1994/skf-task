package com.booking.dataqualityapi.repository;

import com.booking.dataqualityapi.model.EntityInstance.MysqlInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
public class MysqlTableRepository {

    private NamedParameterJdbcTemplate readTemplate;

    private static final String SELECT_TEMPLATE = "select schema_name, "
            + " table_name, "
            + " columns"
            + " from dq_mysql_tables_info "
            + " where 1=1 and mysql_row_status='ACTIVE' ";



    @Autowired
    public MysqlTableRepository(@Qualifier("getMysqlDqConnectionRo") DataSource ro) {
        readTemplate = new NamedParameterJdbcTemplate(ro);
    }

    public List<MysqlInstance> searchTableList(final Optional<String> schema,
                                               final Optional<String> table,
                                               final int offset,
                                               final int limit) {
        final StringBuilder query = new StringBuilder();
        query.append(SELECT_TEMPLATE);
        final MapSqlParameterSource selectParams = new MapSqlParameterSource();
        buildExactMatchCondition(selectParams, query, schema, table);
        query.append(" limit :offset_param, :limit_param ");

        selectParams.addValue("offset_param", offset);
        selectParams.addValue("limit_param", limit);


        return readTemplate.query(query.toString(), selectParams, new RowMapper<MysqlInstance>() {
            @Override
            public MysqlInstance mapRow(ResultSet resultSet, int i) throws SQLException {
                MysqlInstance session = new MysqlInstance(
                        resultSet.getString("schema_name"),
                        resultSet.getString("table_name"),
                        resultSet.getString("columns").split(",")
                );
                return session;
            }
        });
    }

    private void buildExactMatchCondition(final MapSqlParameterSource selectParams,
                                          final StringBuilder query,
                                          final Optional<String> schema,
                                          final Optional<String> table) {
        if (schema.isPresent()) {
            query.append(" and  schema_name = :schema_param ");
            selectParams.addValue("schema_param", schema.get().toLowerCase());
        }
        if (table.isPresent()) {
            query.append(" and  table_name = :name_param ");
            selectParams.addValue("name_param", table.get().toLowerCase());
        }
    }


}
