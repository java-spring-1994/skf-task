package com.booking.dataqualityapi.repository.catalogue;

import com.booking.dataqualityapi.model.catalogue.CatalogueMetric;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CatalogueMetricsRepository extends JpaRepository<CatalogueMetric, Integer> {

    Optional<CatalogueMetric> findOneByName(final String name);

}
