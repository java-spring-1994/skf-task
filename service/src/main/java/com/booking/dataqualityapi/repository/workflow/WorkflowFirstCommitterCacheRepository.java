package com.booking.dataqualityapi.repository.workflow;

import com.booking.dataqualityapi.model.workflow.WorkflowFirstCommitterCache;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface WorkflowFirstCommitterCacheRepository extends CrudRepository<WorkflowFirstCommitterCache, Integer> {
    @Query("from WorkflowFirstCommitterCache cache where cache.workflowGitPath = :workflowGitPath")
    WorkflowFirstCommitterCache getOneByGitPath(@Param("workflowGitPath") final String workflowGitPath);
}
