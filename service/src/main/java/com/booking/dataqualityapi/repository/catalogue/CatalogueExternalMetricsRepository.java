package com.booking.dataqualityapi.repository.catalogue;

import com.booking.dataqualityapi.model.catalogue.CatalogueExternalMetric;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CatalogueExternalMetricsRepository extends JpaRepository<CatalogueExternalMetric, Integer> {

    Optional<CatalogueExternalMetric> findOneByName(final String name);
}
