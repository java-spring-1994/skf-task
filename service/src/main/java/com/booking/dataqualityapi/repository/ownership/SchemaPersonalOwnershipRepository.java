package com.booking.dataqualityapi.repository.ownership;

import com.booking.dataqualityapi.model.ownership.SchemaPersonalOwner;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SchemaPersonalOwnershipRepository extends CrudRepository<SchemaPersonalOwner, Integer> {

    SchemaPersonalOwner findByEntityTypeAndEntityNamespace(String entityType, String entityNamespace);

    SchemaPersonalOwner findByEntityTypeAndEntityNamespaceAndActiveTrue(String entityType, String entityNamespace);

    List<SchemaPersonalOwner> findByStaffId(int staffId);
}
