package com.booking.dataqualityapi.repository.workflow;

import com.booking.dataqualityapi.model.workflow.WorkflowQuery;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

public interface WorkflowQueryRepository extends Repository<WorkflowQuery, Integer> {
    @Query("select query from WorkflowQuery query where query.id = :id")
    WorkflowQuery getQueryById(@Param("id") final Integer id);
}
