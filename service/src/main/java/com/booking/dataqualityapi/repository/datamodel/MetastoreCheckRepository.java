package com.booking.dataqualityapi.repository.datamodel;

import com.booking.dataquality.model.datamodel.MetastoreCheck;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MetastoreCheckRepository extends JpaRepository<MetastoreCheck, Integer> {
    @Query("from MetastoreCheck m where m.entityId=:entityId")
    List<MetastoreCheck> findAllByEntityId(@Param("entityId") final int entityId);

    @Query("from MetastoreCheck m where m.entityId=:entityId and m.status='active'")
    List<MetastoreCheck> findAllActiveByEntityId(@Param("entityId") final int entityId);

    @Query("from MetastoreCheck m where m.entityId=:entityId and m.ruleId=:ruleId")
    MetastoreCheck findOneByEntityIdAndRuleId(
        @Param("entityId") final int entityId,
        @Param("ruleId") final int ruleId
    );
}
