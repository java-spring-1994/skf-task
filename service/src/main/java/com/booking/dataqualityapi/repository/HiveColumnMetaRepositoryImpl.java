package com.booking.dataqualityapi.repository;

import com.booking.dataqualityapi.model.HiveColumnMeta;
import com.booking.dataqualityapi.utils.DatacenterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Repository
public class HiveColumnMetaRepositoryImpl {

    private static final String GET_PARTITION_COLUMNS_TEMPLATE = "select * from  %s "
            + "where schema_name=? and table_name=? "
            + "and partition_col = 1 "
            + "and mysql_row_status='ACTIVE' "
            + "order by partition_order_idx ";

    private static final String SEARCH_QUERY_TEMPLATE = "select * from %s "
            + " where mysql_row_status='ACTIVE' ";

    private JdbcTemplate readTemplate;

    @Autowired
    public HiveColumnMetaRepositoryImpl(@Qualifier("getMysqlDqConnectionRo") DataSource ro) {
        this.readTemplate = new JdbcTemplate(ro);
    }

    /**
     *  Returns partition columns ordered by partition index.
     */

    public List<HiveColumnMeta> getOrderedPartitionColumns(final String dc,
                                                           final String schemaName,
                                                           final String tableName) {
        final String query = String.format(GET_PARTITION_COLUMNS_TEMPLATE, DatacenterUtils.getHiveColumnMeta(dc));
        return readTemplate.query(query,
                new String[]{schemaName, tableName},
                new HiveColumnMeta.HiveColumnMetaExtractor());
    }

    public List<HiveColumnMeta> getHiveTableColumns(final String dc,
                                                    final Optional<String> schemaName,
                                                    final Optional<String> tableName,
                                                    final Optional<String> columnName,
                                                    final Integer offset,
                                                    final Integer limit) {
        final StringBuilder query = new StringBuilder();
        query.append(String.format(SEARCH_QUERY_TEMPLATE, DatacenterUtils.getHiveColumnMeta(dc)));
        List<String> parameters = new LinkedList<>();
        if (schemaName.isPresent()) {
            query.append(" and schema_name=? ");
            parameters.add(schemaName.get());
        }
        if (tableName.isPresent()) {
            query.append(" and table_name=? ");
            parameters.add(tableName.get());
        }
        if (columnName.isPresent()) {
            query.append(" and column_name like ? ");
            parameters.add(columnName.get() + "%");
        }
        query.append(" limit ?, ?");
        parameters.add(offset + "");
        parameters.add(limit + "");
        return readTemplate.query(query.toString(),
                parameters.toArray(new String[0]),
                new HiveColumnMeta.HiveColumnMetaExtractor());
    }
}
