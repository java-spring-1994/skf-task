package com.booking.dataqualityapi.repository;

import com.booking.dataqualityapi.dto.partition.PartitionInfo;
import com.booking.dataqualityapi.dto.partition.PartitionListInfo;
import com.booking.dataqualityapi.dto.partition.TablePartitionsInfo;
import com.booking.dataqualityapi.utils.DatacenterUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class HiveTablePartitionRepositoryImpl {
    private static final String BASIC_PARTITION_QUERY_TEMPLATE = "select "
            + "PART_NAME as  PART_NAME, "
            + "COLUMN_STATS_ACCURATE  as COLUMN_STATS_ACCURATE, "
            + "NUM_FILES as NUM_FILES, "
            + "NUM_ROWS as  NUM_ROWS, "
            + "RAW_DATA_SIZE as RAW_DATA_SIZE, "
            + "TOTAL_SIZE as TOTAL_SIZE, "
            + "TRANSIENT_LAST_DDL_TIME as TRANSIENT_LAST_DDL_TIME "
            + "from %s where TBL_NAME = ? and SCHEME = ?  and mysql_row_status = 'ACTIVE' ";
    private static final String COUNT_PARTITION_QUERY_TEMPLATE = "select COUNT(1) as PART_COUNT, "
           + "      SUM(NUM_FILES) as NUM_FILES_SUM, "
           + "      SUM(TOTAL_SIZE) as TOTAL_SIZE_SUM, "
           + " AVG(case when NUM_ROWS > 0 then NUM_ROWS else 0  end) as NUM_ROWS_AVG, "
           + " AVG(case when RAW_DATA_SIZE > 0 then RAW_DATA_SIZE else 0 end) as RAW_DATA_SIZE_AVG "
           + " from %s where TBL_NAME = ? and SCHEME = ? and mysql_row_status = 'ACTIVE' ";

    private JdbcTemplate readTemplate;


    @Autowired
    public HiveTablePartitionRepositoryImpl(@Qualifier("getMysqlDqConnectionRo") DataSource ro) {

        this.readTemplate = new JdbcTemplate(ro);
    }

    private static final Pair<StringBuilder, List<Object>> partitionQuery(final String query,
                                                                          String schema, String table, Optional<String> search) {
        final StringBuilder sql = new StringBuilder(query);
        List<Object> params = new ArrayList<>();
        params.add(table);
        params.add(schema);
        if (search.isPresent() && search.get().length() > 0) {
            sql.append(" and PART_NAME like ? ");
            params.add("%" + search.get() + "%");
        }

        return Pair.of(sql, params);
    }


    public List<PartitionInfo> getPartitions(String datacenter,
                                             String schema,
                                             String table,
                                             Optional<String> search,
                                             Optional<Integer> limit) throws SQLException {
        return getPartitions(datacenter, schema, table, Sort.Direction.DESC, search, limit);
    }


    public List<PartitionInfo> getPartitions(String datacenter,
                                             String schema,
                                             String table,
                                             Sort.Direction direction,
                                             Optional<String> search,
                                             Optional<Integer> limit) throws SQLException {

        Pair<StringBuilder, List<Object>> query = partitionQuery(String.format(BASIC_PARTITION_QUERY_TEMPLATE,
                DatacenterUtils.getHivePartitionsList(datacenter)), schema, table, search);
        query.getLeft().append(" ORDER BY PART_NAME ");
        if (direction.isDescending()) {
            query.getLeft().append(" DESC ");
        } else {
            query.getLeft().append(" ASC ");
        }
        query.getKey().append("  LIMIT ?  ");
        query.getRight().add(limit.orElse(80));
        return getPreparedQuery(query);
    }

    public List<PartitionInfo> getPartitions(String datacenter,
                                             String schema,
                                             String table,
                                             Optional<String> search,
                                             List<String> partitionNames) throws SQLException {
        Pair<StringBuilder, List<Object>> queryAndParams = partitionQuery(String.format(BASIC_PARTITION_QUERY_TEMPLATE,
                DatacenterUtils.getHivePartitionsList(datacenter)), schema, table, search);

        if (partitionNames.size() > 0) {
            queryAndParams.getLeft().append(" and PART_NAME IN ( ");
            for (int i = 0; i < partitionNames.size() - 1; i++) {
                queryAndParams.getLeft().append(" ?, ");
                queryAndParams.getRight().add(partitionNames.get(i));
            }
            queryAndParams.getLeft().append(" ? ) ");
            queryAndParams.getRight().add(partitionNames.get(partitionNames.size() - 1));
        }
        return getPreparedQuery(queryAndParams);

    }

    private List<PartitionInfo> getPreparedQuery(Pair<StringBuilder, List<Object>> queryAndParams) {
        return readTemplate.query(queryAndParams.getKey().toString(), queryAndParams.getValue().toArray(), new RowMapper<PartitionInfo>() {
            @Override
            public PartitionInfo mapRow(ResultSet resultSet, int i) throws SQLException {
                PartitionInfo info = new PartitionInfo(
                        resultSet.getString("PART_NAME"),
                        resultSet.getString("COLUMN_STATS_ACCURATE"),
                        resultSet.getObject("NUM_FILES", Integer.class),
                        resultSet.getObject("NUM_ROWS", Long.class),
                        resultSet.getObject("RAW_DATA_SIZE", Long.class),
                        resultSet.getObject("TOTAL_SIZE", Long.class),
                        resultSet.getObject("TRANSIENT_LAST_DDL_TIME", Long.class)
                );
                return info;
            }
        });
    }


    public PartitionListInfo getPartitionsListInfo(String datacenter,
                                                   String schema,
                                                   String table,
                                                   Optional<String> search,
                                                   Optional<Integer> offsetOpt,
                                                   Optional<Integer> lengthOpt) throws SQLException {
        Pair<StringBuilder, List<Object>> query = partitionQuery(String.format(BASIC_PARTITION_QUERY_TEMPLATE,
                DatacenterUtils.getHivePartitionsList(datacenter)), schema, table, search);
        final Integer offset = offsetOpt.orElse(0);
        final Integer length = Math.min(1000, lengthOpt.orElse(1000));
        query.getLeft().append(" ORDER BY PART_NAME DESC");
        query.getKey().append("  LIMIT ?  ");

        query.getRight().add(offset);
        query.getKey().append("  ,?  ");
        query.getRight().add(length + 1);
        final List<PartitionInfo> partitions = getPreparedQuery(query);
        return new PartitionListInfo(partitions.subList(0, Math.min(partitions.size(), length)), offset, length, partitions.size() > length);
    }

    public TablePartitionsInfo getPartitionsInfo(String datacenter,
                                                 String schema,
                                                 String table) throws SQLException {
        Pair<StringBuilder, List<Object>> queryAndParams =
                partitionQuery(String.format(COUNT_PARTITION_QUERY_TEMPLATE,
                        DatacenterUtils.getHivePartitionsList(datacenter)), schema, table, Optional.empty());
        return readTemplate.queryForObject(queryAndParams.getKey().toString(),
                queryAndParams.getValue().toArray(),
                new RowMapper<TablePartitionsInfo>() {
                    @Override
                    public TablePartitionsInfo mapRow(ResultSet resultSet, int i) throws SQLException {
                        TablePartitionsInfo tbI = new TablePartitionsInfo();
                        tbI.setPartitionCount(resultSet.getObject("PART_COUNT", Long.class));
                        tbI.setPartitionFilesSum(resultSet.getObject("NUM_FILES_SUM", Long.class));
                        tbI.setPartitionNumberOfRowsAvg(resultSet.getObject("NUM_ROWS_AVG", Long.class));
                        tbI.setTotalSizeSum(resultSet.getObject("TOTAL_SIZE_SUM", Long.class));
                        tbI.setPartitionRawDataSizeAvg(resultSet.getObject("RAW_DATA_SIZE_AVG", Long.class));
                        return tbI;
                    }
                });

    }


}
