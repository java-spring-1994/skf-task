package com.booking.dataqualityapi.repository;

import com.booking.dataqualityapi.model.WikiEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Repository for wiki entity database operations.
 */

@Repository
public interface WikiEntityRepository extends CrudRepository<WikiEntity, Integer> {

    @Query("select wiki from WikiEntity wiki where wiki.entity.type = :type "
            + "and wiki.entity.namespace = :namespace "
            + "and wiki.entity.name =:name order by wiki.createTime desc ")
    List<WikiEntity> findTop1ByNamespaceAndTable(@Param("type") final String type,
                                                 @Param("namespace") final String namespace,
                                                 @Param("name") final String name);

    @Query("select wiki from WikiEntity wiki where wiki.entity.type = :type "
            + "and wiki.entity.namespace = :namespace "
            + "and wiki.entity.name =:name order by wiki.createTime desc ")
    List<WikiEntity> findAllByNamespaceAndTable(@Param("type") final String type,
                                                 @Param("namespace") final String namespace,
                                                 @Param("name") final String name);

}
