package com.booking.dataqualityapi.interceptors;

import com.booking.spring.authx.AuthXInterceptor;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This mocks AuthXInterceptor, registered in InterceptorConfigurerAdapter.
 * The original interceptor has conflict with swagger-ui, also not needed since we use our own auth logic.
 */
@Component
public class AuthXInterceptorMock extends AuthXInterceptor {

    public AuthXInterceptorMock() {
        super(null, null);
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        return true;
    }
}

