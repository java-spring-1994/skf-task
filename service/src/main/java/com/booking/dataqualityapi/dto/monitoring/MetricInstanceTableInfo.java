package com.booking.dataqualityapi.dto.monitoring;

import java.util.Objects;

public class MetricInstanceTableInfo {
    private final String dc;
    private final String schemaName;
    private final String tableName;

    public MetricInstanceTableInfo(final String dc,
                                   final String schemaName,
                                   final String tableName) {
        this.dc = dc;
        this.schemaName = schemaName;
        this.tableName = tableName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MetricInstanceTableInfo that = (MetricInstanceTableInfo) o;
        return dc.equals(that.dc)
                && schemaName.equals(that.schemaName)
                && tableName.equals(that.tableName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dc, schemaName, tableName);
    }

    public String getDc() {
        return dc;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getTableName() {
        return tableName;
    }
}
