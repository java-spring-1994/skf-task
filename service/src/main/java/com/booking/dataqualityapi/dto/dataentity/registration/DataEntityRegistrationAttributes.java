package com.booking.dataqualityapi.dto.dataentity.registration;

public class DataEntityRegistrationAttributes {
    private int ownerOrgunitId;

    public void setOwnerOrgunitId(int ownerOrgunitId) {
        this.ownerOrgunitId = ownerOrgunitId;
    }

    public int getOwnerOrgunitId() {
        return ownerOrgunitId;
    }
}
