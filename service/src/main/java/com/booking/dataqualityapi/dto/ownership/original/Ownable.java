package com.booking.dataqualityapi.dto.ownership.original;

import com.booking.dataqualityapi.dto.KeyValue;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class Ownable {

    private String name;

    private String type;

    @JsonProperty("orgunit_id")
    private long orgUnitId;

    @JsonProperty("append_reason")
    private String appendReason;

    private List<KeyValue> meta;

    private String script;

    @JsonProperty("staff_id")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer staffId;

    @JsonProperty("staff_loginname")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String staffLoginName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public String getAppendReason() {
        return appendReason;
    }

    public void setAppendReason(String appendReason) {
        this.appendReason = appendReason;
    }

    public List<KeyValue> getMeta() {
        return meta;
    }

    public void setMeta(List<KeyValue> meta) {
        this.meta = meta;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    public String getStaffLoginName() {
        return staffLoginName;
    }

    public void setStaffLoginName(String staffLoginName) {
        this.staffLoginName = staffLoginName;
    }
}
