package com.booking.dataqualityapi.dto;

public class SaveWorkflowPayload {
    private String workflowName;
    private String dc;

    public SaveWorkflowPayload() { }

    public SaveWorkflowPayload(String workflowName, String dc) {
        this.workflowName = workflowName;
        this.dc = dc;
    }

    public String getWorkflowName() {
        return workflowName;
    }

    public String getDc() {
        return dc;
    }
}
