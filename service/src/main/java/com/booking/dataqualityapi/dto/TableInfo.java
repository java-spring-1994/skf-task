package com.booking.dataqualityapi.dto;

import io.swagger.annotations.ApiModelProperty;

public abstract class TableInfo {

    @ApiModelProperty(value = "Checks that table name, schema name or create contains pii substring")
    protected Boolean pii;
    protected String name;
    protected String namespace;

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getPii() {
        return pii;
    }

    public void setPii(Boolean pii) {
        this.pii = pii;
    }

}