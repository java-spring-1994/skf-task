package com.booking.dataqualityapi.dto.workflow;

public class WorkflowInfo {
    private long id;
    private String name;
    private String type;
    private String repoLink;
    private String description;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRepoLink() {
        return repoLink;
    }

    public void setRepoLink(String repoLink) {
        this.repoLink = repoLink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public WorkflowInfo() {}

    public WorkflowInfo(long id, String name, String type, String repoLink, String description) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.repoLink = repoLink;
        this.description = description;
    }
}
