package com.booking.dataqualityapi.dto.alerts;

import com.booking.dataquality.model.alerting.AnomalyAlertConfig;

public class AnomalyAlertConfigInfo {
    private String schemaName;
    private String tableName;
    private String columnName;
    private String metric;
    private String alertApiTemplate;
    private Double anomalyBoundaryScore;

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public Double getAnomalyBoundaryScore() {
        return anomalyBoundaryScore;
    }

    public void setAnomalyBoundaryScore(Double anomalyBoundaryScore) {
        this.anomalyBoundaryScore = anomalyBoundaryScore;
    }

    public String getAlertApiTemplate() {
        return alertApiTemplate;
    }

    public void setAlertApiTemplate(String alertApiTemplate) {
        this.alertApiTemplate = alertApiTemplate;
    }

    public static AnomalyAlertConfigInfo fromModel(final AnomalyAlertConfig cfg,
                                                   final String alertApiTemplate) {
        final AnomalyAlertConfigInfo cfgInfo = new AnomalyAlertConfigInfo();
        cfgInfo.setSchemaName(cfg.getSchemaName());
        cfgInfo.setTableName(cfg.getTableName());
        cfgInfo.setColumnName(cfg.getTableName());
        cfgInfo.setMetric(cfg.getMetric());
        cfgInfo.setAnomalyBoundaryScore(cfg.getAnomalyScore());
        cfgInfo.setAlertApiTemplate(alertApiTemplate);
        return cfgInfo;
    }
}
