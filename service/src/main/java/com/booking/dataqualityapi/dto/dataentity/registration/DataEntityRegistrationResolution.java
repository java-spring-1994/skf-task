package com.booking.dataqualityapi.dto.dataentity.registration;

import com.fasterxml.jackson.annotation.JsonInclude;

public class DataEntityRegistrationResolution {
    private DataEntityRegistrationStatus status;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;

    public DataEntityRegistrationResolution() {
    }

    public DataEntityRegistrationResolution(DataEntityRegistrationStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public DataEntityRegistrationStatus getStatus() {
        return status;
    }

    public void setStatus(DataEntityRegistrationStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
