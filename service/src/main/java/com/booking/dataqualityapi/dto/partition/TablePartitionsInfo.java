package com.booking.dataqualityapi.dto.partition;

/**
 * Payload contains partition information for the table.
 * First partition
 * Last partition
 * Existing partitions count
 * Expected partition count beetween first and last partition
 */

public class TablePartitionsInfo {

    private PartitionInfo firstPartition;
    private PartitionInfo lastPartition;
    private Long partitionCount;
    private Long partitionFilesSum;
    private Long partitionNumberOfRowsAvg;
    private Long totalSizeSum;
    private Long partitionRawDataSizeAvg;

    public TablePartitionsInfo() {
    }

    public PartitionInfo getFirstPartition() {
        return firstPartition;
    }

    public void setFirstPartition(PartitionInfo firstPartition) {
        this.firstPartition = firstPartition;
    }

    public PartitionInfo getLastPartition() {
        return lastPartition;
    }

    public void setLastPartition(PartitionInfo lastPartition) {
        this.lastPartition = lastPartition;
    }

    public Long getPartitionCount() {
        return partitionCount;
    }

    public void setPartitionCount(Long partitionCount) {
        this.partitionCount = partitionCount;
    }

    public Long getPartitionFilesSum() {
        return partitionFilesSum;
    }

    public void setPartitionFilesSum(Long partitionFilesSum) {
        this.partitionFilesSum = partitionFilesSum;
    }

    public Long getPartitionNumberOfRowsAvg() {
        return partitionNumberOfRowsAvg;
    }

    public void setPartitionNumberOfRowsAvg(Long partitionNumberOfRowsAvg) {
        this.partitionNumberOfRowsAvg = partitionNumberOfRowsAvg;
    }

    public Long getTotalSizeSum() {
        return totalSizeSum;
    }

    public void setTotalSizeSum(Long totalSizeSum) {
        this.totalSizeSum = totalSizeSum;
    }

    public Long getPartitionRawDataSizeAvg() {
        return partitionRawDataSizeAvg;
    }

    public void setPartitionRawDataSizeAvg(Long partitionRawDataSizeAvg) {
        this.partitionRawDataSizeAvg = partitionRawDataSizeAvg;
    }
}
