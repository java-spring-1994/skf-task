package com.booking.dataqualityapi.dto;

/**
 * Wiki Text payload.
 */

public class WikiTextPayload {
    private String text;

    public WikiTextPayload() {

    }

    public WikiTextPayload(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
