package com.booking.dataqualityapi.dto.dataentity;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum DataEntityType {
    @JsonProperty("hive")
    HIVE,
    @JsonProperty("mysql")
    MYSQL,
    @JsonProperty("kafka")
    KAFKA;

    public String convertToDbValue() {
        if (this.equals(HIVE) || this.equals(MYSQL)) {
            return String.format("%s-table", this.name().toLowerCase());
        } else if (this.equals(KAFKA)) {
            return "kafka-topic";
        }
        throw new IllegalArgumentException("Invalid enum state");
    }

    public static DataEntityType convertFromDbType(final String dbType) {
        switch (dbType) {
            case "hive-table":
                return HIVE;
            case "mysql-table":
                return MYSQL;
            case "kafka-topic":
                return KAFKA;
            default:
                throw new IllegalArgumentException("Unsupported db type was sent -> " + dbType);
        }
    }
}
