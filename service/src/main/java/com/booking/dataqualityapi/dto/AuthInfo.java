package com.booking.dataqualityapi.dto;

public class AuthInfo {
    private final Long staffId;
    private final String loginName;

    public AuthInfo(final Long staffId, final String loginName) {
        this.staffId = staffId;
        this.loginName = loginName;
    }

    public Long getStaffId() {
        return staffId;
    }

    public String getLoginName() {
        return loginName;
    }
}
