package com.booking.dataqualityapi.dto.dataentity;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class DataEntity {
    public static final String DEFAULT_ENTITY_ACCOUNT = "";
    @JsonProperty("entity_name")
    private String entityName;
    @JsonProperty("entity_namespace")
    private String entityNamespace;
    @JsonProperty("entity_account")
    private String entityAccount;
    @JsonProperty("entity_type")
    private DataEntityType entityType;

    public DataEntity() {
    }

    public DataEntity(String entityName, String entityNamespace, String entityAccount, DataEntityType entityType) {
        this.entityName = entityName;
        this.entityNamespace = entityNamespace;
        this.entityAccount = entityAccount;
        this.entityType = entityType;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getEntityNamespace() {
        return entityNamespace;
    }

    public void setEntityNamespace(String entityNamespace) {
        this.entityNamespace = entityNamespace;
    }

    public DataEntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(DataEntityType entityType) {
        this.entityType = entityType;
    }
    //CHECKSTYLE:OFF
    //generated methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataEntity that = (DataEntity) o;
        return Objects.equals(entityName, that.entityName) &&
                Objects.equals(entityNamespace, that.entityNamespace) &&
                Objects.equals(entityAccount, that.entityAccount) &&
                entityType == that.entityType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(entityName, entityNamespace, entityAccount, entityType);
    }
    //CHECKSTYLE:ON
}
