package com.booking.dataqualityapi.dto.ownership.dq;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Ownership meta information need to make assignment in ownership service.
 * Different from {@link OwnershipMeta} because of ownership service specifics.
 */
public class OwnershipAssignmentMeta {
    private String reason;

    @JsonProperty("staff_id")
    private Integer staffId;

    @JsonProperty("staff_loginname")
    private String staffLoginname;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    public String getStaffLoginname() {
        return staffLoginname;
    }

    public void setStaffLoginname(String staffLoginname) {
        this.staffLoginname = staffLoginname;
    }
}
