package com.booking.dataqualityapi.dto.schemaOverView;


public class SchemaOverViewEntity {

    private String schemaName;
    private String tableName;
    private long totalSize;
    private  String createdBy;
    private String datacenter;
    private long lastModifiedAgo;
    private String lastPartitionCreated;
    private String lastPartitionUpdated;

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(long totalSize) {
        this.totalSize = totalSize;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getDatacenter() {
        return datacenter;
    }

    public void setDatacenter(String datacenter) {
        this.datacenter = datacenter;
    }

    public long getLastModifiedAgo() {
        return lastModifiedAgo;
    }

    public void setLastModifiedAgo(long lastModifiedAgo) {
        this.lastModifiedAgo = lastModifiedAgo;
    }

    public String getLastPartitionCreated() {
        return lastPartitionCreated;
    }

    public void setLastPartitionCreated(String lastPartitionCreated) {
        this.lastPartitionCreated = lastPartitionCreated;
    }

    public String getLastPartitionUpdated() {
        return lastPartitionUpdated;
    }

    public void setLastPartitionUpdated(String lastPartitionUpdated) {
        this.lastPartitionUpdated = lastPartitionUpdated;
    }
}
