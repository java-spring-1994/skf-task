package com.booking.dataqualityapi.dto.workflow;

import java.util.Objects;

public class WorkflowEntity {
    private WorkflowTypeLowerCase workflowTypeLowerCase;
    private String workflowName;

    public WorkflowEntity(WorkflowTypeLowerCase workflowTypeLowerCase, String workflowName) {
        this.workflowTypeLowerCase = workflowTypeLowerCase;
        this.workflowName = workflowName;
    }

    public WorkflowTypeLowerCase getWorkflowType() {
        return workflowTypeLowerCase;
    }

    public void setWorkflowType(WorkflowTypeLowerCase workflowTypeLowerCase) {
        this.workflowTypeLowerCase = workflowTypeLowerCase;
    }

    public String getWorkflowName() {
        return workflowName;
    }

    public void setWorkflowName(String workflowName) {
        this.workflowName = workflowName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WorkflowEntity that = (WorkflowEntity) o;
        return workflowTypeLowerCase == that.workflowTypeLowerCase
            && workflowName.equals(that.workflowName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(workflowTypeLowerCase, workflowName);
    }
}
