package com.booking.dataqualityapi.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum DataCenter {
    @JsonProperty("ams4")
    AMS4,
    @JsonProperty("lhr4")
    LHR4,
    @JsonProperty("gc")
    GC
}
