package com.booking.dataqualityapi.dto;

public class OozieCoordinatorInfo {
    private String coordinatorName;
    private String coordinatorId;
    private String frequency;
    private String frequencyTimeUnit;
    private String nextMaterializedTime;
    private long coordStartTime;
    private long coordEndTime;

    public OozieCoordinatorInfo() {}

    public OozieCoordinatorInfo(
        String coordinatorName, String coordinatorId, String frequency,
        String frequencyTimeUnit, String nextMaterializedTime, long coordStartTime, long coordEndTime) {
        this.coordinatorName = coordinatorName;
        this.coordinatorId = coordinatorId;
        this.frequency = frequency;
        this.frequencyTimeUnit = frequencyTimeUnit;
        this.nextMaterializedTime = nextMaterializedTime;
        this.coordStartTime = coordStartTime;
        this.coordEndTime = coordEndTime;
    }

    public String getCoordinatorName() {
        return coordinatorName;
    }

    public void setCoordinatorName(String coordinatorName) {
        this.coordinatorName = coordinatorName;
    }

    public String getCoordinatorId() {
        return coordinatorId;
    }

    public void setCoordinatorId(String coordinatorId) {
        this.coordinatorId = coordinatorId;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getFrequencyTimeUnit() {
        return frequencyTimeUnit;
    }

    public void setFrequencyTimeUnit(String frequencyTimeUnit) {
        this.frequencyTimeUnit = frequencyTimeUnit;
    }

    public String getNextMaterializedTime() {
        return nextMaterializedTime;
    }

    public void setNextMaterializedTime(String nextMaterializedTime) {
        this.nextMaterializedTime = nextMaterializedTime;
    }

    public long getCoordStartTime() {
        return coordStartTime;
    }

    public void setCoordStartTime(long coordStartTime) {
        this.coordStartTime = coordStartTime;
    }

    public long getCoordEndTime() {
        return coordEndTime;
    }

    public void setCoordEndTime(long coordEndTime) {
        this.coordEndTime = coordEndTime;
    }
}