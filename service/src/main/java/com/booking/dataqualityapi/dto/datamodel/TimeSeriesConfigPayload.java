package com.booking.dataqualityapi.dto.datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TimeSeriesConfigPayload {

    @JsonProperty("date_column")
    private String dateColumn;

    @JsonProperty("date_format")
    private String dateFormat;

    @JsonProperty("date_partitioned")
    private Boolean datePartitioned;

    @JsonProperty("hour_column")
    private String hourColumn;

    @JsonProperty("hour_format")
    private String hourFormat;

    @JsonProperty("hour_partitioned")
    private Boolean hourPartitioned;

    @JsonProperty("enum_column")
    private String enumColumn;

    @JsonProperty("enum_values")
    private String[] enumValues;

    public String getDateColumn() {
        return dateColumn;
    }

    public void setDateColumn(String dateColumn) {
        this.dateColumn = dateColumn;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public Boolean isDatePartitioned() {
        return datePartitioned;
    }

    public void setDatePartitioned(Boolean datePartitioned) {
        this.datePartitioned = datePartitioned;
    }

    public String getHourColumn() {
        return hourColumn;
    }

    public void setHourColumn(String hourColumn) {
        this.hourColumn = hourColumn;
    }

    public Boolean isHourPartitioned() {
        return hourPartitioned;
    }

    public void setHourPartitioned(Boolean hourPartitioned) {
        this.hourPartitioned = hourPartitioned;
    }

    public String getHourFormat() {
        return hourFormat;
    }

    public void setHourFormat(String hourFormat) {
        this.hourFormat = hourFormat;
    }

    public String getEnumColumn() {
        return enumColumn;
    }

    public void setEnumColumn(String enumColumn) {
        this.enumColumn = enumColumn;
    }

    public String[] getEnumValues() {
        return enumValues;
    }

    public void setEnumValues(String[] enumValues) {
        this.enumValues = enumValues;
    }
}
