package com.booking.dataqualityapi.dto.monitoring;

import com.booking.dataqualityapi.dto.DataCenter;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;

public class MetricPayload {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate date;
    private DataCenter dc;
    private String schemaName;
    private String tableName;
    private String columnName;
    private String metric;
    private Double value;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public DataCenter getDc() {
        return dc;
    }

    public void setDc(DataCenter dc) {
        this.dc = dc;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
