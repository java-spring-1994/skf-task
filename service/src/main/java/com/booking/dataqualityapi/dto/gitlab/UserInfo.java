package com.booking.dataqualityapi.dto.gitlab;


public class UserInfo {
    private String id;
    private String name;
    private String userName;
    private String state;

    public UserInfo(String id, String name, String userName, String state) {
        this.id = id;
        this.name = name;
        this.userName = userName;
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
