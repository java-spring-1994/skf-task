package com.booking.dataqualityapi.dto.dqguard;

import com.booking.dataqualityapi.dto.DataCenter;
import com.booking.dataqualityapi.dto.HadoopCluster;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Set;

public class DQGuardMetricPayload {
    @JsonFormat(shape = JsonFormat.Shape.STRING)

    public static class DQGuardAppMeta {
        private String severity;
        @JsonProperty("app_type")
        private String appType;
        @JsonProperty("graphite_version")
        private String graphiteVersion;
        @JsonProperty("grafana_url")
        private String grafanaUrl;
        @JsonProperty("oozie_coord_name")
        private String oozieCoordName;
        @JsonProperty("dqguard_version")
        private String dqguardVersion;
        private String platform;
        @JsonProperty("app_name")
        private String appName;
        @JsonProperty("oozie_job_id")
        private String oozieJobId;
        @JsonProperty("oozie_coord_id")
        private String oozieCoordId;
        private Set<String> integration;
        @JsonProperty("bypass_feature")
        private String bypassFeature;
        @JsonProperty("nominal_time")
        private String nominalTime;
        private String dc;

        public String getSeverity() {
            return severity;
        }

        public String getAppType() {
            return appType;
        }

        public String getGraphiteVersion() {
            return graphiteVersion;
        }

        public String getGrafanaUrl() {
            return grafanaUrl;
        }

        public String getOozieCoordName() {
            return oozieCoordName;
        }

        public String getDqguardVersion() {
            return dqguardVersion;
        }

        public String getPlatform() {
            return platform;
        }

        public String getAppName() {
            return appName;
        }

        public String getOozieJobId() {
            return oozieJobId;
        }

        public String getOozieCoordId() {
            return oozieCoordId;
        }

        public Set<String> getIntegration() {
            return integration;
        }

        public String getBypassFeature() {
            return bypassFeature;
        }

        public String getNominalTime() {
            return nominalTime;
        }

        public String getDc() {
            return dc;
        }
    }

    @Deprecated
    private DataCenter dc;
    private HadoopCluster hadoopCluster;
    private String sourceId;
    private String source;
    private String type;
    private String alias;
    private String partition;
    private String metric;
    private Double value;
    private String severity;
    private DQGuardAppMeta appMeta;
    private String appType;
    private String appName;
    private Set<String> emails;
    private String yyyyMmDd;

    public DataCenter getDc() {
        return dc;
    }

    public HadoopCluster getHadoopCluster() {
        return hadoopCluster;
    }

    public String getSourceId() {
        return sourceId;
    }

    public String getSource() {
        return source;
    }

    public String getType() {
        return type;
    }

    public String getAlias() {
        return alias;
    }

    public String getPartition() {
        return partition;
    }

    public String getMetric() {
        return metric;
    }

    public Double getValue() {
        return value;
    }

    public String getSeverity() {
        return severity;
    }

    public DQGuardAppMeta getAppMeta() {
        return appMeta;
    }


    public String getAppType() {
        return appType;
    }

    public String getAppName() {
        return appName;
    }

    public Set<String> getEmails() {
        return emails;
    }

    public String getYyyyMmDd() {
        return yyyyMmDd;
    }
}
