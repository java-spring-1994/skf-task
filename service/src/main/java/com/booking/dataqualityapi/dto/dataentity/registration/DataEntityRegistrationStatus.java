package com.booking.dataqualityapi.dto.dataentity.registration;

public enum DataEntityRegistrationStatus {
    OK,
    FAILED,
    ALREADY_REGISTERED
}
