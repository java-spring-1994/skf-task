package com.booking.dataqualityapi.dto.alerts;

import io.swagger.annotations.ApiModelProperty;

public class AnomalyAlertConfigPayload {

    @ApiModelProperty(value = "Sends alert if column contains anomalies "
            + " with higher score then boundaries ")
    private Double anomalyBoundaryScore;

    public Double getAnomalyBoundaryScore() {
        return anomalyBoundaryScore;
    }

    public void setAnomalyBoundaryScore(Double anomalyBoundaryScore) {
        this.anomalyBoundaryScore = anomalyBoundaryScore;
    }
}
