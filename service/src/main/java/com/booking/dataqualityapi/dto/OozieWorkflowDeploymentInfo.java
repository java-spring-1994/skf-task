package com.booking.dataqualityapi.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@SuppressWarnings("unused")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OozieWorkflowDeploymentInfo {
    private String workflowName;
    private String cluster;
    private String deploymentDate;
    private String deploymentUser;
    private String deploymentUserName;
    private String deployedHash;
    private String deployedHashShort;
    private String gitlabUrl;

    public String getWorkflowName() {
        return workflowName;
    }

    public String getCluster() {
        return cluster;
    }

    public String getDeploymentDate() {
        return deploymentDate;
    }

    public String getDeploymentUser() {
        return deploymentUser;
    }

    public String getDeploymentUserName() {
        return deploymentUserName;
    }

    public String getDeployedHash() {
        return deployedHash;
    }

    public String getDeployedHashShort() {
        return deployedHashShort;
    }

    public String getGitlabUrl() {
        return gitlabUrl;
    }

    public OozieWorkflowDeploymentInfo(
        String workflowName, String cluster, String deploymentDate,
        String deploymentUser, String deploymentUserName, String deployedHash,
        String deployedHashShort, String gitlabUrl) {
        this.workflowName = workflowName;
        this.cluster = cluster;
        this.deploymentDate = deploymentDate;
        this.deploymentUser = deploymentUser;
        this.deploymentUserName = deploymentUserName;
        this.deployedHash = deployedHash;
        this.deployedHashShort = deployedHashShort;
        this.gitlabUrl = gitlabUrl;
    }

    public OozieWorkflowDeploymentInfo(
        String workflowName, String cluster
    ) {
        this(
            workflowName, cluster,
            null, null, null,
            null, null, null);
    }
}
