package com.booking.dataqualityapi.dto.dataentity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SchemaEntity {
    @JsonProperty("name")
    private String name;

    @JsonProperty("entity_account")
    private String entityAccount;

    @JsonProperty("entity_type")
    private DataEntityType entityType;

    public SchemaEntity() {}

    public SchemaEntity(final String name, final String entityAccount, final DataEntityType entityType) {
        this.name = name;
        this.entityAccount = entityAccount;
        this.entityType = entityType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEntityAccount() {
        return entityAccount;
    }

    public void setEntityAccount(String entityAccount) {
        this.entityAccount = entityAccount;
    }

    public DataEntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(DataEntityType entityType) {
        this.entityType = entityType;
    }
}
