package com.booking.dataqualityapi.dto.gitlab;

public class CommitInfo {
    private String id;
    private long createdAtEpoch;
    private String authorName;
    private String authorEmail;
    private String message;

    public CommitInfo() {}

    public CommitInfo(String id, long createdAtEpoch, String authorName,
                      String authorEmail, String message) {
        this.id = id;
        this.authorName = authorName;
        this.authorEmail = authorEmail;
        this.message = message;
        this.createdAtEpoch = createdAtEpoch;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getCreatedAtEpoch() {
        return createdAtEpoch;
    }

    public void setCreatedAtEpoch(long createdAtEpoch) {
        this.createdAtEpoch = createdAtEpoch;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
