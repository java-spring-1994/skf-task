package com.booking.dataqualityapi.dto.monitoring;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public class AnomalyJobConfigPayload {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate fromDate;

    private Short lookBackWindow;


    public LocalDate getFromDate() {
        return fromDate;
    }

    public Short getLookBackWindow() {
        return lookBackWindow;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public void setLookBackWindow(Short lookBackWindow) {
        this.lookBackWindow = lookBackWindow;
    }
}
