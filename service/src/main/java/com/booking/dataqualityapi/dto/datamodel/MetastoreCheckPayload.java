package com.booking.dataqualityapi.dto.datamodel;

import com.booking.dataquality.model.datamodel.MetastoreCheck;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MetastoreCheckPayload {
    @JsonProperty("entity_id")
    private int entityId;

    @JsonProperty("rule_id")
    private int ruleId;

    private MetastoreCheck.CheckStatus status;

    private int frequency;
    private int offset;
    private String param1;
    private String param2;

    public int getEntityId() {
        return entityId;
    }

    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }

    public int getRuleId() {
        return ruleId;
    }

    public void setRuleId(int ruleId) {
        this.ruleId = ruleId;
    }

    public MetastoreCheck.CheckStatus getStatus() {
        return status;
    }

    public void setStatus(MetastoreCheck.CheckStatus status) {
        this.status = status;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getParam1() {
        return param1;
    }

    public void setParam1(String param1) {
        this.param1 = param1;
    }

    public String getParam2() {
        return param2;
    }

    public void setParam2(String param2) {
        this.param2 = param2;
    }
}
