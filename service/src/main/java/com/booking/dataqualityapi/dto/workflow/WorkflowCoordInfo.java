package com.booking.dataqualityapi.dto.workflow;

import java.util.Date;

public class WorkflowCoordInfo {

    private String dc;

    private String coordId;

    private String coordName;

    private String coordStatus;

    private Date startTime;

    private Date endTime;

    private Date createdTime;

    private Date updatedTime;

    public WorkflowCoordInfo() {}

    public WorkflowCoordInfo(
        String dc, String coordId, String coordName,
        String coordStatus, Date startTime, Date endTime, Date createdTime, Date updatedTime) {
        this.dc = dc;
        this.coordId = coordId;
        this.coordName = coordName;
        this.coordStatus = coordStatus;
        this.startTime = startTime;
        this.endTime = endTime;
        this.createdTime = createdTime;
        this.updatedTime = updatedTime;
    }

    public String getDc() {
        return dc;
    }

    public void setDc(String dc) {
        this.dc = dc;
    }

    public String getCoordId() {
        return coordId;
    }

    public void setCoordId(String coordId) {
        this.coordId = coordId;
    }

    public String getCoordName() {
        return coordName;
    }

    public void setCoordName(String coordName) {
        this.coordName = coordName;
    }

    public String getCoordStatus() {
        return coordStatus;
    }

    public void setCoordStatus(String coordStatus) {
        this.coordStatus = coordStatus;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }
}
