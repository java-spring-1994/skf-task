package com.booking.dataqualityapi.dto.workflow;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;

public class WorkflowQueryInfo {
    @JsonIgnore
    private int id;
    private String name;
    private String jobInfo;
    private String dc;
    private String query;
    private Date createTime;
    private String queryScriptLink;
    private String workflowLink;
    private String coordinatorLink;

    public WorkflowQueryInfo() {
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJobInfo() {
        return jobInfo;
    }

    public void setJobInfo(String jobInfo) {
        this.jobInfo = jobInfo;
    }

    public String getDc() {
        return dc;
    }

    public void setDc(String dc) {
        this.dc = dc;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getQueryScriptLink() {
        return queryScriptLink;
    }

    public void setQueryScriptLink(String queryScriptLink) {
        this.queryScriptLink = queryScriptLink;
    }

    public String getWorkflowLink() {
        return workflowLink;
    }

    public void setWorkflowLink(String workflowLink) {
        this.workflowLink = workflowLink;
    }

    public String getCoordinatorLink() {
        return coordinatorLink;
    }

    public void setCoordinatorLink(String coordinatorLink) {
        this.coordinatorLink = coordinatorLink;
    }
}
