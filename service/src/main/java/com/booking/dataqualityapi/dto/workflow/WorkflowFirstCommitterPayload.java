package com.booking.dataqualityapi.dto.workflow;

import com.booking.dataqualityapi.dto.gitlab.CommitInfo;

public class WorkflowFirstCommitterPayload {
    private String workflowGitName;
    private CommitInfo commitInfo;

    public String getWorkflowGitName() {
        return workflowGitName;
    }

    public void setWorkflowGitName(String workflowGitName) {
        this.workflowGitName = workflowGitName;
    }

    public CommitInfo getCommitInfo() {
        return commitInfo;
    }

    public void setCommitInfo(CommitInfo commitInfo) {
        this.commitInfo = commitInfo;
    }
}
