package com.booking.dataqualityapi.dto;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateTagInfo {

    @NotNull
    @Size(min = 2, max = 100)

    private String tag;

    private String description;

    @ApiModelProperty(allowableValues = "system, free_text",
            value = "free_text",
            example = "free_text")
    private String type;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

