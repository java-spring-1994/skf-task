package com.booking.dataqualityapi.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Asign tag to data entity action dto.
 */
@ApiModel
public class AssignTagInfo {

    @ApiModelProperty(required = true)
    private String tag;

    @ApiModelProperty(required = false)
    private String tagDescription;

    @ApiModelProperty(required = false)
    private String reason;

    @ApiModelProperty(required = false)
    private String staffName;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTagDescription() {
        return tagDescription;
    }

    public void setTagDescription(String tagDescription) {
        this.tagDescription = tagDescription;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }
}


