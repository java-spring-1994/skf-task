package com.booking.dataqualityapi.dto.ownership.dq;

public class SchemaEntityOwnershipInfoDump {
    private SchemaEntityOwnershipInfo info;
    private boolean active;

    public SchemaEntityOwnershipInfoDump() {}

    public SchemaEntityOwnershipInfoDump(SchemaEntityOwnershipInfo info, boolean active) {
        this.info = info;
        this.active = active;
    }

    public SchemaEntityOwnershipInfo getInfo() {
        return info;
    }

    public void setInfo(SchemaEntityOwnershipInfo info) {
        this.info = info;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
