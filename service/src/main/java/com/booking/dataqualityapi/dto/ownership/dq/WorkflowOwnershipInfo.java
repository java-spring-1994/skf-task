package com.booking.dataqualityapi.dto.ownership.dq;

import com.booking.dataqualityapi.dto.workflow.WorkflowEntity;

public class WorkflowOwnershipInfo {
    private WorkflowEntity workflowEntity;
    private OwnershipMeta ownershipMeta;

    public WorkflowOwnershipInfo(WorkflowEntity workflowEntity, OwnershipMeta ownershipMeta) {
        this.workflowEntity = workflowEntity;
        this.ownershipMeta = ownershipMeta;
    }

    public WorkflowEntity getWorkflowEntity() {
        return workflowEntity;
    }

    public OwnershipMeta getOwnershipMeta() {
        return ownershipMeta;
    }
}
