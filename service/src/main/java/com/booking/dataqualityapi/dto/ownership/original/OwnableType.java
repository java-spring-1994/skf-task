package com.booking.dataqualityapi.dto.ownership.original;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OwnableType {

    @JsonProperty("type_name")
    private String typeName;

    private String description;

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
