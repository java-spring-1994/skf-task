package com.booking.dataqualityapi.dto.readiness;

public enum DataCenter {
    AMS4,
    LHR4,
    GC
}
