package com.booking.dataqualityapi.dto.ownership.dq;

public class DataEntityOwnershipInfoDump {
    private DataEntityOwnershipInfo info;
    private boolean active;

    public DataEntityOwnershipInfoDump() {}

    public DataEntityOwnershipInfoDump(DataEntityOwnershipInfo info, boolean active) {
        this.info = info;
        this.active = active;
    }

    public DataEntityOwnershipInfo getInfo() {
        return info;
    }

    public void setInfo(DataEntityOwnershipInfo info) {
        this.info = info;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
