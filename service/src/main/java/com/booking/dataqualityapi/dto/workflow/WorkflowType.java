package com.booking.dataqualityapi.dto.workflow;

public enum WorkflowType {
    OOZIE,
    AIRFLOW
}
