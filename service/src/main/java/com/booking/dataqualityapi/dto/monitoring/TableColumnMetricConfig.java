package com.booking.dataqualityapi.dto.monitoring;

public class TableColumnMetricConfig {

    private String value;

    public TableColumnMetricConfig() {

    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
