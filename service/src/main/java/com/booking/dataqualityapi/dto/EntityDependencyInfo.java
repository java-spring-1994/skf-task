package com.booking.dataqualityapi.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EntityDependencyInfo {

    private String workflowName;
    private String workflowStatus;
    private String workflowId;
    private String workflowStartTime;
    private String workflowEndTime;
    private String workflowNominalTime;
    private String workflowCoordId;
    private String missingDependencies;
    private Date dependencyLastUsed;
    private List<String> tables = new ArrayList<>();
    private List<EntityDependencyInfo> upstream = new ArrayList<>();
    private List<EntityDependencyInfo> downstream = new ArrayList<>();

    public EntityDependencyInfo() {}

    public EntityDependencyInfo(String workflowName) {
        setWorkflowName(workflowName);
    }

    public String getWorkflowName() {
        return workflowName;
    }

    public String getWorkflowStatus() {
        return workflowStatus;
    }

    public void setWorkflowStatus(String workflowStatus) {
        this.workflowStatus = workflowStatus;
    }

    public String getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(String workflowId) {
        this.workflowId = workflowId;
    }

    public String getWorkflowStartTime() {
        return workflowStartTime;
    }

    public void setWorkflowStartTime(String workflowStartTime) {
        this.workflowStartTime = workflowStartTime;
    }

    public String getWorkflowEndTime() {
        return workflowEndTime;
    }

    public void setWorkflowEndTime(String workflowEndTime) {
        this.workflowEndTime = workflowEndTime;
    }

    public String getWorkflowNominalTime() {
        return workflowNominalTime;
    }

    public void setWorkflowNominalTime(String workflowNominalTime) {
        this.workflowNominalTime = workflowNominalTime;
    }

    public String getWorkflowCoordId() {
        return workflowCoordId;
    }

    public void setWorkflowCoordId(String workflowCoordId) {
        this.workflowCoordId = workflowCoordId;
    }

    public void setWorkflowName(String workflowName) {
        this.workflowName = workflowName;
    }

    public String getMissingDependencies() {
        return missingDependencies;
    }

    public void setMissingDependencies(String missingDependencies) {
        this.missingDependencies = missingDependencies;
    }

    public Date getDependencyLastUsed() {
        return dependencyLastUsed;
    }

    public void setDependencyLastUsed(Date dependencyLastUsed) {
        this.dependencyLastUsed = dependencyLastUsed;
    }

    public List<String> getTables() {
        return tables;
    }

    public void setTables(List<String> tables) {
        this.tables = tables;
    }

    public List<EntityDependencyInfo> getUpstream() {
        return upstream;
    }

    public void setUpstream(List<EntityDependencyInfo> upstream) {
        this.upstream = upstream;
    }

    public List<EntityDependencyInfo> getDownstream() {
        return downstream;
    }

    public void setDownstream(List<EntityDependencyInfo> downstream) {
        this.downstream = downstream;
    }
}
