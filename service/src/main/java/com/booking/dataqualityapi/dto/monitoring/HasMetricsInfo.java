package com.booking.dataqualityapi.dto.monitoring;

public class HasMetricsInfo {

    private final boolean hasMetrics;

    public HasMetricsInfo(boolean hasMetrics) {
        this.hasMetrics = hasMetrics;
    }

    public boolean isHasMetrics() {
        return hasMetrics;
    }
}
