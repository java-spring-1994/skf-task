package com.booking.dataqualityapi.dto.ownership.dq;

import com.booking.dataqualityapi.dto.dataentity.SchemaEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

public class SchemaEntityOwnershipInfo {
    @JsonProperty("schema_entity")
    private SchemaEntity schemaEntity;

    @JsonUnwrapped
    private OwnershipMeta ownershipMeta;

    public SchemaEntityOwnershipInfo(final SchemaEntity schemaEntity, final OwnershipMeta ownershipMeta) {
        this.schemaEntity = schemaEntity;
        this.ownershipMeta = ownershipMeta;
    }

    public SchemaEntityOwnershipInfo(
        final SchemaEntity schemaEntity,
        final long ownerOrgUnitId,
        final String ownerOrgUnitName,
        final int changedByStaffId,
        final String reason) {
        this(schemaEntity, new OwnershipMeta(ownerOrgUnitId, ownerOrgUnitName, changedByStaffId, reason));
    }

    public SchemaEntity getSchemaEntity() {
        return schemaEntity;
    }

    public void setSchemaEntity(SchemaEntity schemaEntity) {
        this.schemaEntity = schemaEntity;
    }

    public OwnershipMeta getOwnershipMeta() {
        return ownershipMeta;
    }

    public void setOwnershipMeta(OwnershipMeta ownershipMeta) {
        this.ownershipMeta = ownershipMeta;
    }
}
