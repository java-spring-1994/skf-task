package com.booking.dataqualityapi.dto;

import com.booking.dataqualityapi.dto.partition.PartitionColumnInfo;
import com.booking.dataqualityapi.model.EntityInstance.ColumnMetaType;
import com.booking.dataqualityapi.utils.PartitionsUtil;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class HiveTableInfo extends TableInfo {

    private List<ColumnInfo> columns;
    private List<PartitionColumnInfo> partitionColumns;
    private String location;
    private String description;

    public HiveTableInfo() {
    }

    public List<ColumnInfo> getColumns() {
        return columns;
    }

    public void setColumns(List<ColumnInfo> columns) {
        this.columns = columns;
    }

    public void setColumnMeta(List<ColumnMetaType> columns) {
        this.columns = columns
                .stream()
                .map(v -> new ColumnInfo(
                    v.getColumnName(), v.getType(),
                    v.getColumnComment(), v.getPartitionedBy() > 0))
                .collect(Collectors.toList());

        this.partitionColumns = columns
                .stream()
                .filter(v -> (v.getPartitionedBy() > 0))
                .sorted(Comparator.comparingInt(ColumnMetaType::getPartitionOrderIndex))
                .map(v -> new PartitionColumnInfo(
                    v.getColumnName(), PartitionsUtil.guessPartitionColumnTimeSeriesType(v.getColumnName()), v.getPartitionOrderIndex()))
                .collect(Collectors.toList());
    }

    public void setSchema(String schema) {
        this.namespace = schema;
    }

    public void setTable(String name) {
        this.name = name;
    }

    public List<PartitionColumnInfo> getPartitionColumns() {
        return partitionColumns;
    }

    public void setPartitionColumns(List<PartitionColumnInfo> partitionColumns) {
        this.partitionColumns = partitionColumns;
    }

    public String getLocation() {
        return location;
    }

    public void setHdfsLocation(String location) {
        this.location = location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static class ColumnInfo {
        private final String name;
        private final String type;
        private final String description;
        private final boolean isPartitionColumn;

        ColumnInfo(final String name, final String type, final String description, final boolean isPartitionColumn) {
            this.name = name;
            this.type = type;
            this.description = description;
            this.isPartitionColumn = isPartitionColumn;
        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }

        public String getDescription() {
            return description;
        }

        public boolean isPartitionColumn() {
            return isPartitionColumn;
        }
    }
}
