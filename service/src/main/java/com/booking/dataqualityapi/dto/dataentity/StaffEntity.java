package com.booking.dataqualityapi.dto.dataentity;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class StaffEntity {

    @JsonProperty("staff_ids")
    private List<Integer> staffIds;

    public List<Integer> getStaffIds() {
        return staffIds;
    }

    public void setStaffIds(List<Integer> staffIds) {
        this.staffIds = staffIds;
    }
}
