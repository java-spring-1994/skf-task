package com.booking.dataqualityapi.dto.datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum DataModelSchemaType {
    @JsonProperty("json")
    JSON,
    @JsonProperty("avro")
    AVRO;

    @Override
    public String toString() {
        return this.name().toLowerCase();
    }

    public static DataModelSchemaType fromString(final String entityType) {
        return DataModelSchemaType.valueOf(entityType.toUpperCase());
    }
}
