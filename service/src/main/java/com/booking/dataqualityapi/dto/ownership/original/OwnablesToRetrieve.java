package com.booking.dataqualityapi.dto.ownership.original;

import java.util.List;

public class OwnablesToRetrieve {
    private List<OwnableNameType> ownables;

    public OwnablesToRetrieve(List<OwnableNameType> ownables) {
        this.ownables = ownables;
    }

    public List<OwnableNameType> getOwnables() {
        return ownables;
    }

    public void setOwnables(List<OwnableNameType> ownables) {
        this.ownables = ownables;
    }
}
