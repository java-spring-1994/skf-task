package com.booking.dataqualityapi.dto.readiness;

import com.booking.dataqualityapi.dto.EntityType;
import com.booking.dataqualityapi.dto.workflow.WorkflowType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class DataReadinessInfoDTO {
    private String name;
    private String namespace;
    private EntityType entityType;
    private DataCenter dataCenter;
    private String workflowIdentifier;
    private WorkflowType workflowType;
    private Date currentMaterializedTime;
    private Date nextMaterializedTime;
    private ReadinessStatus status;
    private Map<String, String> metadata;

    public DataReadinessInfoDTO() {
    }


    public DataReadinessInfoDTO(String name, String namespace, EntityType entityType,
                                DataCenter dataCenter, String workflowIdentifier, WorkflowType workflowType,
                                Date currentMaterializedTime, Date nextMaterializedTime,
                                ReadinessStatus status, Map<String, String> metadata) {
        this.name = name;
        this.namespace = namespace;
        this.entityType = entityType;
        this.dataCenter = dataCenter;
        this.workflowIdentifier = workflowIdentifier;
        this.workflowType = workflowType;
        this.currentMaterializedTime = currentMaterializedTime;
        this.nextMaterializedTime = nextMaterializedTime;
        this.status = status;
        this.metadata = metadata;
    }

    public String getNamespace() {
        return namespace;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public DataCenter getDataCenter() {
        return dataCenter;
    }

    public String getWorkflowIdentifier() {
        return workflowIdentifier;
    }

    public WorkflowType getWorkflowType() {
        return workflowType;
    }

    public Date getCurrentMaterializedTime() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return sdf.parse(sdf.format(currentMaterializedTime));
    }

    public Date getNextMaterializedTime() {
        return nextMaterializedTime;
    }

    public ReadinessStatus getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    public Map<String, String> getMetadata() {
        return metadata;
    }
}

