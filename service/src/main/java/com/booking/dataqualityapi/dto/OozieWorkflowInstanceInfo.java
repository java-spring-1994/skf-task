package com.booking.dataqualityapi.dto;

public class OozieWorkflowInstanceInfo {
    private String workflowId;
    private String coordId;
    private String missingDependencies;

    /**
     * Expected a string in format YYYY-MM-DD HH:MM:SS.
     */
    private String nominalDate;
    private String nominalHour;
    private String nominalMinute;
    private String workflowStartTime;
    private String workflowEndTime;
    private String workflowStatus;

    public OozieWorkflowInstanceInfo() {}

    public OozieWorkflowInstanceInfo(
        String workflowId, String coordId, String missingDependencies, String nominalDate,
        String nominalHour, String nominalMinute, String workflowStartTime, String workflowEndTime,
        String workflowStatus) {
        this.workflowId = workflowId;
        this.coordId = coordId;
        this.missingDependencies = missingDependencies;
        this.nominalDate = nominalDate;
        this.nominalHour = nominalHour;
        this.nominalMinute = nominalMinute;
        this.workflowStartTime = workflowStartTime;
        this.workflowEndTime = workflowEndTime;
        this.workflowStatus = workflowStatus;
    }

    public String getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(String workflowId) {
        this.workflowId = workflowId;
    }

    public String getCoordId() {
        return coordId;
    }

    public void setCoordId(String coordId) {
        this.coordId = coordId;
    }

    public String getMissingDependencies() {
        return missingDependencies;
    }

    public void setMissingDependencies(String missingDependencies) {
        this.missingDependencies = missingDependencies;
    }

    public String getNominalDate() {
        return nominalDate;
    }

    public void setNominalDate(String nominalDate) {
        this.nominalDate = nominalDate;
    }

    public String getNominalHour() {
        return nominalHour;
    }

    public void setNominalHour(String nominalHour) {
        this.nominalHour = nominalHour;
    }

    public String getNominalMinute() {
        return nominalMinute;
    }

    public void setNominalMinute(String nominalMinute) {
        this.nominalMinute = nominalMinute;
    }

    public String getWorkflowStartTime() {
        return workflowStartTime;
    }

    public void setWorkflowStartTime(String workflowStartTime) {
        this.workflowStartTime = workflowStartTime;
    }

    public String getWorkflowEndTime() {
        return workflowEndTime;
    }

    public void setWorkflowEndTime(String workflowEndTime) {
        this.workflowEndTime = workflowEndTime;
    }

    public String getWorkflowStatus() {
        return workflowStatus;
    }

    public void setWorkflowStatus(String workflowStatus) {
        this.workflowStatus = workflowStatus;
    }


    public static final class OozieWorkflowInstanceInfoBuilder {
        private OozieWorkflowInstanceInfo oozieWorkflowInstanceInfo;

        private OozieWorkflowInstanceInfoBuilder() {
            oozieWorkflowInstanceInfo = new OozieWorkflowInstanceInfo();
        }

        public static OozieWorkflowInstanceInfoBuilder anOozieWorkflowInstanceInfo() {
            return new OozieWorkflowInstanceInfoBuilder();
        }

        public OozieWorkflowInstanceInfoBuilder workflowId(String workflowId) {
            oozieWorkflowInstanceInfo.setWorkflowId(workflowId);
            return this;
        }

        public OozieWorkflowInstanceInfoBuilder missingDependencies(String missingDependencies) {
            oozieWorkflowInstanceInfo.setMissingDependencies(missingDependencies);
            return this;
        }

        public OozieWorkflowInstanceInfoBuilder nominalDate(String nominalDate) {
            oozieWorkflowInstanceInfo.setNominalDate(nominalDate);
            return this;
        }

        public OozieWorkflowInstanceInfoBuilder nominalHour(String nominalHour) {
            oozieWorkflowInstanceInfo.setNominalHour(nominalHour);
            return this;
        }

        public OozieWorkflowInstanceInfoBuilder nominalMinute(String nominalMinute) {
            oozieWorkflowInstanceInfo.setNominalMinute(nominalMinute);
            return this;
        }

        public OozieWorkflowInstanceInfoBuilder workflowStartTime(String workflowStartTime) {
            oozieWorkflowInstanceInfo.setWorkflowStartTime(workflowStartTime);
            return this;
        }

        public OozieWorkflowInstanceInfoBuilder workflowEndTime(String workflowEndTime) {
            oozieWorkflowInstanceInfo.setWorkflowEndTime(workflowEndTime);
            return this;
        }

        public OozieWorkflowInstanceInfoBuilder workflowStatus(String workflowStatus) {
            oozieWorkflowInstanceInfo.setWorkflowStatus(workflowStatus);
            return this;
        }

        public OozieWorkflowInstanceInfo build() {
            return oozieWorkflowInstanceInfo;
        }
    }
}