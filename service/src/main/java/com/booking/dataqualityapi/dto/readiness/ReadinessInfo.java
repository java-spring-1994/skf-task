package com.booking.dataqualityapi.dto.readiness;

import com.booking.dataqualityapi.dto.OozieWorkflowInstanceInfo;

import java.util.List;

public class ReadinessInfo {

    private String entityNamespace;
    private String entityName;
    private String cluster;
    private String workflowName;
    private String nextMaterializedTime;
    private Status status;

    /* Will be implemented later
    private boolean isPartitioned = false;
    private List<String> expectedPartitionOutput;*/
    private List<String> actualPartitionOutput;

    private OozieWorkflowInstanceInfo workflowInstanceInfo;

    public enum Status {
        READY,
        NOT_READY,
        UNKNOWN
    }

    public String getEntityNamespace() {
        return entityNamespace;
    }

    public void setEntityNamespace(String entityNamespace) {
        this.entityNamespace = entityNamespace;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public String getWorkflowName() {
        return workflowName;
    }

    public void setWorkflowName(String workflowName) {
        this.workflowName = workflowName;
    }

    public String getNextMaterializedTime() {
        return nextMaterializedTime;
    }

    public void setNextMaterializedTime(String nextMaterializedTime) {
        this.nextMaterializedTime = nextMaterializedTime;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<String> getActualPartitionOutput() {
        return actualPartitionOutput;
    }

    public void setActualPartitionOutput(List<String> actualPartitionOutput) {
        this.actualPartitionOutput = actualPartitionOutput;
    }

    public OozieWorkflowInstanceInfo getWorkflowInstanceInfo() {
        return workflowInstanceInfo;
    }

    public void setWorkflowInstanceInfo(OozieWorkflowInstanceInfo workflowInstanceInfo) {
        this.workflowInstanceInfo = workflowInstanceInfo;
    }


    public static final class ReadinessInfoBuilder {
        private String entityNamespace;
        private String entityName;
        private String cluster;
        private String workflowName;
        private String nextMaterializedTime;
        private Status status;
        /* Will be implemented later
                private boolean isPartitioned = false;
                private List<String> expectedPartitionOutput;*/
        private List<String> actualPartitionOutput;
        private OozieWorkflowInstanceInfo workflowInstanceInfo;

        private ReadinessInfoBuilder() {
        }

        public static ReadinessInfoBuilder aReadinessInfo() {
            return new ReadinessInfoBuilder();
        }

        public ReadinessInfoBuilder entityNamespace(String entityNamespace) {
            this.entityNamespace = entityNamespace;
            return this;
        }

        public ReadinessInfoBuilder entityName(String entityName) {
            this.entityName = entityName;
            return this;
        }

        public ReadinessInfoBuilder cluster(String cluster) {
            this.cluster = cluster;
            return this;
        }

        public ReadinessInfoBuilder workflowName(String workflowName) {
            this.workflowName = workflowName;
            return this;
        }

        public ReadinessInfoBuilder nextMaterializedTime(String nextMaterializedTime) {
            this.nextMaterializedTime = nextMaterializedTime;
            return this;
        }

        public ReadinessInfoBuilder status(Status status) {
            this.status = status;
            return this;
        }

        public ReadinessInfoBuilder actualPartitionOutput(List<String> actualPartitionOutput) {
            this.actualPartitionOutput = actualPartitionOutput;
            return this;
        }

        public ReadinessInfoBuilder workflowInstanceInfo(OozieWorkflowInstanceInfo workflowInstanceInfo) {
            this.workflowInstanceInfo = workflowInstanceInfo;
            return this;
        }

        public ReadinessInfo build() {
            ReadinessInfo readinessInfo = new ReadinessInfo();
            readinessInfo.setEntityNamespace(entityNamespace);
            readinessInfo.setEntityName(entityName);
            readinessInfo.setCluster(cluster);
            readinessInfo.setWorkflowName(workflowName);
            readinessInfo.setNextMaterializedTime(nextMaterializedTime);
            readinessInfo.setStatus(status);
            readinessInfo.setActualPartitionOutput(actualPartitionOutput);
            readinessInfo.setWorkflowInstanceInfo(workflowInstanceInfo);
            return readinessInfo;
        }
    }
}
