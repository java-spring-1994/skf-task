package com.booking.dataqualityapi.dto.partition;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PartitionColumnInfo {
    private final String columnName;
    private final String partitionType;
    // Not using primitive type int because value can be NULL
    private final Integer partitionOrderIndex;

    public PartitionColumnInfo(String columnName, String partitionType, Integer partitionOrderIndex) {
        this.columnName = columnName;
        this.partitionType = partitionType;
        this.partitionOrderIndex = partitionOrderIndex;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getPartitionType() {
        return partitionType;
    }

    public Integer getPartitionOrderIndex() {
        return partitionOrderIndex;
    }
}
