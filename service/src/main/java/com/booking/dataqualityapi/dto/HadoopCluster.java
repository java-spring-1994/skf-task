package com.booking.dataqualityapi.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum HadoopCluster {
    @JsonProperty("ams4")
    AMS4,
    @JsonProperty("lhr4")
    LHR4,
    @JsonProperty("fpa-ams4")
    FPA_AMS4,
    @JsonProperty("fpa-lhr4")
    FPA_LHR4;

    @Override
    public String toString() {
        switch (this) {
            case FPA_AMS4: return "fpa-ams4";
            case FPA_LHR4: return "fpa-lhr4";
            default: return super.toString().toLowerCase();
        }
    }
}
