package com.booking.dataqualityapi.dto.monitoring;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class AnomalyInfo {
    private String cluster;
    private String schemaName;
    private String tableName;
    private String column;
    private String metric;
    private Double value;
    private Double anomalyScore;
    private Double runningAvg;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date date;

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public void setAnomalyScore(Double anomalyScore) {
        this.anomalyScore = anomalyScore;
    }

    public void setRunningAvg(Double runningAvg) {
        this.runningAvg = runningAvg;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCluster() {
        return cluster;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public String getColumn() {
        return column;
    }

    public String getMetric() {
        return metric;
    }

    public Double getValue() {
        return value;
    }

    public Double getAnomalyScore() {
        return anomalyScore;
    }

    public Double getRunningAvg() {
        return runningAvg;
    }

    public Date getDate() {
        return date;
    }

}


