package com.booking.dataqualityapi.dto.dataentity;

import java.util.Collections;
import java.util.List;

public class DataEntityListInfo {

    private final List<DataEntityInfo> entities;
    private final Integer offset;
    private final Integer length;
    private final boolean hasMoreItems;

    public DataEntityListInfo(final List<DataEntityInfo> entities,
                              final Integer offset,
                              final Integer length,
                              final boolean hasMoreItems) {
        this.entities = Collections.unmodifiableList(entities);
        this.offset = offset;
        this.length = length;
        this.hasMoreItems = hasMoreItems;
    }

    public List<DataEntityInfo> getEntities() {
        return entities;
    }

    public Integer getOffset() {
        return offset;
    }

    public Integer getLength() {
        return length;
    }

    public boolean isHasMoreItems() {
        return hasMoreItems;
    }
}
