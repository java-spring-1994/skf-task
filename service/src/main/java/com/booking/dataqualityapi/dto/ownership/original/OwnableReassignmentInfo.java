package com.booking.dataqualityapi.dto.ownership.original;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;

public class OwnableReassignmentInfo {

    @JsonProperty("orgunit_id")
    private long orgUnitId;

    private List<Long> ownables;

    private String reason;

    @JsonProperty("staff_id")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer staffId;

    @JsonProperty("staff_loginname")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String staffLoginName;

    public long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public List<Long> getOwnables() {
        return ownables;
    }

    public void setOwnables(List<Long> ownables) {
        this.ownables = ownables;
    }

    public void setOwnable(long ownable) {
        if (this.ownables == null) {
            this.ownables = new ArrayList<>();
        }
        this.ownables.add(ownable);
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    public String getStaffLoginName() {
        return staffLoginName;
    }

    public void setStaffLoginName(String staffLoginName) {
        this.staffLoginName = staffLoginName;
    }
}
