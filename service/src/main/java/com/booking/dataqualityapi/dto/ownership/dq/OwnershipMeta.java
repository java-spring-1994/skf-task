package com.booking.dataqualityapi.dto.ownership.dq;

import com.booking.dataqualityapi.model.office.Staff;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Ownership meta information. Supports personal and team.
 */
public class OwnershipMeta {
    @JsonProperty("owner_orgunit_id")
    private Long ownerOrgUnitId;
    @JsonProperty("owner_orgunit_name")
    private String ownerOrgUnitName;

    @JsonProperty("owner_staff_id")
    private Integer staffId;
    @JsonProperty("owner_staff_loginname")
    private String staffLoginNanme;
    @JsonProperty("owner_staff_name")
    private String staffName;
    @JsonProperty("owner_staff_extra_info")
    private Staff staffExtraInfo;

    @JsonProperty("changed_by_staff_id")
    private Integer changedByStaffId;

    private String reason;

    public OwnershipMeta() {}

    /**
     * For team ownership.
     */
    public OwnershipMeta(
        long ownerOrgUnitId, String ownerOrgUnitName, int changedByStaffId, String reason
    ) {
        this.ownerOrgUnitId = ownerOrgUnitId;
        this.ownerOrgUnitName = ownerOrgUnitName;
        this.changedByStaffId = changedByStaffId;
        this.reason = reason;
    }

    /**
     * For personal ownership.
     */
    public OwnershipMeta(int staffId, String staffLoginName, String staffName, String reason) {
        this.staffId = staffId;
        this.staffLoginNanme = staffLoginName;
        this.staffName = staffName;
        this.reason = reason;
    }

    public boolean isPersonal() {
        return (staffId != null);
    }

    public Long getOwnerOrgUnitId() {
        return ownerOrgUnitId;
    }

    public void setOwnerOrgUnitId(long ownerOrgUnitId) {
        this.ownerOrgUnitId = ownerOrgUnitId;
    }

    public String getOwnerOrgUnitName() {
        return ownerOrgUnitName;
    }

    public void setOwnerOrgUnitName(String ownerOrgUnitName) {
        this.ownerOrgUnitName = ownerOrgUnitName;
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    public String getStaffLoginNanme() {
        return staffLoginNanme;
    }

    public void setStaffLoginNanme(String staffLoginNanme) {
        this.staffLoginNanme = staffLoginNanme;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public Staff getStaffExtraInfo() {
        return staffExtraInfo;
    }

    public void setStaffExtraInfo(Staff staffExtraInfo) {
        this.staffExtraInfo = staffExtraInfo;
    }

    public Integer getChangedByStaffId() {
        return changedByStaffId;
    }

    public void setChangedByStaffId(int changedByStaffId) {
        this.changedByStaffId = changedByStaffId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
