package com.booking.dataqualityapi.dto.partition;

import java.util.List;

public class PartitionListInfo {
    private final List<PartitionInfo> partitions;
    private final Integer offset;
    private final Integer length;
    private final boolean hasMoreItems;

    public PartitionListInfo(List<PartitionInfo> partitions, Integer offset, Integer length, boolean hasMoreItems) {
        this.partitions = partitions;
        this.offset = offset;
        this.length = length;
        this.hasMoreItems = hasMoreItems;
    }

    public List<PartitionInfo> getPartitions() {
        return partitions;
    }

    public Integer getOffset() {
        return offset;
    }

    public Integer getLength() {
        return length;
    }

    public boolean isHasMoreItems() {
        return hasMoreItems;
    }
}
