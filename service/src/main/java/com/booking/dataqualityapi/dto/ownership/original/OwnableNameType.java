package com.booking.dataqualityapi.dto.ownership.original;

public class OwnableNameType {
    private String name;
    private String type;

    public OwnableNameType() {}

    public OwnableNameType(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
