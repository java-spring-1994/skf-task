package com.booking.dataqualityapi.dto.readiness;

public enum ReadinessStatus {
    READY,
    NOT_READY,
    UNKNOWN
}
