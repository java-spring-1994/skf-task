package com.booking.dataqualityapi.dto;

import java.util.List;

public class ListInfo<T> {

    private final int offset;
    private final int size;
    private final boolean hasMore;
    private final List<T> items;

    public ListInfo(int offset, int limit, boolean hasMore, List<T> items) {
        this.offset = offset;
        this.size = limit;
        this.hasMore = hasMore;
        this.items = items;
    }

    public int getOffset() {
        return offset;
    }

    public int getSize() {
        return size;
    }

    public boolean isHasMore() {
        return hasMore;
    }

    public List<T> getItems() {
        return items;
    }
}
