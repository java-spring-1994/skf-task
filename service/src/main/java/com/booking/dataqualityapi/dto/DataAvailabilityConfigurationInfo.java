package com.booking.dataqualityapi.dto;

import com.booking.dataqualityapi.model.DataAvailabilityConfiguration;

public class DataAvailabilityConfigurationInfo {
    private final boolean syncToDas;
    private final DataAvailabilityConfiguration configuration;

    public DataAvailabilityConfigurationInfo(DataAvailabilityConfiguration configuration) {
        this.syncToDas = (configuration != null);
        this.configuration = configuration;
    }

    public boolean isSyncToDas() {
        return syncToDas;
    }

    public DataAvailabilityConfiguration getConfiguration() {
        return configuration;
    }
}
