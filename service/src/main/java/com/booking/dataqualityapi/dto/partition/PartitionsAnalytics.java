package com.booking.dataqualityapi.dto.partition;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

public class PartitionsAnalytics {

    private final List<PartitionInfo> partitions;
    private final String info;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private final LocalDate from;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private final LocalDate to;

    public PartitionsAnalytics(final List<PartitionInfo> partitions, final String message, final LocalDate from, final LocalDate to) {
        this.partitions = Collections.unmodifiableList(partitions);
        this.info = message;
        this.from = from;
        this.to = to;
    }

    public List<PartitionInfo> getPartitions() {
        return partitions;
    }

    public String getInfo() {
        return info;
    }

    public LocalDate getFrom() {
        return from;
    }

    public LocalDate getTo() {
        return to;
    }
}
