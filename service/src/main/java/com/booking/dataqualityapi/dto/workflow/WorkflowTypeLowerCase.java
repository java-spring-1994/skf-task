package com.booking.dataqualityapi.dto.workflow;

// Lower case to be able to use as http parameter and ownable name without extra conversions
public enum WorkflowTypeLowerCase {
    oozie,
    airflow
}
