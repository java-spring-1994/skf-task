package com.booking.dataqualityapi.dto.dataentity;

import com.booking.dataqualityapi.dto.partition.PartitionColumnInfo;
import com.booking.dataqualityapi.model.DataSourceOwner;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DataEntityInfo {
    private Integer id;
    private String name;
    private String namespace;
    private String account;
    private String type;

    private DataInstance[] instances;
    private Integer[] owners;
    private Map<String, String> labels;
    private MysqlImportInfo mysqlImport;


    public DataEntityInfo() {
    }

    public MysqlImportInfo getMysqlImport() {
        return mysqlImport;
    }

    public void setMysqlImport(MysqlImportInfo mysqlImport) {
        this.mysqlImport = mysqlImport;
    }

    public DataInstance[] getInstances() {
        return instances;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ApiModelProperty(value = "Get data instances")
    public void setInstances(DataInstance[] instances) {
        this.instances = instances;
    }

    @ApiModelProperty(value = "data entity name ")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ApiModelProperty(value = "data entity namespace")
    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    @ApiModelProperty(value = "data entity account")
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @ApiModelProperty(value = "data entity type", allowableValues = "hive, mysql")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @ApiModelProperty(value = "Data entity owners staff ids, empty if data entity doesn't have owners")
    public Integer[] getOwners() {
        return owners;
    }

    private static final Logger logger = LoggerFactory.getLogger(DataEntityInfo.class);

    public void setOwners(List<DataSourceOwner> owners) {
        this.owners = owners.stream().map(DataSourceOwner::getStaffId).toArray(Integer[]::new);
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public static class DataInstance {
        private final String datacenter;
        private final String type;
        private final String subType;
        private final List<PartitionColumnInfo> partitionColumns;

        public DataInstance(final String datacenter,
                            final String type,
                            final String subType,
                            final List<PartitionColumnInfo> partitionColumns) {
            this.datacenter = datacenter;
            this.type = type;
            this.subType = subType;
            this.partitionColumns = partitionColumns;
        }

        @ApiModelProperty(value = "Can be empty", allowableValues = "AMS4, LHR4, GC")
        public String getDatacenter() {
            return datacenter;
        }

        @ApiModelProperty(value = "Can be empty", allowableValues = "hive, mysql")
        public String getType() {
            return type;
        }

        @ApiModelProperty(value = "Can be empty", allowableValues = "MANAGED_TABLE, EXTERNAL_TABLE, VIRTUAL_VIEW")
        public String getSubType() {
            return subType;
        }

        public List<PartitionColumnInfo> getPartitionColumns() {
            return partitionColumns;
        }
    }

    public static class MysqlImportInfo {

        private final String mysqlTableName;
        private final String mysqlSchemaName;

        public MysqlImportInfo(String schemaName, String tableName) {
            this.mysqlTableName = tableName;
            this.mysqlSchemaName = schemaName;
        }

        public String getMysqlTableName() {
            return mysqlTableName;
        }

        public String getMysqlSchemaName() {
            return mysqlSchemaName;
        }
    }
}
