package com.booking.dataqualityapi.dto.partition;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.LocalDateTime;

@JsonInclude(JsonInclude.Include.NON_NULL)
public final class PartitionInfo {
    private final String partitionName;
    private final String collumnStatsAccurate;
    private final Integer numberOfFiles;
    private final Long numberOfRows;
    private final Long rawDataSize;
    private final Long totalSize;
    private final Long lastUpdateTime;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private final LocalDateTime date;
    private final Boolean isMissed;


    public PartitionInfo(String partitionName, String collumnStatsAccurate, Integer numberOfFiles,
                         Long numberOfRows, Long rawDataSize, Long totalSize, Long lastlUpdateTime) {
        this(partitionName, collumnStatsAccurate, numberOfFiles, numberOfRows, rawDataSize, totalSize, lastlUpdateTime, false, null);
    }

    public PartitionInfo(String partitionName, String collumnStatsAccurate, Integer numberOfFiles,
                         Long numberOfRows, Long rawDataSize, Long totalSize, Long lastlUpdateTime,
                         Boolean isMissed,
                         LocalDateTime date) {
        this.partitionName = partitionName;
        this.collumnStatsAccurate = collumnStatsAccurate;
        this.numberOfFiles = numberOfFiles;
        this.numberOfRows = numberOfRows;
        this.rawDataSize = rawDataSize;
        this.totalSize = totalSize;
        this.lastUpdateTime = lastlUpdateTime;
        this.isMissed = isMissed;
        this.date = date;
    }

    public Boolean getMissed() {
        return isMissed;
    }

    public String getPartitionName() {
        return partitionName;
    }

    public String getCollumnStatsAccurate() {
        return collumnStatsAccurate;
    }

    public Integer getNumberOfFiles() {
        return numberOfFiles;
    }

    public Long getNumberOfRows() {
        return numberOfRows;
    }

    public Long getRawDataSize() {
        return rawDataSize;
    }

    public Long getTotalSize() {
        return totalSize;
    }

    public Long getLastUpdateTime() {
        return lastUpdateTime;
    }

    public LocalDateTime getDate() {
        return date;
    }


    public PartitionInfo addDate(final LocalDateTime newDate) {
        return new PartitionInfo(partitionName, collumnStatsAccurate, numberOfFiles, numberOfRows, rawDataSize, totalSize, lastUpdateTime, isMissed, newDate);
    }

    public PartitionInfo withNewName(final String name) {
        return new PartitionInfo(name, collumnStatsAccurate, numberOfFiles, numberOfRows, rawDataSize, totalSize, lastUpdateTime, isMissed, date);
    }
}
