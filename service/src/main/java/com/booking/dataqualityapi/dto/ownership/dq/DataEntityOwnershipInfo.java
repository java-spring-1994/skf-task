package com.booking.dataqualityapi.dto.ownership.dq;

import com.booking.dataqualityapi.dto.dataentity.DataEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

public class DataEntityOwnershipInfo {
    @JsonProperty("data_entity")
    private DataEntity dataEntity;

    @JsonUnwrapped
    private OwnershipMeta ownershipMeta;

    public DataEntityOwnershipInfo() {}

    public DataEntityOwnershipInfo(final DataEntity dataEntity, final OwnershipMeta ownershipMeta) {
        this.dataEntity = dataEntity;
        this.ownershipMeta = ownershipMeta;
    }

    public DataEntityOwnershipInfo(
        final DataEntity dataEntity,
        final long ownerOrgUnitId,
        final String ownerOrgUnitName,
        final int changedByStaffId,
        final String reason) {
        this(dataEntity, new OwnershipMeta(ownerOrgUnitId, ownerOrgUnitName, changedByStaffId, reason));
    }

    public DataEntity getDataEntity() {
        return dataEntity;
    }

    public void setDataEntity(DataEntity dataEntity) {
        this.dataEntity = dataEntity;
    }

    public OwnershipMeta getOwnershipMeta() {
        return ownershipMeta;
    }

    public void setOwnershipMeta(OwnershipMeta ownershipMeta) {
        this.ownershipMeta = ownershipMeta;
    }
}
