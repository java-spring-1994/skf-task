package com.booking.dataqualityapi.dto.ownership.original;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OwnableRegistrationInfo {

    private Ownable ownable;

    @JsonProperty("append_reason")
    private String appendReason;

    public Ownable getOwnable() {
        return ownable;
    }

    public void setOwnable(Ownable ownable) {
        this.ownable = ownable;
    }

    public String getAppendReason() {
        return appendReason;
    }

    public void setAppendReason(String appendReason) {
        this.appendReason = appendReason;
    }
}
