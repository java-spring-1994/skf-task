package com.booking.dataqualityapi.dto;

public enum EntityType {
    HIVE,
    MYSQL
}
