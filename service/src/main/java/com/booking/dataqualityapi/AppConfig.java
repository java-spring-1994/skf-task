package com.booking.dataqualityapi;

import com.booking.spring.EmbeddedServletContainerConfiguration;
import com.booking.spring.controllers.ReadinessProbe;
import com.booking.spring.events.EventsConfiguration;
import com.booking.spring.filter.BookingServletFilter;
import com.booking.spring.filter.FilterConfig;
import com.booking.spring.handler.InterceptorConfigurerAdapter;
import com.booking.spring.listeners.LoggerInitializerListener;
import com.booking.spring.systemsettings.SystemSettingsConfiguration;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;


@Configuration
@Import({
    InterceptorConfigurerAdapter.class,
    EmbeddedServletContainerConfiguration.class,
    EventsConfiguration.class,
    FilterConfig.class,
    BookingServletFilter.class,
    SystemSettingsConfiguration.class
})
@EnableScheduling
public class AppConfig {

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    //---------------- Controllers ----------------//
    @Bean
    ReadinessProbe getReadinessProbe() {
        return new ReadinessProbe();
    }

    //---------------- Listeners ----------------//
    /**
     * Initializing flog appender.
     *
     * @return LoggerInitializerListener
     */
    @Bean
    LoggerInitializerListener getLoggerInitializerListener() {
        return new LoggerInitializerListener();
    }

}
