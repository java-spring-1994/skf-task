package com.booking.dataqualityapi.config;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.context.annotation.Configuration;

import java.io.FileReader;
import java.io.IOException;

/**
 * Created by vfedotov on 10/1/18.
 */

@Configuration
public class GitlabConfig {

    private final String host;
    private final Integer apiVersion;
    private final String token;

    public GitlabConfig() throws Exception {
        host = System.getenv().get("GITLAB_HOST");
        apiVersion = Integer.parseInt(System.getenv().get("GITLAB_API_VERSION"));
        token = readGitlabSecretToken();
    }

    public String getHost() {
        return host;
    }

    public Integer getApiVersion() {
        return apiVersion;
    }

    public String getToken() {
        return token;
    }

    private String readGitlabSecretToken() throws IOException, ParseException {
        String filePath = "/var/run/secrets/booking.com/gitlab_conf.json";
        FileReader reader = new FileReader(filePath);

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);

        return (String) jsonObject.get("gitlab_token");
    }
}
