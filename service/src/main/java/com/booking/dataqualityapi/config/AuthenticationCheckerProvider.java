package com.booking.dataqualityapi.config;

import com.booking.dataqualityapi.auth.checker.AuthXServiceToServiceAuthenticationChecker;
import com.booking.dataqualityapi.auth.checker.AuthXSingleSignOnAuthenticationChecker;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AuthenticationCheckerProvider {
    private static final AuthXSingleSignOnAuthenticationChecker employeeSingleSignOnAuthChecker =
            new AuthXSingleSignOnAuthenticationChecker();
    private static final AuthXServiceToServiceAuthenticationChecker serviceToServiceAuthChecker =
            new AuthXServiceToServiceAuthenticationChecker();

    @Bean
    public AuthXSingleSignOnAuthenticationChecker getEmployeeSingleSignOnAuthChecker() {
        return employeeSingleSignOnAuthChecker;
    }

    @Bean
    public AuthXServiceToServiceAuthenticationChecker getServiceToServiceAuthChecker() {
        return serviceToServiceAuthChecker;
    }
}
