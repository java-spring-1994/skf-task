package com.booking.dataqualityapi.config;

import com.booking.jdbc.DataSourceFactory;
import com.booking.jdbc.DataSources;
import com.booking.jdbc.MysqlDatabaseConfiguration;
import com.booking.jdbc.PooledDataSourceFactory;
import com.zaxxer.hikari.HikariDataSource;
import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class MysqlConnectionConfiguration {

    static {
        DataSourceFactory dataSourceFactory = new PooledDataSourceFactory("dataquality-api") {

            @Override
            protected void configureConnectionPool(MysqlDatabaseConfiguration configuration, HikariDataSource pooledDataSource) {
                switch (configuration.getHandle()) {
                    case "dataquality":
                        pooledDataSource.setMaximumPoolSize(Math.max(pooledDataSource.getMaximumPoolSize(), 50));
                        pooledDataSource.setMinimumIdle(Math.max(pooledDataSource.getMinimumIdle(), 15));
                        pooledDataSource.setRegisterMbeans(false);
                        break;
                    case "office":
                    case "hadoopstats":
                    case "hadoop_resources_stats":
                        pooledDataSource.setMaximumPoolSize(Math.max(pooledDataSource.getMaximumPoolSize(), 10));
                        pooledDataSource.setMinimumIdle(Math.max(pooledDataSource.getMinimumIdle(), 2));
                        pooledDataSource.setRegisterMbeans(false);
                        break;
                    default:
                        break;
                }
            }
        };
        DataSources.init(dataSourceFactory);
    }

    @Primary
    @Bean
    public DataSource getMysqlDqConnectionRw() {
        return DataSources.instance().readWrite("dataquality");
    }

    @Bean
    public DataSource getMysqlDqConnectionRo() {
        return DataSources.instance().readOnly("dataquality");
    }

    @Bean
    public DataSource getMysqlHadoopstatsConnectionRo() {
        return DataSources.instance().readOnly("hadoopstats");
    }

    @Bean
    public DataSource getMysqlHadoopResourcesStatsConnectionRo() {
        return DataSources.instance().readOnly("hadoop_resources_stats");
    }

    @Bean
    public DataSource getMysqlOfficeConnectionRo() {
        return DataSources.instance().readOnly("office");
    }

}
