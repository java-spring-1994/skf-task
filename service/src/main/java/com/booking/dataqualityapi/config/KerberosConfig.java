package com.booking.dataqualityapi.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.booking.bigdata.utils.kerberos")
public class KerberosConfig {
}
