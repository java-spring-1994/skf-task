package com.booking.dataqualityapi.config;

import com.booking.dataqualityapi.utils.DatacenterUtils;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OozieApiConfig {

    private static final String OOZIE_AMS4_HOST_KEY = "OOZIE_API_AMS4_HOST";
    private static final String DEFAULT_OOZIE_AMS4_HOST = "http://hadoop-ams4-oozie.anycast.prod.booking.com:11000";

    private static final String OOZIE_LHR4_HOST_KEY = "OOZIE_API_LHR4_HOST";
    private static final String DEFAULT_OOZIE_LHR4_HOST = "http://hadoop-lhr4-oozie.anycast.prod.booking.com:11000";

    private static final String OOZIE_API_VERSION_KEY = "OOZIE_API_VERSION";
    private static final String DEFAULT_OOZIE_API_VERSION = "2";

    private final String apiHostAms4;
    private final String apiHostLhr4;

    private final int apiVersion;

    public OozieApiConfig() {
        apiHostAms4 = System.getenv().getOrDefault(OOZIE_AMS4_HOST_KEY, DEFAULT_OOZIE_AMS4_HOST);
        apiHostLhr4 = System.getenv().getOrDefault(OOZIE_LHR4_HOST_KEY, DEFAULT_OOZIE_LHR4_HOST);
        apiVersion = Integer.parseInt(System.getenv().getOrDefault(OOZIE_API_VERSION_KEY, DEFAULT_OOZIE_API_VERSION));
    }

    /**
     * Returns base oozie API url for manual http calls.
     * @param datacenter datacenter
     * @return base oozie API url.
     */
    public String getApiBaseUrl(DatacenterUtils.Datacenter datacenter) {
        String endpoint = null;
        switch (datacenter) {
            case AMS4:
                endpoint = String.format("%s/oozie/v%d", apiHostAms4, apiVersion);
                break;
            case LHR4:
                endpoint = String.format("%s/oozie/v%d", apiHostLhr4, apiVersion);
                break;
            default:
                break;
        }
        return endpoint;
    }

    /** Returns base oozie API url for oozie client library.
     * @param datacenter datacenter
     * @return base oozie API url.
     */
    public String getBaseUrlForOozieClientLib(DatacenterUtils.Datacenter datacenter) {
        String endpoint = null;
        switch (datacenter) {
            case AMS4:
                endpoint = String.format("%s/oozie", apiHostAms4);
                break;
            case LHR4:
                endpoint = String.format("%s/oozie", apiHostLhr4);
                break;
            default:
                break;
        }
        return endpoint;
    }
}
