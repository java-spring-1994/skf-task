package com.booking.dataqualityapi.config;

import com.booking.dataqualityapi.services.HiveService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.sql.Connection;
import java.sql.DriverManager;

@Configuration
public class HiveConfig {

    @Autowired
    KerberosConfig kerberosConfig;

    private static final Logger logger = LoggerFactory.getLogger(HiveService.class);
    private static final String DRIVER_NAME = "org.apache.hive.jdbc.HiveDriver";

    public Connection getHiveConnection(String dc) {
        Connection conn = null;
        String hiveEndpoint;
        switch (dc.toLowerCase()) {
            case "lhr4":
                hiveEndpoint = "jdbc:hive2://hadoop-lhr4-hiveserver2.anycast.prod.booking.com:10000/default;principal=hive/_HOST@BKNGDATA.COM";
                break;
            case "ams4":
                hiveEndpoint = "jdbc:hive2://hadoop-ams4-hiveserver2.anycast.prod.booking.com:10000/default;principal=hive/_HOST@BKNGDATA.COM";
                break;
            default:
                hiveEndpoint = "jdbc:hive2://hadoop-lhr4-hiveserver2.anycast.prod.booking.com:10000/default;principal=hive/_HOST@BKNGDATA.COM";
                break;
        }
        try {
            Class.forName(DRIVER_NAME);
            conn = DriverManager.getConnection(hiveEndpoint);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return conn;
    }
}
