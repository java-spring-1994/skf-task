package com.booking.dataqualityapi.config;

import com.booking.dataqualityapi.auth.checker.AuthXSingleSignOnAuthenticationChecker;
import com.booking.systemsettings.SystemSettings;
import io.swagger.models.auth.In;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    public static final String BOOKING_AUTH = "BOOKING_AUTH";

    @Bean
    public Docket postsApi() {

        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("com.booking.dataqualityapi.controllers"))
                .paths(PathSelectors.any())
                .build()
                .ignoredParameterTypes(Authentication.class)
                .apiInfo(apiInfo()).protocols(getProtocols())
                .securitySchemes(Collections.singletonList(apiKey()));

//         uncomment code if booking headers need to be added
//        .globalOperationParameters(
//                        Arrays.asList(getXBookingTimeOut(), getXBookingTkub   opic()));
    }

    private SecurityScheme apiKey() {
        return new ApiKey(BOOKING_AUTH, AuthXSingleSignOnAuthenticationChecker.getCookieName(), In.HEADER.toValue());
    }

    private Set<String> getProtocols() {
        final Set<String> result = new LinkedHashSet<>();
        if (SystemSettings.DEV_SERVER()) {
            result.add("http");
        } else {
            result.add("https");
        }
        return result;
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Data Quality Rest API",
                "Api for interaction with data quality rest service",
                "1.0.2",
                "need to be defined",
                new Contact("Data Quality Team", "http://data.booking.com",
                        "dataquality@booking.com"),
                "License of API", "API license URL", Collections.emptyList());
    }

    /**
     * add XBookingTimeOut header parameter
     * <p>
     * see more https://docs.booking.com/tech_guidelines/api-guidelines/index.html#http-headers
     * <p>
     * 'X-Booking-Timeout-Ms'
     * <p>
     * The client should set this header to inform a server about its timeout.
     * Basically, this is a request cancellation mechanism which helps us stopping wasting server resources once client has timed out.
     * The value set by a client has to comply with max timeout. If no value provided, default timeout will be applied.
     * We will aim to remove default timeout from the frameworks and replace them with default timeouts set by HTTP proxy.
     * Main idea behind this step: there must be no unbounded HTTP request in our infra.
     *
     * @return ope api parameter
     */

    private Parameter getXBookingTimeOut() {
        ParameterBuilder parameterBuilder = new ParameterBuilder();
        parameterBuilder.name("X-Booking-Timeout-Ms")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .defaultValue("1000")
                .required(true)
                .build();
        return parameterBuilder.build();
    }

    /**
     * Self-descriptive name identifying consumer.
     * This is a proven mechanism helping service’s maintainers to identify clients and, if needed, take actions.
     * Also topic allows aggregating information and, for example, providing a client with dashboard reflecting its usage.
     * The good self-descriptive topic would include the client name and usage. ie: mycompany_landingpage_search_hotel
     *
     * @return
     */
    private Parameter getXBookingTopic() {
        ParameterBuilder parameterBuilder = new ParameterBuilder();
        parameterBuilder.name("X-Booking-Topic")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .defaultValue("dataquality")
                .required(true)
                .build();
        return parameterBuilder.build();
    }
}

