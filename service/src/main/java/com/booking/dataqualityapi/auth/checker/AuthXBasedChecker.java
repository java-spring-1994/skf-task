package com.booking.dataqualityapi.auth.checker;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.http.HttpServletRequest;

/**
 * Defines a checker that can authenticate a user based on a provided request, nominally using an AuthX client.
 * Implementors are expected to override a single method,
 * {@link AuthXSingleSignOnAuthenticationChecker#authenticate(HttpServletRequest)},
 * that takes a given {@link HttpServletRequest} and tries to perform an authentication action
 * against it.
 * <p>
 * The intent is that implementors of this interface can be used in a stand-alone fashion (e.g. called inside
 * controller methods) but that they can also be integrated as part of Spring plumbing in filters so that
 * individual controller methods do not have to worry about authentication. In order to support the latter,
 * maintainers should note that we return Spring types and throw exceptions from Spring Security.
 * </p>
 */
public interface AuthXBasedChecker {
    /**
     * Authenticates a given request. Upon successful authentication, the result of the authentication
     * action will be returned (e.g. the user login, a token for further authz processing). Upon failure
     * an {@link AuthenticationException} will be thrown.
     *
     * @param request the request to authenticate.
     * @return the result of a (successful) authentication. This should also, by convention, implement the
     *         {@link com.booking.dataqualityapi.auth.token.AuthXBasedAuthenticationInfo} interface
     * @throws AuthenticationException if we failed to authenticate the request.
     */
    Authentication authenticate(HttpServletRequest request) throws AuthenticationException;
}
