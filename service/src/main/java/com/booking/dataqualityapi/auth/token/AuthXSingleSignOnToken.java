package com.booking.dataqualityapi.auth.token;

import com.booking.authxclient.response.AuthenticateRefreshTokenResponse;
import org.springframework.security.authentication.AbstractAuthenticationToken;

import java.util.Collections;
import java.util.Optional;

/**
 * An {@link AbstractAuthenticationToken} populated from the results of calling AuthX to refresh a
 * Single Sign On (SSO) token. Objects of this class are assumed to be authenticated by default.
 */
public class AuthXSingleSignOnToken extends AbstractAuthenticationToken implements AuthXBasedAuthenticationInfo {
    private AuthenticateRefreshTokenResponse refreshTokenResponse;

    /**
     * Creates a new AuthXSingleSignOnToken.
     * @param refreshTokenResponse the response received from AuthX after refreshing a SSO token.
     */
    public AuthXSingleSignOnToken(final AuthenticateRefreshTokenResponse refreshTokenResponse) {
        super(Collections.emptyList());
        this.refreshTokenResponse = refreshTokenResponse;
        this.setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return getAccessToken();
    }

    @Override
    public Object getPrincipal() {
        return this.refreshTokenResponse.getStaffLoginName();
    }

    /**
     * The new refresh token received from AuthX.
     *
     * @return a refresh token that can be populated in a cookie sent back to the caller so that they
     *         can present it as part of authenticating subsequent calls.
     */
    public String getRefreshToken() {
        return this.refreshTokenResponse.getRefreshToken();
    }

    @Override
    public Optional<String> getAccessToken() {
        return Optional.of(this.refreshTokenResponse.getAccessToken());
    }

    @Override
    public AuthXBasedAuthenticationTokenType getTokenType() {
        return AuthXBasedAuthenticationTokenType.EMPLOYEE_SSO;
    }
}
