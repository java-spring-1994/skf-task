package com.booking.dataqualityapi.auth.token;

/**
 * An unchecked exception that is thrown when we fail to retrieve the attribute from an access token
 * via AuthX.
 */
public class SubjectAttributeRetrievalFailedException extends RuntimeException {
    public SubjectAttributeRetrievalFailedException(final Throwable t) {
        super(t);
    }
}
