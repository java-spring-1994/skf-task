package com.booking.dataqualityapi.auth.token;

import java.util.Optional;

/**
 * An interface defining the basic information that we can pull out about a request that was
 * authenticated via AuthX.
 */
public interface AuthXBasedAuthenticationInfo {
    /**
     * Retrieves the access token from a successful authentication. This token can be used for
     * subsequent authorization checks.
     *
     * @return an empty {@link Optional} if there was not access token from the authentication, otherwise
     *         a populated {@link Optional} containing the access token.
     */
    Optional<String> getAccessToken();

    /**
     * Returns how the authentication was done (e.g. employee SSO, S2S (Service to Service)).
     *
     * @return how the authentication was done.
     */
    AuthXBasedAuthenticationTokenType getTokenType();
}
