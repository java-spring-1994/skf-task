package com.booking.dataqualityapi.auth.token;

import com.booking.authxclient.exceptions.AuthXClientException;
import org.springframework.security.authentication.AbstractAuthenticationToken;

import java.util.Collections;
import java.util.Optional;

/**
 * An {@link AbstractAuthenticationToken} populated from the results of calling AuthX to perform
 * S2S (service to service) authentication. Objects of this class are assumed to be authenticated by default.
 * <p>
 * If it was provided in the request, this class also contains an IAM access token (e.g. the service is making a
 * call on behalf of a user identified by the IAM access token). If the IAM access token is provided it is
 * assumed to have been validated by the creator of objects of this class.
 * </p>
 */
public class AuthXServiceToServiceToken extends AbstractAuthenticationToken implements AuthXBasedAuthenticationInfo {

    private final String serviceName;
    private final Optional<String> iamAccessToken;
    private final Optional<String> staffLoginName;
    private final Optional<Long> staffUserId;


    /**
     * Creates a new AuthXServiceToServiceToken.
     *
     * @param serviceName    The name of the service that was successfully authenticated (i.e. the service that
     *                       sent the request).
     * @param iamAccessToken The IAM access token included in the request, if any.
     */
    public AuthXServiceToServiceToken(
            final String serviceName,
            final Optional<String> iamAccessToken,
            final Optional<String> staffLoginName,
            final Optional<Long> staffUserId) {
        super(Collections.emptyList());
        setAuthenticated(true);

        this.serviceName = serviceName;
        this.iamAccessToken = iamAccessToken;
        this.staffLoginName = staffLoginName;
        this.staffUserId = staffUserId;
    }

    @Override
    public Optional<String> getAccessToken() {
        return this.iamAccessToken;
    }

    @Override
    public AuthXBasedAuthenticationTokenType getTokenType() {
        return AuthXBasedAuthenticationTokenType.S2S;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return this.serviceName;
    }

    /**
     * Retrieves the login name of the staff member that the IAM access token refers to.
     *
     * @return An empty {@link Optional} if there is no IAM access token, otherwise a populated {@link Optional}
     *         with the login name.
     */
    public Optional<String> getStaffLoginName() {
        return staffLoginName;
    }

    /**
     * Retrieves the numeric user ID of the staff member that the IAM access token refers to.
     *
     * @return An empty {@link Optional} if there is no IAM access token, otherwise a populated {@link Optional}
     *         with the user ID.
     * @throws AuthXClientException if there was an error communicating with AuthX.
     */
    public Optional<Long> getStaffUserId() {
        return this.staffUserId;
    }
}
