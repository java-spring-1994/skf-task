package com.booking.dataqualityapi.auth;

import com.booking.dataqualityapi.auth.checker.AuthXBasedChecker;
import com.booking.dataqualityapi.auth.filter.AuthXCheckerWrappingFilter;
import com.booking.dataqualityapi.config.AuthenticationCheckerProvider;
import com.google.common.collect.ImmutableMap;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * This class defines the web security configuration for the application. It can be used, for example,
 * to configure authentication against certain requests by method (e.g. PUT) and path.
 */
@EnableWebSecurity
@Order
public class DataQualityApiSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    private static AuthenticationCheckerProvider authenticationCheckerProvider =
            new AuthenticationCheckerProvider();

    @SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
    private static final Map<String, List<String>> EMPLOYEE_SSO_ANT_MATCHER_PATH_AND_METHODS =
            ImmutableMap.<String, List<String>>builder()
                    .put("/wiki/*/*/*", Arrays.asList(HttpMethod.POST.name()))
                    .put("/wiki/workflow/*", Arrays.asList(HttpMethod.POST.name()))
                    .put("/tag", Arrays.asList(HttpMethod.POST.name()))
                    .put("/tag/*/*/*", Arrays.asList(HttpMethod.POST.name()))
                    .put("/tag/*/*/*/*", Arrays.asList(HttpMethod.DELETE.name()))
                    .put("/dqguard/*/*", Arrays.asList(HttpMethod.POST.name()))
                    .put("/auth", Arrays.asList(HttpMethod.GET.name()))
                    .put("/anomalies/alerts-configuration/*/*/*/*",
                            Arrays.asList(HttpMethod.POST.name(), HttpMethod.DELETE.name()))
                    .put("/metrics/*/*/*", Arrays.asList(HttpMethod.POST.name()))
                    .put("/metrics/*/*/approve_request_monitoring", Arrays.asList(HttpMethod.POST.name()))
                    .put("/metrics/*/*/reject_request_monitoring", Arrays.asList(HttpMethod.POST.name()))
                    .build();

    private static final Map<String, List<String>> S2S_ANT_MATCHER_PATH_AND_METHODS =
            ImmutableMap.<String, List<String>>builder()
                    .build();

    private static final Map<String, List<String>> SSO_AND_S2S_ANT_MATCHER_PATH_AND_METHODS =
            ImmutableMap.<String, List<String>>builder()
                    .put("/catalogue-metric/*", Arrays.asList(HttpMethod.POST.name(), HttpMethod.DELETE.name()))
                    .build();

    /**
     * Configures web security for the application. The currently does not enforce authentication
     * or authorisation globally (i.e. all requests will pass through Spring's filters successfully)
     * but this does not stop individual controller methods from applying their own authentication
     * and authorization controls.
     * <p>
     * As the application is largely read-only for now this mode is probably OK, but as it grows
     * it makes more sense to apply the authentication centrally here rather than making it the
     * responsibility of each controller method.
     * </p>
     *
     * @param http the HTTP security context for the application.
     * @throws Exception inherited from superclass.
     */
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.cors()
            .and().csrf().disable()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
                .authorizeRequests()
                    .antMatchers("/**").permitAll();

        addEmployeeSingleSignOnRequestMatcher(http);
        addServiceToServiceRequestMatcher(http);
        addSingleSignOnAndServiceToServiceComboRequestMatcher(http);
    }

    private void addEmployeeSingleSignOnRequestMatcher(final HttpSecurity http) throws Exception {
        addRequestMatchersAndFilterCommon(
                EMPLOYEE_SSO_ANT_MATCHER_PATH_AND_METHODS,
                Collections.singletonList(authenticationCheckerProvider.getEmployeeSingleSignOnAuthChecker()),
                http);
    }

    private void addServiceToServiceRequestMatcher(final HttpSecurity http) throws Exception {
        addRequestMatchersAndFilterCommon(
                S2S_ANT_MATCHER_PATH_AND_METHODS,
                Collections.singletonList(authenticationCheckerProvider.getServiceToServiceAuthChecker()),
                http);
    }

    private void addSingleSignOnAndServiceToServiceComboRequestMatcher(
            final HttpSecurity http) throws Exception {
        addRequestMatchersAndFilterCommon(
                SSO_AND_S2S_ANT_MATCHER_PATH_AND_METHODS,
                Arrays.asList(
                        authenticationCheckerProvider.getServiceToServiceAuthChecker(),
                        authenticationCheckerProvider.getEmployeeSingleSignOnAuthChecker()),
                http);
    }

    private void addRequestMatchersAndFilterCommon(
            final Map<String, List<String>> requestMatcherPathAndMethods,
            final List<AuthXBasedChecker> checkers,
            final HttpSecurity http) throws Exception {
        if (requestMatcherPathAndMethods.isEmpty()) {
            return;
        }

        final List<RequestMatcher> matchers = new ArrayList<>();
        for (Map.Entry<String, List<String>> entry : requestMatcherPathAndMethods.entrySet()) {
            for (String method : entry.getValue()) {
                matchers.add(new AntPathRequestMatcher(entry.getKey(), method));
            }
        }

        final OrRequestMatcher orMatcher = new OrRequestMatcher(matchers);
        final AuthXCheckerWrappingFilter wrappingFilter = new AuthXCheckerWrappingFilter(
                orMatcher,
                checkers);

        http.authorizeRequests()
                .requestMatchers(orMatcher).authenticated()
            .and()
                .addFilterAt(wrappingFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
