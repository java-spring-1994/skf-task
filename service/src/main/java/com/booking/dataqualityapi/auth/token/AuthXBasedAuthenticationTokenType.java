package com.booking.dataqualityapi.auth.token;

/**
 * Identifies how the authentication of a particular request was done. Offered as an alternative to checking
 * the types of the different {@link AuthXBasedAuthenticationInfo} objects.
 */
public enum AuthXBasedAuthenticationTokenType {
    /** Employee Single Sign On. */
    EMPLOYEE_SSO,

    /** Service to Service. */
    S2S;
}
