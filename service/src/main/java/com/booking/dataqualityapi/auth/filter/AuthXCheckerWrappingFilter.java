package com.booking.dataqualityapi.auth.filter;

import com.booking.dataqualityapi.auth.checker.AuthXBasedChecker;
import com.booking.dataqualityapi.auth.checker.AuthXSingleSignOnAuthenticationChecker;
import com.booking.dataqualityapi.auth.token.AuthXSingleSignOnToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * A wrapper around one or more {@link com.booking.dataqualityapi.auth.checker.AuthXBasedChecker} objects
 * so that we can invoke them as part of Spring plumbing. The key bits are:
 * <ul>
 *     <li>
 *         We override the {@link #attemptAuthentication(HttpServletRequest, HttpServletResponse)} method so that
 *         it calls the checker. Since the checker already returns an {@link Authentication} object and throws
 *         {@link AuthenticationException} we are good to just call it
 *     </li>
 *     <li>
 *         We also override {@link #successfulAuthentication(HttpServletRequest, HttpServletResponse, FilterChain, Authentication)}
 *         because the default behaviour does redirects and such that we don't want. Because this is done in the
 *         context of an API operation we just want to continue processing the request after a successful
 *         authentication. Our override implementation does this by setting the {@link Authentication} in the
 *         {@link SecurityContextHolder} and then continues processing by calling
 *         {@link FilterChain#doFilter(ServletRequest, ServletResponse)}
 *     </li>
 * </ul>
 */
public class AuthXCheckerWrappingFilter extends AbstractAuthenticationProcessingFilter {

    private final List<AuthXBasedChecker> authCheckers;

    public AuthXCheckerWrappingFilter(
            final RequestMatcher requiresAuthenticationRequestMatcher,
            final List<AuthXBasedChecker> authChecker) {
        super(requiresAuthenticationRequestMatcher);

        if (authChecker.size() == 0) {
            throw new IllegalArgumentException("At least one auth checker needs to be provided");
        }

        this.authCheckers = authChecker;
    }

    @Override
    public Authentication attemptAuthentication(
            final HttpServletRequest httpServletRequest,
            final HttpServletResponse httpServletResponse) throws AuthenticationException {
        for (int i = 0; i < this.authCheckers.size(); i++) {
            try {
                return this.authCheckers.get(i).authenticate(httpServletRequest);
            } catch (AuthenticationException e) {
                // Throw on the last one, otherwise move onto the other checkers
                if (i == this.authCheckers.size() - 1) {
                    throw e;
                }
            }
        }

        throw new IllegalStateException("No return from any configured auth provider");
    }

    @Override
    protected void successfulAuthentication(
            final HttpServletRequest httpServletRequest,
            final HttpServletResponse httpServletResponse,
            final FilterChain chain,
            final Authentication authResult) throws IOException, ServletException {
        SecurityContextHolder.getContext().setAuthentication(authResult);

        // For Single Sign On, the AuthX docs tell us to send back the new refresh token as a cookie
        // in the response
        if (authResult instanceof AuthXSingleSignOnToken) {
            final AuthXSingleSignOnToken token = (AuthXSingleSignOnToken) authResult;
            httpServletResponse.addCookie(
                    new Cookie(AuthXSingleSignOnAuthenticationChecker.getCookieName(), token.getRefreshToken()));
        }

        chain.doFilter(httpServletRequest, httpServletResponse);
    }
}
