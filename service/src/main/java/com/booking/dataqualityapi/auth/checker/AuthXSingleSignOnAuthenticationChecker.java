package com.booking.dataqualityapi.auth.checker;

import com.booking.authxclient.AuthXClient;
import com.booking.authxclient.exceptions.AuthXClientException;
import com.booking.authxclient.response.AuthenticateRefreshTokenResponse;
import com.booking.dataqualityapi.auth.AuthUtils;
import com.booking.dataqualityapi.auth.token.AuthXSingleSignOnToken;
import com.booking.systemsettings.SystemSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Optional;

/**
 * Defines a checker that can authenticate a user based on a provided request, nominally using an AuthX client
 * and employee SSO authentication.
 * <p>
 * The authentication done by this checker is to the level that the cookie in the request is valid and represents
 * a user (that AuthX tells us). Whether the user is allowed to call particular operations is outside the
 * scope of this checker.
 * </p>
 */
public class AuthXSingleSignOnAuthenticationChecker implements AuthXBasedChecker {
    private static final Logger logger = LoggerFactory.getLogger(AuthXSingleSignOnAuthenticationChecker.class);

    protected static final String SSO_COOKIE_NAME = "bkng_iam_rt";
    protected static final String SSO_COOKIE_NAME_DQS = "dqs_bkng_iam_rt";

    /**
     * For authentication driven by checking/reading the value of a cookie in a request, the name of
     * the cookie to check.
     *
     * @return the name of the cookie to check.
     */
    public static String getCookieName() {
        return (SystemSettings.DEV_OR_DQS_SERVER()) ? SSO_COOKIE_NAME_DQS : SSO_COOKIE_NAME;
    }

    @Override
    public AuthXSingleSignOnToken authenticate(
            final HttpServletRequest httpServletRequest) throws AuthenticationException {
        return authenticate(httpServletRequest, AuthUtils.AUTH_X_CLIENT);
    }

    /**
     * The internal logic for authenticating a request. Exists as a separate, package private method for testing
     * purposes - consumers are expected to use the overridden method instead.
     *
     * @param httpServletRequest the request to authenticate.
     * @param authXClientToUse the client to use to communicate with AuthX.
     * @return the result of a (successful) authentication.
     * @throws AuthenticationException if we failed to authenticate the request.
     */
    AuthXSingleSignOnToken authenticate(
            final HttpServletRequest httpServletRequest,
            final AuthXClient authXClientToUse) throws AuthenticationException {
        final String cookieName = getCookieName();
        final Cookie[] requestCookies = httpServletRequest.getCookies();

        if (requestCookies == null || requestCookies.length == 0) {
            logger.debug(
                    "Failed to authenticate {} on {} due to no cookies sent in the request",
                    httpServletRequest.getMethod(),
                    httpServletRequest.getRequestURI(),
                    cookieName);
            throw new AuthenticationCredentialsNotFoundException(
                    String.format("Failed to authenticate due to no %s cookie in the request", cookieName));
        }

        final Optional<Cookie> requestCookie = Arrays.stream(requestCookies)
                .filter(rc -> cookieName.equals(rc.getName()))
                .findAny();
        if (!requestCookie.isPresent()) {
            logger.debug(
                    "Failed to authenticate {} on {} due to no {} cookie sent in the request",
                    httpServletRequest.getMethod(),
                    httpServletRequest.getRequestURI(),
                    cookieName);
            throw new AuthenticationCredentialsNotFoundException(
                    String.format("Failed to authenticate due to no %s cookie in the request", cookieName));
        }

        try {
            final AuthenticateRefreshTokenResponse refreshTokenResponse =
                    authXClientToUse.authenticateRefreshToken(requestCookie.get().getValue());

            return new AuthXSingleSignOnToken(refreshTokenResponse);
        } catch (AuthXClientException e) {
            logger.error("Error while trying to refresh token from auth cookie", e);
            throw new AuthenticationServiceException("Unable to perform authentication", e);
        }
    }
}
