package com.booking.dataqualityapi.auth.checker;

import com.booking.authxclient.AuthXClient;
import com.booking.authxclient.PassportAccessRequestHeaders;
import com.booking.authxclient.exceptions.AuthXClientException;
import com.booking.dataqualityapi.auth.AuthUtils;
import com.booking.dataqualityapi.auth.token.AuthXServiceToServiceToken;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Defines a checker that can authenticate a user based on a provided request, nominally using an AuthX client
 * and S2S (Service To Service) authentication.
 * <p>
 * At a minimum, the request has to contain a header with the token for S2S authentication. The authentication
 * done by this checker is to the level that the token is valid (based on what AuthX) told us and that it represents
 * a given service. Whether the service is allowed to call particular operations is outside the scope of this
 * checker.
 * <p>
 * Optionally, the request can also contain a header with an IAM access token for a given user (i.e. the
 * service called us on-behalf-of a human user). If an IAM access token was provided in the headers then we
 * also check that it is valid. "Valid" in this context means that AuthX tells us it is valid, as opposed
 * to any kind of deep authorization check.
 * </p>
 */
public class AuthXServiceToServiceAuthenticationChecker implements AuthXBasedChecker {
    private static final Logger logger = LoggerFactory.getLogger(AuthXServiceToServiceAuthenticationChecker.class);

    /**
     * {@inheritDoc}
     *
     * @param httpServletRequest the request to authenticate.
     * @return the result of a (successful) authentication.
     * @throws AuthenticationException if we failed to authenticate the request.
     */
    @Override
    public AuthXServiceToServiceToken authenticate(
            final HttpServletRequest httpServletRequest) throws AuthenticationException {
        return authenticate(httpServletRequest, AuthUtils.AUTH_X_CLIENT);
    }

    /**
     * The internal logic for authenticating a request. Exists as a separate, package private method for testing
     * purposes - consumers are expected to use the overridden method instead.
     *
     * @param httpServletRequest the request to authenticate.
     * @param authXClientToUse the client to use to communicate with AuthX.
     * @return the result of a (successful) authentication.
     * @throws AuthenticationException if we failed to authenticate the request.
     */
    AuthXServiceToServiceToken authenticate(
            final HttpServletRequest httpServletRequest,
            final AuthXClient authXClientToUse) throws AuthenticationException {

        final String s2sTokenHeader = httpServletRequest.getHeader(
                PassportAccessRequestHeaders.X_BOOKING_AUTHENTICATION);
        if (s2sTokenHeader == null) {
            logger.debug(
                    "Failed to authenticate {} on {} due to no {} header sent in the request",
                    httpServletRequest.getMethod(),
                    httpServletRequest.getRequestURI(),
                    PassportAccessRequestHeaders.X_BOOKING_AUTHENTICATION);
            throw new AuthenticationCredentialsNotFoundException(
                    String.format(
                            "Failed to authenticate due to no %s header in the request",
                            PassportAccessRequestHeaders.X_BOOKING_AUTHENTICATION));
        }

        // It is possible not to have one of these, e.g. if the service is not calling on behalf of a user
        final Optional<String> iamAccessToken = Optional.ofNullable(
                httpServletRequest.getHeader(PassportAccessRequestHeaders.X_BOOKING_IAM_ACCESS_TOKEN));

        final String serviceName;
        try {
            serviceName = authXClientToUse.s2sAuthenticate(s2sTokenHeader);
        } catch (AuthXClientException e) {
            logger.error("Unable to authenticate token for S2S auth", e);
            throw new AuthenticationServiceException("Unable to perform S2S auth", e);
        }

        if (iamAccessToken.isPresent()) {
            try {
                final Boolean tokenValid = authXClientToUse.iamValidateAccessToken(iamAccessToken.get());
                if (!BooleanUtils.isTrue(tokenValid)) {
                    logger.error("S2S auth was successful, but the provided IAM access token was invalid");
                    throw new BadCredentialsException("Invalid S2S auth token or IAM access token");
                }
                final Long userId = authXClientToUse.getSubjectAttribute(
                        "user_id",
                        Long.class,
                        PassportAccessRequestHeaders.newBuilder()
                                .setIamAccessToken(iamAccessToken.get())
                                .build());
                final String staffLoginName = authXClientToUse.getSubjectAttribute(
                        "ad.loginnamne",
                        String.class,
                        PassportAccessRequestHeaders.newBuilder()
                                .setIamAccessToken(iamAccessToken.get())
                                .build());
                return new AuthXServiceToServiceToken(serviceName,iamAccessToken,Optional.of(staffLoginName),Optional.of(userId));
            } catch (AuthXClientException e) {
                logger.error(
                        "S2S auth was successful, but we were unable to call AuthX to check the "
                        + "provided IAM access token",
                        e);
                throw new AuthenticationServiceException("Unable to perform authentication", e);
            }

        }
        return new AuthXServiceToServiceToken(serviceName, Optional.empty(),  Optional.empty(), Optional.empty());
    }
}
