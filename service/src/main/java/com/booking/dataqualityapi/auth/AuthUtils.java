package com.booking.dataqualityapi.auth;

import com.booking.authxclient.AuthXClient;
import com.booking.authxclient.AuthXClients;
import com.booking.authxclient.PassportAccessRequestHeaders;
import com.booking.authxclient.exceptions.AuthXClientException;
import com.booking.dataqualityapi.auth.token.AuthXBasedAuthenticationInfo;
import com.booking.dataqualityapi.auth.token.AuthXBasedAuthenticationTokenType;
import com.booking.dataqualityapi.auth.token.AuthXServiceToServiceToken;
import com.booking.dataqualityapi.auth.token.AuthXSingleSignOnToken;
import com.booking.systemsettings2.SystemSettings;
import com.booking.systemsettings2.SystemSettingsFactory;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class AuthUtils {

    private static final Set<Long> DM_TEAM_IDS = new HashSet<>();

    static {
        DM_TEAM_IDS.add(47808L);
        DM_TEAM_IDS.add(3851L);
        DM_TEAM_IDS.add(69198L);
        DM_TEAM_IDS.add(34888L);
    }

    public static final AuthXClient AUTH_X_CLIENT = AuthXClients.create();

    private static final String UNKNOWN_PRINCIPLE = "UNKNOWN";
    private static SystemSettings systemSettings = SystemSettingsFactory.getSystemSettings();
    public static final boolean IS_DEV_SERVER = systemSettings.isDevServer();

    public static boolean hasValidAuthentication(
            final Authentication authentication,
            final Set<AuthXBasedAuthenticationTokenType> validTokenTypes) {
        if (authentication == null) {
            return false;
        }

        if (!(authentication instanceof AuthXBasedAuthenticationInfo)) {
            return false;
        }

        return validTokenTypes.contains(((AuthXBasedAuthenticationInfo) authentication).getTokenType());
    }

    public static Optional<String> getLoginName(final Authentication authentication) {
        if (IS_DEV_SERVER) {
            return Optional.of(UNKNOWN_PRINCIPLE);
        }
        if (authentication instanceof AuthXSingleSignOnToken) {
            return Optional.of(authentication.getPrincipal().toString());
        } else if (authentication instanceof AuthXServiceToServiceToken) {
            return Optional.of(((AuthXServiceToServiceToken) authentication).getPrincipal().toString());
        } else {
            return Optional.empty();
        }
    }

    /**
     * Converts login name to staff id.
     *
     * @param loginName - user login name, can be obtained from cookie, see getLoginName function
     * @return staffId - booking user unique identifier
     * @throws AuthXClientException - some auth troubles
     */

    public static Long getStaffId(final String loginName) throws AuthXClientException {
        if (IS_DEV_SERVER) {
            return 1L;
        }
        PassportAccessRequestHeaders headers = PassportAccessRequestHeaders.newBuilder()
                .setStaffLoginname(loginName)
                .build();
        Long staffId = AUTH_X_CLIENT.getSubjectAttribute("id", Long.class, headers);
        return staffId;
    }

    public static Integer getStaffId(final Authentication authentication) throws AuthXClientException {
        final String staffLoginName = AuthUtils.getLoginName(authentication)
                .orElseThrow(() -> new AuthenticationCredentialsNotFoundException("No staff login name found in the request "));
        final Long staffId = AuthUtils.getStaffId(staffLoginName);
        return Math.toIntExact(staffId);
    }

    public static boolean isDMTeam(final Long id) {
        return DM_TEAM_IDS.contains(id);
    }

    public static boolean matchesLoginName(
            final Authentication authentication,
            final String loginName) {
        final Optional<String> loginNameFromAuthentication = getLoginName(authentication);

        if (!loginNameFromAuthentication.isPresent()) {
            return false;
        }

        return loginName.equalsIgnoreCase(loginNameFromAuthentication.get());
    }
}
