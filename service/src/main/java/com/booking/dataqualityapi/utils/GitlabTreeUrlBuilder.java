package com.booking.dataqualityapi.utils;

import java.net.MalformedURLException;
import java.net.URL;
import org.apache.commons.lang.StringUtils;

public class GitlabTreeUrlBuilder {

    // todo use env var GITLAB_HOST
    private static final String GITLAB_HOST = "https://gitlab.booking.com";

    public static String build(final String group, final String repo, final String branch, final String path)
        throws MalformedURLException {

        String url = GITLAB_HOST
            + '/' + group
            + '/' + repo
            + '/' + "tree"
            + '/' + branch
            + '/' + StringUtils.stripStart(path, "/");

        // for validation only
        new URL(url);

        return url;
    }
}
