package com.booking.dataqualityapi.utils;

import java.util.Arrays;

/**
 * Utility class for partition operations.
 */
public class PartitionsUtil {

    private PartitionsUtil() {
        //utility class
    }

    private static final String[] datePartitionPatterns = new String[] {"yyyy_mm_dd", "yyyymmdd", "yyyy-mm-dd", "date"};

    private static final String[] timePartitionPatterns = new String[] {"hh","hour"};

    public static boolean guessIsTimePartition(String partitionName) {
        return Arrays.stream(timePartitionPatterns).anyMatch(partitionName::contains);
    }

    public static boolean guessIsDatePartition(String partitionName) {
        return Arrays.stream(datePartitionPatterns).anyMatch(partitionName::contains);
    }

    /**
     * Guess partition column timeseries type, {date, time or unknown}.
     */
    public static String guessPartitionColumnTimeSeriesType(String partitionColumnName) {
        if (partitionColumnName == null) {
            return "unknown";
        }
        if (guessIsDatePartition(partitionColumnName)) {
            return "date";
        }
        if (guessIsTimePartition(partitionColumnName)) {
            return "hour";
        }
        return "unknown";
    }

}
