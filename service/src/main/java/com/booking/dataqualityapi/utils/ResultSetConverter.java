package com.booking.dataqualityapi.utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class ResultSetConverter {

    public static JSONObject convertToJson(String tableName, ResultSet rs, boolean preserveTypes)
            throws SQLException {

        JSONObject response = new JSONObject();
        ResultSetMetaData rsmd = rs.getMetaData();

        String columnName = "";

        JSONArray columnNames = new JSONArray();
        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
            columnNames.add(removeTableNameFromColumnName(tableName, rsmd.getColumnName(i)));
        }

        JSONArray rows = new JSONArray();
        JSONObject row = null;
        while (rs.next()) {
            int numColumns = rsmd.getColumnCount();
            row = new JSONObject();

            for (int i = 1; i <= numColumns; i++) {
                columnName = removeTableNameFromColumnName(tableName, rsmd.getColumnName(i));

                if (preserveTypes) {
                    if (rsmd.getColumnType(i) == java.sql.Types.BIGINT) {
                        row.put(columnName, rs.getInt(columnName));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.BOOLEAN) {
                        row.put(columnName, rs.getBoolean(columnName));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.DOUBLE) {
                        row.put(columnName, rs.getDouble(columnName));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.FLOAT) {
                        row.put(columnName, rs.getFloat(columnName));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.INTEGER) {
                        row.put(columnName, rs.getInt(columnName));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.VARCHAR) {
                        row.put(columnName, rs.getString(columnName));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.TINYINT) {
                        row.put(columnName, rs.getInt(columnName));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.SMALLINT) {
                        row.put(columnName, rs.getInt(columnName));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.DATE) {
                        row.put(columnName, rs.getDate(columnName));
                    } else if (rsmd.getColumnType(i) == java.sql.Types.TIMESTAMP) {
                        row.put(columnName, rs.getTimestamp(columnName));
                    } else {
                        row.put(columnName, rs.getObject(columnName));
                    }
                } else {
                    row.put(columnName, rs.getString(columnName));
                }
            }

            rows.add(row);
        }

        response.put("columns", columnNames);
        response.put("data", rows);

        return response;
    }

    private static String removeTableNameFromColumnName(String tableName, String columnName) {
        return columnName.replaceFirst(tableName + "\\.", "");
    }
}
