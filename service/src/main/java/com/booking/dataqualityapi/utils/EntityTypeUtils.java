package com.booking.dataqualityapi.utils;

public final class EntityTypeUtils {
    private EntityTypeUtils() {

    }

    /**
     * Utility method to map the entity type in the dto to the value in
     * dq_data_entity table entity type i.e "hive-table". It is redundant
     * to use table while exposing the API outside. So the new DTO Just uses
     * the base types like HIVE, MYSQL.
     *
     * @param name is the enum of the EntityTye DTO
     * @return the value mapping of the enums in the dq_data_entity
     */
    public static final String getEntityTypeInDqEntity(String name) {
        name = name.toUpperCase();
        if (name.equals("HIVE")) {
            return "hive-table";
        } else if (name.equals("MYSQL")) {
            return "mysql-table";
        } else {
            return "";
        }
    }
}
