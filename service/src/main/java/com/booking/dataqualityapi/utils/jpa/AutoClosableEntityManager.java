package com.booking.dataqualityapi.utils.jpa;

import javax.persistence.EntityManager;

public class AutoClosableEntityManager implements AutoCloseable {
    private EntityManager em;

    private AutoClosableEntityManager(EntityManager em) {
        this.em = em;
    }

    public static AutoClosableEntityManager wrap(EntityManager em) {
        return new AutoClosableEntityManager(em);
    }

    public EntityManager getInstance() {
        return em;
    }

    @Override
    public void close() {
        if (em != null) {
            em.close();
        }
    }
}
