package com.booking.dataqualityapi.utils;

import java.util.Arrays;
import java.util.List;

/**
 * Utility class for convertion datacenter to instance table name.
 */
public final class DatacenterUtils {

    private DatacenterUtils() {
        //utility class
    }

    public static String getHiveInstanceTable(String dc) {
        switch (dc.toLowerCase()) {
            case "gc":
                return "dq_hive_gc_table_instance";
            case "ams4":
                return "dq_hive_ams4_table_instance";
            case "lhr4":
                return "dq_hive_lhr4_table_instance";
            default:
                throw new UnsupportedOperationException("Unsupported datacenter " + dc);
        }
    }

    public static String getHiveColumnMeta(String dc) {
        switch (dc.toLowerCase()) {
            case "gc":
                return "dq_hive_gc_column_meta";
            case "ams4":
                return "dq_hive_ams4_column_meta";
            case "lhr4":
                return "dq_hive_lhr4_column_meta";
            default:
                throw new UnsupportedOperationException("Unsupported datacenter " + dc);
        }
    }

    public static String getHivePartitionsList(String dc) {
        switch (dc.toLowerCase()) {
            case "lhr4":
                return "dq_hive_lhr4_partition_list";
            case "ams4":
                return "dq_hive_ams4_partition_list";
            case "gc":
                return "dq_hive_gc_partition_list";
            default:
                throw new IllegalArgumentException(String.format("Invalid datacenter name [%s]", dc));
        }
    }

    public enum Datacenter {
        AMS4,
        LHR4,
        GC
    }

    public static final List<Datacenter> DEFAULT_OOZIE_DC_LIST = Arrays.asList(
        Datacenter.AMS4, Datacenter.LHR4
    );
}
