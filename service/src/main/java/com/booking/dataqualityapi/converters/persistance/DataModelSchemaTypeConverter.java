package com.booking.dataqualityapi.converters.persistance;

import com.booking.dataqualityapi.dto.datamodel.DataModelSchemaType;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class DataModelSchemaTypeConverter implements AttributeConverter<DataModelSchemaType, String> {
    @Override
    public String convertToDatabaseColumn(DataModelSchemaType state) {
        return state.toString();
    }

    @Override
    public DataModelSchemaType convertToEntityAttribute(String state) {
        return DataModelSchemaType.fromString(state);
    }
}