package com.booking.dataqualityapi;

import com.booking.spring.filter.BookingServletFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication(
    scanBasePackages = {"com.booking.dataqualityapi", "com.booking.spring.exception"},
    scanBasePackageClasses = {BookingServletFilter.class},
    exclude = {ErrorMvcAutoConfiguration.class})
@EntityScan(
    basePackages = {"com.booking.dataqualityapi", "com.booking.dataquality"})
public class MainApplication {
    private static Logger logger = LoggerFactory.getLogger(MainApplication.class);

    public static void main(String[] args) {
        logger.warn("Running the spring-boot application...");
        SpringApplication.run(MainApplication.class, args);

    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            // .. TODO ..
        };
    }
}