package com.booking.dataqualityapi.exceptions;

/**
 * Created by vfedotov on 10/16/18.
 */
public class GitlabException extends Exception {
    public GitlabException(String message) {
        super(message);
    }
}
