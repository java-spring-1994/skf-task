package com.booking.dataqualityapi.exceptions;

public class DataEntityRegistrationException extends Exception {
    public DataEntityRegistrationException(String message) {
        super(message);
    }
}
