package com.booking.dataqualityapi.exceptions;

public class DataUnavailableException extends Exception {
    public DataUnavailableException(String message) {
        super(message);
    }
}
