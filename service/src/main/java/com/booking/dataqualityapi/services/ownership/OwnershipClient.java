package com.booking.dataqualityapi.services.ownership;

import com.booking.authxclient.AuthXClient;
import com.booking.authxclient.AuthXClients;
import com.booking.authxclient.exceptions.AuthXClientException;
import com.booking.dataqualityapi.dto.ownership.original.OwnableReassignmentInfo;
import com.booking.dataqualityapi.dto.ownership.original.OwnableRegistrationInfo;
import com.booking.dataqualityapi.dto.ownership.original.OwnablesToRetrieve;
import com.booking.systemsettings.SystemSettings;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class OwnershipClient {

    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory.getLogger(OwnershipClient.class);

    private static final String ENDPOINT =
        SystemSettings.DEV_OR_DQS_SERVER()
            ? "https://ownership.dqs.booking.com/v1" :
            "https://ownership.booking.com/v1";

    private static OwnershipClient ownershipClient;
    private static final AuthXClient AUTHXCLIENT = AuthXClients.create();

    private final ObjectMapper jacksonObjectMapper;

    private OwnershipClient(final ObjectMapper jacksonObjectMapper) {
        this.jacksonObjectMapper = jacksonObjectMapper;
    }

    static synchronized OwnershipClient getInstance(final ObjectMapper jacksonObjectMapper) {
        if (ownershipClient == null) {
            ownershipClient = new OwnershipClient(jacksonObjectMapper);
        }

        return ownershipClient;
    }

    // --------------- OrgUnit ----------------- //

    JsonNode getOrgUnitInfo(final long orgUnitId)
        throws AuthXClientException, InterruptedException, IOException {
        return executeHttp(buildGetRequest(ENDPOINT + "/Orgunit/info/v1?orgunit_id=" + String.valueOf(orgUnitId)));
    }

    JsonNode getOrgUnitOwnables(final long orgUnitId, final String type, final int includeOwner, final int includeSubTeams)
        throws AuthXClientException, IOException, InterruptedException {
        return executeHttp(buildGetRequest(
            ENDPOINT
                + "/Orgunit/ownables/v1?orgunit_id=" + String.valueOf(orgUnitId)
                + "&type=" + type
                + "&include_owner=" + String.valueOf(includeOwner)
                + "&include_subteams=" + String.valueOf(includeSubTeams)
            ));
    }

    // --------------- Ownable ----------------- //

    JsonNode retrieveOwnable(final String name, final String type)
        throws AuthXClientException, IOException, InterruptedException {
        return executeHttp(buildGetRequest(ENDPOINT + "/Ownable/retrieve/v1?name=" + name + "&type=" + type));
    }

    JsonNode retrieveOwnablesBulk(final OwnablesToRetrieve ownablesToRetrieve)
        throws AuthXClientException, IOException, InterruptedException {
        return executeHttp(buildPostRequest(ENDPOINT + "/Ownable/retrieve_bulk/v1", jacksonObjectMapper.writeValueAsString(ownablesToRetrieve)));
    }

    JsonNode searchOwnables(final String query, final int includeArchived)
        throws AuthXClientException, IOException, InterruptedException {
        return executeHttp(buildGetRequest(ENDPOINT + "/Ownable/search/v1?query=" + query + "&include_archived=" + includeArchived));
    }

    JsonNode registerNewOwnable(final OwnableRegistrationInfo info)
        throws AuthXClientException, InterruptedException, IOException {
        return executeHttp(buildPostRequest(ENDPOINT + "/Ownable/register/v1", jacksonObjectMapper.writeValueAsString(info)));
    }

    JsonNode reassignOwnable(final OwnableReassignmentInfo info)
        throws AuthXClientException, InterruptedException, IOException {
        return executeHttp(buildPostRequest(ENDPOINT + "/Ownable/reassign/v1", jacksonObjectMapper.writeValueAsString(info)));
    }

    JsonNode archiveOwnable(final long ownableId)
        throws AuthXClientException, InterruptedException, IOException {
        return executeHttp(buildPostRequest(ENDPOINT + "/Ownable/archive/v1", "{\"ownable_id\":" + ownableId + "}"));
    }

    JsonNode unarchiveOwnable(final long ownableId)
        throws AuthXClientException, InterruptedException, IOException {
        return executeHttp(buildPostRequest(ENDPOINT + "/Ownable/unarchive/v1", "{\"ownable_id\":" + ownableId + "}"));
    }

    // ---------------- Type ----------------- //

    private JsonNode getAllOwnablesForType(final String type, final boolean includeArchived)
        throws IOException, AuthXClientException, InterruptedException {
        int includeArchivedInt = includeArchived ? 1 : 0;
        return executeHttp(
            buildGetRequest(
                ENDPOINT
                    + "/OwnableType/get_ownables_for_type/v1?type=" + type
                    + "&include_archived=" + includeArchivedInt
            )
        );
    }

    JsonNode getAllOwnablesForType(final String type)
        throws IOException, AuthXClientException, InterruptedException {
        return getAllOwnablesForType(type, true);
    }

    JsonNode getActiveOwnablesForType(final String type)
        throws IOException, AuthXClientException, InterruptedException {
        return getAllOwnablesForType(type, false);
    }

    private static HttpUriRequest buildGetRequest(String url) throws AuthXClientException {
        if (SystemSettings.DEV_OR_DQS_SERVER()) {
            LOGGER.error(AUTHXCLIENT.s2sIssueToken("ownership"));
        }
        if (SystemSettings.DEV_OR_DQS_SERVER()) {
            LOGGER.error("Ownership service: building GET request, URL: " + url);
        }
        return RequestBuilder
            .get()
            .setUri(url)
            .addHeader("Content-Type", "application/json")
            .addHeader("X-S2S-Token", AUTHXCLIENT.s2sIssueToken("ownership"))
            .build();
    }

    private static HttpUriRequest buildPostRequest(String uri, String jsonBody) throws AuthXClientException, UnsupportedEncodingException {
        if (SystemSettings.DEV_OR_DQS_SERVER()) {
            LOGGER.error("Ownership service: building POST request, URI: " + uri);
            LOGGER.error("Ownership service: building POST request, body: " + jsonBody);
        }
        return RequestBuilder
            .post()
            .setUri(uri)
            .setEntity(new StringEntity(jsonBody))
            .addHeader("Content-Type", "application/json")
            .addHeader("X-S2S-Token", AUTHXCLIENT.s2sIssueToken("ownership"))
            .build();
    }

    private JsonNode executeHttp(final HttpUriRequest request) throws IOException, InterruptedException {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            CloseableHttpResponse response = httpClient.execute(request);
            int statusCode = response.getStatusLine().getStatusCode();
            // to mitigate the typical 'object not found' scenario for REST API
            if (statusCode == HttpStatus.SC_NOT_FOUND) {
                return null;
            } else if (javax.ws.rs.core.Response.Status.fromStatusCode(statusCode).getFamily() != javax.ws.rs.core.Response.Status.Family.SUCCESSFUL) {
                throw new InterruptedException("Request error, response is " + response.toString());
            }
            return jacksonObjectMapper.readTree(response.getEntity().getContent());
        }
    }
}

