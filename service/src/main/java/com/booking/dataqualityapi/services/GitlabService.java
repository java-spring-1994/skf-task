package com.booking.dataqualityapi.services;

import com.booking.dataqualityapi.config.GitlabConfig;
import com.booking.dataqualityapi.dto.gitlab.CommitInfo;
import com.booking.dataqualityapi.dto.gitlab.UserInfo;
import com.booking.dataqualityapi.exceptions.GitlabException;
import java.io.IOException;
import java.net.URLEncoder;
import java.time.Instant;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by vfedotov on 10/1/18.
 */

@Service
public class GitlabService {

    private static final Logger logger = LoggerFactory.getLogger(GitlabService.class);

    @Autowired
    private GitlabConfig gitlabConfig;

    private final CloseableHttpClient httpClient = HttpClients.createDefault();
    private static final int GITLAB_CONNECTION_TIMEOUT = 5000;
    private static final int GITLAB_LONG_TIMEOUT = 120000; // 2 minutes

    public static final int     CORE_MAIN_PROJECT_ID = 177;
    public static final String  CORE_MAIN_DEFAULT_BRANCH = "trunk";

    private static final int     SHORTEN_GIT_HASH_LENGTH = 12;

    private static final int    MAX_PAGE_SIZE = 100;

    private String baseUri;

    private String getBaseUri() {
        if (baseUri == null) {
            baseUri = String.format("%s/api/v%d", gitlabConfig.getHost(), gitlabConfig.getApiVersion());
        }
        return baseUri;
    }

    public boolean checkFileExistsInRepo(int projectId, String filePath, String branch) throws IOException {
        String uri = String.format("%s/projects/%d/repository/files/%s?ref=%s",
                getBaseUri(), projectId, URLEncoder.encode(filePath, "UTF-8"), branch);

        HttpGet httpGet = new HttpGet(uri);
        httpGet.setConfig(RequestConfig.custom()
                .setConnectionRequestTimeout(GITLAB_CONNECTION_TIMEOUT)
                .setConnectTimeout(GITLAB_CONNECTION_TIMEOUT)
                .build());
        httpGet.setHeader("PRIVATE-TOKEN", gitlabConfig.getToken());
        boolean result = false;
        try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
            // the following is to avoid the 'Timeout waiting for connection from pool' issue
            // response entity should be consumed, so the stream can be closed correctly
            EntityUtils.consumeQuietly(response.getEntity());
            result = (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK);
        }
        return result;
    }

    CommitInfo getFirstCommit(int projectId, String path, String branch) throws IOException, ParseException, java.text.ParseException {
        String uri = String.format("%s/projects/%d/repository/commits?path=%s&ref=%s&per_page=%d",
            getBaseUri(), projectId, URLEncoder.encode(path, "UTF-8"), branch, MAX_PAGE_SIZE);

        HttpGet httpGet = new HttpGet(uri);
        RequestConfig requestConfig = RequestConfig.custom()
            .setConnectionRequestTimeout(GITLAB_LONG_TIMEOUT)
            .setConnectTimeout(GITLAB_LONG_TIMEOUT)
            .build();
        httpGet.setConfig(requestConfig);
        httpGet.setHeader("PRIVATE-TOKEN", gitlabConfig.getToken());
        CommitInfo info = null;
        try (CloseableHttpResponse firstResponse = httpClient.execute(httpGet)) {
            if (firstResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                Header[] headers = firstResponse.getHeaders("X-Total-Pages");
                HttpEntity httpEntity = firstResponse.getEntity();
                if (headers.length == 1) {
                    int nrPages = Integer.parseInt(headers[0].getValue());
                    if (nrPages > 1) {
                        // Need to get the last page
                        uri += String.format("&page=%d", nrPages);
                        httpGet = new HttpGet(uri);
                        httpGet.setConfig(requestConfig);
                        httpGet.setHeader("PRIVATE-TOKEN", gitlabConfig.getToken());
                        try (CloseableHttpResponse secondResponse = httpClient.execute(httpGet)) {
                            if (secondResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                                httpEntity = secondResponse.getEntity();
                            }
                        }
                    }
                }
                String responseString = EntityUtils.toString(httpEntity, "UTF-8");
                JSONParser parser = new JSONParser();
                JSONArray responseArr;
                responseArr = (JSONArray) parser.parse(responseString);
                if (responseArr != null && !responseArr.isEmpty()) {
                    // last element in array
                    JSONObject jsonObj = (JSONObject) responseArr.get(responseArr.size() - 1);
                    info = new CommitInfo(
                        (String)jsonObj.get("id"),
                        Instant.parse((String)jsonObj.get("created_at")).getEpochSecond(),
                        (String)jsonObj.get("author_name"),
                        (String)jsonObj.get("author_email"),
                        (String)jsonObj.get("message"));
                }
            }
        }

        return info;
    }

    String shortenGitHash(String gitHash) {
        if (gitHash == null || gitHash.equals("")) {
            return gitHash;
        }
        return gitHash.substring(0, SHORTEN_GIT_HASH_LENGTH);
    }

    UserInfo getUserInfo(String userName) throws GitlabException {
        UserInfo info = null;

        String uri = String.format("%s/users?username=%s",
                getBaseUri(), userName);

        HttpGet httpGet = new HttpGet(uri);
        httpGet.setConfig(RequestConfig.custom()
                .setConnectionRequestTimeout(GITLAB_CONNECTION_TIMEOUT)
                .setConnectTimeout(GITLAB_CONNECTION_TIMEOUT)
                .build());
        httpGet.setHeader("PRIVATE-TOKEN", gitlabConfig.getToken());

        try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String responseString;
                try {
                    responseString = EntityUtils.toString(entity, "UTF-8");
                } catch (IOException e) {
                    throw new GitlabException("IOException: " + e.getMessage());
                }
                JSONParser parser = new JSONParser();
                JSONObject responseObj;
                try {
                    logger.info("getUserInfo response: " + responseString);
                    JSONArray responseArray = (JSONArray) parser.parse(responseString);
                    responseObj = (JSONObject) responseArray.get(0);
                } catch (ParseException | IndexOutOfBoundsException e) {
                    throw new GitlabException(e.getClass().toString() + ": " + e.getMessage());
                }
                info = new UserInfo(
                    responseObj.get("id").toString(),
                    (String)responseObj.get("name"),
                    (String)responseObj.get("username"),
                    (String)responseObj.get("state")
                );
                // todo we can cache this info
            }
        } catch (IOException e) {
            throw new GitlabException("IOException: " + e.getMessage());
        }

        return info;
    }
}
