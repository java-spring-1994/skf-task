package com.booking.dataqualityapi.services;

import com.google.common.cache.LoadingCache;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Service
public class CacheService {

    @Autowired
    HiveService hiveService;

    private static final Logger logger = LoggerFactory.getLogger(CacheService.class);

    private LoadingCache<ImmutableTriple<String, String, String>, String> sampleDataCache = CacheBuilder.newBuilder()
            .expireAfterWrite(1, TimeUnit.DAYS)
            .removalListener(new RemovalListener<ImmutableTriple<String, String, String>, String>() {
                public void onRemoval(RemovalNotification<ImmutableTriple<String, String, String>, String> removal) {
                    logger.info("Removing key {} - {} from cache because {} at {}",
                            removal.getKey().getLeft(), removal.getKey().getMiddle(), removal.getKey().getRight(),
                            removal.getCause().toString(), new java.util.Date());
                }
            })
            .build(
                new CacheLoader<ImmutableTriple<String, String, String>, String>() {
                    public String load(ImmutableTriple<String, String, String> dcTableNamePartition) throws Exception {
                        logger.info("Adding key {} - {} to cache",
                                dcTableNamePartition.getLeft(), dcTableNamePartition.getMiddle(), dcTableNamePartition.getRight()
                        );
                        return hiveService.getSampleData(
                                dcTableNamePartition.getLeft(), dcTableNamePartition.getMiddle(), dcTableNamePartition.getRight()
                        );
                    }
                }
            );

    public String getSampleData(String dc, String tableName, String partition)
            throws ExecutionException {
        return sampleDataCache.get(ImmutableTriple.of(dc, tableName, partition));
    }

    public void clearCache() {
        logger.info("Clearing the cache");
        sampleDataCache.invalidateAll();
    }
}
