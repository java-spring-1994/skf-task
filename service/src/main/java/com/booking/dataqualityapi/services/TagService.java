package com.booking.dataqualityapi.services;

import com.booking.authxclient.exceptions.AuthXClientException;
import com.booking.dataqualityapi.dto.AssignTagInfo;
import com.booking.dataqualityapi.dto.CreateTagInfo;
import com.booking.dataqualityapi.model.DataSource;
import com.booking.dataqualityapi.model.EntityTag;
import com.booking.dataqualityapi.model.Tag;
import com.booking.dataqualityapi.repository.EntityTagRepository;
import com.booking.dataqualityapi.repository.TagRepository;
import com.booking.dataqualityapi.utils.EntityTypeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.ws.rs.NotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class TagService {


    private final TagRepository tagRepository;
    private final DataSourcesService dataSourceService;
    private final EntityTagRepository entityTagRepository;
    private final GCSyncStatusChangeAlertService alertService;

    @Autowired
    public TagService(final TagRepository tagRepository,
                      final DataSourcesService dataSourceService,
                      final EntityTagRepository entityTagRepository,
                      final GCSyncStatusChangeAlertService alertService) {
        this.tagRepository = tagRepository;
        this.dataSourceService = dataSourceService;
        this.entityTagRepository = entityTagRepository;
        this.alertService = alertService;
    }

    public Tag create(final CreateTagInfo tagForm,
                      final Integer authorId) {
        final Tag tag = new Tag();
        tag.setTag(tagForm.getTag());
        tag.setDescription(tagForm.getDescription());
        tag.setStaffId(authorId);
        tag.setType(tagForm.getType());
        return tagRepository.save(tag);
    }

    public List<Tag> getTags(final Optional<String> tagInput, final Optional<String> type) {

        final Tag exampleTag = new Tag();

        exampleTag.setTag(tagInput.orElse(null));
        exampleTag.setType(type.orElse(null));

        final ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("tag", match -> match.contains())
                .withMatcher("type", match -> match.exact());

        return tagRepository.findAll(Example.of(exampleTag, matcher), new Sort(Sort.Direction.ASC, "tag"));
    }

    public List<EntityTag> getTags(final String entityType, final String namespace,
                                   final String name) {
        return entityTagRepository.findByNamespaceAndNameAndType(EntityTypeUtils.getEntityTypeInDqEntity(entityType), namespace, name);
    }

    public EntityTag addTagToDataEntity(final String entityType, final String name, final String namespace, final AssignTagInfo tagInfo,
                                        final Integer staffId, final String loginName) throws AuthXClientException {
        DataSource ds = dataSourceService.findByTypeAndNamespaceAndName(entityType, namespace, name);
        if (ds == null) {
            throw new NotFoundException(String.format("%s.%s.%s data entity couldn't be founded", entityType, namespace, name));
        }

        Tag tag = tagRepository.getByTag(tagInfo.getTag());
        if (tag == null) {
            tag = createFreeTextTag(tagInfo, staffId);
        }

        dataSourceService.saveTagToLabels(ds, tag.getTag());

        final EntityTag entityTag = createTagToEntityConnection(
                tagInfo,
                ds,
                staffId,
                tag);


        final EntityTag savedEntityToTag =  entityTagRepository.save(entityTag);

        alertService.send(namespace, name,
                tag.getTag(),
                GCSyncStatusChangeAlertService.GC_SYNC_ENABLED,
                loginName);

        return savedEntityToTag;
    }

    private EntityTag createTagToEntityConnection(AssignTagInfo tagInfo, DataSource ds, Integer staffId, Tag tag) {
        EntityTag entityTag = new EntityTag();
        entityTag.setDataSource(ds);
        entityTag.setStaffId(staffId);
        entityTag.setStaffName(tagInfo.getStaffName());
        entityTag.setTag(tag);
        entityTag.setReason(tagInfo.getReason());
        return entityTag;
    }

    private Tag createFreeTextTag(AssignTagInfo tagInfo, Integer staffId) {
        Tag tag = new Tag();
        tag.setType("free_text");
        tag.setTag(tagInfo.getTag());
        tag.setDescription(tagInfo.getTagDescription());
        tag.setStaffId(staffId);
        return tagRepository.save(tag);
    }

    public EntityTag deleteTagFromEntity(final String entityType,
                                         final String namespace,
                                         final String name,
                                         final String tag,
                                         final String loginName) {
        final EntityTag entityTag = entityTagRepository.find1ByTypeAndEntityAndTag(EntityTypeUtils.getEntityTypeInDqEntity(entityType), namespace, name, tag);
        if (entityTag == null) {
            throw new NotFoundException(String.format("Tag [%s] hasn't been assigned to entity %s.%s.%s ", entityType, tag, namespace, name));
        }

        dataSourceService.deleteTagFromLabels(namespace, name, tag);

        entityTagRepository.deleteById(entityTag.getId());

        alertService.send(namespace,
                name,
                tag,
                GCSyncStatusChangeAlertService.GC_SYNC_DISABLED,
                loginName);

        return entityTag;
    }
}
