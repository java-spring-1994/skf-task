package com.booking.dataqualityapi.services.datamodel;

import com.booking.dataquality.model.datamodel.MetastoreCheck;
import com.booking.dataqualityapi.dto.datamodel.MetastoreCheckPayload;
import com.booking.dataqualityapi.dto.datamodel.TimeSeriesConfigPayload;
import com.booking.dataqualityapi.model.datamodel.TimeSeriesConfig;
import com.booking.dataqualityapi.repository.DataSourceRepository;
import com.booking.dataqualityapi.repository.datamodel.MetastoreCheckRepository;
import com.booking.dataqualityapi.repository.datamodel.TimeSeriesConfigRepository;
import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class DataModelService {

    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory.getLogger(DataModelService.class);

    private final TimeSeriesConfigRepository timeSeriesConfigRepository;
    private final DataSourceRepository dataSourceRepository;
    private final MetastoreCheckRepository metastoreCheckRepository;

    @Autowired
    public DataModelService(
        final TimeSeriesConfigRepository timeSeriesConfigRepository,
        final DataSourceRepository dataSourceRepository,
        final MetastoreCheckRepository metastoreCheckRepository
    ) {
        this.timeSeriesConfigRepository = timeSeriesConfigRepository;
        this.dataSourceRepository = dataSourceRepository;
        this.metastoreCheckRepository = metastoreCheckRepository;
    }

    public List<MetastoreCheck> getMetastoreChecks(final int dataEntityId) {
        return metastoreCheckRepository.findAllActiveByEntityId(dataEntityId);
    }

    public boolean disableMetastoreChecks(int dataEntityId) {
        boolean disabled = false;
        List<MetastoreCheck> metastoreChecks = metastoreCheckRepository.findAllByEntityId(dataEntityId);
        if (metastoreChecks != null) {
            metastoreChecks.forEach(c -> c.setStatus(MetastoreCheck.CheckStatus.disabled));
            disabled = !metastoreCheckRepository.saveAll(metastoreChecks).isEmpty();
        }
        return disabled;
    }

    public MetastoreCheck saveMetastoreCheck(final MetastoreCheckPayload metastoreCheckPayload) {
        MetastoreCheck metastoreCheck = metastoreCheckRepository.findOneByEntityIdAndRuleId(
            metastoreCheckPayload.getEntityId(), metastoreCheckPayload.getRuleId());
        if (metastoreCheck != null) {
            metastoreCheck.setStatus(metastoreCheckPayload.getStatus());
            metastoreCheck.setFrequency(metastoreCheckPayload.getFrequency());
            metastoreCheck.setOffset(metastoreCheckPayload.getOffset());
            metastoreCheck.setParam1(metastoreCheckPayload.getParam1());
            metastoreCheck.setParam2(metastoreCheckPayload.getParam2());
            return metastoreCheckRepository.save(metastoreCheck);
        } else {
            return metastoreCheckRepository.save(
                new MetastoreCheck(
                    metastoreCheckPayload.getEntityId(),
                    metastoreCheckPayload.getRuleId(),
                    metastoreCheckPayload.getStatus(),
                    metastoreCheckPayload.getFrequency(),
                    metastoreCheckPayload.getOffset(),
                    metastoreCheckPayload.getParam1(),
                    metastoreCheckPayload.getParam2()
                )
            );
        }
    }

    public TimeSeriesConfig getTimeSeriesConfig(final int entityId) {
        return timeSeriesConfigRepository.find1ByEntityId(entityId);
    }

    public TimeSeriesConfig saveTimeSeriesConfig(final int entityId, final TimeSeriesConfigPayload configPayload) {
        TimeSeriesConfig config = timeSeriesConfigRepository.find1ByEntityId(entityId);
        if (config == null) {
            if (dataSourceRepository.findById(entityId).isPresent()) {
                com.booking.dataqualityapi.model.DataSource dataSource =
                    dataSourceRepository.findById(entityId).get();
                config = new TimeSeriesConfig();
                config.setDataSource(dataSource);
            }
        }
        populateTimeSeriesConfig(configPayload, config);
        return timeSeriesConfigRepository.save(config);
    }

    private static void populateTimeSeriesConfig(final TimeSeriesConfigPayload configPayload,
                                                 final TimeSeriesConfig config) {
        config.setDateColumn(configPayload.getDateColumn());
        config.setDateFormat(configPayload.getDateFormat());
        config.setDatePartitioned(configPayload.isDatePartitioned());
        config.setHourColumn(configPayload.getHourColumn());
        config.setHourFormat(configPayload.getHourFormat());
        config.setHourPartitioned(configPayload.isHourPartitioned());
        config.setEnumColumn(configPayload.getEnumColumn());
        config.setEnumValues(Optional.ofNullable(configPayload.getEnumValues()).orElse(new String[0]));
    }

    public void deleteTimeSeriesConfig(int dataEntityId) throws NotFoundException {
        TimeSeriesConfig cfg = timeSeriesConfigRepository.find1ByEntityId(dataEntityId);
        if (cfg == null) {
            throw new NotFoundException(String.format("Config for data entity id %d not found", dataEntityId));
        }
        timeSeriesConfigRepository.delete(cfg);
    }

    public void deleteTimeSeriesConfig(String namespace, String name) throws NotFoundException {
        TimeSeriesConfig cfg = timeSeriesConfigRepository.find1ByEntityNamespaceAndName(namespace, name);
        if (cfg == null) {
            throw new NotFoundException(String.format("Config for data entity %s.%s not found", namespace, name));
        }
        timeSeriesConfigRepository.delete(cfg);
    }


}
