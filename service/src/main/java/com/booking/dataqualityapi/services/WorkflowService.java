package com.booking.dataqualityapi.services;

import com.booking.dataquality.utils.SqlFunction;
import com.booking.dataqualityapi.dto.OozieWorkflowDeploymentInfo;
import com.booking.dataqualityapi.dto.OozieWorkflowInstanceInfo;
import com.booking.dataqualityapi.dto.OozieWorkflowSloInfo;
import com.booking.dataqualityapi.dto.workflow.WorkflowCoordInfo;
import com.booking.dataqualityapi.dto.workflow.WorkflowInfo;
import com.booking.dataqualityapi.model.office.Staff;
import com.booking.dataqualityapi.model.job.WorkflowJobStats;
import com.booking.dataqualityapi.model.job.WorkflowJobSummary;
import com.booking.dataqualityapi.model.workflow.OozieWorkflowSlo;
import com.booking.dataqualityapi.model.workflow.WorkflowNamePathCache;
import com.booking.dataqualityapi.model.workflow.WorkflowSession;
import com.booking.dataqualityapi.repository.office.StaffRepository;
import com.booking.dataqualityapi.repository.workflow.WorkflowCoordRepository;
import com.booking.dataqualityapi.repository.workflow.WorkflowFirstCommitterCacheRepository;
import com.booking.dataqualityapi.repository.workflow.WorkflowJobRepositoryImpl;
import com.booking.dataqualityapi.repository.workflow.WorkflowJobStatsRepository;
import com.booking.dataqualityapi.repository.workflow.WorkflowNamePathCacheRepository;
import com.booking.dataqualityapi.utils.DatacenterUtils;
import com.booking.dataqualityapi.utils.GitlabTreeUrlBuilder;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class WorkflowService {

    private static final Logger logger = LoggerFactory.getLogger(WorkflowService.class);

    @Autowired
    private GitlabService gitlabService;

    @Autowired
    private EntityDependencyService dependencyService;

    @Autowired
    private OozieApiService oozieApiService;

    private DataSource dqDataSource;
    private DataSource hadoopstatsDataSource;

    @Autowired
    private WorkflowNamePathCacheRepository workflowNamePathCacheRepository;

    @Autowired
    private WorkflowFirstCommitterCacheRepository workflowFirstCommitterCacheRepository;

    @Autowired
    private WorkflowJobRepositoryImpl workflowSessionRepository;

    @Autowired
    private WorkflowCoordRepository workflowCoordInfoRepository;

    @Autowired
    private WorkflowJobStatsRepository workflowJobStatsRepository;

    @Autowired
    private StaffRepository staffRepository;

    private static final String WORKFLOW_REGISTRY_TABLE = "dq_workflow_registry";
    private static final String WORKFLOW_OUTPUTS_TABLE = "dq_workflow_outputs";

    private static final String OOZIE_WORKFLOW_DEPLOYMENT_STATUS = "wf_deploy_status";

    private static final String GET_TABLE_FOR_WORKFLOW_QUERY =
            "SELECT entity_namespace, entity_name FROM dq_workflow_output_table_mapping WHERE workflow_name = ? ORDER BY mysql_row_updated_at DESC";

    private static final String GET_WORKFLOW_FOR_TABLE_QUERY =
            "SELECT workflow_name FROM dq_workflow_output_table_mapping WHERE entity_namespace = ? and entity_name = ? ORDER BY mysql_row_updated_at DESC";

    private static final String GET_DATA_ENTITY_ID =
            "SELECT id FROM dq_data_entity WHERE entity_namespace = ? and entity_name = ? AND entity_type = 'hive-table' ";

    @Autowired
    public WorkflowService(
            @Qualifier("getMysqlDqConnectionRo") DataSource dqDataSource,
            @Qualifier("getMysqlHadoopstatsConnectionRo") DataSource hadoopstatsDataSource
    ) {
        this.dqDataSource = dqDataSource;
        this.hadoopstatsDataSource = hadoopstatsDataSource;
    }

    public List<WorkflowSession> getWorkflowSessions(Optional<String> dc,
                                                        String wfName,
                                                        Date from,
                                                        Date to) {
        return workflowSessionRepository.getWorkflowSessions(dc, wfName, from, to);
    }


    public List<WorkflowCoordInfo> getRunningWorkflowCoordInfo(final String wfName) {
        return workflowCoordInfoRepository.findRunningByCoordName(wfName)
                .stream().map(cord -> {
                    WorkflowCoordInfo cordModel =  new WorkflowCoordInfo();
                    cordModel.setCoordStatus(cord.getCoordStatus());
                    cordModel.setCoordId(cord.getCoordId());
                    cordModel.setCoordName(cord.getCoordName());
                    cordModel.setDc(cord.getDc());
                    cordModel.setEndTime(cord.getEndTime());
                    cordModel.setStartTime(cord.getStartTime());
                    cordModel.setCreatedTime(cord.getCreatedTime());
                    cordModel.setUpdatedTime(cord.getUpdatedTime());
                    return cordModel;
                }).collect(Collectors.toList());
    }

    /**
     * Get workflow info from registry by id.
     * @param workflowId workflow unique id
     * @return WorkflowInfo (may be null)
     * @throws SQLException if there was an error in sql execution.
     */
    private WorkflowInfo retrieveWorkflowInfo(String workflowId) throws SQLException {
        final String query = String.format(
                "SELECT * FROM %s "
                    + "WHERE id = ?",
                WORKFLOW_REGISTRY_TABLE);

        SqlFunction<Connection, PreparedStatement> createPreparedStatement = c -> {
            final PreparedStatement statement = c.prepareStatement(query);
            statement.setString(1, workflowId);
            return statement;
        };

        try (
            Connection connection = dqDataSource.getConnection();
            PreparedStatement statement = createPreparedStatement.invoke(connection);
            ResultSet result = statement.executeQuery()
        ) {
            if (result.next()) {
                return new WorkflowInfo(
                        Integer.parseInt(workflowId),
                        result.getString("name"),
                        result.getString("type"),
                        result.getString("repo_link"),
                        result.getString("description"));
            } else {
                return null;
            }
        }
    }

    /**
     * Get associated workflow for data entity.
     * @param namespace the entity schema
     * @param name the entity name
     * @return WorkflowInfo (may be null)
     * @throws SQLException if there was an error in sql execution.
     */
    public WorkflowInfo getAssociatedWorkflow(String namespace, String name) throws SQLException {
        return getAssociatedWorkflow(getDataEntityId(namespace, name));
    }

    /**
     * Get associated workflow for data entity.
     * @param dataEntityId the id of data entity (from data entity registry)
     * @return WorkflowInfo (may be null)
     * @throws SQLException if there was an error in sql execution.
     */
    public WorkflowInfo getAssociatedWorkflow(String dataEntityId) throws SQLException {
        final String query = String.format(
                "SELECT * FROM %s "
                + "WHERE id = "
                + "(SELECT workflow_id FROM %s where data_entity_id = ?)",
                WORKFLOW_REGISTRY_TABLE,
                WORKFLOW_OUTPUTS_TABLE);

        SqlFunction<Connection, PreparedStatement> createPreparedStatement = c -> {
            final PreparedStatement statement = c.prepareStatement(query);
            statement.setString(1, dataEntityId);
            return statement;
        };

        try (
            Connection connection = dqDataSource.getConnection();
            PreparedStatement statement = createPreparedStatement.invoke(connection);
            ResultSet result = statement.executeQuery()
        ) {
            if (result.next()) {
                return new WorkflowInfo(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("type"),
                        result.getString("repo_link"),
                        result.getString("description"));
            } else {
                return null;
            }

        }
    }


    public List<OozieWorkflowDeploymentInfo> getAutoDetectedWorkflowInfo(String namespace, String name)
        throws SQLException, MalformedURLException {

        List<OozieWorkflowDeploymentInfo> result = new ArrayList<>();

        String workflowName = null;
        //we need to check all datacenters cause we don't know which datacenter it's deployed to
        for (DatacenterUtils.Datacenter dc : DatacenterUtils.Datacenter.values()) {
            //get workflow for table
            String wfName = dependencyService.getWorkflowForTable(dc.name().toLowerCase(), namespace, name);
            if (StringUtils.isEmpty(wfName)) {
                continue;
            }
            workflowName = wfName;
            // check for deployment with workflow name
            result = getWorkflowDeploymentInfo(wfName, dc);
            if (!result.isEmpty()) {
                return result;
            }
        }

        // We didn't get any deployment info from hadoopstats, but we know the workflow name
        // add the workflow name
        if (result.isEmpty() && workflowName != null) {
            for (DatacenterUtils.Datacenter dc: DatacenterUtils.Datacenter.values()) {
                result.add(
                    new OozieWorkflowDeploymentInfo(workflowName, "hadoop_" + dc.name().toLowerCase())
                );
            }
        }

        return result;
    }

    public List<OozieWorkflowDeploymentInfo> getWorkflowDeploymentInfo(final String wfName)
        throws SQLException, MalformedURLException {
        List<OozieWorkflowDeploymentInfo> result = Collections.emptyList();
        if (StringUtils.isEmpty(wfName)) {
            return result;
        }
        for (DatacenterUtils.Datacenter dc : DatacenterUtils.Datacenter.values()) {
            result = getWorkflowDeploymentInfo(wfName, dc);
            if (CollectionUtils.isNotEmpty(result)) {
                break;
            }
        }
        return result;
    }


    private List<OozieWorkflowDeploymentInfo> getWorkflowDeploymentInfo(final String wfName, final DatacenterUtils.Datacenter dc)
        throws SQLException, MalformedURLException {

        // Sometimes we have deployment records like this:
        // 1. workflow: et-experiments-transactions
        // 2. workflow: et-infra/et-experiments-transactions
        // We don't know which one is actually active 'et-experiments-transactions'
        // Taking last one is not always correct
        // So we have to get workflow path from oozie first
        // See https://jira.booking.com/jira/projects/DQ/issues/DQ-257

        String wfPath = getWorkflowPathFromCache(wfName, dc.name().toLowerCase());
        if (StringUtils.isEmpty(wfPath)) {
            // call to oozie if cache miss
            wfPath = oozieApiService.getWorkflowPath(dc, wfName);
            if (StringUtils.isNotEmpty(wfPath)) {
                // update in cache
                updateWorkflowPathInCache(wfName, wfPath, dc.name().toLowerCase());
            }
        }
        List<OozieWorkflowDeploymentInfo> result = new ArrayList<>();
        if (StringUtils.isNotEmpty(wfPath)) {
            // check for deployment with app path
            result = getDeploymentInfoFromHadoopStats(wfPath);
        }
        return result;
    }


    /**
     * Retrieves oozie workflow deployment info.
     * @param workflowId the id of oozie workflow (from workflow registry)
     * @return oozie workflow deployment info
     * @throws SQLException if there was an error in sql execution.
     */
    public List<OozieWorkflowDeploymentInfo> getOozieWorkflowDeploymentInfo(String workflowId)
        throws SQLException, MalformedURLException {

        List<OozieWorkflowDeploymentInfo> result = new ArrayList<>();
        WorkflowInfo wfInfo = retrieveWorkflowInfo(workflowId);

        if (wfInfo != null && wfInfo.getType().equals("oozie")) {
            return getDeploymentInfoFromHadoopStats(wfInfo.getName());
        }

        return result;
    }

    private List<OozieWorkflowDeploymentInfo> getDeploymentInfoFromHadoopStats(String wfName)
        throws SQLException, MalformedURLException {

        List<OozieWorkflowDeploymentInfo> result = new ArrayList<>();

        final String query = String.format(
                "SELECT * FROM %s "
                        + "WHERE workflow = ?",
                OOZIE_WORKFLOW_DEPLOYMENT_STATUS);

        SqlFunction<Connection, PreparedStatement> createPreparedStatement = c -> {
            final PreparedStatement statement = c.prepareStatement(query);
            statement.setString(1, wfName);
            return statement;
        };

        try (
                Connection connection = hadoopstatsDataSource.getConnection();
                PreparedStatement statement = createPreparedStatement.invoke(connection);
                ResultSet rs = statement.executeQuery()
        ) {
            while (rs.next()) {
                // todo use a proper staff service
                String userName = rs.getString("deployment_user");
                String userFullName = userName;
                try {
                    Staff staff = staffRepository.getByLoginName(userName);
                    userFullName = staff.getName();

                } catch (Exception exc) {
                    logger.error(
                            String.format("Could not get user info for loginname %s, error: %s", userName, exc.getMessage())
                    );
                }

                String commit = rs.getString("latest_wf_deployed_sha1");

                String deploymentPath = rs.getString("deployment_path");
                String gitlabUrl = null;
                if (StringUtils.isNotEmpty(deploymentPath)) {
                    String repoPath = StringUtils.removeStart(deploymentPath, "/usr/local/git_tree/main/");
                    gitlabUrl = GitlabTreeUrlBuilder.build(
                        "core", "main", "trunk", repoPath
                    );
                }
                result.add(
                        new OozieWorkflowDeploymentInfo(
                                wfName,
                                rs.getString("cluster"),
                                rs.getString("deployment_date"),
                                userName,
                                userFullName,
                                commit,
                                gitlabService.shortenGitHash(commit),
                                gitlabUrl
                        )
                );
            }
        }

        return result;
    }

    /**
     * Get the workflow name associated with the specified table.
     * This first looks at the data collected by the hive-event-listener.
     * If it can't find a mapping there, then it checks the manual workflow assignment.
     *
     * @param schema schema name
     * @param table table name
     * @return workflow name
     * @throws SQLException sqlexception
     */
    public String getWorkflowForEntity(String schema, String table) throws SQLException {

        SqlFunction<Connection, PreparedStatement> createPreparedStatement = c -> {
            final PreparedStatement statement = c.prepareStatement(GET_WORKFLOW_FOR_TABLE_QUERY);
            statement.setString(1, schema);
            statement.setString(2, table);
            return statement;
        };

        try (
            Connection statsConn = dqDataSource.getConnection();
            PreparedStatement statement = createPreparedStatement.invoke(statsConn);
            ResultSet rs  = statement.executeQuery()
        ) {
            if (rs.next()) {
                return rs.getString("workflow_name");
            } else {
                String dataEntityId = getDataEntityId(schema, table);
                if (dataEntityId != null) {
                    WorkflowInfo wfInfo = getAssociatedWorkflow(dataEntityId);
                    if (wfInfo != null) {
                        String wfName = wfInfo.getName();
                        if (wfName != null) {
                            return wfName.substring(wfName.lastIndexOf("/")  + 1);
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * Get the table name associated with the specified workflow.
     * This works on the data collected by the hive-event-listener and not the manual workflow assignment.
     * If more than one table is written by this workflow, then the table that was last update will be returned
     * @param workflowName name of the workflow
     * @return concatenated string of schema and table
     * @throws SQLException sqlexception
     */
    public String getEntityForWorkflow(String workflowName) throws SQLException {

        SqlFunction<Connection, PreparedStatement> createPreparedStatement = c -> {
            final PreparedStatement statement = c.prepareStatement(GET_TABLE_FOR_WORKFLOW_QUERY);
            statement.setString(1, workflowName);
            return statement;
        };

        try (
            Connection statsConn = dqDataSource.getConnection();
            PreparedStatement statement = createPreparedStatement.invoke(statsConn);
            ResultSet rs  = statement.executeQuery()
        ) {
            if (rs.next()) {
                return rs.getString("entity_namespace") + "." + rs.getString("entity_name");
            } else {
                return null;
            }
        }
    }

    private String getDataEntityId(String entityNameSpace, String entityName) throws SQLException {

        SqlFunction<Connection, PreparedStatement> createPreparedStatement = c -> {
            final PreparedStatement statement = c.prepareStatement(GET_DATA_ENTITY_ID);
            statement.setString(1, entityNameSpace);
            statement.setString(2, entityName);
            return statement;
        };

        try (
            Connection statsConn = dqDataSource.getConnection();
            PreparedStatement statement = createPreparedStatement.invoke(statsConn);
            ResultSet rs  = statement.executeQuery()
        ) {
            if (rs.next()) {
                return rs.getString("id");
            } else {
                return null;
            }
        }
    }

    private String getWorkflowPathFromCache(final String name, final String dc) {
        WorkflowNamePathCache cacheItem = workflowNamePathCacheRepository.find1ByNameAndDc(name, dc);
        // cache value is invalid after 1 day
        Date yesterday = new Date(System.currentTimeMillis() - 1000L * 60L * 60L * 24L);

        if (cacheItem == null || cacheItem.getUpdatedTime().before(yesterday)) {
            return null;
        } else {
            return cacheItem.getPath();
        }
    }

    private void updateWorkflowPathInCache(final String name, final String path, final String dc) {
        WorkflowNamePathCache cacheItem = workflowNamePathCacheRepository.find1ByNameAndDc(name, dc);
        Date currentDate = new Date();
        if (cacheItem == null) {
            workflowNamePathCacheRepository.save(new WorkflowNamePathCache(name, path, dc, currentDate));
        } else {
            cacheItem.setPath(path);
            cacheItem.setUpdatedTime(currentDate);
            workflowNamePathCacheRepository.save(cacheItem);
        }
    }

    public List<WorkflowJobStats> getJobStats(final Date from,
                                              final Date to,
                                              final String coordName,
                                              final Optional<String> cluster) {
        return workflowJobStatsRepository.getJobs(from, to, coordName, cluster);
    }

    public List<OozieWorkflowInstanceInfo> getCurrentStatusInfo(String dc, String wfName, int size) {
        return dependencyService.getCurrentWorkflowInstanceInfo(dc, wfName, size);
    }

    public WorkflowJobSummary getJobSummaryStats(Date from, Date to, String wfName) {
        return  workflowJobStatsRepository.getWorkflowSummary(from,to, wfName);
    }

    public OozieWorkflowSloInfo getSmartStatusInfo(String dc, String wfName, int percentile, int size) {
        List<OozieWorkflowSlo> workflowTimes = dependencyService.getWorkflowSlo(dc, wfName, size);
        OozieWorkflowSloInfo sloInfo = new OozieWorkflowSloInfo();
        if (workflowTimes.size() < 2 || !isDailyWorkflow(workflowTimes)) {
            return sloInfo;
        }
        sloInfo.setStartTime(
            Optional.ofNullable(
                getPercentile(workflowTimes, OozieWorkflowSlo::getStartTime, percentile)
            ).map(LocalTime::toString).orElse(null)
        );
        sloInfo.setEndTime(
            Optional.ofNullable(
                getPercentile(workflowTimes, OozieWorkflowSlo::getEndTime, percentile)
            ).map(LocalTime::toString).orElse(null)
        );
        return sloInfo;
    }

    private boolean isDailyWorkflow(List<OozieWorkflowSlo> workflowTimes) {
        LocalDateTime fistTime = convertToLocalDateTime(workflowTimes.get(0).getNominalTime());
        LocalDateTime secondTime = convertToLocalDateTime(workflowTimes.get(1).getNominalTime());
        Duration duration = Duration.between(fistTime, secondTime);
        return Math.abs(duration.toHours()) > 22;
    }

    private static LocalTime getPercentile(List<OozieWorkflowSlo> list,
                                           Function<OozieWorkflowSlo, Date> mapper,
                                           final int percentile) {
        final int position = list.size() * percentile / 100;
        if (position < 2) {
            return null;
        }
        List<LocalTime> startTimes = list.stream().map(mapper)
                .map(WorkflowService::convertToLocalDateTime)
                .map(LocalDateTime::toLocalTime).sorted()
                .collect(Collectors.toList());
        return startTimes.get(position);
    }

    private static LocalDateTime convertToLocalDateTime(Date dateToConvert) {
        return Instant.ofEpochMilli(dateToConvert.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }

    public List<String> getAllWorkflows() {
        return workflowCoordInfoRepository.findAllWorkFlowName();
    }

    public List<String> searchWorkflows(String wfName, List<String> excludedWorkflows) {
        if (excludedWorkflows != null && excludedWorkflows.size() > 0) {
            return workflowCoordInfoRepository.findWorkFlowByNameExcluding(wfName, excludedWorkflows);
        }
        return workflowCoordInfoRepository.findWorkFlowByName(wfName);
    }

}
