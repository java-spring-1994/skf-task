package com.booking.dataqualityapi.services;

import com.booking.dataquality.dto.notification.NotificationMutationDTO;
import com.booking.dataquality.dto.notification.NotificationPayloadDTO;
import com.booking.dataquality.dto.notification.NotificationQueryDTO;
import com.booking.dataquality.dto.notification.NotificationVariablesDTO;
import com.booking.systemsettings.SystemSettings;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Set;

@Service
public class NotificationsService {
    private static final Logger logger = LoggerFactory.getLogger(NotificationsService.class);
    private static final String WIKI_UPDATE_NOTIFICATION_TYPE = "Wiki Update";

    private static final String ENDPOINT =
            SystemSettings.DEV_OR_DQS_SERVER()
                    ? "https://mls.dqs.booking.com/graphql" :
                    "https://mls.prod.booking.com/graphql";

    private static final String NOTIFICATION_SCOPE = "DQ";
    private static final String SEND_NOTIFICATION_QUERY
            = "mutation SendNotification($notification: NotificationInput!) {sendNotification(notification: $notification) {id}}";

    private final ObjectMapper jacksonObjectMapper;

    @Autowired
    public NotificationsService(
            final RestTemplateBuilder restTemplateBuilder,
            final ObjectMapper jacksonObjectMapper) {
        this.jacksonObjectMapper = jacksonObjectMapper;
    }

    private void send(final Set<Integer> recipients, final String type, final String body, final NotificationPayloadDTO payload) throws IOException {
        if (recipients == null
                || recipients.size() == 0) {
            return;
        }
        final String json = createNotification(recipients, type, body, payload);
        StringEntity bodyEntity = new StringEntity(json, ContentType.APPLICATION_JSON);
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost(ENDPOINT);
            httpPost.setEntity(bodyEntity);
            httpClient.execute(httpPost);
        }
    }

    public void sendWikiUpdateNotification(final Set<Integer> recipients, final String body, final NotificationPayloadDTO payload) {
        try {
            send(recipients, WIKI_UPDATE_NOTIFICATION_TYPE, body, payload);
        } catch (IOException e) {
            logger.error("Error Sending notification", e);
        }
    }

    private String createNotification(final Set<Integer> recipients, final String type, final String body,
                                      final NotificationPayloadDTO payload) throws JsonProcessingException {
        final NotificationMutationDTO notificationMutation = new NotificationMutationDTO();
        notificationMutation.setTargetUserID(recipients);
        notificationMutation.setPayload(payload);
        notificationMutation.setScope(NOTIFICATION_SCOPE);
        notificationMutation.setType(type);
        notificationMutation.setBody(body);

        final NotificationVariablesDTO variables = new NotificationVariablesDTO();
        variables.setN(notificationMutation);

        final NotificationQueryDTO query = new NotificationQueryDTO();
        query.setQuery(SEND_NOTIFICATION_QUERY);
        query.setVariables(variables);

        return jacksonObjectMapper.writeValueAsString(query);
    }
}
