package com.booking.dataqualityapi.services;

import com.booking.dataqualityapi.dto.HiveTableInfo;
import com.booking.dataqualityapi.dto.dataentity.DataEntity;
import com.booking.dataqualityapi.dto.dataentity.registration.DataEntityRegistrationAttributes;
import com.booking.dataqualityapi.dto.dataentity.registration.DataEntityRegistrationResolution;
import com.booking.dataqualityapi.dto.dataentity.registration.DataEntityRegistrationStatus;
import com.booking.dataqualityapi.exceptions.DataEntityRegistrationException;
import com.booking.dataqualityapi.model.DataSource;
import com.booking.dataqualityapi.model.EntityInstance.HiveInstanceType;
import com.booking.dataqualityapi.repository.DataSourceRepository;
import com.booking.dataqualityapi.services.ownership.OwnershipService;
import com.booking.dataqualityapi.utils.EntityTypeUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.NotFoundException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Service
public class DataSourcesService {

    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory.getLogger(DataSourcesService.class);

    private static final Set<String> TAGS_TO_SYNC = new HashSet<>(Arrays.asList("deprecated","gc_sync"));

    private DataSourceRepository dataSourcesRepository;
    private ModelMapper modelMapper;

    @Autowired
    private OwnershipService ownershipService;

    @Autowired
    public DataSourcesService(DataSourceRepository dataSourcesRepository, ModelMapper modelMapper) {
        this.dataSourcesRepository = dataSourcesRepository;
        this.modelMapper = modelMapper;
    }

    public List<DataSource> listDataSources(Optional<String> namespace, Optional<String> name, Optional<Integer> staffId,
                                            Map<String, String> labelsMap, Integer length, Integer offset) {
        return dataSourcesRepository.findAllHiveDataSources(namespace, name, staffId, labelsMap, length, offset);
    }

    public DataSource findByHiveDataEntityNamespaceAndName(final String namespace, final String name) {
        return dataSourcesRepository.findHiveDataEntityByNamespaceAndName(namespace, name)
                .orElseThrow(() -> new NotFoundException(String.format("entity [%s.%s] could't be found",
                        namespace, name)));
    }

    public DataSource findByTypeAndNamespaceAndName(final String type, final String namespace, final String name) {
        return dataSourcesRepository.findByTypeAndNamespaceAndName(EntityTypeUtils.getEntityTypeInDqEntity(type), namespace, name)
                .orElseThrow(() -> new NotFoundException(String.format("entity [%s.%s.%s] could't be found",
                        type, namespace, name)));
    }

    public DataSource findDataEntityByNamespaceNameAndType(final String type, final  String namespace, final String name) {
        return dataSourcesRepository.findOneByTypeAndAccountAndNamespaceAndName(type, DataEntity.DEFAULT_ENTITY_ACCOUNT, namespace, name);
    }

    public HiveTableInfo getEntityColumnInfo(final String namespace, final String name, final String dc) throws NotFoundException {
        Optional<DataSource> entity = dataSourcesRepository.findHiveDataEntityByNamespaceAndName(namespace, name);
        return entity.map(e -> getEntityColumnInfo(e, dc))
                .orElseThrow(() -> new NotFoundException(String.format("entity [%s.%s.%s] couldn't be found",
                        dc, namespace, name)));
    }

    public HiveTableInfo getEntityColumnInfo(DataSource entity, String dc) {
        HiveTableInfo result = null;
        if (entity != null) {
            HiveInstanceType instance = getHiveInstanceFor(entity, dc);
            if (instance != null) {
                HiveTableInfo ht = modelMapper.map(instance, HiveTableInfo.class);

                // TODO: This should have a source of truth instead of this heuristics.
                ht.setPii(isPii(instance.getCreatedBy())
                    || isPii(instance.getTable())
                    || isPii(instance.getSchema()));

                result = ht;
            }
        }
        return result;
    }

    void saveTagToLabels(DataSource dataSource, String tagName) {
        if (!TAGS_TO_SYNC.contains(tagName)) {
            return;
        }
        saveLabel(dataSource, tagName, "true");
    }

    public void saveLabel(Integer dsId, String label, String value) {
        if (dataSourcesRepository.findById(dsId).isPresent()) {
            saveLabel(dataSourcesRepository.findById(dsId).get(), label, value);
        }
    }

    private void saveLabel(DataSource entity, String label, String value) {
        Map<String, String> labels = entity.getLabels();
        labels.put(label, value);
        dataSourcesRepository.save(entity);
    }


    Optional<String> deleteTagFromLabels(String namespace, String name, String tagName) {
        if (!TAGS_TO_SYNC.contains(tagName)) {
            return Optional.empty();
        }
        return deleteLabel(findByHiveDataEntityNamespaceAndName(namespace, name), tagName);
    }

    public Optional<String> deleteLabel(Integer dsId, String label) {
        if (dataSourcesRepository.findById(dsId).isPresent()) {
            return deleteLabel(dataSourcesRepository.findById(dsId).get(), label);
        } else {
            return Optional.empty();
        }
    }

    private Optional<String> deleteLabel(DataSource entity, String label) {
        Map<String, String> labels = entity.getLabels();
        if (!labels.containsKey(label)) {
            return Optional.empty();
        }
        labels.remove(label);
        dataSourcesRepository.save(entity);
        return Optional.of(label);
    }

    public HiveInstanceType getHiveInstanceFor(DataSource entity, String dc) {
        HiveInstanceType instance;
        switch (dc.toUpperCase()) {
            case "LHR4":
                instance = entity.getLhr4Instance();
                break;
            case "AMS4":
                instance = entity.getAms4Instance();
                break;
            case "GC":
                instance = entity.getGoogleCloudInstance();
                break;
            default:
                throw new IllegalArgumentException("Must be one of LHR4/AMS4/GC.");
        }
        return instance;
    }

    /**
     * Marks table as pii if table name contain pii or schema contains pii or owner contains pii.
     *
     * @param toCheck - table attribute to check for PII
     * @return true if table looks like pii, false otherwise
     */
    private boolean isPii(String toCheck) {
        if (toCheck == null) {
            return false;
        }
        return toCheck.toLowerCase().contains("pii");
    }

    static void validateRegistrationAttributes(final DataEntityRegistrationAttributes attributes)
        throws DataEntityRegistrationException {
        if (attributes.getOwnerOrgunitId() <= 0) {
            throw new DataEntityRegistrationException("owner orgunit id must be a positive number");
        }
        // todo call ownership service to check validity of an orgunit id
        // todo validity means - orgunit exists and active and has type "team"
    }

    public DataEntityRegistrationResolution registerDataEntity(
        final String type, final String account, final String namespace, final String name,
        final DataEntityRegistrationAttributes attributes) {

        DataEntityRegistrationResolution resolution = new DataEntityRegistrationResolution(
            DataEntityRegistrationStatus.OK, null
        );

        try {
            validateRegistrationAttributes(attributes);
        } catch (DataEntityRegistrationException e) {
            resolution.setStatus(DataEntityRegistrationStatus.FAILED);
            resolution.setMessage("Attributes validation has failed: " + e.getMessage());
            return resolution;
        }

        DataSource dataSource = dataSourcesRepository.findOneByTypeAndAccountAndNamespaceAndName(
            type, account, namespace, name
        );
        if (null == dataSource) {
            try {
                ownershipService.assignOwnership(
                    type, account, namespace, name,
                    attributes.getOwnerOrgunitId(), "Registered ownership via dataquality api",
                    null, null
                );
            } catch (Exception e) {
                resolution.setStatus(DataEntityRegistrationStatus.FAILED);
                resolution.setMessage("Unable to store ownership info: " + e.getMessage());
                return resolution;
            }

            // saving only after and if ownership request is completed
            dataSource = new DataSource();
            dataSource.setName(name);
            dataSource.setNamespace(namespace);
            dataSource.setType(type);
            dataSource.setAccount(account);
            dataSourcesRepository.save(dataSource);
        } else {
            resolution.setStatus(DataEntityRegistrationStatus.ALREADY_REGISTERED);
            return resolution;
        }

        return resolution;
    }
}
