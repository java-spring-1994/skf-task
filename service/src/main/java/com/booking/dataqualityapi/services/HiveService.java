package com.booking.dataqualityapi.services;

import com.booking.bigdata.utils.kerberos.KerberosAuthenticatedDoAs;
import com.booking.dataqualityapi.config.HiveConfig;
import com.booking.dataqualityapi.exceptions.DataUnavailableException;
import com.booking.dataqualityapi.exceptions.PermissionDeniedException;
import com.booking.dataqualityapi.utils.ResultSetConverter;
import org.apache.hadoop.security.AccessControlException;
import org.apache.hive.service.cli.HiveSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.ConnectException;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class HiveService {

    private static final Logger logger = LoggerFactory.getLogger(HiveService.class);

    @Autowired
    HiveConfig hiveConfig;

    @KerberosAuthenticatedDoAs
    String getSampleData(String dc, String tableName, String partitionSpec)
            throws DataUnavailableException, PermissionDeniedException, ConnectException, SQLException {

        String response;
        String table = tableName;
        String whereClause = "";
        int i = 1;

        if (tableName.contains(".")) {
            table = tableName.split("\\.")[1];
        }

        StringBuilder query = buildQuery(tableName, whereClause, partitionSpec);
        try (final Connection connection = hiveConfig.getHiveConnection(dc)) {
            if (connection == null) {
                throw new ConnectException("Unable to connect to Hive! "
                        + "There might be high load on the servers right now. Please try again in a while.");
            }
            try (final PreparedStatement stmt = connection.prepareStatement(query.toString())) {
                if (partitionSpec != null) {
                    for (String partition : partitionSpec.split("/")) {
                        stmt.setString(i++, partition.split("=")[1]);
                    }
                }
                try (final ResultSet rs = stmt.executeQuery()) {
                    if (!rs.isBeforeFirst()) {
                        throw new DataUnavailableException("This table has no data!");
                    }
                    response = ResultSetConverter.convertToJson(table, rs, false).toJSONString();
                }
            }
        } catch (HiveSQLException sqle) {
            int errorCode = sqle.toTStatus().getErrorCode();
            logger.error("Exception while getting sample, dc {}, table {}, partition spec {}, error code {}, message: {}",
                dc, tableName, partitionSpec, errorCode, sqle.getMessage());
            if (errorCode == 10041) {
                throw new DataUnavailableException("No partitions have been created in this table!");
            } else if (isCause(AccessControlException.class, sqle)) {
                throw new PermissionDeniedException("We do not have sufficient privileges to access this table's data.");
            } else {
                throw sqle;
            }
        } catch (Exception e) {
            logger.error("Exception while getting sample, dc {}, table {}, partition spec {}, exception type {}, message {} ",
                dc, tableName, partitionSpec, e.getClass().toString(), e.getMessage());
            throw e;
        }
        return response;
    }

    private StringBuilder buildQuery(String tableName, String whereClause, String partitionSpec) {
        StringBuilder query = new StringBuilder("SELECT * FROM " + tableName);
        if (partitionSpec != null) {
            query.append(" WHERE ");
            StringBuilder whereClauseBuilder = new StringBuilder(whereClause);
            for (String partition : partitionSpec.split("/")) {
                whereClauseBuilder.append(partition.split("=")[0]).append(" = ? AND ");
            }
            whereClause = whereClauseBuilder.toString();
            query.append(whereClause, 0, whereClause.length() - 4);
        }
        query.append(" LIMIT 50");
        return query;
    }

    private static boolean isCause(Class<? extends Throwable> expected, Throwable exc) {
        return expected.isInstance(exc) || (
                exc != null && isCause(expected, exc.getCause()));
    }
}