package com.booking.dataqualityapi.services;

import com.booking.bigdata.service.client.http.HttpClient;
import com.booking.bigdata.utils.kerberos.KerberosAuthenticatedDoAs;
import com.booking.dataqualityapi.config.OozieApiConfig;
import com.booking.dataqualityapi.dto.OozieCoordinatorInfo;
import com.booking.dataqualityapi.dto.OozieWorkflowInstanceInfo;
import com.booking.dataqualityapi.dto.workflow.WorkflowCoordInfo;
import com.booking.dataqualityapi.utils.DatacenterUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.oozie.client.AuthOozieClient;
import org.apache.oozie.client.CoordinatorJob;
import org.apache.oozie.client.OozieClientException;
import org.quartz.CronExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_TIME;

@Service
public class OozieApiService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OozieApiService.class);

    @Autowired
    private OozieApiConfig oozieApiConfig;

    private final ObjectMapper objectMapper = new ObjectMapper();
    private static final int API_FETCH_LIMIT = 10;

    private static final DateTimeFormatter OOZIE_DATE_TIME_FORMAT = DateTimeFormatter.RFC_1123_DATE_TIME;

    private static final String BOOZIE_WORKFLOW_ENDPOINT = "https://boozie.anycast.prod.booking.com/dashboard/workflow/HADOOP_%s/%s";

    private static final String BOOZIE_COORDINATOR_ENDPOINT = "https://boozie.anycast.prod.booking.com/dashboard/coordinator/HADOOP_%s/%s";

    /**
     * Returns information about last/current coordinator instance with matching name, null if not found.
     * @param coordName name of coordinator to look for
     * @param datacenter datacenter
     * @return coordinator info.
     * @throws OozieClientException when unable to fetch coordinator info.
     */
    @KerberosAuthenticatedDoAs
    public WorkflowCoordInfo getCoordInfo(final String coordName, final DatacenterUtils.Datacenter datacenter) throws OozieClientException {
        AuthOozieClient oozieClient = new AuthOozieClient(
            oozieApiConfig.getBaseUrlForOozieClientLib(datacenter)
        );
        oozieClient.setRetryCount(2);

        List<CoordinatorJob> jobs =  oozieClient.getCoordJobsInfo(String.format("name=%s", coordName), 0, 1);
        if (jobs != null && jobs.size() != 0) {
            // 1st element should be always the last/current one
            CoordinatorJob job = jobs.get(0);
            return new WorkflowCoordInfo(
                datacenter.name().toLowerCase(), job.getId(), coordName, job.getStatus().toString(),
                job.getStartTime(), job.getEndTime(),
                // these are not available in API, only collected in the listener
                null, null);
        }
        return null;
    }

    @KerberosAuthenticatedDoAs
    OozieCoordinatorInfo getRunningCoordinatorInfo(String workflowName, DatacenterUtils.Datacenter datacenter) throws IOException {

        OozieCoordinatorInfo oozieCoordinatorInfo = null;

        // example of API call
        // http://hadoop-ams4-oozie.anycast.prod.booking.com:11000/oozie/v2/jobs?filter=name%3Dreservation-flatter;status%3DRUNNING&jobtype=coordinator

        //        String uri = String.format("%s/jobs?filter=name%%3D%s;status%%3DRUNNING&offset=1&len=1&jobtype=coordinator",
        //                getBaseUri(datacenter), URIUtil.encodeAll(workflowName));
        //
        //        LOGGER.debug("Calling uri {}", uri);

        String baseUrl = getBaseUri(datacenter);
        String path = String.format("jobs?filter=name%%3D%s;status%%3DRUNNING&offset=1&len=1&jobtype=coordinator",
                URIUtil.encodeAll(workflowName));

        String response = HttpClient.builder(baseUrl)
                .withSPNEGO()
                .build()
                .get(path);

        if (response != null) {

            LOGGER.debug("Got coordinator details : {}", response);
            //Parse our JSON response
            JsonNode coordJobs = objectMapper.readTree(response).get("coordinatorjobs");
            if (coordJobs.size() > 0) {
                try {
                    JsonNode coordinatorDetails = coordJobs.get(0);

                    String coordinatorId = coordinatorDetails.get("coordJobId").asText();
                    String nextMaterializedTime = coordinatorDetails.get("nextMaterializedTime").asText();
                    String frequency = coordinatorDetails.get("frequency").asText();
                    String frequencyTimeunit = coordinatorDetails.get("timeUnit").asText();
                    long coordStartTime = getTimestampForOozieDateTime(
                        coordinatorDetails.get("startTime").asText());
                    long coordEndTime = getTimestampForOozieDateTime(
                        coordinatorDetails.get("endTime").asText());

                    oozieCoordinatorInfo = new OozieCoordinatorInfo(
                        workflowName,
                        coordinatorId,
                        frequency,
                        frequencyTimeunit,
                        nextMaterializedTime,
                        coordStartTime,
                        coordEndTime
                    );
                } catch (Exception e) {
                    LOGGER.error("Could not get coordinator info because => ", e);
                }
            } else {
                LOGGER.debug(
                    "Empty list with coordinator jobs in oozie response, workflow {}, datacenter {}, oozie base url {}, path {}",
                    workflowName, datacenter.toString(), baseUrl, path);
            }
        }
        return oozieCoordinatorInfo;
    }

    @KerberosAuthenticatedDoAs
    public List<OozieWorkflowInstanceInfo> getLastCoordinatorActionsByName(
        final DatacenterUtils.Datacenter dc, final String coordName, final int size) throws IOException {

        OozieCoordinatorInfo coordInfo = getRunningCoordinatorInfo(coordName, dc);
        return coordInfo == null ? null : getLastCoordinatorActions(dc, coordInfo.getCoordinatorId(), size);
    }

    @KerberosAuthenticatedDoAs
    private List<OozieWorkflowInstanceInfo> getLastCoordinatorActions(
        final DatacenterUtils.Datacenter dc, final String coordId, final int size) throws IOException {

        List<OozieWorkflowInstanceInfo> result = new ArrayList<>();

        String baseUrl = getBaseUri(dc);
        String path = String.format("job/%s?show=info&offset=1&len=%d&order=desc", coordId, size);

        String response = HttpClient.builder(baseUrl)
            .withSPNEGO()
            .build()
            .get(path);

        if (response != null) {
            JsonNode coordActions = objectMapper.readTree(response).get("actions");

            for (final JsonNode actionNode : coordActions) {
                try {
                    result.add(
                        new OozieWorkflowInstanceInfo(
                            actionNode.get("externalId").isNull() ? null : actionNode.get("externalId").asText(),
                            actionNode.get("coordJobId").asText(),
                            "",
                            getFormattedOozieDateTime(actionNode.get("nominalTime").asText()),
                            "",
                            "",
                            actionNode.get("createdTime").asText(),
                            actionNode.get("lastModifiedTime").asText(),
                            actionNode.get("status").asText()
                        )
                    );
                } catch (Exception e) {
                    LOGGER.error("Could not get coordinator actions info because: {}. base url {}, path {}",
                        e.getMessage(), baseUrl, path);
                }
            }
        }

        return result;
    }

    @KerberosAuthenticatedDoAs
    OozieWorkflowInstanceInfo getWorkflowInfo(DatacenterUtils.Datacenter datacenter, OozieCoordinatorInfo oozieCoordinatorInfo,
                                              String date, String hour, String minute) throws IOException {

        OozieWorkflowInstanceInfo workflowInstanceInfo = OozieWorkflowInstanceInfo
                .OozieWorkflowInstanceInfoBuilder
                .anOozieWorkflowInstanceInfo()
                    .nominalDate(date)
                    .nominalHour(hour)
                    .nominalMinute(minute)
                    .workflowStatus("NOT_FOUND")
                    .build();

        // We check only by provided day, without hours and minutes
        boolean checkByDateOnly = (hour == null && minute == null);

        int offset;
        long requestedTimeMs = -1;
        LocalDate requestedDate = null;
        if (checkByDateOnly) {
            LOGGER.debug("Checking workflow info by nominal date only");
            requestedDate = LocalDate.parse(date);
            LOGGER.debug("Requested date is {}", requestedDate);

            offset = getOffset(oozieCoordinatorInfo, getTimestampForDate(date, "00", "00")) - (API_FETCH_LIMIT / 2);
        } else {
            requestedTimeMs = getTimestampForDate(date, hour, minute);
            LOGGER.debug("Got requestedTimeMs {}", requestedTimeMs);

            offset = getOffset(oozieCoordinatorInfo, requestedTimeMs) - (API_FETCH_LIMIT / 2);
        }
        offset = Math.max(offset, 0);
        LOGGER.debug("Offset for oozie api call is {}", offset);

        LOGGER.debug("Got offset {}", offset);

        String baseUrl = getBaseUri(datacenter);
        String path = String.format("job/%s?show=info&offset=%s&len=%s&order=desc",
                oozieCoordinatorInfo.getCoordinatorId(), offset, API_FETCH_LIMIT);

        String response = HttpClient.builder(baseUrl)
            .withSPNEGO()
            .build()
            .get(path);

        if (response != null) {
            LOGGER.debug("Got all workflows details : {}", response);

            Iterator<JsonNode> allWorkflowsItr = objectMapper.readTree(response).get("actions").iterator();

            boolean found;

            while (allWorkflowsItr.hasNext()) {
                JsonNode workflow = allWorkflowsItr.next();
                String nominalTime = workflow.get("nominalTime").asText();

                if (checkByDateOnly) {
                    assert requestedDate != null;
                    LocalDate nominalDate = getDateForOozieDateTime(nominalTime);
                    LOGGER.debug("nominal date = {}, requested date = {}", nominalDate, requestedDate);
                    found = nominalDate.equals(requestedDate);
                } else {
                    long nominalTimeMs = getTimestampForOozieDateTime(nominalTime);
                    LOGGER.debug("nominal = {}, requested = {}", nominalTimeMs, requestedTimeMs);
                    found = (nominalTimeMs == requestedTimeMs);
                }

                if (found) {
                    workflowInstanceInfo.setWorkflowStatus(workflow.get("status").asText());
                    workflowInstanceInfo.setWorkflowId(workflow.get("externalId").asText(null));
                    LOGGER.debug("Got workflow id {}", workflowInstanceInfo.getWorkflowId());
                    if (workflowInstanceInfo.getWorkflowId() == null) {
                        workflowInstanceInfo.setMissingDependencies(workflow.get("missingDependencies").asText());
                        LOGGER.debug("missing dependencies {}", workflowInstanceInfo.getMissingDependencies());
                    }
                    break;
                }
            }
        }
        return workflowInstanceInfo;
    }

    @KerberosAuthenticatedDoAs
    OozieWorkflowInstanceInfo enrichWorkflowDetails(DatacenterUtils.Datacenter datacenter,
                                                         OozieWorkflowInstanceInfo workflowInstanceInfo) throws IOException {
        String baseUrl = getBaseUri(datacenter);
        String path = String.format("job/%s?show=info", workflowInstanceInfo.getWorkflowId());

        String response = HttpClient.builder(baseUrl)
            .withSPNEGO()
            .build()
            .get(path);

        if (response != null) {
            LOGGER.debug("Got workflow details : {}", response);
            JsonNode workflowDetails = objectMapper.readTree(response);

            workflowInstanceInfo.setWorkflowStartTime(workflowDetails.get("startTime").asText());
            workflowInstanceInfo.setWorkflowEndTime(workflowDetails.get("endTime").asText());

            return workflowInstanceInfo;
        }

        return null;
    }

    @KerberosAuthenticatedDoAs
    String getWorkflowLink(DatacenterUtils.Datacenter datacenter, String oozieJobInfo) {
        String wfId = getWorkflowId(oozieJobInfo);
        if (checkJobExistsInOozie(datacenter, wfId)) {
            return String.format(BOOZIE_WORKFLOW_ENDPOINT, datacenter.name().toUpperCase(), wfId);
        } else {
            return null;
        }
    }

    @KerberosAuthenticatedDoAs
    String getCoordinatorLink(DatacenterUtils.Datacenter datacenter, String oozieJobInfo) {
        String coordId = getCoordinatorId(oozieJobInfo);
        if (checkJobExistsInOozie(datacenter, coordId)) {
            return String.format(BOOZIE_COORDINATOR_ENDPOINT, datacenter.name().toUpperCase(), coordId);
        } else {
            return null;
        }
    }

    @KerberosAuthenticatedDoAs
    String getWorkflowPath(DatacenterUtils.Datacenter datacenter, String workflowName) {
        try {
            String baseUrl = getBaseUri(datacenter);
            String path = String.format("jobs?filter=name%%3D%s&offset=1&len=20&jobtype=wf", URIUtil.encodeAll(workflowName));

            HttpClient httpClient = HttpClient.builder(baseUrl)
                .withSPNEGO()
                .build();

            String response = httpClient.get(path);
            if (response != null) {
                JsonNode workflows = objectMapper.readTree(response).get("workflows");
                for (JsonNode wf: workflows) {
                    // We need to ignore sub workflows, because they might be deployed at a different path
                    if (wf.get("externalId") == null
                            || StringUtils.isEmpty(wf.get("externalId").asText())
                            || "null".equals(wf.get("externalId").asText())
                    ) {
                        String wfId = wf.get("id").asText();

                        path = String.format("job/%s?show=info", wfId);

                        response = httpClient.get(path);

                        if (response != null) {
                            String appPath = objectMapper.readTree(response).get("appPath").asText();
                            if (appPath.contains("/oozie_wfs_restricted/")) {
                                // some of the restricted workflows are deployed in a separate directory
                                // https://hue.booking.com/filebrowser/#/oozie_wfs_restricted
                                // for these workflows, we ignore the parent folder because that is the team directory
                                appPath = appPath.replaceAll("/oozie_wfs_restricted/", "");
                                return StringUtils.removeEnd(appPath.substring(appPath.indexOf("/") + 1), "/");
                            } else {
                                return StringUtils.removeEnd(appPath.replaceAll("/oozie_wfs/", "").replaceAll("/oozie_workflows/", ""), "/");
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Unable to get workflow path, workflow {}, dc {}, exception: {}", workflowName, datacenter.toString(), e.getMessage());
        }
        return null;
    }

    @KerberosAuthenticatedDoAs
    String getScriptLinkForWorkflowAction(DatacenterUtils.Datacenter datacenter, String oozieJobInfo) throws IOException {
        return getScriptLinkForWorkflowAction(datacenter, getWorkflowId(oozieJobInfo), getActionName(oozieJobInfo));
    }

    @KerberosAuthenticatedDoAs
    private String getScriptLinkForWorkflowAction(DatacenterUtils.Datacenter datacenter, String workflowId, String actionName) throws IOException {

        String baseUrl = getBaseUri(datacenter);
        String path = String.format("job/%s?show=info", workflowId);

        String response = HttpClient.builder(baseUrl)
            .withSPNEGO()
            .build()
            .get(path);

        if (response != null) {
            LOGGER.debug("Got workflow details : {}", response);
            JsonNode workflowDetails = objectMapper.readTree(response);

            String appPath = workflowDetails.get("appPath").asText();
            String fileName = "";

            for (JsonNode action : workflowDetails.get("actions")) {
                if (actionName.equals(action.get("name").asText())) {
                    String actionType = action.get("type").asText();
                    if (actionType.startsWith("hive")) {
                        fileName = getFileNameFromConf(action.get("conf").asText(), "script");
                    } else if (actionType.startsWith("spark")) {
                        fileName = getFileNameFromConf(action.get("conf").asText(), "file");
                        if (fileName == null) {
                            fileName = getFileNameFromConf(action.get("conf").asText(), "jar");
                        }
                    } else {
                        // for other types like sub-workflow, we ignore
                        return null;
                    }
                    break;
                }
            }
            return getHueEndpoint(datacenter) + "/" + buildScriptUrl(fileName, appPath);
        }
        return null;
    }

    private String getBaseUri(DatacenterUtils.Datacenter datacenter) {
        return oozieApiConfig.getApiBaseUrl(datacenter) + "/";
    }

    @KerberosAuthenticatedDoAs
    private int getOffset(OozieCoordinatorInfo oozieCoordinatorInfo, long requestedTimeMs) {

        String frequencyStr = oozieCoordinatorInfo.getFrequency();

        long frequencyMs = NumberUtils.toLong(frequencyStr, 0L);

        switch (oozieCoordinatorInfo.getFrequencyTimeUnit()) {
            case "CRON":
                Date nextRun = getNextValidActionTimeForCronFrequency(new Date(), frequencyStr);
                Date runAfterNext = getNextValidActionTimeForCronFrequency(nextRun, frequencyStr);
                frequencyMs = runAfterNext.getTime() - nextRun.getTime();
                break;
            case "MINUTE":
                frequencyMs = frequencyMs * 60 * 1000;
                break;
            case "HOUR":
                frequencyMs = frequencyMs * 60 * 60 * 1000;
                break;
            case "DAY":
                frequencyMs = frequencyMs * 24 * 60 * 60 * 1000;
                break;
            case "WEEK":
                frequencyMs = frequencyMs * 7 * 24 * 60 * 60 * 1000;
                break;
            case "MONTH":
                frequencyMs = frequencyMs * 30 * 7 * 24 * 60 * 60 * 1000;
                break;
            default:
                //throw new Exception("Invalid Timeunit {}" + frequencyTimeunit);
        }
        LOGGER.debug("Got frequencyMs {} ", frequencyMs);

        //calculate length and offset for nominal date based on frequency and nextMaterializedTime
        long nextMaterializedTimeMs = getTimestampForOozieDateTime(oozieCoordinatorInfo.getNextMaterializedTime());
        LOGGER.debug("Got nextMaterializedTimeMs {}", nextMaterializedTimeMs);

        return Math.round((nextMaterializedTimeMs - requestedTimeMs) / (float) frequencyMs);
    }

    static long getTimestampForDate(String date, String hour, String minute) {

        int intYear = Integer.parseInt(date.split("-")[0]);
        int intMonth = Integer.parseInt(date.split("-")[1]);
        int intDay = Integer.parseInt(date.split("-")[2]);
        int intHour = Integer.parseInt(hour);
        int intMinute = Integer.parseInt(minute);

        LocalDateTime localNow = LocalDateTime.now();
        ZoneId currentZone = ZoneId.systemDefault();
        ZonedDateTime zonedNow = ZonedDateTime.of(localNow, currentZone);
        ZonedDateTime zonedNextTarget =
                zonedNow.withYear(intYear).withMonth(intMonth).withDayOfMonth(intDay).withHour(intHour).withMinute(intMinute).withSecond(0);

        return zonedNextTarget.toEpochSecond() * 1000;
    }

    static long getTimestampForOozieDateTime(String dateTime) {
        LocalDateTime localNow = LocalDateTime.parse(dateTime, OOZIE_DATE_TIME_FORMAT);
        ZoneId currentZone = ZoneId.systemDefault();
        ZonedDateTime zonedNow = ZonedDateTime.of(localNow, currentZone);
        return zonedNow.toInstant().toEpochMilli();
    }

    static LocalDate getDateForOozieDateTime(String dateTime) {
        LocalDateTime parsedDateTime = LocalDateTime.parse(dateTime, OOZIE_DATE_TIME_FORMAT);
        return parsedDateTime.toLocalDate();
    }

    static String getFormattedOozieDateTime(String dateTime) {
        LocalDateTime parsedDateTime = LocalDateTime.parse(dateTime, OOZIE_DATE_TIME_FORMAT);

        // YYYY-MM-DD HH:MM:SS
        DateTimeFormatter customSimpleDateTimeFormat = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .append(ISO_LOCAL_DATE)
            .appendLiteral(' ')
            .append(ISO_LOCAL_TIME)
            .toFormatter();

        return parsedDateTime.format(customSimpleDateTimeFormat);
    }

    private String getWorkflowId(String jobInfo) {
        return jobInfo.substring(jobInfo.indexOf("wf.id=") + 6).split(",")[0];
    }

    private String getCoordinatorId(String jobInfo) {
        return jobInfo.substring(jobInfo.indexOf("coord.id=") + 9).split("@")[0];
    }

    private String getActionName(String jobInfo) {
        return jobInfo.substring(jobInfo.indexOf("action.name=") + 12).split(",")[0];
    }

    private String getHueEndpoint(DatacenterUtils.Datacenter datacenter) {
        String endpoint = null;
        switch (datacenter) {
            case AMS4:
                endpoint = "https://hue-a.booking.com/filebrowser/#/";
                break;
            case LHR4:
                endpoint = "https://hue.booking.com/filebrowser/#/";
                break;
            default:
                break;
        }
        return endpoint;
    }

    private String getFileNameFromConf(String conf, String attribute) {
        return StringUtils.substringBetween(conf, "<" + attribute + ">", "</" + attribute + ">");
    }

    private String buildScriptUrl(String filename, String path) {
        if (filename.contains("/tmp/")) {
            return filename;
        } else {
            if (filename.contains(path)) {
                return path + "/" + filename.replaceAll("hdfs://", "").replaceAll("nameservice1", "").replaceAll(path, "");
            } else {
                return filename.replaceAll("hdfs://", "").replaceAll("nameservice1", "");
            }
        }
    }

    @KerberosAuthenticatedDoAs
    private boolean checkJobExistsInOozie(DatacenterUtils.Datacenter datacenter, String job) {
        try {
            String baseUrl = getBaseUri(datacenter);
            String path = String.format("job/%s", job);

            HttpClient.builder(baseUrl)
                .withSPNEGO()
                .build()
                .get(path);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Get the next action time after a given time
     * This method has been copied form the Oozie Coord Utils class to make sure
     * we evalute the cron expression the same way that oozie does
     * https://gitlab.booking.com/hadoop/oozie/blob/HEAD/core/src/main/java/org/apache/oozie/command/coord/CoordCommandUtils.java#L800
     * @param targetDate target date
     * @param freq cron expression
     * @return the next valid action time
     */
    @KerberosAuthenticatedDoAs
    private static Date getNextValidActionTimeForCronFrequency(Date targetDate, String freq) {

        Date nextTime = null;
        try {
            String[] cronArray = freq.split(" ");
            // Current CronExpression doesn't support operations
            // where both date of months and day of weeks are specified.
            // As a result, we need to split this scenario into two cases
            // and return the earlier time
            if (!cronArray[2].trim().equals("?") && !cronArray[4].trim().equals("?")) {

                // When any one of day of month or day of week fields is a wildcard
                // we need to replace the wildcard with "?"
                if (cronArray[2].trim().equals("*") || cronArray[4].trim().equals("*")) {
                    if (cronArray[2].trim().equals("*")) {
                        cronArray[2] = "?";
                    } else {
                        cronArray[4] = "?";
                    }
                    freq = StringUtils.join(cronArray, " ");

                    // The cronExpression class takes second
                    // as the first field where oozie is operating on
                    // minute basis
                    CronExpression expr = new CronExpression("0 " + freq);
                    nextTime = expr.getNextValidTimeAfter(targetDate);
                } else {
                    // If both fields are specified by non-wildcards,
                    // we need to split it into two expressions
                    String[] cronArray1 = freq.split(" ");
                    String[] cronArray2 = freq.split(" ");

                    cronArray1[2] = "?";
                    cronArray2[4] = "?";

                    String freq1 = StringUtils.join(cronArray1, " ");
                    String freq2 = StringUtils.join(cronArray2, " ");

                    // The cronExpression class takes second
                    // as the first field where oozie is operating on
                    // minute basis
                    CronExpression expr1 = new CronExpression("0 " + freq1);
                    CronExpression expr2 = new CronExpression("0 " + freq2);
                    nextTime = expr1.getNextValidTimeAfter(targetDate);
                    Date nextTime2 = expr2.getNextValidTimeAfter(targetDate);
                    nextTime = nextTime.compareTo(nextTime2) < 0 ? nextTime : nextTime2;
                }
            } else {
                // The cronExpression class takes second
                // as the first field where oozie is operating on
                // minute basis
                CronExpression expr = new CronExpression("0 " + freq);
                nextTime = expr.getNextValidTimeAfter(targetDate);
            }
        } catch (Exception e) {
            LOGGER.error("Could not evaluate cron expression '" + freq + "' because => ", e);
        }
        return nextTime;
    }
}
