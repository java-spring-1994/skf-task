package com.booking.dataqualityapi.services;

import com.booking.dataquality.dto.monitoring.MonitoringConfig;
import com.booking.dataqualityapi.dto.DataAvailabilityConfigurationInfo;
import com.booking.dataqualityapi.model.DataAvailabilityConfiguration;
import com.booking.dataqualityapi.model.DataSource;
import com.booking.dataqualityapi.model.datamodel.TimeSeriesConfig;
import com.booking.dataqualityapi.repository.DataAvailabilityConfigurationRepository;
import com.booking.dataqualityapi.repository.DataSourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.NotFoundException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Service
public class DataAvailabilityConfigurationService {

    @Autowired
    private DataAvailabilityConfigurationRepository dataAvailabilityConfigurationRepository;

    @Autowired
    private DataSourceRepository dataSourceRepository;

    /**
     * Enable auto publishing for the table.
     *
     * @param namespace - schema
     * @param name      - table name
     * @return object from database
     */
    public DataAvailabilityConfiguration enableDataAvailabilityUpdates(String namespace,
                                                                       String name) {
        DataAvailabilityConfiguration dataAvailabilityConfiguration =
                dataAvailabilityConfigurationRepository.find1ByNamespaceAndName(namespace, name);
        if (dataAvailabilityConfiguration != null) {
            return dataAvailabilityConfiguration;
        }

        DataSource dataSource = dataSourceRepository.findHiveDataEntityByNamespaceAndName(namespace, name)
                .orElseThrow(() -> new NotFoundException(
                        String.format("Table[%s.%s] hasn't been found", namespace, name)));


        if (dataSource.isPartitioned() && dataSource.getTsConfig() == null) {
            throw new IllegalArgumentException(String.format("The table %s.%s is partitioned, "
                            + " but Time Series config isn't set. Please set timeseries config first",
                    namespace, name));
        }

        dataAvailabilityConfiguration = new DataAvailabilityConfiguration();
        dataAvailabilityConfiguration.setName(name);
        dataAvailabilityConfiguration.setNamespace(namespace);
        dataAvailabilityConfiguration.setUpdatedTimestamp(new Date());
        dataAvailabilityConfiguration.setInterval(calculateInterval(dataSource));
        return dataAvailabilityConfigurationRepository.save(dataAvailabilityConfiguration);
    }


    public String calculateInterval(DataSource dataSource) {
        final String day = "day";
        final String hour = "hour";
        if (dataSource.isPartitioned() == false) {
            return day;
        }
        if (dataSource.isPartitioned() && dataSource.getTsConfig() == null) {
            throw new IllegalArgumentException(String.format("The table %s.%s is partitioned, "
                            + " but Time Series config isn't set. Please set timeseries config first",
                    dataSource.getNamespace(), dataSource.getNamespace()));
        }
        TimeSeriesConfig ts = dataSource.getTsConfig();

        if (ts.getEnumColumn() != null) {
            throw new IllegalArgumentException("Only daily and hourly partitions supported");
        }

        if (ts.getDateColumn() == null) {
            throw new IllegalArgumentException(
                    String.format(" Date column is empty. Invalid time series config.TS [%s] ", ts + ""));
        }

        if (ts.getHourColumn() != null) {
            return hour;
        }
        return day;
    }

    public DataAvailabilityConfigurationInfo getDataAvailabilityTableConfiguration(String namespace,
                                                                                   String name) {
        DataAvailabilityConfiguration configuration = dataAvailabilityConfigurationRepository
                .find1ByNamespaceAndName(namespace, name);
        return new DataAvailabilityConfigurationInfo(configuration);
    }

    public DataAvailabilityConfiguration deleteSyncToDas(String namespace,
                                                         String name) {
        DataAvailabilityConfiguration configuration = dataAvailabilityConfigurationRepository
                .find1ByNamespaceAndName(namespace, name);
        if (configuration == null) {
            throw new NotFoundException(String.format(" Sync configuration doesn't exist for %s.%s", namespace, name));
        }
        dataAvailabilityConfigurationRepository.delete(configuration);
        return configuration;
    }

    public List<MonitoringConfig> getTablesSyncForSyncToDas() {
        List<MonitoringConfig> result = new LinkedList<>();
        for (DataAvailabilityConfiguration cfg : dataAvailabilityConfigurationRepository.findAll()) {
            MonitoringConfig monitoringConfig = new MonitoringConfig(
                    cfg.getName(), cfg.getNamespace());
            DataSource dataSource = dataSourceRepository.findHiveDataEntityByNamespaceAndName(cfg.getNamespace(),
                    cfg.getName()).orElse(null);
            if (dataSource == null) {
                continue;
            }
            TimeSeriesConfig timeSeriesConfig = dataSource.getTsConfig();
            if (timeSeriesConfig != null) {
                monitoringConfig.setDayPartitionColumn(timeSeriesConfig.getDateColumn());
                monitoringConfig.setDayPartitionFormat(timeSeriesConfig.getDateFormat());
                monitoringConfig.setHourPartitionColumn(timeSeriesConfig.getHourColumn());
            }
            result.add(monitoringConfig);
        }
        return result;
    }
}
