package com.booking.dataqualityapi.services.catalogue;

import com.booking.dataqualityapi.dto.catalogue.CatalogueDerivedMetricInfo;
import com.booking.dataqualityapi.dto.catalogue.CatalogueExternalMetricInfo;
import com.booking.dataqualityapi.dto.catalogue.CatalogueMetricInfo;
import com.booking.dataqualityapi.dto.catalogue.CatalogueMetricListInfo;
import com.booking.dataqualityapi.dto.catalogue.CatalogueMetricSearchInfo;
import com.booking.dataqualityapi.model.catalogue.CatalogueDerivedMetric;
import com.booking.dataqualityapi.model.catalogue.CatalogueExternalMetric;
import com.booking.dataqualityapi.model.catalogue.CatalogueMetric;
import com.booking.dataqualityapi.repository.catalogue.CatalogueDerivedMetricsRepository;
import com.booking.dataqualityapi.repository.catalogue.CatalogueExternalMetricsRepository;
import com.booking.dataqualityapi.repository.catalogue.CatalogueMetricsRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CatalogueMetricService {
    @Autowired
    CatalogueDerivedMetricsRepository catalogueDerivedMetricsRepository;

    @Autowired
    CatalogueExternalMetricsRepository catalogueExternalMetricsRepository;

    @Autowired
    CatalogueMetricsRepository catalogueMetricsRepository;

    @Transactional
    public List<CatalogueDerivedMetricInfo> save(final String staffLoginName,
                                                 final CatalogueDerivedMetricInfo[] derivedMetrics) {
        List<CatalogueDerivedMetric> result = new ArrayList<>();
        for (CatalogueDerivedMetricInfo metricInfo : derivedMetrics) {
            CatalogueDerivedMetric derivedMetric = convertToCatalogueDerivedMetric(staffLoginName,
                    metricInfo);
            for (String externalMetricName : metricInfo.getExternalMetricNames()) {
                CatalogueExternalMetric externalMetric =
                        catalogueExternalMetricsRepository.findOneByName(externalMetricName)
                                .orElseThrow(() -> new NotFoundException(
                                        String.format("External Metric %s hasn't been found", externalMetricName)
                                ));
                derivedMetric.addExternalMetric(externalMetric);
            }
            result.add(catalogueDerivedMetricsRepository.save(derivedMetric));
        }
        return result.stream().map(this::modelToDerivedMetricDTO).collect(Collectors.toList());
    }

    @Transactional
    public List<CatalogueExternalMetricInfo> save(final String staffLoginName,
                                                  final CatalogueExternalMetricInfo[] externalMetrics) {
        List<CatalogueExternalMetric> result = new ArrayList<>();
        for (CatalogueExternalMetricInfo metricInfo : externalMetrics) {
            CatalogueExternalMetric externalMetric = convertToCatalogueExternalMetric(staffLoginName,
                    metricInfo);
            result.add(catalogueExternalMetricsRepository.save(externalMetric));
        }
        return result.stream().map(this::modelToExternalMetricDTO).collect(Collectors.toList());
    }

    @Transactional
    public CatalogueMetricInfo deleteMetric(final String name) {
        Optional<CatalogueMetric> metric = catalogueMetricsRepository.findOneByName(name);
        catalogueMetricsRepository.delete(metric.orElseThrow(() -> new NotFoundException(
                String.format("Metric with name %s doesn't exist", name))));
        return metric.map(this::modelToDTO).get();
    }

    public CatalogueMetricListInfo getMetrics(CatalogueMetricSearchInfo searchInfo) {
        List<CatalogueMetric> result = new ArrayList<>();
        for (final String metricName : searchInfo.getMetriNames()) {
            catalogueMetricsRepository.findOneByName(metricName)
                    .ifPresent(e -> result.add(e));
        }
        return modelToDTO(result);
    }

    public CatalogueMetricListInfo getAllMetrics() {
        return modelToDTO(catalogueMetricsRepository.findAll());
    }

    private CatalogueMetricListInfo modelToDTO(final List<CatalogueMetric> metrics) {
        final List<CatalogueExternalMetricInfo> externalMetrics = new ArrayList<>();
        final List<CatalogueDerivedMetricInfo> derivedMetrics = new ArrayList<>();
        for (CatalogueMetric metric : metrics) {
            if (metric instanceof CatalogueExternalMetric) {
                externalMetrics.add(modelToExternalMetricDTO((CatalogueExternalMetric) metric));
                continue;
            }
            if (metric instanceof CatalogueDerivedMetric) {
                derivedMetrics.add(modelToDerivedMetricDTO((CatalogueDerivedMetric) metric));
                continue;
            }
            throw new IllegalArgumentException("Wrong metric type has been used " + metric.getClass());
        }
        return new CatalogueMetricListInfo(externalMetrics, derivedMetrics);
    }


    private CatalogueMetricInfo modelToDTO(final CatalogueMetric model) {
        if (model instanceof CatalogueExternalMetric) {
            return modelToExternalMetricDTO((CatalogueExternalMetric) model);
        } else if (model instanceof CatalogueDerivedMetric) {
            return modelToDerivedMetricDTO((CatalogueDerivedMetric) model);
        }
        throw new IllegalArgumentException("Unsupported class " + model.getClass());
    }

    @NotNull
    private CatalogueExternalMetricInfo modelToExternalMetricDTO(CatalogueExternalMetric model) {
        final CatalogueExternalMetricInfo result = new CatalogueExternalMetricInfo();
        populateCatalogueExternalMetricInfo((CatalogueExternalMetric) model, result);
        populateCatalogueMetricInfo(model, result);
        return result;
    }

    @NotNull
    private CatalogueDerivedMetricInfo modelToDerivedMetricDTO(CatalogueDerivedMetric model) {
        final CatalogueDerivedMetricInfo result = new CatalogueDerivedMetricInfo();
        populateCatalogueMetricInfo(model, result);
        populateCatalogueDerivedMetricInfo((CatalogueDerivedMetric) model, result);
        final Set<String> externalMetricNames = new HashSet<>();
        final List<CatalogueExternalMetricInfo> externalMetrics = new ArrayList<>();
        for (CatalogueExternalMetric externalMetric : ((CatalogueDerivedMetric) model).getExternalMetrics()) {
            externalMetricNames.add(externalMetric.getName());
            externalMetrics.add(modelToExternalMetricDTO(externalMetric));
        }
        result.setExternalMetrics(externalMetrics);
        result.setExternalMetricNames(externalMetricNames);
        return result;
    }

    private void populateCatalogueMetricInfo(final CatalogueMetric model,
                                             final CatalogueMetricInfo dto) {
        dto.setDescription(model.getDescription());
        dto.setDisplayName(model.getDisplayName());
        dto.setHidden(model.isHidden());
        dto.setName(model.getName());
        dto.setReadiness(model.getReadiness());
    }

    private void populateCatalogueExternalMetricInfo(final CatalogueExternalMetric model,
                                                     final CatalogueExternalMetricInfo dto) {
        dto.setSource(model.getSource());
        dto.setEndpoint(model.getEndpoint());
        dto.setTableName(model.getTableName());
        dto.setTableNamespace(model.getNamespace());
        dto.setColumn(model.getColumn());
        dto.setAggregation(model.getAggregation());
        dto.setRuntimeKey(model.getRuntimeKey());
        dto.setRevenueMetric(model.isRevenueMetric());
        if (model.getFilterMap() != null) {
            model.getFilterMap().entrySet().forEach(e -> dto.getFilterMap().put(e.getKey(), e.getValue()));
        }
    }

    private void populateCatalogueDerivedMetricInfo(final CatalogueDerivedMetric model,
                                                    final CatalogueDerivedMetricInfo dto) {
        dto.setFormula(model.getFormula());
    }


    private CatalogueMetric populateModelFields(final String staffLoginName,
                                                final CatalogueMetricInfo metricInfo,
                                                final CatalogueMetric metric) {
        metric.setCreateTime(new Date());
        metric.setUpdateTime(new Date());
        metric.setDescription(metricInfo.getDescription());
        metric.setHidden(metricInfo.isHidden());
        metric.setDisplayName(metricInfo.getDisplayName());
        metric.setName(metricInfo.getName());
        metric.setOwner(staffLoginName);
        metric.setReadiness(metricInfo.getReadiness());
        metric.setType(metricInfo.getType().toString());
        return metric;
    }

    private CatalogueDerivedMetric convertToCatalogueDerivedMetric(final String staffLoginName,
                                                                   final CatalogueDerivedMetricInfo metricInfo) {
        CatalogueDerivedMetric derivedMetric = new CatalogueDerivedMetric();
        derivedMetric.setFormula(metricInfo.getFormula());
        derivedMetric.setUpdateTime(new Date());
        populateModelFields(staffLoginName, metricInfo, derivedMetric);
        return derivedMetric;
    }

    private CatalogueExternalMetric convertToCatalogueExternalMetric(final String staffLoginName,
                                                                     final CatalogueExternalMetricInfo metricInfo) {
        CatalogueExternalMetric externalMetric = new CatalogueExternalMetric();
        externalMetric.setAggregation(metricInfo.getAggregation());
        externalMetric.setColumn(metricInfo.getColumn());
        externalMetric.setEndpoint(metricInfo.getEndpoint());
        externalMetric.setTableName(metricInfo.getTableName());
        externalMetric.setNamespace(metricInfo.getTableNamespace());
        externalMetric.setRuntimeKey(metricInfo.getRuntimeKey());
        externalMetric.setSource(metricInfo.getSource());
        externalMetric.setRevenueMetric(metricInfo.isRevenueMetric());
        externalMetric.setUpdateTime(new Date());
        if (metricInfo.getFilterMap() != null) {
            metricInfo.getFilterMap().entrySet().forEach(e -> externalMetric.getFilterMap().put(e.getKey(), e.getValue()));
        }
        populateModelFields(staffLoginName, metricInfo, externalMetric);
        return externalMetric;
    }

}
