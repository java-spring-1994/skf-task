package com.booking.dataqualityapi.services;

import com.booking.dataqualityapi.dto.partition.PartitionInfo;
import com.booking.dataqualityapi.dto.partition.PartitionListInfo;
import com.booking.dataqualityapi.dto.partition.PartitionsAnalytics;
import com.booking.dataqualityapi.dto.partition.TablePartitionsInfo;
import com.booking.dataqualityapi.model.HiveColumnMeta;
import com.booking.dataqualityapi.model.datamodel.TimeSeriesConfig;
import com.booking.dataqualityapi.repository.HiveColumnMetaRepositoryImpl;
import com.booking.dataqualityapi.repository.HiveTablePartitionRepositoryImpl;
import com.booking.dataqualityapi.services.partitions.PartitionNamesBuilder;
import com.booking.dataqualityapi.utils.PartitionsUtil;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;


@Service
public class PartitionService {

    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory.getLogger(PartitionService.class);

    @Autowired
    private HiveTablePartitionRepositoryImpl partitionRepository;

    @Autowired
    private TimeSeriesConfigService timeSeriesConfigService;

    @Autowired
    private HiveColumnMetaRepositoryImpl columnMetaRepository;

    private static final String ET_PLUGGABLE_SCHEMA = "et_pluggable";
    private static final String ET_PLUGGABLE_DATE_REGION_NAME = "nominal_timestamp";


    public PartitionListInfo getPartitionsListInfo(String datacenter, String namespace,
                                                   String name,
                                                   Optional<String> search,
                                                   Optional<Integer> offset,
                                                   Optional<Integer> length) throws SQLException {
        return partitionRepository.getPartitionsListInfo(datacenter, namespace, name, search, offset, length);
    }

    public PartitionsAnalytics calculateMissedPartitions(
            final String datacenter,
            final String schema,
            final String table,
            final Optional<String> dateFrom,
            final Optional<String> dateTo,
            final Optional<String> search)
            throws SQLException {

        final int NumberOfPartitionsToCheck = 80;
        final List<PartitionInfo> partitionsInfo = partitionRepository.getPartitions(datacenter, schema, table, search, Optional.of(NumberOfPartitionsToCheck));

        TimeSeriesConfig tsConfig = timeSeriesConfigService.getHiveConfig(schema, table);


        Function<Optional<String>, Optional<LocalDate>> toLocalDate = (dateStr) -> Optional.ofNullable(parseDate(dateStr,
                DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        final Optional<LocalDate> to = toLocalDate.apply(dateTo);
        final Optional<LocalDate> from = toLocalDate.apply(dateFrom);

        if (!to.isPresent() || !from.isPresent()) {
            return new PartitionsAnalytics(partitionsInfo,
                    "Date range is absent or invalid, partitions displayed without missing partitions calculation.", from.orElse(null), to.orElse(null));
        }

        List<HiveColumnMeta> partitionColumns = columnMetaRepository.getOrderedPartitionColumns(datacenter, schema, table);
        final List<String> partColumns = partitionColumns.stream().map(HiveColumnMeta::getColumnName).collect(Collectors.toList());

        final PartitionNamesBuilder partitionNamesBuilder = new PartitionNamesBuilder(from.get(), to.get(), partColumns);

        if (tsConfig != null) {
            partitionNamesBuilder.withDatePartitionColumn(tsConfig.getDateColumn());
            partitionNamesBuilder.withDateFormatter(
                    DateTimeFormatter.ofPattern(
                            TimeSeriesConfigService.convertPythonDateFormatToJava(tsConfig.getDateFormat())
                    )
            );
            partitionNamesBuilder.withTimePartitionColumn(tsConfig.getHourColumn());
            partitionNamesBuilder.withTimeFormatter(
                    DateTimeFormatter.ofPattern(TimeSeriesConfigService.convertPythonHourFormatToJava(tsConfig.getHourFormat())));
            partitionNamesBuilder.withEnumPartitionColumn(tsConfig.getEnumColumn());
            partitionNamesBuilder.withEnumValues(tsConfig.getEnumValues());
        } else {

            String partition = partitionsInfo.stream().findAny().map(PartitionInfo::getPartitionName).orElse("");
            if (ET_PLUGGABLE_SCHEMA.equals(schema)
                    && partition.startsWith(ET_PLUGGABLE_DATE_REGION_NAME)
                    && (partition.contains("T00:00Z") || partition.contains("T00%3A00Z"))
                    && !partition.contains(PartitionNamesBuilder.PARTITION_NAME_SEPARATOR)) {
                return checkMissedPartitionsForEtPluggableCase(datacenter, schema, table, partitionNamesBuilder, search);
            }

            // going to guess
            // find one partition which looks like date/time only (to check the only last one is not enough)
            Optional<PartitionInfo> dateTimeOnlyPartition = findOneDateTimeOnlyPartition(partitionsInfo);

            if (!dateTimeOnlyPartition.isPresent()) {
                return new PartitionsAnalytics(partitionsInfo,
                        "No missed partition check. Date pattern in partition name hasn't been recognized.", null, null);
            }
            String partitionName = dateTimeOnlyPartition.get().getPartitionName();


            Optional<DateTimeFormatter>  dateFormat = getFormatter(partitionName);
            String dateRegion = getDateRegion(partitionName);
            if (dateRegion == null || !dateFormat.isPresent()) {
                return new PartitionsAnalytics(partitionsInfo,
                        String.format("Date format couldn't be extracted from partition name [%s].", partitionName), null, null);
            }
            partitionNamesBuilder.withDatePartitionColumn(dateRegion);
            partitionNamesBuilder.withDateFormatter(dateFormat.get());
            if (getTimeRegion(partitionName) != null) {
                partitionNamesBuilder.withTimePartitionColumn(getTimeRegion(partitionName));
            }
        }

        List<Pair<String, LocalDateTime>> expectedPartitions = partitionNamesBuilder.generatePossiblePartitions();

        if (search.isPresent() && !search.get().isEmpty()) {
            expectedPartitions = expectedPartitions.stream().filter(name -> name.getKey().contains(search.get())).collect(Collectors.toList());
        }
        final List<PartitionInfo> partitions = partitionRepository.getPartitions(datacenter, schema, table, search,
                expectedPartitions.stream().map(Pair::getKey).collect(Collectors.toList()));

        final Pair<List<PartitionInfo>, String> mergeResult = mergePartitions(expectedPartitions, partitions);

        return new PartitionsAnalytics(mergeResult.getKey(), mergeResult.getValue(), from.get(), to.get());
    }

    //case with et_pluggable legacy partition format
    // et_pluggable has weird partiton format;
    //START of WORK_AROUND CODE
    private PartitionsAnalytics checkMissedPartitionsForEtPluggableCase(final String datacenter,
                                                                        final String schema,
                                                                        final String table,
                                                                        final PartitionNamesBuilder namesBuilder,
                                                                        final Optional<String> search) throws SQLException {



        namesBuilder.withDatePartitionColumn(ET_PLUGGABLE_DATE_REGION_NAME);
        namesBuilder.withDateFormatter(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH'%3A'mm'Z'"));
        List<Pair<String, LocalDateTime>> expectedPartitions1 = namesBuilder.generatePossiblePartitions();
        namesBuilder.withDateFormatter(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm'Z'"));
        List<Pair<String, LocalDateTime>> expectedPartitions2 = namesBuilder.generatePossiblePartitions();

        List<Pair<String, LocalDateTime>> possibleVariant = new LinkedList<>();
        possibleVariant.addAll(expectedPartitions1);
        possibleVariant.addAll(expectedPartitions2);
        if (search.isPresent() && !search.get().isEmpty()) {
            possibleVariant = possibleVariant.stream().filter(name -> name.getKey().contains(search.get())).collect(Collectors.toList());
        }

        List<PartitionInfo> partitions = partitionRepository.getPartitions(datacenter, schema, table, search,
                possibleVariant.stream().map(Pair::getKey)
                        .collect(Collectors.toList()));
        //todo maksim areshkau deals with partition duplication for et_pluggable.cs_report_breakdowns_data ams4
        partitions = partitions.stream().map(part -> part.withNewName(part.getPartitionName()
                .replace(":", "%3A")))
                .filter(distinctByKey(PartitionInfo::getPartitionName)).collect(Collectors.toList());
        final Pair<List<PartitionInfo>, String> mergeResult = mergePartitions(expectedPartitions1, partitions);
        return new PartitionsAnalytics(mergeResult.getKey(), mergeResult.getValue(),namesBuilder.getFrom(), namesBuilder.getTo());

    }

    private static <T> Predicate<T> distinctByKey(
            Function<? super T, ?> keyExtractor) {

        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
    //END of WORK_AROUND CODE

    public TablePartitionsInfo getTablePartitionInfo(final String datacenter, final String schema, final String table)
            throws SQLException {
        final TablePartitionsInfo partitionsInfo = partitionRepository.getPartitionsInfo(datacenter, schema, table);
        if (partitionsInfo.getPartitionCount() > 0) {
            List<PartitionInfo> firstPartitions = partitionRepository.getPartitions(datacenter, schema, table,
                    Sort.Direction.ASC, Optional.empty(), Optional.of(1));
            if (firstPartitions.size() > 0) {
                PartitionInfo firstPartition = firstPartitions.get(0);
                partitionsInfo.setFirstPartition(firstPartition);
            }
        }
        if (partitionsInfo.getPartitionCount() > 1) {
            List<PartitionInfo> lastPartitions = partitionRepository.getPartitions(datacenter, schema, table,
                    Sort.Direction.DESC, Optional.empty(), Optional.of(1));
            if (lastPartitions.size() > 0) {
                partitionsInfo.setLastPartition(lastPartitions.get(0));
            }
        }
        return partitionsInfo;
    }


    private static Optional<DateTimeFormatter> getFormatter(String partitionName) {
        if (partitionName.contains("yyyy_mm_dd")
                || partitionName.contains("date")) {
            return Optional.of(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        }
        String[] segments = partitionName.split(PartitionNamesBuilder.PARTITION_NAME_SEPARATOR);
        for (String segment : segments) {
            if (PartitionsUtil.guessIsDatePartition(segment)) {
                final int eqIndex = segment.indexOf("=");
                if (eqIndex > 0) {
                    String datePattern = segment.substring(0, eqIndex);
                    datePattern = datePattern.replace("mm", "MM");
                    try {
                        return Optional.of(DateTimeFormatter.ofPattern(datePattern));
                    } catch (IllegalArgumentException ex) {
                        return Optional.empty();
                    }
                }
            }
        }
        return Optional.empty();
    }

    private static LocalDate parseDate(Optional<String> dateInput, DateTimeFormatter dateFormat) {
        if (!dateInput.isPresent()) {
            return null;
        }
        String dateStr = dateInput.get();
        try {
            return LocalDate.parse(dateStr, dateFormat);
        } catch (DateTimeParseException ex) {
            //expected exception
            return null;
        }
    }

    private Optional<PartitionInfo> findOneDateTimeOnlyPartition(final List<PartitionInfo> partitions) {
        return partitions.stream().filter(part -> isDateTimeOnlyPartition(part.getPartitionName())).findAny();
    }

    private Pair<List<PartitionInfo>, String> mergePartitions(List<Pair<String, LocalDateTime>> expectedPartitions, List<PartitionInfo> partitions) {
        Map<String, PartitionInfo> mappedPartitions = partitions.stream().collect(Collectors.toMap(
                PartitionInfo::getPartitionName, Function.identity()));

        final List<PartitionInfo> mergedPartitions = new LinkedList<>();
        int missedCount = 0;
        for (Pair<String, LocalDateTime> expectedPartition : expectedPartitions) {
            PartitionInfo part = mappedPartitions.get(expectedPartition.getKey());
            if (part == null) {
                part = createNullPartition(expectedPartition);
                missedCount++;
            } else {
                part = part.addDate(expectedPartition.getValue());
            }
            mergedPartitions.add(part);
        }
        Collections.reverse(mergedPartitions);
        return Pair.of(mergedPartitions,
                String.format("Amount of missed partitions vs expected: %d/%d", missedCount, mergedPartitions.size()));
    }

    private PartitionInfo createNullPartition(Pair<String, LocalDateTime> expectedPartition) {
        return new PartitionInfo(expectedPartition.getKey(),
                null, null,
                null, null,
                null, null, true, expectedPartition.getRight());
    }


    /**
     * Returns date.
     *
     * @param partitionName - date partition name, smth like 'yyyy_mm_dd=2018-09-30'
     * @return - date region like 'yyyy_mm_dd'
     */

    private String getDateRegion(final String partitionName) {
        String[] partitions = partitionName.split(PartitionNamesBuilder.PARTITION_NAME_SEPARATOR);
        for (String regionName : partitions) {
            if (PartitionsUtil.guessIsDatePartition(regionName)) {
                return regionName.substring(0, regionName.indexOf("="));
            }
        }
        return null;
    }

    private String getTimeRegion(final String partitionName) {
        String[] partitions = partitionName.split(PartitionNamesBuilder.PARTITION_NAME_SEPARATOR);
        for (String regionName : partitions) {
            if (PartitionsUtil.guessIsTimePartition(regionName)) {
                return regionName.substring(0, regionName.indexOf("="));
            }
        }
        return null;
    }

    /**
     * Checks that partition name consist only from date time regions.
     *
     * @param partition - partition name to check.
     * @return true if partition contains only date or time regions.
     */

    private boolean isDateTimeOnlyPartition(String partition) {
        String[] regions = partition.split(PartitionNamesBuilder.PARTITION_NAME_SEPARATOR);
        for (String region : regions) {
            if (PartitionsUtil.guessIsDatePartition(region)) {
                continue;
            }
            if (PartitionsUtil.guessIsTimePartition(region)) {
                continue;
            }
            return false;
        }
        return true;
    }

}
