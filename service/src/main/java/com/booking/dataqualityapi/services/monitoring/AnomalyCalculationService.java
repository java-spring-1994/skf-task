package com.booking.dataqualityapi.services.monitoring;

import com.booking.dataquality.model.monitoring.MetricAnomaly;
import com.booking.dataquality.services.AbstractAnomalyCalculationService;
import com.booking.dataqualityapi.repository.monitoring.MetricAnomalyRepository;
import com.booking.dataqualityapi.repository.monitoring.MetricRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.Future;


@Service
public class AnomalyCalculationService extends AbstractAnomalyCalculationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnomalyCalculationService.class);

    @Autowired
    private AnomalyAlertConfigurationService anomalyAlertConfigurationService;

    @Autowired
    public AnomalyCalculationService(MetricRepository metricRepository,
                                     MetricAnomalyRepository metricAnomalyRepository) {
        super(metricRepository, metricAnomalyRepository);
    }

    @Async
    public Future<List<MetricAnomaly>> calculateNewAnomalies(final String dc,
                                                             final String namespace,
                                                             final String name,
                                                             final Integer boundaryScore) {
        return new AsyncResult<>(calcAnomalies(dc, namespace, name, boundaryScore));
    }

    @Async
    public Future<List<MetricAnomaly>> calculateNewAnomalies(final String dc,
                                                             final String namespace,
                                                             final String name,
                                                             final LocalDate from,
                                                             final LocalDate to,
                                                             final Integer boundaryScore) {
        return new AsyncResult<>(calcAnomalies(dc, namespace, name, from, to, boundaryScore));
    }

    @Override
    public void sendAlerts(final String namespace,
                           final String name,
                           final List<MetricAnomaly> anomaliesList,
                           final List<MetricAnomaly> oldAnomalies) {
        anomalyAlertConfigurationService.sendAlertsForAnomalies(namespace,
                name,
                anomaliesList,
                oldAnomalies);
    }

}
