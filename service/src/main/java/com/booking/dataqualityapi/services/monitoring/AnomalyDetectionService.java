package com.booking.dataqualityapi.services.monitoring;

import com.booking.dataqualityapi.dto.monitoring.AnomalyInfo;
import com.booking.dataqualityapi.dto.monitoring.MetricInstanceTableInfo;
import com.booking.dataquality.model.monitoring.MetricAnomaly;
import com.booking.dataqualityapi.repository.monitoring.MetricAnomalyRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service

//CHECKSTYLE:OFF
public class AnomalyDetectionService {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private final MetricAnomalyRepository metricAnomalyRepository;


    @Autowired
    public AnomalyDetectionService(MetricAnomalyRepository metricAnomalyRepository) {
        this.metricAnomalyRepository = metricAnomalyRepository;
    }

    public Map<String, List<AnomalyInfo>> getAnomalies(final Optional<String> dc,
                                                       final String namespace,
                                                       final String name,
                                                       final Optional<String> column,
                                                       final Optional<String> metric,
                                                       final LocalDate from,
                                                       final LocalDate to,
                                                       final float anomalyScore) throws IOException {
        final Map<String, List<AnomalyInfo>> anomalies =
                getAnomalyList(dc, namespace, name, column, metric, from, to, anomalyScore).stream().collect(
                        Collectors.groupingBy(anomaly -> anomaly.getColumn() + ":" + anomaly.getMetric()));
        return anomalies;
    }

    public List<AnomalyInfo> getAnomalyList(final Optional<String> dc,
                                            final String namespace,
                                            final String name,
                                            final Optional<String> column,
                                            final Optional<String> metric,
                                            final LocalDate from,
                                            final LocalDate to,
                                            final float anomalyScore) throws IOException {
        final List<MetricAnomaly> metricAnomalies = metricAnomalyRepository.getMetricAnomalies(namespace,
                name, column.filter(StringUtils::isNoneBlank), metric.filter(StringUtils::isNoneBlank), anomalyScore + 0d, Date.valueOf(from),
                Date.valueOf(to), dc);
        final List<AnomalyInfo> anomalyInfos = metricAnomalies.stream()
                .map(AnomalyDetectionService::convertMetricAnomalyToInfo).collect(Collectors.toList());
        return anomalyInfos;
    }

    @Transactional
    public int deleteAnomalies(String dc, String namespace, String name) {
        return metricAnomalyRepository.deleteAnomalies(dc, namespace, name);
    }

    @Transactional
    public int deleteAllAnomalies() {
        return metricAnomalyRepository.deleteAllAnomalies();
    }

    @Transactional
    public Iterable<MetricAnomaly> saveAnomalies(List<AnomalyInfo> anomalyInfoList) throws Exception {

        Set<MetricInstanceTableInfo> tables = anomalyInfoList.stream()
                .map(anomalyInfo -> new MetricInstanceTableInfo(
                anomalyInfo.getCluster().toLowerCase(),
                anomalyInfo.getSchemaName(),
                anomalyInfo.getTableName())).collect(Collectors.toSet());

        // we won't process more than 10 tables at a time
        if (tables.size() <= 10) {
            for (MetricInstanceTableInfo metricInstanceTableInfo : tables) {
                metricAnomalyRepository.deleteAnomalies(metricInstanceTableInfo.getDc(),
                        metricInstanceTableInfo.getSchemaName(), metricInstanceTableInfo.getTableName());
            }
            List<MetricAnomaly> metricAnomalyList = anomalyInfoList.stream()
                    .map(AnomalyDetectionService::convertAnomalyInfoToMetricAnomaly)
                    .collect(Collectors.toList());

            return metricAnomalyRepository.save(metricAnomalyList);
        } else {
            throw new IllegalArgumentException("To many tables has been passed ->" + tables.size() + " Only 10 is allowed");
        }
    }

    @Transactional
    public Iterable<MetricAnomaly> saveAllAnomalies(List<AnomalyInfo> anomalyInfoList) {

        List<MetricAnomaly> metricAnomalyList = anomalyInfoList.stream()
                .map(AnomalyDetectionService::convertAnomalyInfoToMetricAnomaly)
                .collect(Collectors.toList());
        metricAnomalyRepository.deleteAllAnomalies();
        return metricAnomalyRepository.save(metricAnomalyList);
    }

    private static MetricAnomaly convertAnomalyInfoToMetricAnomaly(final AnomalyInfo anomalyInfo) {
        MetricAnomaly metricAnomaly = new MetricAnomaly();
        metricAnomaly.setColumnName(anomalyInfo.getColumn());
        metricAnomaly.setDate(anomalyInfo.getDate());
        metricAnomaly.setDc(anomalyInfo.getCluster());
        metricAnomaly.setMetric(anomalyInfo.getMetric());
        metricAnomaly.setSchemaName(anomalyInfo.getSchemaName());
        metricAnomaly.setTableName(anomalyInfo.getTableName());
        metricAnomaly.setValue(anomalyInfo.getValue());
        metricAnomaly.setRunningAvg(anomalyInfo.getRunningAvg());
        metricAnomaly.setAnomalyScore(anomalyInfo.getAnomalyScore());
        return metricAnomaly;
    }

    private static AnomalyInfo convertMetricAnomalyToInfo(final MetricAnomaly anomaly) {
        final AnomalyInfo anomalyInfo = new AnomalyInfo();
        anomalyInfo.setAnomalyScore(anomaly.getAnomalyScore());
        anomalyInfo.setRunningAvg(anomaly.getRunningAvg());
        anomalyInfo.setDate(anomaly.getDate());
        anomalyInfo.setCluster(anomaly.getDc());
        anomalyInfo.setSchemaName(anomaly.getSchemaName());
        anomalyInfo.setTableName(anomaly.getTableName());
        anomalyInfo.setColumn(anomaly.getColumnName());
        anomalyInfo.setMetric(anomaly.getMetric());
        anomalyInfo.setValue(anomaly.getValue());
        return anomalyInfo;
    }
}

//CHECKSTYLE:ON
