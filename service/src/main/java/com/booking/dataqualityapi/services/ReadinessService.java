package com.booking.dataqualityapi.services;

import com.booking.dataqualityapi.dto.OozieCoordinatorInfo;
import com.booking.dataqualityapi.dto.OozieWorkflowInstanceInfo;
import com.booking.dataqualityapi.model.readiness.DataReadinessInfo;
import com.booking.dataqualityapi.model.readiness.DataReadinessMetadata;
import com.booking.dataqualityapi.repository.readiness.DataReadinessRepository;
import com.booking.dataqualityapi.repository.readiness.DataReadinessMetadataRepository;
import com.booking.dataqualityapi.utils.DatacenterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReadinessService {

    private static final Logger logger = LoggerFactory.getLogger(ReadinessService.class);
    private static final String GET_ACTUAL_WF_OUTPUTS_QUERY =
            "SELECT part_name FROM dq_hive_%s_partition_list WHERE scheme = ? AND tbl_name = ? "
                    + " AND transient_last_ddl_time BETWEEN ? AND ?";
    @Autowired
    private OozieApiService oozieApiService;
    private DataSource dqDataSourceRo;
    @Autowired
    private DataReadinessRepository datareadinessRepository;

    @Autowired
    private DataReadinessMetadataRepository dataReadinessMetadataRepository;

    @Autowired
    public ReadinessService(
            @Qualifier("getMysqlDqConnectionRo") DataSource dqDataSourceRo
    ) {
        this.dqDataSourceRo = dqDataSourceRo;
    }

    public OozieWorkflowInstanceInfo getWorkflowStatus(OozieCoordinatorInfo oozieCoordinatorInfo, DatacenterUtils.Datacenter datacenter,
                                                       String date, String hour, String minute) throws IOException {

        if (oozieCoordinatorInfo != null) {
            OozieWorkflowInstanceInfo workflowInstanceInfo = getWorkflowInfo(datacenter, oozieCoordinatorInfo, date, hour, minute);
            if (workflowInstanceInfo.getWorkflowId() != null && !"".equals(workflowInstanceInfo.getWorkflowId())
                    && !"null".equals(workflowInstanceInfo.getWorkflowId())) {
                return enrichWorkflowDetails(datacenter, workflowInstanceInfo);
            } else {
                return workflowInstanceInfo;
            }
        }

        return null;

    }

    public OozieCoordinatorInfo getCoordinatorInfo(String workflowName, DatacenterUtils.Datacenter datacenter) throws IOException {
        return oozieApiService.getRunningCoordinatorInfo(workflowName, datacenter);
    }

    private OozieWorkflowInstanceInfo getWorkflowInfo(DatacenterUtils.Datacenter datacenter, OozieCoordinatorInfo oozieCoordinatorInfo,
                                                      String date, String hour, String minute) throws IOException {
        return oozieApiService.getWorkflowInfo(datacenter, oozieCoordinatorInfo, date, hour, minute);
    }

    private OozieWorkflowInstanceInfo enrichWorkflowDetails(DatacenterUtils.Datacenter datacenter,
                                                            OozieWorkflowInstanceInfo workflowInstanceInfo) throws IOException {
        return oozieApiService.enrichWorkflowDetails(datacenter, workflowInstanceInfo);
    }

    public List<String> getActualWfOutputs(DatacenterUtils.Datacenter datacenter, String namespace, String name,
                                           long lowerBoundTs, long upperBoundTs) throws SQLException {

        List<String> outputs = new ArrayList<>();

        try (
                Connection conn = dqDataSourceRo.getConnection();
                PreparedStatement ps = conn.prepareStatement(String.format(GET_ACTUAL_WF_OUTPUTS_QUERY, datacenter.name().toLowerCase()))
        ) {
            ps.setString(1, namespace);
            ps.setString(2, name);
            // the timestamp is in milliseconds, but the ddl time in the data base is in seconds. so we divide by 1000
            // also, the timestamp from Oozie is in GMT, but the timestamp in the database is in UTC. So we add 1 hour = 3600 seconds
            // this might get fucked up during DST.
            ps.setLong(3, (lowerBoundTs / 1000) + 3600);
            ps.setLong(4, (upperBoundTs / 1000) + 3600);

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    outputs.add(rs.getString("part_name"));
                }
            }
        }
        return outputs;
    }

    public long getTimestampForOozieDateTime(String date) {
        return OozieApiService.getTimestampForOozieDateTime(date);
    }

    public int saveDataReadinessInfo(DataReadinessInfo dataReadinessInfo) {
        datareadinessRepository.save(dataReadinessInfo);
        return dataReadinessInfo.getId();
    }

    public void saveDataReadinessMetadata(DataReadinessMetadata dataReadinessMetadata) {
        dataReadinessMetadataRepository.save(dataReadinessMetadata);
    }

    public DataReadinessInfo getReadinessInfocurrentMaterializedDate(String entityType,
                                                                     String namespace, String name,
                                                                     String dataCenter,
                                                                     String currentMaterializedDate) {
        System.out.println(entityType + namespace + name + dataCenter + currentMaterializedDate);
        return datareadinessRepository.findReadinessByEntityTypeNameNamespaceAndCurrentMaterializedTime(entityType,
                namespace, name, dataCenter, currentMaterializedDate);
    }

    public DataReadinessInfo getReadinessInfo(String entityType, String namespace, String name, String dataCenter) {
        System.out.println(entityType + namespace + name + dataCenter);
        return datareadinessRepository.findReadinessByEntityTypeNameAndNamespace(entityType, namespace, name, dataCenter);
    }

    public Map<String, String> getReadinessMetadata(int dataReadinessId) {
        List<DataReadinessMetadata> metadataRes = dataReadinessMetadataRepository.findDataReadinessMetadataByDataReadinessId(dataReadinessId);
        Map<String, String> metadata = new HashMap<>();
        for (DataReadinessMetadata dataReadinessMetadata : metadataRes) {
            metadata.put(dataReadinessMetadata.getKey(), dataReadinessMetadata.getValue());
        }
        return metadata;
    }
}
