package com.booking.dataqualityapi.services.monitoring;

import com.booking.dataquality.model.alerting.AnomalyAlertConfig;
import com.booking.dataquality.model.monitoring.MetricAnomaly;
import com.booking.dataquality.services.AbstractAnomalyAlertConfigurationService;
import com.booking.dataqualityapi.dto.alerts.AnomalyAlertConfigInfo;
import com.booking.dataqualityapi.repository.alerting.AnomalyAlertConfigurationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AnomalyAlertConfigurationService extends AbstractAnomalyAlertConfigurationService {

    @Autowired
    private AnomalyAlertConfigurationRepository repository;

    public List<AnomalyAlertConfigInfo> findAlertConfigurations(final Optional<String> schema,
                                                                final Optional<String> table,
                                                                final Optional<String> column,
                                                                final Optional<String> metric) {
        return repository.findAlertConfigurations(schema, table, column, metric)
                .stream().map(e ->
                        AnomalyAlertConfigInfo.fromModel(e,
                                String.format(AbstractAnomalyAlertConfigurationService.DQ_ALERT_API_NAME_FORMAT,
                                        e.getSchemaName(),
                                        e.getTableName(),
                                        e.getColumnName(),
                                        e.getMetric())))
                .collect(Collectors.toList());
    }

    public AnomalyAlertConfig createConfiguration(String namespace, String name,
                                                  String column, String metric,
                                                  Double anomalyBoundaryScore, String loginName) {
        AnomalyAlertConfig cfg = repository.getAnomalyAlertConfig(namespace, name,
                column, metric);
        if (cfg == null) {
            cfg = new AnomalyAlertConfig();
        }
        cfg.setStatus("ACTIVE");
        cfg.setSchemaName(namespace);
        cfg.setTableName(name);
        cfg.setColumnName(column);
        cfg.setMetric(metric);
        cfg.setAnomalyScore(anomalyBoundaryScore);
        cfg.setStaffLoginName(loginName);
        return repository.save(cfg);
    }

    public AnomalyAlertConfig disableAnomalyAlertConfigurations(String namespace, String name,
                                                                String column, String metric,
                                                                String loginName) {
        AnomalyAlertConfig cfg = repository.getAnomalyAlertConfig(namespace, name,
                column, metric);
        if (cfg == null) {
            throw new IllegalArgumentException(String.format("%s.%s.%s metric -> %s not found",
                    namespace, name, column, metric));
        }
        cfg.setStatus("INACTIVE");
        return repository.save(cfg);
    }

    public void sendAlertsForAnomalies(final String namespace,
                                       final String name,
                                       final List<MetricAnomaly> anomaliesList,
                                       final List<MetricAnomaly> oldAnomaliesList) {
        List<AnomalyAlertConfig> anomalyAlertConfigs =
                repository.findAlertConfigurations(Optional.ofNullable(namespace),
                        Optional.ofNullable(name), Optional.empty(), Optional.empty());
        super.sendAlertsForAnomalies(namespace, name, anomaliesList, oldAnomaliesList, anomalyAlertConfigs);
    }
}
