package com.booking.dataqualityapi.services.monitoring;

import com.booking.dataquality.dto.monitoring.MonitoringConfig;
import com.booking.dataquality.model.monitoring.Metric;
import com.booking.dataqualityapi.dto.DataCenter;
import com.booking.dataqualityapi.dto.dataentity.DataEntity;
import com.booking.dataqualityapi.dto.monitoring.ColumnMetricInfo;
import com.booking.dataqualityapi.dto.monitoring.MetricPayload;
import com.booking.dataqualityapi.dto.monitoring.TableColumnMetricConfig;
import com.booking.dataqualityapi.model.DataSource;
import com.booking.dataqualityapi.model.EntityInstance.Ams4Instance;
import com.booking.dataqualityapi.model.EntityInstance.HiveInstanceType;
import com.booking.dataqualityapi.model.EntityInstance.Lhr4Instance;
import com.booking.dataqualityapi.model.datamodel.TimeSeriesConfig;
import com.booking.dataqualityapi.model.monitoring.DefaultMetricsConfig;
import com.booking.dataqualityapi.model.monitoring.MonitoringRequest;
import com.booking.dataqualityapi.model.monitoring.TableColumnMetricsConfig;
import com.booking.dataqualityapi.repository.ColumnMetricRepository;
import com.booking.dataqualityapi.repository.DataSourceRepository;
import com.booking.dataqualityapi.repository.DefaultMetricsConfigRepository;
import com.booking.dataqualityapi.repository.MonitoringRequestRepository;
import com.booking.dataqualityapi.repository.TableColumnMetricRepository;
import com.booking.dataqualityapi.repository.monitoring.MetricRepository;
import com.booking.dataqualityapi.utils.DateUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class MetricsService {

    private static final Logger logger = LoggerFactory.getLogger(MetricsService.class);

    private final MonitoringRequestRepository requestRepository;
    private final DataSourceRepository dataSourceRepository;
    private final DefaultMetricsConfigRepository defaultMetricsRepository;
    private final MetricRepository metricRepository;
    private final TableColumnMetricRepository tableColumnMetricRepository;
    private final ThreadPoolTaskExecutor taskExecutor;
    private final ColumnMetricRepository columnMetricRepository;
    private final AnomalyCalculationService anomalyCalculationService;


    private static final String NEW_REQUEST_STATUS = "pending";

    @Autowired
    public MetricsService(
            final MonitoringRequestRepository requestRepository,
            final DataSourceRepository dataSourceRepository,
            final DefaultMetricsConfigRepository defaultMetricsRepository,
            final MetricRepository metricRepository,
            final ThreadPoolTaskExecutor taskExecutor,
            final ColumnMetricRepository columnMetricRepository,
            final AnomalyCalculationService anomalyCalculationService,
            final TableColumnMetricRepository tableColumnMetricRepository
    ) {
        this.requestRepository = requestRepository;
        this.dataSourceRepository = dataSourceRepository;
        this.defaultMetricsRepository = defaultMetricsRepository;
        this.metricRepository = metricRepository;
        this.taskExecutor = taskExecutor;
        this.columnMetricRepository = columnMetricRepository;
        this.anomalyCalculationService = anomalyCalculationService;
        this.tableColumnMetricRepository = tableColumnMetricRepository;
    }

    public MonitoringRequest getMonitoringRequest(String schema, String table) {
        return requestRepository.find1BySchemaAndName(schema, table);
    }

    private void validateRequest(MonitoringRequest monitoringRequest)
        throws IllegalArgumentException {
        // check that joined datasource is not null, otherwise throw error
        DataSource dataEntity = dataSourceRepository.findOneByTypeAndAccountAndNamespaceAndName(
            "hive-table",
            DataEntity.DEFAULT_ENTITY_ACCOUNT,
            monitoringRequest.getSchemaName(),
            monitoringRequest.getTableName()
        );
        if (dataEntity == null) {
            throw new IllegalArgumentException(
                String.format("Data entity not found for name %s.%s", monitoringRequest.getSchemaName(), monitoringRequest.getTableName())
            );
        }

        // assuming that there is only one instance on every cluster
        Ams4Instance ams4Instance = dataEntity.getAms4Instance();
        Lhr4Instance lhr4Instance = dataEntity.getLhr4Instance();

        // if table has no instances then we can not check partitions - throw error
        if (ams4Instance == null && lhr4Instance == null) {
            throw new IllegalArgumentException("Data entity has no actual instances");
        }

        Boolean ams4Partitioned = null;
        Boolean lhr4Partitioned = null;
        if (ams4Instance != null) {
            ams4Partitioned = ams4Instance.isPartitioned();
        }
        if (lhr4Instance != null) {
            lhr4Partitioned = lhr4Instance.isPartitioned();
        }
        // if table is partitioned on one of clusters then throw error
        if (ams4Partitioned != null && lhr4Partitioned != null && ams4Partitioned != lhr4Partitioned) {
            throw new IllegalArgumentException("Data source is partitioned on one cluster and not on the other one");
        }

        // if table is partitioned then should have ts config, otherwise - throw error
        // for non-partitioned tables time series config is optional, monitoring can work in 'full snapshot' mode
        if (ams4Partitioned != null && ams4Partitioned || lhr4Partitioned != null && lhr4Partitioned) {
            TimeSeriesConfig tsConfig = dataEntity.getTsConfig();
            if (tsConfig == null || !tsConfig.getDatePartitioned()) {
                throw new IllegalArgumentException("Data source is partitioned, but time series config is not defined properly");
            }
        }
    }

    public MonitoringRequest addMonitoringRequest(String schema, String table) {
        MonitoringRequest monitoringRequest = new MonitoringRequest(
            schema, table, NEW_REQUEST_STATUS
        );

        validateRequest(monitoringRequest);

        return requestRepository.save(monitoringRequest);
    }

    public MonitoringRequest rejectMonitoringRequest(final String schema,
                                                     final String table,
                                                     final String staffLoginName) {
        MonitoringRequest request = requestRepository.find1BySchemaAndName(schema, table);
        if (request == null) {
            throw new NotFoundException(String.format("Request %s.%s hasn't been found", schema, table));
        }
        request.setStatus("rejected");
        request.setAdminNote(String.format("monitoring rejected by %s on %tD", staffLoginName, new Date()));
        return requestRepository.save(request);
    }

    public MonitoringRequest approveMonitoringRequest(final String schema,
                                                      final String table,
                                                      final String staffLoginName) {
        MonitoringRequest request = requestRepository.find1BySchemaAndName(schema, table);
        if (request == null) {
            throw new NotFoundException(String.format("Request %s.%s hasn't been found", schema, table));
        }
        if (hasPartitionColumn(request.getDataSource().getLhr4Instance())
                || hasPartitionColumn(request.getDataSource().getAms4Instance())) {
            if (request.getDataSource().getTsConfig() == null) {
                throw new IllegalArgumentException("Time Series config isn't set");
            }
        }
        request.setStatus("approved");
        request.setAdminNote(String.format("monitoring approved by %s on %tD", staffLoginName, new Date()));
        return requestRepository.save(request);
    }

    private boolean hasPartitionColumn(HiveInstanceType instanceType) {
        if (instanceType == null) {
            return false;
        }
        return instanceType.isPartitioned();
    }

    public List<DefaultMetricsConfig> getDefaultMetricsConfig() {
        return defaultMetricsRepository.findAll();
    }

    public void calcAnomalies(Metric[] metrics) {
        Arrays.stream(metrics).map(metric -> Triple.of(metric.getDc(), metric.getSchemaName(), metric.getTableName()))
                .distinct().forEach(triple -> anomalyCalculationService.calculateNewAnomalies(triple.getLeft(), triple.getMiddle(), triple.getRight(), 3));
    }

    public int saveMetrics(final MetricPayload[] metricsPayloads) {
        Metric[] metrics = new Metric[metricsPayloads.length];
        for (int i = 0; i < metricsPayloads.length; i++) {
            metrics[i] = convert(metricsPayloads[i]);
        }
        int savedMetrics = metricRepository.save(metrics);
        if (savedMetrics > 0) {
            calcAnomalies(metrics);
        }
        return savedMetrics;
    }

    private static Metric convert(MetricPayload payload) {
        Metric metric = new Metric();
        metric.setDc(payload.getDc().name().toLowerCase());
        metric.setTableName(payload.getTableName());
        metric.setSchemaName(payload.getSchemaName());
        metric.setColumnName(payload.getColumnName());
        metric.setMetric(payload.getMetric());
        metric.setDate(DateUtils.convertToDateViaInstant(payload.getDate()));
        metric.setValue(payload.getValue());
        return metric;
    }

    private static MetricPayload convert(Metric metric) {
        final MetricPayload payload = new MetricPayload();
        payload.setDc(DataCenter.valueOf(metric.getDc().toUpperCase()));
        payload.setColumnName(metric.getColumnName());
        payload.setMetric(metric.getMetric());
        payload.setDate(DateUtils.convertToLocalDateViaInstant(metric.getDate()));
        payload.setTableName(metric.getTableName());
        payload.setSchemaName(metric.getSchemaName());
        payload.setValue(metric.getValue());
        return payload;
    }


    public ColumnMetricInfo[] getColumnMetrics(String namespace, String name) {
        return columnMetricRepository.getColumnMetrics(namespace, name);
    }

    public List<MonitoringConfig> getMonitoringConfig() {
        MonitoringRequest[] approvedRequests = requestRepository.findAllByStatusApproved();
        List<MonitoringConfig> result = new ArrayList<>();
        for (MonitoringRequest request: approvedRequests) {
            TimeSeriesConfig tsConfig = request.getDataSource().getTsConfig();
            MonitoringConfig monitoringConfig = new MonitoringConfig(request.getTableName(), request.getSchemaName());
            if (tsConfig != null && tsConfig.getDatePartitioned()) {
                monitoringConfig.setDayPartitionColumn(tsConfig.getDateColumn());
                monitoringConfig.setDayPartitionFormat(tsConfig.getDateFormat());
                monitoringConfig.setHourPartitionColumn(tsConfig.getHourColumn());
            }
            result.add(monitoringConfig);
        }
        return result;
    }


    public MetricPayload[] getMetrics(final DataCenter dc,
                               final String namespace, final String name,
                               final String column, final String metric,
                               final Date from, final Date to) {
        return metricRepository
            .getMetrics(Optional.of(dc.name().toLowerCase()), namespace, name,
                    Optional.of(column),
                    Optional.of(metric),
                    from, to, "")
            .stream().map(MetricsService::convert).toArray(MetricPayload[]::new);
    }

    public Optional<Integer> getAnyMetricId(final String namespace, final String name,
                                            final Date from, final Date to) {
        return metricRepository.getAnyMetricId(namespace,name, from, to);
    }

    public List<TableColumnMetricsConfig> getTableColumnMetricsConfig(String schema,
                                                                      String table) {
        return tableColumnMetricRepository.findByTableName(schema, table);
    }

    public TableColumnMetricsConfig addCategoricalPercentageMetric(final String schema,
                                                                   final String table,
                                                                   final String column,
                                                                   final String login,
                                                                   final TableColumnMetricConfig cfg) {
        final String metricFormat = "100*sum(if(`{0}`=='%s',1, 0))/count(`{0}`)";
        final String metricNameFormat = "`percentage_%s__s__{0}`";
        return addMetric(schema,table,column,metricFormat,metricNameFormat,login,cfg);
    }


    public TableColumnMetricsConfig addCategoricalCountMetric(final String schema,
                                                                   final String table,
                                                                   final String column,
                                                                   final String login,
                                                                   final TableColumnMetricConfig cfg) {
        final String metricFormat = "sum(if(`{0}`=='%s',1, 0))";
        final String metricNameFormat = "`count_%s__s__{0}`";
        return addMetric(schema,table,column,metricFormat,metricNameFormat, login, cfg);
    }

    private TableColumnMetricsConfig addMetric(final String schema,
                                                              final String table,
                                                              final String column,
                                                              final String metricFormat,
                                                              final String metricNameFormat,
                                                              final String login,
                                                              final TableColumnMetricConfig cfg) {
        return tableColumnMetricRepository.save(new TableColumnMetricsConfig(String.format(metricFormat, cfg.getValue()),
                String.format(metricNameFormat, cfg.getValue()),
                schema, table, column, login));
    }

}
