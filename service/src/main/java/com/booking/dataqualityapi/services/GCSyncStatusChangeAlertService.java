package com.booking.dataqualityapi.services;

import com.booking.alertapi.AlertAPI;
import com.booking.alertapi.Alerter;
import com.booking.systemsettings.SystemSettings;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class GCSyncStatusChangeAlertService {

    private static final String GC_SYNC_TAG = "gc_sync";

    public static final String GC_SYNC_ENABLED = "enabled";

    public static final String GC_SYNC_DISABLED = "disabled";

    private static final Logger LOGGER = LoggerFactory.getLogger(GCSyncStatusChangeAlertService.class);


    //alert_name is alerts which sends in format dataquality.gc_sync.<schema_name>.<table_name>.<status>
    private static final String DQ_ALERT_API_NAME_FORMAT = "dataquality.gc_sync.%s.%s.%s";
    private static final String DQ_TABLE_LINK_FORMAT = " Link to the table https://data.booking.com/Table/hive/%s/%s ";

    private AlertAPI alertAPI = new AlertAPI();

    public void send(final String namespace,
                     final String name,
                     final String tagName,
                     final String tagStatus,
                     final String userName) {

        if (!StringUtils.equals(GC_SYNC_TAG, tagName)) {
            return;
        }

        if (SystemSettings.DEV_OR_DQS_SERVER()) {
            LOGGER.info("GC_SYNC has been changed on dev server, alert isn't sent");
            return;
        }

        final String title = String.format(DQ_ALERT_API_NAME_FORMAT,
                namespace, name,
                tagStatus);

        final String body = String.format(" GC sync has been %s for table %s.%s by %s ",
                tagStatus, namespace, name, userName)
                + String.format(DQ_TABLE_LINK_FORMAT, namespace,
                name);

        if (GC_SYNC_ENABLED.equals(tagStatus)) {
            alertAPI.info(title,
                    body);
        } else {
            alertAPI.alert(title, Alerter.Severity.WARNING, body);
        }
    }
}
