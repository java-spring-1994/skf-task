package com.booking.dataqualityapi.services.partitions;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PartitionNamesBuilder {
    private final LocalDate from;
    private final LocalDate to;
    private final List<String> regionSorter;
    private DateTimeFormatter dateFormatter;
    private String datePartitionColumn;
    private String timePartitionColumn = "default_hh_null_column";
    private DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH");
    private String enumPartitionColumn;
    private List<String> enumValues = Arrays.asList("_empty_enum_value");
    private int hoursInc = 24;

    public static final String PARTITION_NAME_SEPARATOR = "/";

    public PartitionNamesBuilder(final LocalDate from,
                                 final LocalDate to,
                                 final List<String> regionSorter) {
        this.from = from;
        this.to = to;
        this.regionSorter = regionSorter;
    }

    public PartitionNamesBuilder withDateFormatter(final DateTimeFormatter dateFormatter) {
        this.dateFormatter = dateFormatter;
        return this;
    }

    public PartitionNamesBuilder withDatePartitionColumn(final String datePartitionColumn) {
        this.datePartitionColumn = datePartitionColumn;
        return this;
    }

    public PartitionNamesBuilder withTimePartitionColumn(final String timePartitionColumn) {
        this.timePartitionColumn = timePartitionColumn;
        if (StringUtils.isNotEmpty(timePartitionColumn)) {
            hoursInc = 1;
        }
        return this;
    }

    public PartitionNamesBuilder withTimeFormatter(final DateTimeFormatter timeFormatter) {
        this.timeFormatter = timeFormatter;
        return this;
    }

    public PartitionNamesBuilder withEnumPartitionColumn(final String enumPartitionColumn) {
        this.enumPartitionColumn = enumPartitionColumn;
        return this;
    }

    public PartitionNamesBuilder withEnumValues(List<String> enumValues) {
        this.enumValues = enumValues;
        return this;
    }

    public PartitionNamesBuilder withEnumValues(String[] enumValues) {
        if (ArrayUtils.isNotEmpty(enumValues)) {
            this.enumValues = Arrays.asList(enumValues);
        }
        return this;
    }

    public LocalDate getFrom() {
        return from;
    }

    public LocalDate getTo() {
        return to;
    }

    public List<Pair<String, LocalDateTime>> generatePossiblePartitions() {

        List<Pair<String, LocalDateTime>> result = new LinkedList<>();
        for (LocalDateTime dateTime = from.atStartOfDay(); dateTime.isBefore(to.atTime(23, 00))
                || dateTime.isEqual(to.atTime(23, 00));
             dateTime = dateTime.plusHours(hoursInc)) {
            final Map<String, String> partitionMap = new HashMap<>();
            partitionMap.put(datePartitionColumn, dateFormatter.format(dateTime));
            partitionMap.put(timePartitionColumn, timeFormatter.format(dateTime));
            for (String enumValue : enumValues) {
                partitionMap.put(enumPartitionColumn, enumValue);
                final String dateTimePartitionName = buildPartitionName(partitionMap, regionSorter);
                result.add(Pair.of(dateTimePartitionName, dateTime));
            }
        }
        return result;
    }

    private static String buildPartitionName(final Map<String, String> partColumnWithValues, final List<String> partRegions) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < partRegions.size(); i++) {
            String partRegion = partRegions.get(i);
            result.append(partRegion + "=" + partColumnWithValues.get(partRegion));
            if (i != partRegions.size() - 1) {
                result.append(PARTITION_NAME_SEPARATOR);
            }
        }
        return result.toString();
    }

}
