package com.booking.dataqualityapi.services;

import com.booking.dataquality.dto.notification.NotificationPayloadDTO;
import com.booking.dataquality.dto.notification.ThemeDTO;
import com.booking.dataqualityapi.dto.WikiTextPayload;
import com.booking.dataqualityapi.dto.dataentity.DataEntity;
import com.booking.dataqualityapi.dto.dataentity.DataEntityType;
import com.booking.dataqualityapi.model.DataSource;
import com.booking.dataqualityapi.model.DataSourceOwner;
import com.booking.dataqualityapi.model.WikiEntity;
import com.booking.dataqualityapi.model.WikiWorkflow;
import com.booking.dataqualityapi.repository.DataSourceRepository;
import com.booking.dataqualityapi.repository.WikiEntityRepository;
import com.booking.dataqualityapi.repository.WikiWorkflowRepository;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.ws.rs.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Wiki Operation Service.
 */

@Service
public class WikiService {

    private final WikiEntityRepository wikiRepository;
    private final DataSourceRepository entityRepository;
    private final NotificationsService notificationsService;
    private final WikiWorkflowRepository wikiWorkflowRepository;

    @Autowired
    public WikiService(final WikiEntityRepository wikiRepository,
                       final DataSourceRepository entityRepository,
                       final NotificationsService notificationsService, WikiWorkflowRepository wikiWorkflowRepository) {
        this.wikiRepository = wikiRepository;
        this.entityRepository = entityRepository;
        this.notificationsService = notificationsService;
        this.wikiWorkflowRepository = wikiWorkflowRepository;
    }

    public WikiEntity createOrUpdateWikiEntity(String type, String namespace, String name, String staffLoginName,
                                               final int authorStaffId,
                                               WikiTextPayload text) {
        final DataSource dataEntity
            = entityRepository.findOneByTypeAndAccountAndNamespaceAndName(type, DataEntity.DEFAULT_ENTITY_ACCOUNT, namespace, name);

        if (dataEntity == null) {
            throw new NotFoundException(String.format("Data entity hasn't been found for %s %s.%s",type, namespace, name));
        }
        final WikiEntity wikiEntity = wikiRepository.save(new WikiEntity(text.getText(), staffLoginName, dataEntity, new Date()));
        notificationsService.sendWikiUpdateNotification(getWikiRecipients(dataEntity, authorStaffId), " Wiki has been updated",
                convertToPayload(wikiEntity, staffLoginName));
        return wikiEntity;
    }

    private Set<Integer> getWikiRecipients(DataSource dataEntity, final int wikiUpdateAuthor) {
        final Set<Integer> wikiRecipients = new HashSet<>();
        wikiRecipients.addAll(dataEntity.watchers);
        wikiRecipients.addAll(dataEntity.getOwners().stream().map(DataSourceOwner::getStaffId).collect(Collectors.toSet()));
        wikiRecipients.remove(wikiUpdateAuthor);
        return wikiRecipients;
    }

    public WikiEntity getWikiEntityContent(String type, String namespace, String entity) {
        return wikiRepository.findTop1ByNamespaceAndTable(type, namespace, entity).stream().findAny().orElse(null);
    }


    public List<WikiEntity> getAllWikiEntityContent(String type, String namespace, String entity) {
        return wikiRepository.findAllByNamespaceAndTable(type, namespace, entity);
    }

    private static NotificationPayloadDTO convertToPayload(final WikiEntity wikiEntity, final String staffLoginName) {

        final ThemeDTO theme = new ThemeDTO();
        theme.setName("INFO");

        final NotificationPayloadDTO payload = new NotificationPayloadDTO();
        payload.setEntityType(wikiEntity.getEntity().getType());
        payload.setEntityNamespace(wikiEntity.getEntity().getNamespace());
        payload.setEntityName(wikiEntity.getEntity().getName());
        payload.setMessage(String.format("Wiki has been updated for %s.%s by %s", wikiEntity.getEntity().getNamespace(),
                wikiEntity.getEntity().getName(),
                staffLoginName));
        payload.setTheme(theme);

        return payload;
    }

    public WikiWorkflow getWikiWorkflowContent(String workflowName) {
        return wikiWorkflowRepository.findWikiByWorkFlowName(workflowName).stream().findFirst().orElse(null);
    }

    public WikiWorkflow createOrUpdateWikiWorkFlow(String workflowName, String staffLoginName, int toIntExact, WikiTextPayload text) {
        return wikiWorkflowRepository.save(new WikiWorkflow(text.getText(), staffLoginName, new Date(), workflowName));
    }

    public List<WikiWorkflow> getAllWikiWorkflowContent(String workflowName) {
        return wikiWorkflowRepository.findWikiByWorkFlowName(workflowName);
    }

    public Set<DataEntity> getAllDataEntitiesWithWiki() {
        return StreamSupport.stream(wikiRepository.findAll().spliterator(), false)
                .map(WikiEntity::getEntity)
                .map(e -> new DataEntity(e.getName(),
                        e.getNamespace(),
                        e.getAccount(),
                        DataEntityType.convertFromDbType(e.getType())))
                .collect(Collectors.toSet());
    }

}
