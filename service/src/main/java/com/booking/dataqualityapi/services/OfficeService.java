package com.booking.dataqualityapi.services;

import com.booking.dataqualityapi.model.office.Orgunit;
import com.booking.dataqualityapi.model.office.Staff;
import com.booking.dataqualityapi.repository.office.OrgunitRepository;
import com.booking.dataqualityapi.repository.office.StaffRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OfficeService {
    @Autowired
    StaffRepository staffRepository;

    @Autowired
    OrgunitRepository orgunitRepository;

    public Staff getOneStaffById(final int id) {
        return staffRepository.getById(id);
    }

    public Staff getOneStaffByLoginName(final String loginName) {
        return staffRepository.getByLoginName(loginName);
    }

    public List<Orgunit> getAllActiveOrgunits() {
        return orgunitRepository.getAllActive();
    }

    public List<Orgunit> getAllActiveTeams() {
        return orgunitRepository.getAllActiveTeams();
    }
}
