package com.booking.dataqualityapi.services;

import com.booking.dataquality.utils.SqlFunction;
import com.booking.dataqualityapi.dto.EntityDependencyInfo;
import com.booking.dataqualityapi.dto.OozieWorkflowInstanceInfo;
import com.booking.dataqualityapi.model.workflow.OozieWorkflowSlo;
import com.booking.dataqualityapi.dto.workflow.WorkflowQueryInfo;
import com.booking.dataqualityapi.model.DQWorkflowTableMapping;
import com.booking.dataqualityapi.model.HiveTableDependency;
import com.booking.dataqualityapi.model.workflow.WorkflowQuery;
import com.booking.dataqualityapi.repository.DQWorkflowTableRepositoryImpl;
import com.booking.dataqualityapi.repository.HiveTableDependencyRepositoryImpl;
import com.booking.dataqualityapi.repository.workflow.WorkflowQueryRepository;
import com.booking.dataqualityapi.utils.DatacenterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Date;
import java.util.stream.Collectors;

@Service
public class EntityDependencyService {

    private static final Logger logger = LoggerFactory.getLogger(EntityDependencyService.class);

    private DataSource dqDataSourceRo;

    @Autowired
    private OozieApiService oozieApiService;

    private final HiveTableDependencyRepositoryImpl dependencyRepository;
    private final DQWorkflowTableRepositoryImpl workflowTableRepository;
    private final WorkflowQueryRepository queryRepository;


    private static final String GET_WORKFLOW_FOR_TABLE =
            "  SELECT "
                    + "         entity_name, entity_namespace, dc, workflow_name "
                    + "     FROM "
                    + "         dq_workflow_output_table_mapping "
                    + "     WHERE "
                    + "         mysql_row_status = 'ACTIVE' AND dc = ? AND entity_name = ? AND entity_namespace = ? "
                    + "     ORDER BY "
                    + "         mysql_row_updated_at DESC"
                    + "      LIMIT 1 ";

    private static final String GET_DOWNSTREAM_WORKFLOW_DEPENDENCIES_FOR_ENTITY =
            " SELECT  "
                    + "      workflow_name AS workflow_name,  "
                    + "      GROUP_CONCAT(CONCAT_WS('.',entity_namespace, entity_name)) AS entities,  "
                    + "      MAX(mysql_row_updated_at) as wf_last_used "
                    + " FROM ( "
                    + "      SELECT "
                    + "           im.workflow_name,  "
                    + "           om.entity_namespace,  "
                    + "           om.entity_name,  "
                    + "           im.mysql_row_updated_at "
                    + "      FROM  "
                    + "           dq_workflow_input_table_mapping im  "
                    + "      LEFT OUTER JOIN "
                    + "         dq_workflow_output_table_mapping om  "
                    + "      ON  "
                    + "           im.workflow_name = om.workflow_name AND im.dc = om.dc  "
                    + "      WHERE "
                    + "           im.dc = ? AND im.entity_namespace = ? AND im.entity_name = ? AND im.mysql_row_status = 'ACTIVE'  "
                    + "           AND om.workflow_name IS NOT NULL "
                    + " ) downstream  "
                    + " GROUP BY  1 "
                    + " ORDER BY 3 desc "
                    + " LIMIT 500 ";


    private static final String GET_DOWNSTREAM_WORKFLOW_DEPENDENCIES_FOR_WORKFLOW_SINCE_LAST_USED =
            " SELECT  "
                    + "     im.workflow_name,  "
                    + "     im.workflow_id,  "
                    + "     group_concat(concat_ws('.',om2.entity_namespace, om2.entity_name)) as entities, "
                    + "     MAX(im.mysql_row_updated_at) as wf_last_used "
                    + " FROM "
                    + "     dq_workflow_output_table_mapping om "
                    + " LEFT OUTER JOIN "
                    + "     dq_workflow_input_table_mapping im  "
                    + " ON  "
                    + "     om.entity_name = im.entity_name AND om.entity_namespace = im.entity_namespace "
                    + "     AND om.dc = im.dc and om.workflow_name != im.workflow_name "
                    + " LEFT OUTER JOIN "
                    + "     dq_workflow_output_table_mapping om2 "
                    + " ON  "
                    + "     im.workflow_name = om2.workflow_name and im.dc = om2.dc "
                    + " WHERE  "
                    + "     im.mysql_row_status = 'ACTIVE' AND om.mysql_row_status = 'ACTIVE' AND om2.mysql_row_status = 'ACTIVE' "
                    + "     AND om.dc = ? AND om.workflow_name = ?  AND im.mysql_row_updated_at > ? "
                    + " GROUP BY 1";

    private static final String GET_DOWNSTREAM_WORKFLOW_DEPENDENCIES_FOR_WORKFLOW =
            " SELECT  "
                    + "     im.workflow_name,  "
                    + "     im.workflow_id,  "
                    + "     group_concat(concat_ws('.',om2.entity_namespace, om2.entity_name)) as entities, "
                    + "     MAX(im.mysql_row_updated_at) as wf_last_used "
                    + " FROM "
                    + "     dq_workflow_output_table_mapping om "
                    + " LEFT OUTER JOIN "
                    + "     dq_workflow_input_table_mapping im  "
                    + " ON  "
                    + "     om.entity_name = im.entity_name AND om.entity_namespace = im.entity_namespace "
                    + "     AND om.dc = im.dc and om.workflow_name != im.workflow_name "
                    + " LEFT OUTER JOIN "
                    + "     dq_workflow_output_table_mapping om2 "
                    + " ON  "
                    + "     im.workflow_name = om2.workflow_name and im.dc = om2.dc "
                    + " WHERE  "
                    + "     im.mysql_row_status = 'ACTIVE' AND om.mysql_row_status = 'ACTIVE' AND om2.mysql_row_status = 'ACTIVE' "
                    + "     AND om.dc = ? AND om.workflow_name = ? "
                    + " GROUP BY 1";


    private static final String GET_UPSTREAM_WORKFLOW_DEPENDENCIES =
            "SELECT"
                    + " om2.workflow_name AS workflow_name,    "
                    + "        wi.workflow_id,    "
                    + "        wi.coord_action_status as workflow_status,    "
                    + "        wi.coord_id,    "
                    + "        wi.start_time,    "
                    + "        wi.end_time,    "
                    + "        wi.nominal_time,    "
                    + "        wi.missing_dependencies,    "
                    + "        GROUP_CONCAT(CONCAT_WS('.', im.entity_namespace, im.entity_name)) AS entities,    "
                    + "        MAX(im.mysql_row_updated_at) AS wf_last_used    "
                    + " FROM (select * from dq_workflow_input_table_mapping where dc = ?    "
                    + "   AND workflow_name = ?    "
                    + "   AND mysql_row_status = 'ACTIVE' ) im    "
                    + " LEFT OUTER JOIN    "
                    + "   (SELECT base.entity_namespace,    "
                    + "           base.entity_name,    "
                    + "           base.workflow_name,    "
                    + "           base.mysql_row_updated_at    "
                    + "    FROM dq_workflow_output_table_mapping base  "
                    + "    LEFT JOIN  "
                    + "    (  "
                    + "       select entity_namespace, entity_name, dc, max(mysql_row_updated_at) as mysql_row_updated_at   "
                    + "       FROM dq_workflow_output_table_mapping WHERE mysql_row_status = 'ACTIVE' and dc= ? GROUP by 1,2,3  "
                    + "    ) latest  "
                    + "    on base.entity_namespace = latest.entity_namespace and base.entity_name = latest.entity_name and "
                    + "    base.mysql_row_updated_at = latest.mysql_row_updated_at  "
                    + "    and base.dc = latest.dc  "
                    + "    WHERE base.mysql_row_status = 'ACTIVE' and base.dc= ? and latest.mysql_row_updated_at is not null) om2 ON    "
                    + "    im.entity_namespace = om2.entity_namespace    "
                    + " AND im.entity_name = om2.entity_name    "
                    + " AND om2.workflow_name != ?    "
                    + " LEFT JOIN    "
                    + "   (SELECT coord_action_id,    "
                    + "           workflow_id,    "
                    + "           base.coord_name,    "
                    + "           coord_action_status,    "
                    + "           coord_id,    "
                    + "           start_time,    "
                    + "           end_time,    "
                    + "           base.nominal_time,    "
                    + "           missing_dependencies    "
                    + "    FROM dq_workflow_coord_action_info  base  "
                    + "    left join (  "
                    + "    select coord_name, max(nominal_time) as nominal_time from  dq_workflow_coord_action_info WHERE dc = ? group by 1  "
                    + "    ) latest  "
                    + "    on latest.coord_name = base.coord_name and latest.nominal_time = base.nominal_time  "
                    + "    WHERE dc = ? and latest.nominal_time is not null) wi ON wi.coord_name = om2.workflow_name    "
                    + " WHERE om2.workflow_name IS NOT NULL    "
                    + " GROUP BY 1    "
                    + " ORDER BY wf_last_used DESC ";

    private static final String GET_CURRENT_WORKFLOW_DETAILS =
            "SELECT "
                    + "     a.coord_action_id, "
                    + "     a.workflow_id, "
                    + "     a.coord_name AS workflow_name, "
                    + "     a.coord_action_status AS workflow_status, "
                    + "     a.coord_id, "
                    + "     a.start_time, "
                    + "     a.end_time, "
                    + "     a.nominal_time, "
                    + "     a.missing_dependencies "
                    + " FROM "
                    + "     dq_workflow_coord_action_info a "
                    + " WHERE "
                    + "     a.dc = ? AND a.coord_name = ? ORDER BY a.nominal_time DESC limit ?";

    private static final String GET_WORKFLOW_SLO =
            " SELECT "
                    + " a.start_time as start_time, "
                    + " a.end_time as end_time, "
                    + " a.nominal_time as nominal_time "
                    + " FROM "
                    + " dq_workflow_coord_action_info a "
                    + " WHERE "
                    + " a.dc = ? AND a.coord_name = ? "
                    + " and a.coord_action_status= 'SUCCEEDED' "
                    + " and TIMESTAMPDIFF(MINUTE, a.nominal_time,a.start_time) < 24*60 "
                    + " ORDER BY a.nominal_time DESC limit ?; ";

    private static final String GET_WORKFLOW_BY_ID =
            "SELECT "
                + "          a.coord_action_id, "
                + "          a.workflow_id, "
                + "          a.coord_name AS workflow_name, "
                + "          a.coord_action_status AS workflow_status, "
                + "          a.coord_id, "
                + "          a.start_time, "
                + "          a.end_time, "
                + "          a.nominal_time, "
                + "          a.missing_dependencies "
                + "      FROM "
                + "          dq_workflow_coord_action_info a "
                + "      WHERE "
                + "          a.workflow_id = ? ";

    @Autowired
    public EntityDependencyService(
            final HiveTableDependencyRepositoryImpl dependencyRepository,
            final DQWorkflowTableRepositoryImpl workflowTableRepository,
            final WorkflowQueryRepository queryRepository,
            @Qualifier("getMysqlDqConnectionRo") DataSource dqDataSourceRo
    ) {
        this.dqDataSourceRo = dqDataSourceRo;
        this.dependencyRepository = dependencyRepository;
        this.workflowTableRepository = workflowTableRepository;
        this.queryRepository = queryRepository;
    }

    /**
     * Return input table process dependencies.
     *
     * @param id - return query by Id
     * @return - list of table from which table depends
     */
    public WorkflowQueryInfo getWorkflowQuery(final Integer id) {
        WorkflowQuery wfQuery = queryRepository.getQueryById(id);
        if (wfQuery == null) {
            return null;
        }

        WorkflowQueryInfo wfQueryInfo = new WorkflowQueryInfo();

        wfQueryInfo.setDc(wfQuery.getDc());
        wfQueryInfo.setId(wfQuery.getId());
        wfQueryInfo.setJobInfo(wfQuery.getJobInfo());
        wfQueryInfo.setName(wfQuery.getName());
        wfQueryInfo.setQuery(wfQuery.getQuery());
        wfQueryInfo.setCreateTime(wfQuery.getCreateTime());

        wfQueryInfo.setCoordinatorLink(getCoordinatorLink(wfQuery.getDc(), wfQuery.getJobInfo()));
        wfQueryInfo.setWorkflowLink(getWorkflowLink(wfQuery.getDc(), wfQuery.getJobInfo()));
        wfQueryInfo.setQueryScriptLink(getScriptLink(wfQuery.getDc(), wfQuery.getJobInfo()));

        return wfQueryInfo;
    }

    /**
     * Return input table process dependencies.
     *
     * @param dc        - datacenter
     * @param namespace - schema
     * @param name      - table name
     * @return - list of table from which table depends
     */
    public List<HiveTableDependency> findTableDependsFrom(final String dc,
                                                          final String namespace,
                                                          final String name) {
        return dependencyRepository.inputTables(dc, namespace, name);
    }

    /**
     * Return input table process dependencies.
     *
     * @param dc        - datacenter
     * @param namespace - schema
     * @param name      - table name
     * @return - list of tables which depends from table
     */
    public List<HiveTableDependency> findTableUsedIn(final String dc,
                                                     final String namespace,
                                                     final String name) {
        return dependencyRepository.outputTables(dc, namespace, name);
    }

    /**
     * Return the HUE link to the script of the workflow action in the jobInfo.
     *
     * @param dc      - datacenter
     * @param jobInfo - job info of the workflow action
     * @return - a HUE link to the script executed by this workflow action
     */
    public String getScriptLink(String dc, String jobInfo) {
        try {
            return oozieApiService.getScriptLinkForWorkflowAction(DatacenterUtils.Datacenter.valueOf(dc.toUpperCase()), jobInfo);
        } catch (Exception exc) {
            return null;
        }
    }

    /**
     * Return the BOOZIE link to of the workflow in the jobInfo.
     *
     * @param dc      - datacenter
     * @param jobInfo - job info of the workflow action
     * @return - a BOOZIE link to the workflow
     */
    public String getWorkflowLink(String dc, String jobInfo) {
        return oozieApiService.getWorkflowLink(DatacenterUtils.Datacenter.valueOf(dc.toUpperCase()), jobInfo);
    }

    /**
     * Return the BOOZIE link to of the coordinator in the jobInfo.
     *
     * @param dc      - datacenter
     * @param jobInfo - job info of the coordinator action
     * @return - a BOOZIE link to the coordinator
     */
    public String getCoordinatorLink(String dc, String jobInfo) {
        return oozieApiService.getCoordinatorLink(DatacenterUtils.Datacenter.valueOf(dc.toUpperCase()), jobInfo);
    }

    public EntityDependencyInfo getDependencyInfoForWorkflow(final String dc, final String workflowName, Date dependencyLastUsed) {
        EntityDependencyInfo deps = new EntityDependencyInfo();
        deps.setWorkflowName(workflowName);
        logger.error("dependencyLastUsed {}", dependencyLastUsed.toString());
        deps.setTables(
                getWorkflowOutputTables(dc, workflowName)
                        .stream()
                        .map(x -> x.getEntityNamespace() + "." + x.getEntityName())
                        .collect(Collectors.toList())
        );

        OozieWorkflowInstanceInfo oozieWorkflowInstanceInfo = getCurrentWorkflowInstanceInfo(dc, workflowName,1)
                .stream()
                .findFirst()
                .orElse(new OozieWorkflowInstanceInfo());
        deps.setWorkflowId(oozieWorkflowInstanceInfo.getWorkflowId());
        deps.setWorkflowStatus(oozieWorkflowInstanceInfo.getWorkflowStatus());
        deps.setWorkflowStartTime(oozieWorkflowInstanceInfo.getWorkflowStartTime());
        deps.setWorkflowEndTime(oozieWorkflowInstanceInfo.getWorkflowEndTime());
        deps.setWorkflowNominalTime(oozieWorkflowInstanceInfo.getNominalDate());
        deps.setWorkflowCoordId(oozieWorkflowInstanceInfo.getCoordId());
        deps.getUpstream().addAll(getUpstreamDependencyInfo(dc, workflowName));
        deps.getDownstream().addAll(getDownstreamDependencyInfoForWorkflow(dc, workflowName, dependencyLastUsed).getDownstream());
        deps.setMissingDependencies(oozieWorkflowInstanceInfo.getMissingDependencies());
        return deps;
    }


    public EntityDependencyInfo getDependencyInfoForWorkflow(final String dc, final String workflowName) {
        EntityDependencyInfo deps = new EntityDependencyInfo();
        deps.setWorkflowName(workflowName);

        deps.setTables(
                getWorkflowOutputTables(dc, workflowName)
                        .stream()
                        .map(x -> x.getEntityNamespace() + "." + x.getEntityName())
                        .collect(Collectors.toList())
        );

        OozieWorkflowInstanceInfo oozieWorkflowInstanceInfo = getCurrentWorkflowInstanceInfo(dc, workflowName,1)
                .stream()
                .findFirst()
                .orElse(new OozieWorkflowInstanceInfo());
        deps.setWorkflowId(oozieWorkflowInstanceInfo.getWorkflowId());
        deps.setWorkflowStatus(oozieWorkflowInstanceInfo.getWorkflowStatus());
        deps.setWorkflowStartTime(oozieWorkflowInstanceInfo.getWorkflowStartTime());
        deps.setWorkflowEndTime(oozieWorkflowInstanceInfo.getWorkflowEndTime());
        deps.setWorkflowNominalTime(oozieWorkflowInstanceInfo.getNominalDate());
        deps.setWorkflowCoordId(oozieWorkflowInstanceInfo.getCoordId());
        deps.getUpstream().addAll(getUpstreamDependencyInfo(dc, workflowName));
        deps.getDownstream().addAll(getDownstreamDependencyInfoForWorkflow(dc, workflowName).getDownstream());
        deps.setMissingDependencies(oozieWorkflowInstanceInfo.getMissingDependencies());
        return deps;
    }

    public EntityDependencyInfo getDependencyInfoForEntity(final String dc, final String namespace, final String name) {

        EntityDependencyInfo deps = new EntityDependencyInfo();
        String wfName = getWorkflowForTable(dc, namespace, name);
        if (wfName != null) {
            deps = getDependencyInfoForWorkflow(dc, wfName);
            deps.setTables(Collections.singletonList(namespace + "." + name));
            return deps;
        }
        deps.getDownstream().addAll(getDownstreamDependencyInfoForEntity(dc, namespace, name));
        return deps;
    }

    public EntityDependencyInfo getDependencyInfoForWorkFlowId(final String workflowId) {

        EntityDependencyInfo dep = new EntityDependencyInfo();
        SqlFunction<Connection, PreparedStatement> getDependencyByIdQuery = c -> {
            final PreparedStatement statement = c.prepareStatement(GET_WORKFLOW_BY_ID);
            statement.setString(1, workflowId);
            return statement;
        };
        try (
                Connection connection = dqDataSourceRo.getConnection();
                PreparedStatement statement = getDependencyByIdQuery.invoke(connection);
                ResultSet result = statement.executeQuery()
        ) {
            while (result.next()) {
                dep.setWorkflowId(result.getString("workflow_id"));
                dep.setWorkflowName(result.getString("workflow_name"));
                dep.setWorkflowStatus(result.getString("workflow_status"));
                dep.setWorkflowStartTime(result.getString("start_time"));
                dep.setWorkflowEndTime(result.getString("end_time"));
                dep.setWorkflowNominalTime(result.getString("nominal_time"));
                dep.setWorkflowCoordId(result.getString("coord_action_id"));
                dep.setMissingDependencies(result.getString("missing_dependencies"));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return dep;
    }


    public String getWorkflowForTable(final String dc, final String namespace, final String name) {
        SqlFunction<Connection, PreparedStatement> getWfNameQuery = c -> {
            final PreparedStatement statement = c.prepareStatement(GET_WORKFLOW_FOR_TABLE);
            statement.setString(1, dc);
            statement.setString(2, name);
            statement.setString(3, namespace);
            return statement;
        };

        try (
                Connection connection = dqDataSourceRo.getConnection();
                PreparedStatement statement = getWfNameQuery.invoke(connection);
                ResultSet result = statement.executeQuery()
        ) {
            if (result.next()) {
                return result.getString("workflow_name");
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }

        return null;
    }

    public List<OozieWorkflowInstanceInfo> getCurrentWorkflowInstanceInfo(String dc, String workflowName, int size) {

        SqlFunction<Connection, PreparedStatement> getWfDetailsQuery = c -> {
            final PreparedStatement statement = c.prepareStatement(GET_CURRENT_WORKFLOW_DETAILS);
            statement.setString(1, dc);
            statement.setString(2, workflowName);
            statement.setInt(3, size);
            return statement;
        };
        List<OozieWorkflowInstanceInfo> workflowInstanceInfos = new LinkedList<>();

        try (
                Connection connection = dqDataSourceRo.getConnection();
                PreparedStatement statement = getWfDetailsQuery.invoke(connection);
                ResultSet result = statement.executeQuery()
        ) {
            while (result.next()) {
                OozieWorkflowInstanceInfo oozieWorkflowInstanceInfo = new OozieWorkflowInstanceInfo();
                oozieWorkflowInstanceInfo.setWorkflowId(result.getString("workflow_id"));
                oozieWorkflowInstanceInfo.setNominalDate(result.getString("nominal_time"));
                oozieWorkflowInstanceInfo.setWorkflowStartTime(result.getString("start_time"));
                oozieWorkflowInstanceInfo.setWorkflowEndTime(result.getString("end_time"));
                oozieWorkflowInstanceInfo.setWorkflowStatus(result.getString("workflow_status"));
                oozieWorkflowInstanceInfo.setCoordId(result.getString("coord_id"));
                oozieWorkflowInstanceInfo.setMissingDependencies(result.getString("missing_dependencies"));
                workflowInstanceInfos.add(oozieWorkflowInstanceInfo);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return workflowInstanceInfos;
    }

    public List<OozieWorkflowSlo> getWorkflowSlo(final String dc,
                                                 final String workflowName,
                                                 final int size) {
        SqlFunction<Connection, PreparedStatement> getWfSlosQuery = c -> {
            final PreparedStatement statement = c.prepareStatement(GET_WORKFLOW_SLO);
            statement.setString(1, dc);
            statement.setString(2, workflowName);
            statement.setInt(3, size);
            return statement;
        };
        List<OozieWorkflowSlo> workflowSlos = new LinkedList<>();
        try (
                Connection connection = dqDataSourceRo.getConnection();
                PreparedStatement statement = getWfSlosQuery.invoke(connection);
                ResultSet result = statement.executeQuery()
        ) {
            while (result.next()) {
                OozieWorkflowSlo workflowSloInfo = new OozieWorkflowSlo();
                workflowSloInfo.setStartTime(result.getTimestamp("start_time"));
                workflowSloInfo.setEndTime(result.getTimestamp("end_time"));
                workflowSloInfo.setNominalTime(result.getTimestamp("nominal_time"));
                workflowSlos.add(workflowSloInfo);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return workflowSlos;
    }

    private List<EntityDependencyInfo> getUpstreamDependencyInfo(final String dc,
                                                                 final String workflowName) {
        List<EntityDependencyInfo> upstreamDeps = new ArrayList<>();
        SqlFunction<Connection, PreparedStatement> getUpstreamDepsQuery = c -> {
            final PreparedStatement statement1 = c.prepareStatement(GET_UPSTREAM_WORKFLOW_DEPENDENCIES);
            statement1.setString(1, dc);
            statement1.setString(2, workflowName);
            statement1.setString(3, dc);
            statement1.setString(4, dc);
            statement1.setString(5, workflowName);
            statement1.setString(6, dc);
            statement1.setString(7, dc);
            return statement1;
        };

        try (
                Connection connection1 = dqDataSourceRo.getConnection();
                PreparedStatement statement1 = getUpstreamDepsQuery.invoke(connection1);
                ResultSet result1 = statement1.executeQuery()
        ) {
            while (result1.next()) {
                EntityDependencyInfo upstreamDep = new EntityDependencyInfo(result1.getString("workflow_name"));
                upstreamDep.setWorkflowId(result1.getString("workflow_id"));
                upstreamDep.setWorkflowStatus(result1.getString("workflow_status"));
                upstreamDep.setWorkflowCoordId(result1.getString("coord_id"));
                upstreamDep.setWorkflowStartTime(result1.getString("start_time"));
                upstreamDep.setWorkflowEndTime(result1.getString("end_time"));
                upstreamDep.setWorkflowNominalTime(result1.getString("nominal_time"));
                upstreamDep.setMissingDependencies(result1.getString("missing_dependencies"));
                upstreamDep.setDependencyLastUsed(result1.getTimestamp("wf_last_used"));
                upstreamDep.setTables(Arrays.asList(result1.getString("entities").split(",")));
                upstreamDeps.add(upstreamDep);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return upstreamDeps;
    }

    private List<EntityDependencyInfo> getDownstreamDependencyInfoForEntity(final String dc, final String namespace, final String name) {
        List<EntityDependencyInfo> downstreamDeps = new ArrayList<>();
        SqlFunction<Connection, PreparedStatement> getDownstreamDepsQuery = c -> {
            final PreparedStatement statement1 = c.prepareStatement(GET_DOWNSTREAM_WORKFLOW_DEPENDENCIES_FOR_ENTITY);
            statement1.setString(1, dc);
            statement1.setString(2, namespace);
            statement1.setString(3, name);
            return statement1;
        };

        try (
                Connection connection1 = dqDataSourceRo.getConnection();
                PreparedStatement statement1 = getDownstreamDepsQuery.invoke(connection1);
                ResultSet result1 = statement1.executeQuery()
        ) {
            while (result1.next()) {
                EntityDependencyInfo downstreamDep = new EntityDependencyInfo(result1.getString("workflow_name"));
                downstreamDep.setDependencyLastUsed(result1.getTimestamp("wf_last_used"));
                downstreamDep.setTables(Arrays.asList(result1.getString("entities").split(",")));
                downstreamDeps.add(downstreamDep);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return downstreamDeps;
    }

    private EntityDependencyInfo getDownstreamDependencyInfoForWorkflow(final String dc, final String workflowName, Date dependencyLastUsed) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = dateFormat.format(dependencyLastUsed);
        logger.error("date format {}", strDate);
        SqlFunction<Connection, PreparedStatement> getDownstreamDepsQuery = c -> {
            final PreparedStatement statement1 = c.prepareStatement(GET_DOWNSTREAM_WORKFLOW_DEPENDENCIES_FOR_WORKFLOW_SINCE_LAST_USED);
            statement1.setString(1, dc);
            statement1.setString(2, workflowName);
            statement1.setString(3, strDate);
            return statement1;
        };
        return getDownstreamDependencyInfoForWorkflowFromDownStreamQuery(workflowName, getDownstreamDepsQuery);
    }

    private EntityDependencyInfo getDownstreamDependencyInfoForWorkflow(final String dc, final String workflowName) {
        SqlFunction<Connection, PreparedStatement> getDownstreamDepsQuery = c -> {
            final PreparedStatement statement1 = c.prepareStatement(GET_DOWNSTREAM_WORKFLOW_DEPENDENCIES_FOR_WORKFLOW);
            statement1.setString(1, dc);
            statement1.setString(2, workflowName);
            return statement1;
        };
        return getDownstreamDependencyInfoForWorkflowFromDownStreamQuery(workflowName, getDownstreamDepsQuery);
    }

    private EntityDependencyInfo getDownstreamDependencyInfoForWorkflowFromDownStreamQuery(
            final String workflowName, SqlFunction<Connection, PreparedStatement> getDownstreamDepsQuery) {
        EntityDependencyInfo deps = new EntityDependencyInfo();
        deps.setWorkflowName(workflowName);
        try (
                Connection connection1 = dqDataSourceRo.getConnection();
                PreparedStatement statement1 = getDownstreamDepsQuery.invoke(connection1);
                ResultSet result1 = statement1.executeQuery()
        ) {
            while (result1.next()) {
                EntityDependencyInfo downstreamDep = new EntityDependencyInfo(result1.getString("workflow_name"));
                downstreamDep.setDependencyLastUsed(result1.getTimestamp("wf_last_used"));
                downstreamDep.setWorkflowId(result1.getString("workflow_id"));
                downstreamDep.setTables(Arrays.asList(result1.getString("entities").split(",")));
                deps.getDownstream().add(downstreamDep);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return deps;
    }

    public List<DQWorkflowTableMapping> getWorkflowInputTables(final String dc, final String workflowName) {
        logger.info("getWorkflowInputTables({},{})", dc, workflowName);
        return workflowTableRepository.inputTables(dc, workflowName);
    }

    public List<DQWorkflowTableMapping> getWorkflowInputTables(
            final String dc, final String workflowName, final int updatedDaysAgo
    ) {
        logger.info("getWorkflowInputTables({},{},{})", dc, workflowName, updatedDaysAgo);
        return workflowTableRepository.inputTablesLastDays(dc, workflowName, updatedDaysAgo);
    }

    public List<DQWorkflowTableMapping> getWorkflowOutputTables(final String dc, final String workflowName) {
        logger.info("getWorkflowOutputTables({},{})", dc, workflowName);
        return workflowTableRepository.outputTables(dc, workflowName);
    }

    public List<DQWorkflowTableMapping> getWorkflowOutputTables(
            final String dc, final String workflowName, final int updatedDaysAgo
    ) {
        logger.info("getWorkflowOutputTables({},{},{})", dc, workflowName, updatedDaysAgo);
        return workflowTableRepository.outputTablesLastDays(dc, workflowName, updatedDaysAgo);
    }
}
