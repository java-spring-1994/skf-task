package com.booking.dataqualityapi.services.ownership;

import com.booking.authxclient.exceptions.AuthXClientException;
import com.booking.dataqualityapi.dto.dataentity.DataEntity;
import com.booking.dataqualityapi.dto.dataentity.DataEntityType;
import com.booking.dataqualityapi.dto.dataentity.SchemaEntity;
import com.booking.dataqualityapi.dto.ownership.dq.DataEntityOwnershipInfo;
import com.booking.dataqualityapi.dto.ownership.dq.DataEntityOwnershipInfoDump;
import com.booking.dataqualityapi.dto.ownership.dq.OwnershipMeta;
import com.booking.dataqualityapi.dto.ownership.dq.SchemaEntityOwnershipInfo;
import com.booking.dataqualityapi.dto.ownership.dq.SchemaEntityOwnershipInfoDump;
import com.booking.dataqualityapi.dto.ownership.dq.WorkflowOwnershipInfo;
import com.booking.dataqualityapi.dto.ownership.original.Ownable;
import com.booking.dataqualityapi.dto.ownership.original.OwnableNameType;
import com.booking.dataqualityapi.dto.ownership.original.OwnableReassignmentInfo;
import com.booking.dataqualityapi.dto.ownership.original.OwnableRegistrationInfo;
import com.booking.dataqualityapi.dto.ownership.original.OwnablesToRetrieve;
import com.booking.dataqualityapi.dto.workflow.WorkflowEntity;
import com.booking.dataqualityapi.dto.workflow.WorkflowTypeLowerCase;
import com.booking.dataqualityapi.model.ownership.SchemaPersonalOwner;
import com.booking.dataqualityapi.repository.ownership.SchemaPersonalOwnershipRepository;
import com.booking.dataqualityapi.services.OfficeService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class OwnershipService {

    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory.getLogger(OwnershipService.class);

    private final OwnershipClient ownershipClient;

    private static final String DATASOURCE_OWNABLE_TYPE = "datasource";
    private static final String DATASCHEMA_OWNABLE_TYPE = "dataschema";
    private static final String WORKFLOW_OWNABLE_TYPE = "workflow";

    @Autowired
    private SchemaPersonalOwnershipRepository schemaPersonalOwnershipRepository;

    @Autowired
    private OfficeService officeService;

    @Autowired
    public OwnershipService(final ObjectMapper jacksonObjectMapper) {
        ownershipClient = OwnershipClient.getInstance(jacksonObjectMapper);
    }

    private static String buildOwnableNameForTable(
        final String entityType, final String entityAccount, final String entityNamespace, final String entityName
    ) {
        if (entityAccount == null) {
            return String.format("%s.%s.%s", entityType, entityNamespace, entityName);
        } else {
            return String.format("%s.%s.%s.%s", entityType, entityAccount, entityNamespace, entityName);
        }
    }

    private static String buildOwnableNameForSchema(final String entityType, final String entityNamespace) {
        return String.format("%s.%s", entityType, entityNamespace);
    }

    private static String buildOwnableNameForWorkflow(final WorkflowTypeLowerCase workflowTypeLowerCase, final String workflowName) {
        return String.format("%s.%s", workflowTypeLowerCase, workflowName);
    }

    private static DataEntity buildDataEntityFromOwnableName(final String ownableName) {
        String[] parts = ownableName.split("\\.");
        // Ownable name can contain account name, otherwise we use default
        if (parts.length == 3) {
            return new DataEntity(parts[2], parts[1], DataEntity.DEFAULT_ENTITY_ACCOUNT, DataEntityType.valueOf(parts[0].toUpperCase()));
        } else if (parts.length == 4) {
            return new DataEntity(parts[3], parts[2], parts[1], DataEntityType.valueOf(parts[0].toUpperCase()));
        } else {
            throw new RuntimeException("Invalid ownable name: " + ownableName);
        }
    }

    private static SchemaEntity buildSchemaEntityFromOwnableName(final String ownableName) {
        String[] parts = ownableName.split("\\.");
        if (parts.length != 2) {
            throw new RuntimeException("Invalid ownable name: " + ownableName);
        }
        return new SchemaEntity(parts[1], DataEntity.DEFAULT_ENTITY_ACCOUNT, DataEntityType.valueOf(parts[0].toUpperCase()));
    }

    static WorkflowEntity buildWorkflowEntityFromOwnableName(final String ownableName) {
        String[] parts = ownableName.split("\\.", 2);
        if (parts.length != 2) {
            throw new RuntimeException("Invalid ownable name: " + ownableName);
        }
        return new WorkflowEntity(WorkflowTypeLowerCase.valueOf(parts[0]), parts[1]);
    }

    private OwnershipMeta getOwnableInfo(final String ownableName, final String ownableType)
        throws AuthXClientException, InterruptedException, IOException {
        JsonNode jsonResponse = ownershipClient.retrieveOwnable(ownableName, ownableType);

        if (jsonResponse == null || jsonResponse.get("is_archived").asInt() == 1) {
            return null;
        }

        JsonNode ownerNode = jsonResponse.get("owner");

        return new OwnershipMeta(
            ownerNode.get("orgunit_id").asLong(),
            ownerNode.get("name").asText(),
            ownerNode.get("changed_by_staff_id").asInt(),
            ownerNode.get("reason").asText()
        );
    }

    // todo list is not needed
    public List<DataEntityOwnershipInfo> getOwnershipInfo(final String entityType, final String entityNamespace, final String entityName)
        throws AuthXClientException, InterruptedException, IOException {

        final String ownableName = buildOwnableNameForTable(entityType, null, entityNamespace, entityName);

        OwnershipMeta ownershipMeta = getOwnableInfo(
            ownableName,
            DATASOURCE_OWNABLE_TYPE
        );

        if (ownershipMeta == null) {
            return null;
        }

        List<DataEntityOwnershipInfo> result = new ArrayList<>();

        result.add(new DataEntityOwnershipInfo(
            buildDataEntityFromOwnableName(ownableName),
            ownershipMeta
        ));

        return result;
    }

    /**
     * Aggregated ownership logic: either personal or team.
     */
    public SchemaEntityOwnershipInfo getSchemaOwnershipInfo(
        final String entityType, final String entityNamespace)
        throws AuthXClientException, InterruptedException, IOException {
        SchemaEntityOwnershipInfo result = getPersonalSchemaOwnershipInfo(entityType, entityNamespace);
        if (result == null) {
            List<SchemaEntityOwnershipInfo> teamSchemaOwnershipInfo = getTeamSchemaOwnershipInfo(entityType, entityNamespace);
            // todo use single result
            if (teamSchemaOwnershipInfo != null && !teamSchemaOwnershipInfo.isEmpty()) {
                result = teamSchemaOwnershipInfo.get(0);
            }
        }
        return result;
    }

    public List<SchemaEntityOwnershipInfo> getTeamSchemaOwnershipInfo(final String entityType, final String entityNamespace)
        throws AuthXClientException, InterruptedException, IOException {

        final String ownableName = buildOwnableNameForSchema(entityType, entityNamespace);

        OwnershipMeta ownershipMeta = getOwnableInfo(
            ownableName,
            DATASCHEMA_OWNABLE_TYPE
        );

        if (ownershipMeta == null) {
            return null;
        }

        List<SchemaEntityOwnershipInfo> result = new ArrayList<>();

        result.add(new SchemaEntityOwnershipInfo(
            buildSchemaEntityFromOwnableName(ownableName),
            ownershipMeta
        ));

        return result;
    }

    private SchemaEntityOwnershipInfo getPersonalSchemaOwnershipInfo(final String entityType, final String entityNamespace) {
        SchemaPersonalOwner schemaPersonalOwnerRecord =
            schemaPersonalOwnershipRepository.findByEntityTypeAndEntityNamespaceAndActiveTrue(entityType, entityNamespace);

        SchemaEntityOwnershipInfo result = null;

        if (schemaPersonalOwnerRecord != null) {
            OwnershipMeta ownershipMeta = new OwnershipMeta(
                schemaPersonalOwnerRecord.getStaffId(),
                schemaPersonalOwnerRecord.getStaffLoginName(),
                schemaPersonalOwnerRecord.getStaffName(),
                schemaPersonalOwnerRecord.getReason()
            );
            // enriching with Staff info
            ownershipMeta.setStaffExtraInfo(officeService.getOneStaffById(schemaPersonalOwnerRecord.getStaffId()));
            result = new SchemaEntityOwnershipInfo(
                new SchemaEntity(entityNamespace, DataEntity.DEFAULT_ENTITY_ACCOUNT, DataEntityType.valueOf(entityType.toUpperCase())),
                ownershipMeta
            );
        }

        return result;
    }

    public WorkflowOwnershipInfo getWorkflowOwnershipInfo(final WorkflowTypeLowerCase workflowTypeLowerCase, final String workflowName)
        throws AuthXClientException, InterruptedException, IOException {

        final String ownableName = buildOwnableNameForWorkflow(workflowTypeLowerCase, workflowName);

        OwnershipMeta ownershipMeta = getOwnableInfo(
            ownableName,
            WORKFLOW_OWNABLE_TYPE
        );

        if (ownershipMeta == null) {
            return null;
        }

        return new WorkflowOwnershipInfo(
            new WorkflowEntity(workflowTypeLowerCase, workflowName),
            ownershipMeta
        );
    }

    public Map<String, List<DataEntityOwnershipInfo>> getOwnershipInfoBatch(final String[] ownableNames)
        throws AuthXClientException, InterruptedException, IOException {

        Map<String, List<DataEntityOwnershipInfo>> result = new HashMap<>();

        List<OwnableNameType> namesAndTypes = new ArrayList<>();
        for (String ownableName: ownableNames) {
            namesAndTypes.add(new OwnableNameType(ownableName, DATASOURCE_OWNABLE_TYPE));
        }

        JsonNode jsonResponse = ownershipClient.retrieveOwnablesBulk(new OwnablesToRetrieve(namesAndTypes));

        if (jsonResponse != null) {
            JsonNode jsonOwnables = jsonResponse.get("ownables");
            for (final JsonNode jsonOwnable : jsonOwnables) {
                JsonNode jsonTypename = jsonOwnable.get("type_name");
                // there can be other ownable types in response because we don't specify one in the search request
                if (!jsonTypename.asText().equals(DATASOURCE_OWNABLE_TYPE)) {
                    continue;
                }
                if (jsonOwnable.get("is_archived").asInt() == 1) {
                    continue;
                }
                JsonNode jsonOwner = jsonOwnable.get("owner");
                DataEntityOwnershipInfo info = new DataEntityOwnershipInfo(
                    buildDataEntityFromOwnableName(jsonOwnable.get("ownable_name").asText()),
                    jsonOwner.get("orgunit_id").asLong(),
                    jsonOwner.get("name").asText(),
                    //TODO check why changed_by_staff_id is null
                    Optional.ofNullable(jsonOwner.get("changed_by_staff_id")).map(JsonNode::asInt).orElse(0),
                    jsonOwner.get("reason").asText());
                String ownableName = jsonOwnable.get("ownable_name").asText();
                if (result.containsKey(ownableName)) {
                    result.get(ownableName).add(info);
                } else {
                    result.put(ownableName, Collections.singletonList(info));
                }
            }
        }
        return result;
    }

    public List<DataEntityOwnershipInfo> getOwnershipInfoForEntitiesInNamespace(
        final String entityType, final String entityNamespace)
        throws AuthXClientException, InterruptedException, IOException {

        List<DataEntityOwnershipInfo> result = new ArrayList<>();

        JsonNode jsonResponse = ownershipClient.searchOwnables(String.format("%s.%s.", entityType, entityNamespace), 0);
        if (jsonResponse != null) {
            JsonNode jsonOwnables = jsonResponse.get("ownables");
            for (final JsonNode jsonOwnable : jsonOwnables) {
                JsonNode jsonTypename = jsonOwnable.get("type_name");
                // there can be other ownable types in response because we don't specify one in the search request
                if (!jsonTypename.asText().equals(DATASOURCE_OWNABLE_TYPE)) {
                    continue;
                }
                JsonNode jsonOwner = jsonOwnable.get("owner");
                DataEntityOwnershipInfo info = new DataEntityOwnershipInfo(
                    buildDataEntityFromOwnableName(jsonOwnable.get("ownable_name").asText()),
                    jsonOwner.get("orgunit_id").asLong(),
                    jsonOwner.get("name").asText(),
                    jsonOwner.get("changed_by_staff_id").asInt(),
                    jsonOwner.get("reason").asText());
                result.add(info);
            }
        }
        return result;
    }

    /**
     * Assign ownership info to an ownable.
     *
     * If ownable does not exist then we register a new one with specifid ownership info.
     * If ownable does exist with different ownership info then we reassign it.
     * If ownable does exist but archived then we unarchive it.
     *
     * @return a string with result message from ownership service
     */
    private String assignOwnable(
        final String ownableName, final String ownableType,
        final long orgUnitId, final String reason, final Integer staffId, final String staffLoginname)
        throws AuthXClientException, InterruptedException, IOException {

        JsonNode jsonResponse = ownershipClient.retrieveOwnable(ownableName, ownableType);

        String result;

        if (jsonResponse != null && jsonResponse.has("id")) {

            long ownableId = jsonResponse.get("id").asLong();
            long currentOrgunitId = jsonResponse.get("owner").get("orgunit_id").asLong();

            String reassignResult = "";
            String unarchiveResult = "";

            // todo currently reason and other meta is not updated when re-claim with same owners

            if (currentOrgunitId != orgUnitId) {
                OwnableReassignmentInfo info = new OwnableReassignmentInfo();
                info.setOrgUnitId(orgUnitId);
                info.setOwnable(ownableId);
                // reason is required for 'reassign' operation
                final String DefaultReassignReason = "Reassigned via dataquality-api";
                info.setReason(reason.trim().isEmpty() ? DefaultReassignReason : reason);
                info.setStaffId(staffId);
                info.setStaffLoginName(staffLoginname);
                reassignResult = ownershipClient.reassignOwnable(info).toString();
            }

            if (jsonResponse.get("is_archived").asInt() == 1) {
                unarchiveResult = ownershipClient.unarchiveOwnable(ownableId).toString();
            }
            result = reassignResult.isEmpty() ? unarchiveResult : reassignResult;

        } else {

            OwnableRegistrationInfo info = new OwnableRegistrationInfo();
            Ownable ownable = new Ownable();
            ownable.setName(ownableName);
            ownable.setType(ownableType);
            ownable.setOrgUnitId(orgUnitId);
            ownable.setStaffId(staffId);
            ownable.setStaffLoginName(staffLoginname);
            info.setOwnable(ownable);
            info.setAppendReason(reason);
            result = ownershipClient.registerNewOwnable(info).toString();
        }

        return result;
    }

    public String assignOwnership(
        final String entityType, final String entityAccount, final String entityNamespace, final String entityName,
        final long orgUnitId, final String reason, final Integer staffId, final String staffLoginname)
        throws InterruptedException, IOException, AuthXClientException {

        return assignOwnable(
            buildOwnableNameForTable(entityType, entityAccount, entityNamespace, entityName),
            DATASOURCE_OWNABLE_TYPE,
            orgUnitId, reason, staffId, staffLoginname
        );
    }

    public String assignSchemaOwnership(
        final String entityType, final String entityNamespace,
        final OwnershipMeta ownershipMeta
    ) throws InterruptedException, AuthXClientException, IOException {
        if (ownershipMeta.isPersonal()) {
            assignPersonalSchemaOwnership(
                entityType, entityNamespace,
                ownershipMeta.getStaffId(), ownershipMeta.getStaffLoginNanme(), ownershipMeta.getStaffName(),
                ownershipMeta.getReason()
            );
            return dropTeamSchemaOwnership(entityType, entityNamespace);
        } else {
            String result = assignTeamSchemaOwnership(
                entityType, entityNamespace,
                ownershipMeta.getOwnerOrgUnitId(),
                ownershipMeta.getReason(),
                ownershipMeta.getChangedByStaffId(),
                null
            );
            dropPersonalSchemaOwnership(entityType, entityNamespace);
            return result;
        }
    }

    private void assignPersonalSchemaOwnership(
        final String entityType, final String entityNamespace,
        final int staffId, final String staffLoginName, final String staffName, final String reason
    ) {
        // can be not active
        SchemaPersonalOwner currentOwnerRecord =
            schemaPersonalOwnershipRepository.findByEntityTypeAndEntityNamespace(entityType, entityNamespace);
        if (currentOwnerRecord == null) {
            schemaPersonalOwnershipRepository.save(
                new SchemaPersonalOwner(entityType, entityNamespace, staffId, staffLoginName, staffName, reason)
            );
        } else {
            currentOwnerRecord.setActive(true);
            currentOwnerRecord.setStaffId(staffId);
            currentOwnerRecord.setStaffLoginName(staffLoginName);
            currentOwnerRecord.setStaffName(staffName);
            currentOwnerRecord.setReason(reason);
            schemaPersonalOwnershipRepository.save(currentOwnerRecord);
        }
    }

    public String assignTeamSchemaOwnership(
        final String entityType, final String entityNamespace,
        final long orgUnitId, final String reason, final Integer staffId, final String staffLoginname)
        throws InterruptedException, IOException, AuthXClientException {
        return assignOwnable(
            buildOwnableNameForSchema(entityType, entityNamespace),
            DATASCHEMA_OWNABLE_TYPE,
            orgUnitId, reason, staffId, staffLoginname
        );
    }

    public String assignWorkflowOwnership(
        final WorkflowTypeLowerCase workflowTypeLowerCase, final String workflowName,
        final long orgUnitId, final String reason, final Integer staffId, final String staffLoginname
    ) throws InterruptedException, IOException, AuthXClientException {
        return assignOwnable(
            buildOwnableNameForWorkflow(workflowTypeLowerCase, workflowName),
            WORKFLOW_OWNABLE_TYPE,
            orgUnitId, reason, staffId, staffLoginname
        );
    }

    private String dropOwnable(final String ownableName, final String ownableType)
        throws AuthXClientException, InterruptedException, IOException {
        JsonNode jsonResponse = ownershipClient.retrieveOwnable(ownableName, ownableType);
        String result = "";
        if (jsonResponse != null && jsonResponse.has("id")) {
            result = ownershipClient.archiveOwnable(jsonResponse.get("id").asLong()).toString();
        }
        return result;
    }

    public String dropOwnership(
        final String entityType, final String entityAccount, final String entityNamespace,
        final String entityName)
        throws InterruptedException, IOException, AuthXClientException {
        return dropOwnable(
            buildOwnableNameForTable(entityType, entityAccount, entityNamespace, entityName),
            DATASOURCE_OWNABLE_TYPE
        );
    }

    public String dropTeamSchemaOwnership(final String entityType, final String entityNamespace)
        throws InterruptedException, IOException, AuthXClientException {
        return dropOwnable(
            buildOwnableNameForSchema(entityType, entityNamespace),
            DATASCHEMA_OWNABLE_TYPE
        );
    }

    private void dropPersonalSchemaOwnership(final String entityType, final String entityNamespace) {
        SchemaPersonalOwner currentOwner =
            schemaPersonalOwnershipRepository.findByEntityTypeAndEntityNamespaceAndActiveTrue(entityType, entityNamespace);
        if (currentOwner != null) {
            currentOwner.setActive(false);
            schemaPersonalOwnershipRepository.save(currentOwner);
        }
    }

    private Map<String, OwnershipMeta> getOwnedOwnables(final long orgUnitId, final String ownableType)
        throws AuthXClientException, InterruptedException, IOException {
        Map<String, OwnershipMeta> result = new HashMap<>();
        JsonNode jsonResponse = ownershipClient.getOrgUnitOwnables(
            orgUnitId, ownableType, 1, 1
        );
        if (jsonResponse != null) {
            JsonNode jsonOwnables = jsonResponse.get("ownables");
            for (final JsonNode jsonOwnable : jsonOwnables) {
                JsonNode jsonOwner = jsonOwnable.get("owner");
                String ownableName = jsonOwnable.get("ownable_name").asText();
                OwnershipMeta ownershipMeta = new OwnershipMeta(
                    jsonOwner.get("orgunit_id").asLong(),
                    jsonOwner.get("name").asText(),
                    jsonOwner.get("changed_by_staff_id").asInt(),
                    jsonOwner.get("reason").asText()
                );
                result.put(ownableName, ownershipMeta);
            }
        }

        return result;
    }

    public List<DataEntityOwnershipInfo> getOwnedEntities(final long orgUnitId) throws AuthXClientException, InterruptedException, IOException {

        List<DataEntityOwnershipInfo> result = new ArrayList<>();

        Map<String, OwnershipMeta> ownedOwnables = getOwnedOwnables(orgUnitId, DATASOURCE_OWNABLE_TYPE);
        ownedOwnables.forEach(
            (k, v) -> result.add(new DataEntityOwnershipInfo(buildDataEntityFromOwnableName(k), v))
        );

        return result;
    }

    public List<DataEntityOwnershipInfo> getOwnedEntitiesBatch(final List<Long> orgUnitIds) throws AuthXClientException, InterruptedException, IOException {
        List<DataEntityOwnershipInfo> result = new ArrayList<>();

        // there is no batch endpoint
        for (long orgUnitId: orgUnitIds) {
            result.addAll(getOwnedEntities(orgUnitId));
        }
        return result;
    }

    public List<SchemaEntityOwnershipInfo> getOwnedTeamSchemaEntities(final long orgUnitId) throws AuthXClientException, InterruptedException, IOException {
        List<SchemaEntityOwnershipInfo> result = new ArrayList<>();

        Map<String, OwnershipMeta> ownedOwnables = getOwnedOwnables(orgUnitId, DATASCHEMA_OWNABLE_TYPE);
        ownedOwnables.forEach(
            (k, v) -> result.add(new SchemaEntityOwnershipInfo(buildSchemaEntityFromOwnableName(k), v))
        );
        return result;
    }

    public List<SchemaEntity> getOwnedTeamSchemaEntitiesV2(final long orgUnitId) throws AuthXClientException, InterruptedException, IOException {
        List<SchemaEntity> result = new ArrayList<>();

        Map<String, OwnershipMeta> ownedOwnables = getOwnedOwnables(orgUnitId, DATASCHEMA_OWNABLE_TYPE);
        ownedOwnables.forEach(
            (k, v) -> result.add(buildSchemaEntityFromOwnableName(k))
        );
        return result;
    }

    public List<SchemaEntity> getOwnedPersonalSchemaEntities(final int staffId) {
        List<SchemaEntity> result = new ArrayList<>();
        List<SchemaPersonalOwner> schemaPersonalOwners = schemaPersonalOwnershipRepository.findByStaffId(staffId);
        schemaPersonalOwners.forEach(
            (k) -> result.add(k.getSchemaEntity())
        );
        return result;
    }

    public List<WorkflowOwnershipInfo> getOwnedWorkflows(final long orgUnitId) throws AuthXClientException, InterruptedException, IOException {
        List<WorkflowOwnershipInfo> result = new ArrayList<>();

        Map<String, OwnershipMeta> ownedOwnables = getOwnedOwnables(orgUnitId, WORKFLOW_OWNABLE_TYPE);
        ownedOwnables.forEach(
            (k, v) -> result.add(new WorkflowOwnershipInfo(buildWorkflowEntityFromOwnableName(k), v))
        );

        return result;
    }

    /**
     * Dump of all ownership records for data entities, active and not.
     */
    public List<DataEntityOwnershipInfoDump> getAllOwnershipInfo() throws AuthXClientException, InterruptedException, IOException {
        List<DataEntityOwnershipInfoDump> result = new ArrayList<>();
        JsonNode jsonResponse = ownershipClient.getAllOwnablesForType(DATASOURCE_OWNABLE_TYPE);
        if (jsonResponse != null) {
            JsonNode jsonOwnables = jsonResponse.get("ownables");
            for (final JsonNode jsonOwnable : jsonOwnables) {
                final String ownableName = jsonOwnable.get("ownable_name").asText();
                // A hardcoded fix for invalid ownables. Impossible to remove from ownership service.
                if (ownableName.startsWith("mysql-table")) {
                    continue;
                }
                if (ownableName.startsWith("mysqly")) {
                    continue;
                }
                if (Arrays.asList(new String[] {
                    "foa-historical-monthly-susp-count",
                    "commission_control.covid_cancellations_waived_via_cs",
                    "$ownable",
                    "${ownable}",
                    "seo_pagename_visitor_stats",
                    "PotentialDuplicateFeedbacks"
                }).contains(ownableName)) {
                    continue;
                }

                JsonNode jsonOwner = jsonOwnable.get("owner");
                DataEntityOwnershipInfo info;
                try {
                    info = new DataEntityOwnershipInfo(
                        buildDataEntityFromOwnableName(ownableName),
                        jsonOwner.get("orgunit_id").asLong(),
                        jsonOwner.get("name").asText(),
                        jsonOwner.get("changed_by_staff_id").asInt(),
                        jsonOwner.get("reason").asText());
                } catch (Exception exc) {
                    LOGGER.warn(exc.getMessage());
                    continue;
                }
                boolean active = jsonOwnable.get("is_archived").asLong() == 0;
                DataEntityOwnershipInfoDump infoDump = new DataEntityOwnershipInfoDump(info, active);
                result.add(infoDump);
            }
        }

        return result;
    }

    /**
     * Dump of all ownership records for schema entities, active and not.
     */
    public List<SchemaEntityOwnershipInfoDump> getAllSchemaOwnershipInfo() throws AuthXClientException, InterruptedException, IOException {
        List<SchemaEntityOwnershipInfoDump> result = new ArrayList<>();
        JsonNode jsonResponse = ownershipClient.getAllOwnablesForType(DATASCHEMA_OWNABLE_TYPE);
        if (jsonResponse != null) {
            JsonNode jsonOwnables = jsonResponse.get("ownables");
            for (final JsonNode jsonOwnable : jsonOwnables) {
                // A hardcoded fix for invalid ownables. Impossible to remove from ownership service.
                if (jsonOwnable.get("ownable_name").asText().equals("mysql.office.wbso_time_spent_in_tool")) {
                    continue;
                }
                if (jsonOwnable.get("ownable_name").asText().equals("bi")) {
                    continue;
                }
                if (jsonOwnable.get("ownable_name").asText().equals("hive.hadoop_test.danperez")) {
                    continue;
                }
                if (jsonOwnable.get("ownable_name").asText().equals("dakozlova")) {
                    continue;
                }
                JsonNode jsonOwner = jsonOwnable.get("owner");
                SchemaEntityOwnershipInfo info;
                try {
                    info = new SchemaEntityOwnershipInfo(
                        buildSchemaEntityFromOwnableName(jsonOwnable.get("ownable_name").asText()),
                        jsonOwner.get("orgunit_id").asLong(),
                        jsonOwner.get("name").asText(),
                        jsonOwner.get("changed_by_staff_id").asInt(),
                        jsonOwner.get("reason").asText());
                } catch (Exception exc) {
                    LOGGER.warn(exc.getMessage());
                    continue;
                }
                boolean active = jsonOwnable.get("is_archived").asLong() == 0;
                SchemaEntityOwnershipInfoDump infoDump = new SchemaEntityOwnershipInfoDump(info, active);
                result.add(infoDump);
            }
        }

        return result;
    }
}
