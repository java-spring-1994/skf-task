package com.booking.dataqualityapi.services;

import com.booking.dataqualityapi.dto.ListInfo;
import com.booking.dataqualityapi.model.EntityInstance.MysqlInstance;
import com.booking.dataqualityapi.model.HiveColumnMeta;
import com.booking.dataqualityapi.repository.HiveColumnMetaRepositoryImpl;
import com.booking.dataqualityapi.repository.MysqlTableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SearchService {

    @Autowired
    HiveColumnMetaRepositoryImpl hiveColumnRepository;

    @Autowired
    MysqlTableRepository mysqlTableRepository;

    public ListInfo<HiveColumnMeta> getColumns(String cluster,
                                               Optional<String> schema,
                                               Optional<String> table,
                                               Optional<String> column,
                                               Integer offset,
                                               Integer limit) {
        final List<HiveColumnMeta> columns = hiveColumnRepository.getHiveTableColumns(cluster, schema,
                table, column, offset, limit + 1);
        boolean hasMore = columns.size() > limit;
        if (hasMore) {
            columns.remove(columns.size() - 1);
        }
        final ListInfo<HiveColumnMeta> metaInfo = new ListInfo(offset, columns.size(),
                hasMore, columns);
        return metaInfo;
    }

    public ListInfo<MysqlInstance> getMysqlTables(Optional<String> schema,
                                                  Optional<String> table,
                                                  Integer offset,
                                                  Integer limit) {
        final List<MysqlInstance> mysqlInstances = mysqlTableRepository.searchTableList(schema, table,
                offset, limit + 1);
        boolean hasMore = mysqlInstances.size() > 0 && mysqlInstances.size() > limit;
        if (hasMore) {
            mysqlInstances.remove(mysqlInstances.size() - 1);
        }
        final ListInfo<MysqlInstance> result = new ListInfo<>(offset, mysqlInstances.size(), hasMore, mysqlInstances);
        return result;
    }
}
