package com.booking.dataqualityapi.services;

import java.io.IOException;
import javax.ws.rs.core.Response;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Based on Graphite Render API.
 * @see <a href="https://graphite.readthedocs.io/en/latest/render_api.html">link</a>
 */
@Service
public class GraphiteService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GraphiteService.class);

    private static final String HOST = System.getenv().get("GRAPHITE_WEB_HOST");

    private final CloseableHttpClient httpClient = HttpClients.createDefault();

    /**
     * To check that specific target (metric) exist in graphite.
     *
     * @param target graphite target
     * @return True if target exists
     * @throws IOException when http request is not completed successfully
     */
    public boolean hasTarget(final String target) throws IOException {
        String uri = String.format("%s/render/?target=%s&from=-5min&until=now&format=json", HOST, target);

        HttpGet httpGet = new HttpGet(uri);
        try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
            // the following is to avoid the 'Timeout waiting for connection from pool' issue
            // response entity should be consumed, so the stream can be closed correctly
            EntityUtils.consumeQuietly(response.getEntity());
            int statusCode = response.getStatusLine().getStatusCode();
            return (Response.Status.fromStatusCode(statusCode).getFamily() == Response.Status.Family.SUCCESSFUL);
        }
    }
}
