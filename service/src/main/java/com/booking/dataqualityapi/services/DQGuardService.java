package com.booking.dataqualityapi.services;

import com.booking.dataqualityapi.model.DQGuardConfig;
import com.booking.dataqualityapi.repository.DQGuardConfigRepository;
import java.io.IOException;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DQGuardService {

    private final DQGuardConfigRepository dqGuardConfigRepository;

    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory.getLogger(DQGuardService.class);

    @Autowired
    private GraphiteService graphiteService;

    @Autowired
    public DQGuardService(final DQGuardConfigRepository dqGuardConfigRepository) {
        this.dqGuardConfigRepository = dqGuardConfigRepository;
    }

    public DQGuardConfig getConfig(final String worklfow) {
        return dqGuardConfigRepository.find1ByWorkflow(worklfow);
    }

    public DQGuardConfig saveConfig(String workflow, String config, String who) {
        DQGuardConfig dqGuardConfig = dqGuardConfigRepository.find1ByWorkflow(workflow);
        if (dqGuardConfig != null) {
            dqGuardConfig.setConfig(config);
            dqGuardConfig.setLastModifiedBy(who);
            return dqGuardConfigRepository.save(dqGuardConfig);
        } else {
            return dqGuardConfigRepository.save(new DQGuardConfig(workflow, config, who, new Date()));
        }
    }

    public boolean checkIfTableIsMonitored(final String schema, final String table) throws IOException {
        return checkIfTableIsMonitoredWithinWorkflow(schema, table, "*");
    }

    public boolean checkIfWorkflowIsIntegrated(final String workflow) throws IOException {
        return checkIfTableIsMonitoredWithinWorkflow("*", "*", workflow);
    }

    public boolean checkIfTableIsMonitoredWithinWorkflow(final String schema, final String table, final String workflow) throws IOException {
        String target = String.format("general.dq.v3.metric.*.oozie.%s.*.%s.%s.*.*.*", workflow, schema, table);
        return graphiteService.hasTarget(target);
    }
}
