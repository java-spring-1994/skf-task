package com.booking.dataqualityapi.services;

import com.booking.dataqualityapi.model.DataSource;
import com.booking.dataqualityapi.model.datamodel.TimeSeriesConfig;
import com.booking.dataqualityapi.repository.datamodel.TimeSeriesConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class TimeSeriesConfigService {

    private final TimeSeriesConfigRepository timeSeriesConfigRepository;

    @Autowired
    private DataSourcesService dataSourcesService;

    @Autowired
    public TimeSeriesConfigService(
        final TimeSeriesConfigRepository timeSeriesConfigRepository
    ) {
        this.timeSeriesConfigRepository = timeSeriesConfigRepository;
    }

    private TimeSeriesConfig getConfig(final int entityId) {
        return timeSeriesConfigRepository.find1ByEntityId(entityId);
    }

    TimeSeriesConfig getHiveConfig(final String entityNamespace, final String entityName) {
        DataSource dataSource = dataSourcesService.findByHiveDataEntityNamespaceAndName(entityNamespace, entityName);
        return getConfig(dataSource.getId());
    }

    static String convertPythonDateFormatToJava(final String pyFormat) {
        String result = pyFormat;
        Map<String, String> pyDateToJ = new HashMap<String, String>() {
            {
                put("%m", "MM");
                put("%Y", "yyyy");
                put("%d", "dd");
                put("%H", "HH");
                put("%M", "mm");
                put("%S", "ss");
            }
        };
        for (Map.Entry<String, String> entry : pyDateToJ.entrySet()) {
            result = result.replace(entry.getKey(), entry.getValue());
        }
        return result;
    }

    static String convertPythonHourFormatToJava(final String pyHourFormat) {
        if (pyHourFormat == null
                || pyHourFormat.isEmpty()
                || "%H".equals(pyHourFormat)) {
            return "HH";
        }
        if ("%-H".equals(pyHourFormat)) {
            return "H";
        }
        return "HH";
    }
}
