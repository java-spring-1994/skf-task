package com.booking.dataqualityapi.services;


import com.booking.dataqualityapi.services.partitions.PartitionNamesBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PartitionServiceTest {
    private final static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    /**
     * Partitions generation test with hours
     */
    @Test
    public void getPartitionsWithHours() {
        checkPartitions(LocalDate.now().minusDays(30), LocalDate.now().minusDays(1),
            "yyyy_mm_dd", "hh",
            720,
            "yyyy_mm_dd=" + dateFormatter.format(LocalDate.now().minusDays(5)) + "/hh=09");
    }

    /**
     * Partitions generation test without hours
     */
    @Test
    public void getPartitionsWithoutHours() {
        checkPartitions(LocalDate.now().minusDays(30), LocalDate.now().minusDays(1),
            "date", null,
            30,
            "date=" + dateFormatter.format(LocalDate.now().minusDays(5)));
    }

    /**
     * Generates partitions names and checks that expected name exist in generated list
     */

    private void checkPartitions(LocalDate from, LocalDate to,
                                 String datePartitionCol, String timePartitionCol,
                                 int expectedNumber, String expectedPartitionName) {
        List<String> regionSorter = new LinkedList<>();
        regionSorter.add(datePartitionCol);
        PartitionNamesBuilder partitionNamesBuilder = new PartitionNamesBuilder(from, to,regionSorter);
        partitionNamesBuilder.withDateFormatter(dateFormatter);
        partitionNamesBuilder.withDatePartitionColumn(datePartitionCol);
        if (StringUtils.isNotEmpty(timePartitionCol)) {
            regionSorter.add(timePartitionCol);
            partitionNamesBuilder.withTimePartitionColumn(timePartitionCol);
            partitionNamesBuilder.withTimeFormatter(DateTimeFormatter.ofPattern("HH"));
        }
        List<Pair<String, LocalDateTime>> result = partitionNamesBuilder.generatePossiblePartitions();
        assertEquals("Partitions should be generated", expectedNumber, result.size());
        assertTrue("Partitions list should contain " + expectedPartitionName, result.stream()
                .anyMatch(pair -> pair.getKey().equals(expectedPartitionName)));
    }
}