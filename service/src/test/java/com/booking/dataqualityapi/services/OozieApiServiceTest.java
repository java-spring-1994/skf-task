package com.booking.dataqualityapi.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by vfedotov on 3/4/19.
 */
public class OozieApiServiceTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void getTimestampForOozieDateTime() throws Exception {
        long ts1 = OozieApiService.getTimestampForOozieDateTime("Thu, 28 Feb 2019 23:30:00 GMT");
        long ts2 = OozieApiService.getTimestampForDate("2019-02-28", "23", "30");
        long ts3 = OozieApiService.getTimestampForDate("2019-02-28", "00", "00");
        long ts4 = OozieApiService.getTimestampForDate("2019-01-28", "23", "30");
        assertEquals(ts1, ts2);
        assertNotEquals(ts1, ts3);
        assertNotEquals(ts1, ts4);
    }

    @Test
    public void getDateForOozieDateTime() throws Exception {
        LocalDate date = OozieApiService.getDateForOozieDateTime("Mon, 04 Mar 2019 00:00:00 GMT");
        assertEquals(LocalDate.parse("2019-03-04"), date);
    }

    @Test
    public void getFormattedOozieDateTime() {
        String formattedDateTime1 = OozieApiService.getFormattedOozieDateTime("Mon, 04 Mar 2019 02:10:05 GMT");
        assertEquals("2019-03-04 02:10:05", formattedDateTime1);

        String formattedDateTime2 = OozieApiService.getFormattedOozieDateTime("Wed, 22 Jul 2020 00:00:00 GMT");
        assertEquals("2020-07-22 00:00:00", formattedDateTime2);
    }

}