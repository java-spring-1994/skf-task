package com.booking.dataqualityapi.services;

import static org.junit.jupiter.api.Assertions.*;

import com.booking.dataqualityapi.dto.dataentity.registration.DataEntityRegistrationAttributes;
import com.booking.dataqualityapi.exceptions.DataEntityRegistrationException;
import org.junit.Test;

public class DataSourcesServiceTest {

    @Test
    public void validateRegistrationAttributes() {
        // negative numbers are not allowed
        DataEntityRegistrationAttributes attributes1 = new DataEntityRegistrationAttributes();
        attributes1.setOwnerOrgunitId(-123456);
        assertThrows(
            DataEntityRegistrationException.class,
            () -> DataSourcesService.validateRegistrationAttributes(attributes1));

        // 0 is not an allowed value
        DataEntityRegistrationAttributes attributes2 = new DataEntityRegistrationAttributes();
        attributes2.setOwnerOrgunitId(0);
        assertThrows(
            DataEntityRegistrationException.class,
            () -> DataSourcesService.validateRegistrationAttributes(attributes2));

        // numbers like 1 are allowed according to the current logic
        DataEntityRegistrationAttributes attributes3 = new DataEntityRegistrationAttributes();
        attributes3.setOwnerOrgunitId(1);
        assertDoesNotThrow(
            () -> DataSourcesService.validateRegistrationAttributes(attributes3)
        );

        // a real DataManagement team id
        DataEntityRegistrationAttributes attributes4 = new DataEntityRegistrationAttributes();
        attributes4.setOwnerOrgunitId(60003843);
        assertDoesNotThrow(
            () -> DataSourcesService.validateRegistrationAttributes(attributes4)
        );
    }
}