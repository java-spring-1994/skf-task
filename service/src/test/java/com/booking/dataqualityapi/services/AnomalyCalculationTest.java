package com.booking.dataqualityapi.services;

import com.booking.dataquality.anomalies.AnomalyCalculation;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class AnomalyCalculationTest {
    private static double[] PAY_BHPAYCHARGEABLEEXCLUSIVE_IMPORT_CASE = {53050291.0,
            53117702.0,
            53184091.0,
            53261571.0,
            53333093.0,
            53374055.0,
            53422345.0,
            53493235.0,
            53558373.0,
            53620370.0,
            53677004.0,
            53791951.0,
            53829384.0,
            53872535.0,
            53926846.0,
            53981935.0,
            54037566.0,
            54087574.0,
            54135281.0,
            54171933.0,
            54219465.0,
            57081577.0,
            59947521.0,
            60731280.0};

    @Test
    public void testAnomalCalculationService(){
        final AnomalyCalculation acs = new AnomalyCalculation();
        double [] anomalies = acs.buildAnomalies(PAY_BHPAYCHARGEABLEEXCLUSIVE_IMPORT_CASE);
        System.out.println(Arrays.toString(anomalies));
        int anomaliesCount  = 0;
        for(int i=0;i<anomalies.length;i++){
            if(anomalies[i]>5.0){
                anomaliesCount++;
            }
        }
        assertEquals("There should be 3 anomalies",3, anomaliesCount);
    }
}
