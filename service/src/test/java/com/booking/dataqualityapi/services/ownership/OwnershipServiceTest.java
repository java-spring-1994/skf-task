package com.booking.dataqualityapi.services.ownership;

import com.booking.dataqualityapi.dto.workflow.WorkflowEntity;
import com.booking.dataqualityapi.dto.workflow.WorkflowTypeLowerCase;
import org.junit.Assert;
import org.junit.Test;

public class OwnershipServiceTest {

    @Test
    public void buildWorkflowEntityFromOwnableName() {
        Assert.assertEquals(
            OwnershipService.buildWorkflowEntityFromOwnableName("oozie.name1"),
            new WorkflowEntity(WorkflowTypeLowerCase.valueOf("oozie"), "name1"));

        Assert.assertEquals(
            OwnershipService.buildWorkflowEntityFromOwnableName("oozie.name1.version2"),
            new WorkflowEntity(WorkflowTypeLowerCase.valueOf("oozie"), "name1.version2"));

        Assert.assertEquals(
            OwnershipService.buildWorkflowEntityFromOwnableName("oozie.name1.version2.postfix"),
            new WorkflowEntity(WorkflowTypeLowerCase.valueOf("oozie"), "name1.version2.postfix"));

        Assert.assertThrows(
            RuntimeException.class,
            () -> OwnershipService.buildWorkflowEntityFromOwnableName("bla")
        );

        Assert.assertThrows(
            IllegalArgumentException.class,
            () -> OwnershipService.buildWorkflowEntityFromOwnableName("uuzie.name1"));
    }
}