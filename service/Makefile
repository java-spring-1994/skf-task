PROJECT_NAME=data-quality
SERVICE_NAME=dataquality-api

# welcome to your Makefile! feel free to adapt this to your workflow in
# whatever way works for you. Or, if you want, don't use it at all: this is
# just a set of suggestions from our track on one way to approach doing
# software development using docker images and a Kubernetes cluster.

# a make annotation: don't look for files with these names
.PHONY: docker-build docker-tag docker-push clean rollout update \
	mvn-package am-i-working-on-dev do-i-have-a-running-pod can-i-talk-to-artifactory

define ARTIFACTORY_MESSAGE
*** WARNING ***

You're missing the docker credential helper plugin

You can install the following:

curl https://s3-main-01.booking.com/scripts/setup-docker-credential-plugin.sh | sh

Caution: Please be aware that the above script destroys your existing docker configuration once executed. You can back it up from ~/.docker/config.json if necessary.

Docs: https://docs.booking.com/kubernetes/integrations/artifactory-docker.html

endef

export ARTIFACTORY_MESSAGE

can-i-talk-to-artifactory:
	@which docker-credential-iam >/dev/null || (echo "$$ARTIFACTORY_MESSAGE"; exit 1)

# "update" takes the image built by docker and patches an existing pod to
# replace the 'app' container. This works because the Perl and Java charts make
# sure to name the application container "app". The nice part of this is that
# we only restart the one container, not the whole pod. This is the first
# recipe, so you can just type 'make' to do this.
update: do-i-have-a-running-pod docker-push
	-kubectl patch po/$(pod_name) -p '{"spec":{"containers":[{"name":"app","image":"$(image)"}]}}'

# "clean" deletes the Application object. This will cascade-delete all
# Releases, and anything created by those Releases (like Deployments or
# Services). This is handy in dev, but SUPER DANGEROUS everywhere else.
clean: am-i-working-on-dev
	kubectl delete -f kubernetes/application-dev.yaml

# "rollout" does a Shipper-based rollout of your app. This is only needed if
# you're starting from scratch (to provision your dev environment), or if you
# prefer to do a rollout for each change. That's totally OK, but it will be
# a bit slower than "update" because it will replace the entire pod.
# NOTE: when you want to roll out in prod or DQS, the process will look
# similar, there's just no makefile for that :)
rollout: docker-push
	sed "s^tag: latest^tag: \"$(docker_image_id)\"^g; s^PERSONAL_DEV_REPO^$(dev_repo)^" kubernetes/application-dev.yaml | kubectl apply -f -

docker-push: docker-tag can-i-talk-to-artifactory
	docker push $(image)

docker-tag: docker-build
	docker tag $(dev_repo):$(dev_tag) $(image)

docker-build: Dockerfile mvn-package am-i-working-on-dev dot-deploy
	docker build --pull -t $(dev_repo):$(dev_tag) . --build-arg ENV=dev --build-arg RUN_PATH=reload_and_run.sh

# "mvn-package" compiles and packages the Java service ahead of a Docker build. This is not a hard requirement so feel
# free to change this to whatever you want. Maybe you want to run tests beforehand or maybe you have some other steps.
# Whatecver they are, add them here.
#
# MVN_RUN_OPTS is an environment variable that allows you to set options for your maven commands dynamically
# 			   for example: setting it to quiet mode using -q or -s settings.xml. This can be particularly useful
#			   during CI where the environment may change.
#-Dcheckstyle.skip
mvn-package:
	mvn $(MVN_RUN_OPTS) clean package -Dmaven.test.skip=true

do-i-have-a-running-pod:
	@kubectl get pods -l shipper-app=$(SERVICE_NAME) | grep 'Running\|Error\|CrashLoopBackOff' &> /dev/null

# this makefile is only for development; 'make clean' would be very dangerous in prod :)
am-i-working-on-dev:
	@kubectl config current-context | grep -- '-dev-' > /dev/null

# makefile hacks: export the multi-line value as an environment variable
export deploy_meta
dot-deploy:
	echo "$$deploy_meta" > .deploy

define deploy_meta
commit: nogit-$(timestamp)
tag: nogit-$(timestamp)
deployed-from: $(shell hostname)
deployed-by: $(USER)
deploy-date: $(shell date -u '+%Y%m%d%H%M%S')
endef

# helpers
dev_repo=docker.artifactory.booking.com/projects/$(USER)/$(PROJECT_NAME)/$(SERVICE_NAME)
image=$(dev_repo):$(docker_image_id)
pod_name=$(shell kubectl get pod -l shipper-app=$(SERVICE_NAME) -o jsonpath="{.items[0].metadata.name}")
# this is := because we don't want to repeatedly collect the timestamp
timestamp:=$(shell date +%s)
# this is never pushed, but it makes it much easier to find built images
dev_tag=development
docker_image_id=$(shell docker inspect -f='{{ .Id }}' $(dev_repo):$(dev_tag) | cut -c 8- | head -c 10)
# get the root of the repo
toplevel=$(shell git rev-parse --show-toplevel)


# ==== by Damien Krotkine ====
LATEST_POD_NAME := kubectl get pods -l app=$(SERVICE_NAME) -o jsonpath='{range .items[*]}{.metadata.creationTimestamp}{" "}{.metadata.name}{"\t"}{"\n"}' | sort -r | head -n 1 | sed 's/.* //'

app-logs:
	@kubectl logs --tail=200 -f $$( $(LATEST_POD_NAME) ) -c app

name:
	@$(LATEST_POD_NAME)

ip:
	@kubectl get pod $$( $(LATEST_POD_NAME) ) -o yaml | grep podIP | sed 's/podIP://' | tr -d '[:space:]'; echo

login:
	@kubectl exec -it $$( $(LATEST_POD_NAME) ) -c app -- /bin/bash
# ============================