#Introduction

This document describes application main enty points -> used frameworks, deployments links, 
impediments.

#General Requirements

1. [Booking.com API guidlines](https://docs.booking.com/tech_guidelines/api-guidelines/index.html)
2. [Kubernetes DQS deployment description](https://docs.booking.com/kubernetes/rolling-out/deploying-dqs.html)

#List of used frameworks

1. [Spring Boot](http://spring.io/projects/spring-boot)
Used as main application skeleton.

2. [Swagger](https://swagger.io/)
Used for generating API documentation(https://<deploy-ip>/swagger-ui.html) and for [js client](https://github.com/swagger-api/swagger-js)
Unsolved problems:
    1. cookie authorization hasn't supported by [swaggger spring-fox version 2.9.2](https://github.com/springfox/springfox/issues/1796) yet 

3. [Hibernate](http://hibernate.org/)
Used as orm framework. Configuration is stored in application.properties file.
    1. Right now only wiki service are using hibernate model, other staff need to be refactored

#Application links

1. [DQS deployment Link](https://dataquality.dqs.booking.com/swagger-ui.html)
2. [PROD deployment Link](https://dataquality.prod.booking.com/swagger-ui.html)

#Grep logs in Kibana in Prod
[Only app container, with wildcard \*request\*](https://logs.booking.com/app/kibana#/discover?_g=\(refreshInterval:\(pause:!t,value:0\),time:\(from:now-1h,mode:quick,to:now\)\)&_a=\(columns:!\(_source\),filters:!\(\('$state':\(store:appState\),meta:\(alias:!n,disabled:!f,index:'0f4eafd0-19b9-11e9-9681-e766447cc6bc',key:extra.Persona,negate:!f,params:\(query:b-data-quality-dataquality-api,type:phrase\),type:phrase,value:b-data-quality-dataquality-api\),query:\(match:\(extra.Persona:\(query:b-data-quality-dataquality-api,type:phrase\)\)\)\),\('$state':\(store:appState\),meta:\(alias:!n,disabled:!f,index:'01344ff0-1a67-11e9-81aa-cfbc3c4d1e8b',key:extra.ContainerName,negate:!f,params:\(query:app,type:phrase\),type:phrase,value:app\),query:\(match:\(extra.ContainerName:\(query:app,type:phrase\)\)\)\),\('$state':\(store:appState\),meta:\(alias:!n,disabled:!f,index:'01344ff0-1a67-11e9-81aa-cfbc3c4d1e8b',key:query,negate:!f,type:custom,value:'%7B%22wildcard%22:%7B%22extra.text%22:%22*bookinggo*%22%7D%7D'\),query:\(wildcard:\(extra.text:'*request*'\)\)\)\),index:'01344ff0-1a67-11e9-81aa-cfbc3c4d1e8b',interval:auto,query:\(language:lucene,query:''\),sort:!\('@timestamp',desc\)\))
 
