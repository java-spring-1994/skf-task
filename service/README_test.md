[JaCoCo code test coverage](https://www.eclemma.org/jacoco/)

To get report:
```
mvn test jacoco:report
```
It will appear in service/target/site/jacoco