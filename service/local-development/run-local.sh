#!/bin/sh

# This is an auto-generated script.
# You can re-generate it with `bk java:generate:run-configs` command

# After running this script, you can see your service swagger documentation
# in `http://localhost:8080/swagger` or `https://localhost:8443/swagger` addresses.

cd "$( dirname "${BASH_SOURCE[0]}" )"/..

export SERVICE_DIRECTORY_PROJECT=data-quality
export SERVICE_DIRECTORY_SERVICE=dataquality-api

export SERVER_ROLE=b-data-quality-dataquality-api;
export APP_ENV=dev;

export GITLAB_API_VERSION=4;

JAVA_OPTS=" \
-server \
-Duser.timezone=Europe/Amsterdam \
-Djavax.net.ssl.sessionCacheSize=10000 \
-Djavax.net.ssl.trustStore=/etc/bookings/cacerts_copy \
-Djavax.net.ssl.trustStorePassword=changeit \
-Dsun.net.spi.nameservice.provider.1=dns,sun \
-Dsun.net.spi.nameservice.provider.2=default \
-Dsun.net.spi.nameservice.domain=lhr4.dqs.booking.com"

exec java ${JAVA_OPTS} -cp target/app.jar:target/lib/* \
      com.booking.dataqualityapi.MainApplication \
    --spring.config.location=file:local-development/config-local.yaml,classpath:application.properties,classpath:application-${APP_ENV}.properties


